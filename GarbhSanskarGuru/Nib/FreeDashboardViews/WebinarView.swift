//
//  WebinarView.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 30/08/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

@IBDesignable class DesignableView: UIView {
}

class WebinarView: UICollectionReusableView {
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var noWebinarView: UIView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblHeaderDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
