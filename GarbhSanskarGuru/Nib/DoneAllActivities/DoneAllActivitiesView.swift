//
//  DoneAllActivitiesView.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 16/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class DoneAllActivitiesView: ConstrainedView {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblThankYou: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    var actionBlock: (KAction)->Void = {_ in}

    class func instantiateView(with view: UIView) -> DoneAllActivitiesView {
        
        let objView = UINib(nibName: "DoneAllActivitiesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DoneAllActivitiesView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        
        objView.lblThankYou.text = _appDelegator.languageBundle?.localizedString(forKey: "Thank You", value: "", table: nil)
        objView.lblMsg.text = _appDelegator.languageBundle?.localizedString(forKey: "Dear mumma, you are taking excellent care of mine. Thank you mumma Thank You", value: "", table: nil)
        
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if !touch.view!.isEqual(self.baseView) {
                self.removeFromSuperview()
            }
        }
    }
}

extension DoneAllActivitiesView {
    
    @IBAction func btnClosePopUpTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
}
