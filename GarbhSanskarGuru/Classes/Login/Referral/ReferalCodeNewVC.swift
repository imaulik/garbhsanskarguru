//
//  ReferalCodeNewVC.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 09/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct ReferalNewData {
    
    var referralCode = ""
    var giftAppCode = ""
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["user_id"] = _user.id
        dict["ref_code"] = referralCode
        dict["gift_code"] = giftAppCode
        return dict
    }
}

class ReferalCodeNewVC: ParentViewController {
    var total = 0.0
    var referralData = ReferalNewData()
    var paymentType:PaymentTypeLocalSelected = PaymentTypeLocalSelected.notSelected
    var isFromSlideMenuToBuyFullCourse:Bool = false
    
    @IBOutlet weak var txtCouponCode: UITextField!
    @IBOutlet weak var txtGiftCode: UITextField!
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceInfo: UILabel!
    @IBOutlet weak var lblGSTInfo: UILabel!
    @IBOutlet weak var lblTotalPriceInfo: JPWidthLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPrice()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
}

extension ReferalCodeNewVC {
    
    func prepareUI() {
        
        self.lblTitle?.text = _user.name
        
        if (_user.referralStatus == .pending && _user.pregnancyStatus == .pregnant) {
            self.prepareOnBoarding()
        }
    }
    
    func prepareSkipPaymentBoarding() {
        self.view.endEditing(true)
        self.skipGiftCode { (done) in
            if done {
                if _user.paymentStatus == .pending {
                    if(self.paymentType == .trial) {
                        _user.setLocalSelectedPaymentType(type: .trial)
                        _appDelegator.saveContext()
                    }
                    else if(self.paymentType == .full) {
                        _user.setLocalSelectedPaymentType(type: .full)
                        _appDelegator.saveContext()
                    }
                    self.performSegue(withIdentifier: "paymentSegue", sender: nil)
                    
                }else {
                    self.performSegue(withIdentifier: "directSegue", sender: nil)
                }
            }
        }
    }
    
    func prepareSkipPaymentTrialBoarding() {
        self.view.endEditing(true)
        self.skipGiftCodeTrial { (done) in
            if done {
                if(self.paymentType == .trial) {
                    _user.setLocalSelectedPaymentType(type: .trial)
                    _appDelegator.saveContext()
                }
                else if(self.paymentType == .full) {
                    _user.setLocalSelectedPaymentType(type: .full)
                    _appDelegator.saveContext()
                }
                self.performSegue(withIdentifier: "directSegue", sender: nil)
            }
        }
    }
    
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.actionBlock = { (action) in
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            self.getPregnancyDay(currentDay: currDay, block: { (done) in
                if done {
                    onBoardingView.removeFromSuperview()
                }
            })
        }
    }
    
    func prepareReappPurchasePopUpBoarding() {
        let onBoardingView = ReappPurchaseView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            
            switch action {
            case .done:
                DispatchQueue.main.async {
                    self.reappPurchase(block: { (done) in
                        if done {
                            self.prepareSkipPaymentBoarding()
                        }
                    })
                }
                break
            default:
                break
            }
            onBoardingView.removeFromSuperview()
        }
    }
    
    
    
    func prepareReappPurchasePopUpUsingCode() {
        let onBoardingView = ReappPurchaseView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .done:
                DispatchQueue.main.async {
                    self.reappPurchase(block: { (done) in
                        if done {
                            let  checkuser = false
                            UserDefaults.standard.set(checkuser, forKey: "checkdemoUser")
                            self.chechGiftCode()
                        }
                    })
                }
                break
            default:
                break
            }
            onBoardingView.removeFromSuperview()
        }
    }
    
}

extension ReferalCodeNewVC {
    
    @IBAction func btnProceedForPaymentTapped(_ sender: UIButton) {
        if(self.isFromSlideMenuToBuyFullCourse){
            self.prepareReappPurchasePopUpBoarding()
        }
        else {
            if paymentType == .full{
                self.prepareSkipPaymentBoarding()
            }else {
                let checkTrialUser  = UserDefaults.standard.bool(forKey:"checkdemoUser")
                if checkTrialUser == true{
                    self.prepareSkipPaymentTrialBoarding()
                }else {
                    self.prepareSkipPaymentBoarding()
                }
            }
        }
    }
    
    @IBAction func btnProceedForCodeTapped(_ sender: UIButton) {
        if(paymentType == .trial){
            _ = KPValidationToast.shared.showToastOnStatusBar(message: "Please choose full course to enable it.")
        }
        else {
            if referralData.referralCode.isEmpty && referralData.giftAppCode.isEmpty {
                _ = ValidationToast.showStatusMessage(message: "Please enter any one code")
            }
            else {
                self.view.endEditing(true)
                if(self.isFromSlideMenuToBuyFullCourse){
                    self.prepareReappPurchasePopUpUsingCode()
                }else {
                    self.chechGiftCode()
                }
            }
        }
        
    }
}

extension ReferalCodeNewVC {
    
    func getPrice() {
        self.showCentralSpinner()
        KPWebCall.call.getConfigPrice { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int , status == 200{
                
                if let data = dict["data"] as? NSDictionary{
                    print(data)
                    _user.amountDemoApp = data.getInt32Value(key: "demo_price")
                    _user.amount = data.getInt32Value(key: "price")
                    _user.gstPrice =  data.getDoubleValue(key:"demo_gst_price")
                    _user.gstPresntage = data.getDoubleValue(key:"demo_gst_per")
                    _user.gstFullPrice = data.getDoubleValue(key:"full_gst_price")
                    
                    _appDelegator.saveContext()
                    
                    if(self.paymentType == .trial){
                        self.lblHeaderTitle.text = "Explore Garbh Sanskar Guru App For 3 Days Trial"
                        let checkTrialUser  = UserDefaults.standard.bool(forKey:"checkdemoUser")
                        if checkTrialUser == true{
                            self.lblGSTInfo.isHidden = true
                            self.lblPriceInfo.isHidden = true
                            self.lblPrice.text = "Free"
                            self.lblTotalPriceInfo.text = "Total App Price"
                        }else {
                            self.lblGSTInfo.isHidden = false
                            self.lblPriceInfo.isHidden = false
                            
                            let gstPrice = _user.gstPrice
                            self.lblPriceInfo.text = "Demo Course Price : ₹ \(_user.amountDemoApp)"
                            
                            self.lblGSTInfo.text = "GST @ \(Int(_user.gstPresntage)) % : ₹ \(Int(gstPrice.rounded()))"
                            self.lblTotalPriceInfo.text = "Demo App Price(Round off)"
                            self.total = Double(_user.amountDemoApp) + gstPrice.rounded()
                            self.lblPrice.text = "₹ \(Int(self.total.rounded()))"
                            
                        }
                        
                        self.txtCouponCode.isUserInteractionEnabled = false
                        self.txtGiftCode.isUserInteractionEnabled = false
                    }
                    else if(self.paymentType == .full) {
                        self.lblHeaderTitle.text = "Explore Garbh Sanskar Guru App"
                        self.lblGSTInfo.isHidden = false
                        self.lblPriceInfo.isHidden = false
                        
                        let gstPrice = _user.gstFullPrice
                        self.lblPriceInfo.text = "Total Course Price : ₹ \(_user.amount)"
                        
                        self.lblGSTInfo.text = "GST @ \(Int(_user.gstPresntage)) % : ₹ \(Int(gstPrice.rounded()))"
                        self.lblTotalPriceInfo.text = "Total App Price(Round off)"
                        self.total = Double(_user.amount) + gstPrice.rounded()
                        self.lblPrice.text = "₹ \(Int(self.total.rounded()))"
                        
                    }
                }
            }
        }
    }
    
    
    
    //  func chechGiftCode(block: @escaping (Bool) -> ()) {
    func chechGiftCode() {
        self.showCentralSpinner()
        KPWebCall.call.checkGiftCode(param: referralData.paramDict()) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["message"] as? NSDictionary{
                    
                    let igift_id = data["gift_id"] as? String
                    let iis_gift = data["is_gift"] as? Int
                    
                    // for code added
                    _user.setPaymentStatus(dict: data)
                    _appDelegator.saveContext()
                    
                    if _user.paymentStatus == .pending {
                        if  data["amount"] as? Int == 0 {
                            _user.uniqueId =  data.getStringValue(key: "unique_id")
                            _appDelegator.saveContext()
                            //                            self.updatePaymentStatus(mycode:"AGENTCOUPAN_100" , gift_id: igift_id ?? "", is_gift: iis_gift ?? 0) { (complete) in
                            //                                if complete {
                            //                                    self.performSegue(withIdentifier: "directSegue", sender: nil)
                            //                                }
                            //                            }
                            
                            self.updatePaymentStatusForCode(mycode:"AGENTCOUPAN_100" , gift_id: igift_id ?? "", is_gift: iis_gift ?? 0) { (complete) in
                                if complete {
                                    self.performSegue(withIdentifier: "directSegue", sender: nil)
                                }
                            }
                        }else {
                            _user.uniqueId =  data.getStringValue(key: "unique_id")
                            _user.amount = data.getInt32Value(key: "amount")
                            _user.gstPresntage = data.getDoubleValue(key:"demo_gst_per")
                            _user.gstFullPrice = data.getDoubleValue(key:"gst_price")
                            _user.usdPrice = data.getInt32Value(key: "usd_price")
                            _user.localSelectedPaymentType = 2
                            _appDelegator.saveContext()
                            
                            let paymentVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            paymentVC.isfromCode = true
                            self.navigationController?.pushViewController(paymentVC, animated: true)
                        }
                        
                    }else {
                        _user.uniqueId =  data.getStringValue(key: "unique_id")
                        _appDelegator.saveContext()
                        var code = ""
                        if self.txtCouponCode.text != "" {
                            code = self.txtCouponCode.text ?? ""
                        }else {
                            code = self.txtGiftCode.text ?? ""
                        }
                        //                        self.updatePaymentStatus(mycode: code + "_100" , gift_id: igift_id ?? "", is_gift: iis_gift ?? 0) { (complete) in
                        //                            if complete {
                        //                                if(self.isFromSlideMenuToBuyFullCourse){
                        //                                    _user.whichUser = WhichUser.reg_user.rawValue
                        //                                }
                        //                                self.performSegue(withIdentifier: "directSegue", sender: nil)
                        //                            }
                        //                        }
                        
                        self.updatePaymentStatusForCode(mycode: code + "_100" , gift_id: igift_id ?? "", is_gift: iis_gift ?? 0) { (complete) in
                            if complete {
                                if(self.isFromSlideMenuToBuyFullCourse){
                                    _user.whichUser = WhichUser.reg_user.rawValue
                                }
                                self.performSegue(withIdentifier: "directSegue", sender: nil)
                            }
                        }
                    }
                    
                } else {
                    self.showError(data: json, view: self.view)
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func reappPurchase(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        
        let param: [String: Any] = ["user_id": _user.id]
        KPWebCall.call.setReappPurchase(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let userInfo = dict["data"] as? NSDictionary{
                    DispatchQueue.main.async {
                        _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
                        _user.initWith(entry: userInfo)
                        _user.whichUser = WhichUser.reg_user.rawValue
                        _appDelegator.saveContext()
                        block(true)
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            } else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    
    func skipGiftCodeTrial(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.skipGiftCode { (json, statusCode) in
            self.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary{
                    DispatchQueue.main.async {
                        
                        _user.uniqueId =  data.getStringValue(key: "unique_id")
                        
                        //  _user.setReferral(dict: data)
                        _appDelegator.saveContext()
                        
                        if data.getStringValue(key: "payment_status") == "Pending" {
//                            self.updatePaymentStatus(mycode: "TRIAL_FREE", gift_id: "", is_gift: 0) { complete in
//                                if complete {
//                                    self.performSegue(withIdentifier: "directSegue", sender: nil)
//                                    block(false)
//                                }
//                            }
                            self.updatePaymentStatusForCode(mycode: "TRIAL_FREE", gift_id: "", is_gift: 0) { complete in
                                if complete {
                                    self.performSegue(withIdentifier: "directSegue", sender: nil)
                                    block(false)
                                }
                            }
                        }
                        else {
                            block(true)
                        }
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            } else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    
    func skipGiftCode(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.skipGiftCode { (json, statusCode) in
            self.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary{
                    DispatchQueue.main.async {
                        _user.uniqueId =  data.getStringValue(key: "unique_id")
                        //                        _user.setReferral(dict: data)
                        _appDelegator.saveContext()
                        
                        block(true)
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            } else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func updatePaymentStatus(mycode : String,gift_id : String ,is_gift : Int ,block: @escaping (Bool) -> ()) {
        let randomNumber = arc4random_uniform(55555)
        //        let param: [String: Any] = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode": "COUPAN_100", "transaction_id": randomNumber, "amount": _user.amount]
        var param: [String: Any] = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode":mycode, "transaction_id": randomNumber, "amount": _user.amount]
        
        if self.paymentType == .full {
            param ["which_user"] = "reg_user"
            param ["check_day"] = "Undone"
        }else{
            param ["which_user"] = "demo_user"
            param ["check_day"] = "Done"
        }
        
        if gift_id != "" {
            param["gift_id"] = gift_id
        }
        
        if is_gift != 0 {
            param["is_gift"] = is_gift
        }
        
        self.showCentralSpinner()
        KPWebCall.call.updatePaymentStatus(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    _user.setPaymentStatus(dict: data)
                    _appDelegator.saveContext()
                    if data.getStringValue(key: "payment_status") == "Pending" {
                        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment is pending")
                        block(false)
                    }else {
                        block(true)
                        //screen open
                    }
                }else if let msg = dict["message"] as? NSDictionary{
                    weakself.showError(data: msg, view: weakself.view)
                    block(false)
                }else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    
    
    func updatePaymentStatusForCode(mycode : String,gift_id : String ,is_gift : Int ,block: @escaping (Bool) -> ()) {
        let randomNumber = arc4random_uniform(55555)
        var param: [String: Any] = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode":mycode, "transaction_id": randomNumber, "amount": "0"]
        
        if self.paymentType == .full {
            param ["which_user"] = "reg_user"
            param ["check_day"] = "Undone"
        }else{
            param ["which_user"] = "demo_user"
            param ["check_day"] = "Done"
        }
        
        if gift_id != "" {
            param["gift_id"] = gift_id
        }
        
        if is_gift != 0 {
            param["is_gift"] = is_gift
        }
        
        self.showCentralSpinner()
        KPWebCall.call.updatePaymentStatus(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    _user.setPaymentStatus(dict: data)
                    _appDelegator.saveContext()
                    if data.getStringValue(key: "payment_status") == "Pending" {
                        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment is pending")
                        block(false)
                    }else {
                        block(true)
                        //screen open
                    }
                }else if let msg = dict["message"] as? NSDictionary{
                    weakself.showError(data: msg, view: weakself.view)
                    block(false)
                }else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}

extension ReferalCodeNewVC: UITextFieldDelegate {
    
    @IBAction func tfEditingChange(_ sender: UITextField) {
        if sender.tag == 0 {
            referralData.referralCode = sender.text!.trimmedString()
        }else {
            referralData.giftAppCode = sender.text!.trimmedString()
        }
    }
}
