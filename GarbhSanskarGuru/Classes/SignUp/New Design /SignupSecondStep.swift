//
//  SignupSecondStep.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 10/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SignupSecondStep: ParentViewController {

   // MARK: - Outlet Declaration
    
    @IBOutlet var label_Desc: JPWidthLabel!
    @IBOutlet var label_title: JPWidthLabel!
    @IBOutlet var btnPregnent: UIButton!
    @IBOutlet var btnPlaning: UIButton!
            
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label_Desc.text = _appDelegator.languageBundle?.localizedString(forKey: "signup_sp2_desc", value: "", table: nil)
        label_title.text = _appDelegator.languageBundle?.localizedString(forKey: "signup_sp2_title", value: "", table: nil)

        makeSelectionType(type: 1) // default value is -1 means no selection.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        prepareUI()
    }
    
    // MARK: - Private Methods
    
    func prepareUI(){
        DispatchQueue.main.async {
            self.btnPregnent.makeCircle()
            self.btnPlaning.makeCircle()
        }
    }
    
    func makeSelectionType(type:Int){
        let bgColor = UIColor.colourWith(red: 239, green: 65, blue: 114, alpha: 1.0)
        if type == 0{
            // pregnent select
            btnPlaning.backgroundColor = .clear
            btnPregnent.backgroundColor = bgColor
            btnPlaning.layer.borderColor = bgColor.cgColor
            btnPlaning.layer.borderWidth = 1
            _appDelegator.objInfo.planingStatus = "pregnent"
        }else if type == 1{
            // planing select
            btnPregnent.backgroundColor = .clear
            btnPlaning.backgroundColor = bgColor
            btnPregnent.layer.borderColor = bgColor.cgColor
            btnPregnent.layer.borderWidth = 1
            _appDelegator.objInfo.planingStatus = "planing"
        }else{
            btnPregnent.backgroundColor = .clear
            btnPregnent.layer.borderColor = bgColor.cgColor
            btnPregnent.layer.borderWidth = 1

            btnPlaning.backgroundColor = .clear
            btnPlaning.layer.borderColor = bgColor.cgColor
            btnPlaning.layer.borderWidth = 1
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.setupUI(isCancelable: true)
        onBoardingView.actionBlock = { (action) in
            if action == .done{
                guard !onBoardingView.txtDay.text!.isEmpty else {
                    _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                    return
                }
                onBoardingView.endEditing(true)
                let currDay = onBoardingView.txtDay.text!.integerValue!
                print("Current Day \(currDay)")
                
                guard currDay > 0 && currDay < 280 else {
                    _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                    return
                }
                 onBoardingView.removeFromSuperview()
                _appDelegator.objInfo.currentDay = currDay
                self.makeSelectionType(type: 0)
            } else {
                onBoardingView.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Button Action Methods
    
    @IBAction func action_pregnentTapped(_ sender: UIButton) {
        prepareOnBoarding()
    }
    
    @IBAction func action_planningTapped(_ sender: UIButton) {
        self.makeSelectionType(type: 1)
    }
    
}
