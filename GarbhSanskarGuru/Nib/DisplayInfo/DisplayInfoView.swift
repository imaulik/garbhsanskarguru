//
//  DisplayInfoView.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 22/04/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class DisplayInfoView: UIView {

   @IBOutlet var animatedViews: UIView!

    @IBOutlet var buttonOk: UIButton!
    var actionBlock: (KAction)->Void = {_ in}
   
    @IBOutlet var objscrollview: UIScrollView!
    @IBOutlet var label_title: JPWidthLabel!
    
    @IBOutlet var labelFirstTitle: JPWidthLabel!
    @IBOutlet var labelFirstValue: JPWidthLabel!
    
    @IBOutlet var labelSecondTitle: JPWidthLabel!
    @IBOutlet var labelSecondValue: JPWidthLabel!
    
    @IBOutlet var labelThirdTitle: JPWidthLabel!
    @IBOutlet var labelThirdValue: JPWidthLabel!
    
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> DisplayInfoView {
       let objView = UINib(nibName: "DisplayInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DisplayInfoView
       objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
       objView.layoutIfNeeded()
       view.addSubview(objView)
       return objView
   }
    
    override func layoutSubviews() {
        //UIScreen.height
        print("height scrollview : \(self.objscrollview.contentSize)")
    }
    
    
    func setupUI(){
        self.buttonOk.layer.cornerRadius = buttonOk.frame.size.height / 2
        self.buttonOk.clipsToBounds = true
    }
    
    func setupValues(valueIndex:Int){
        let headerTitle = "info_headerTitle_" + "\(String(describing: valueIndex))"
        
        let firstTitle = "info_firstTitle_" + "\(String(describing: valueIndex))"
        let secondTitle = "info_secondTitle_" + "\(String(describing: valueIndex))"
        let thirdTitle = "info_thirdTitle_" + "\(String(describing: valueIndex))"
        
        let firstValue = "info_firstValue_" + "\(String(describing: valueIndex))"
        let secondValue = "info_secondValue_" + "\(String(describing: valueIndex))"
        let thirdValue = "info_thirdValue_" + "\(String(describing: valueIndex))"
        
        
        self.label_title.text = _appDelegator.languageBundle?.localizedString(forKey: headerTitle, value: "", table: nil)
        self.labelFirstTitle.text = _appDelegator.languageBundle?.localizedString(forKey: firstTitle, value: "", table: nil)
        self.labelSecondTitle.text = _appDelegator.languageBundle?.localizedString(forKey: secondTitle, value: "", table: nil)
        self.labelThirdTitle.text = _appDelegator.languageBundle?.localizedString(forKey: thirdTitle, value: "", table: nil)
        
        self.labelFirstValue.text = _appDelegator.languageBundle?.localizedString(forKey: firstValue, value: "", table: nil)
        self.labelSecondValue.text = _appDelegator.languageBundle?.localizedString(forKey: secondValue, value: "", table: nil)
        self.labelThirdValue.text = _appDelegator.languageBundle?.localizedString(forKey: thirdValue, value: "", table: nil)
    }
    
   func animateView(with tag: Int) {
       UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
           self.animatedViews.transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
           
       }, completion: { _ in
           UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
               self.animatedViews.transform = .identity
           }, completion: { _ in
           })
       })
   }
   
   
   @IBAction func onBtnOkAction(_ sender: UIButton) {
       self.actionBlock(.done)
   }

}
