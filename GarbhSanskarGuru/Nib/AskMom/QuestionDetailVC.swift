//
//  QuestionDetailVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 19/04/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class QuestionDetailVC: ParentViewController {
    
    @IBOutlet weak var txtAnswer: UITextView!
    var arrAnswerList : [AnswerList]?
    var dicQuestionList:QuestionList?
    var offset = 0
    var limit = 10
    var totalCount = 0
    @IBOutlet weak var tblAnswers: UITableView!
    @IBOutlet weak var bottomChatConstant: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblAnswers.register(UINib(nibName: "QuestionCell", bundle: nil), forCellReuseIdentifier: "QuestionCell")
        self.tblAnswers.register(UINib(nibName: "AnswerCell", bundle: nil), forCellReuseIdentifier: "AnswerCell")
        self.tblAnswers.register(UINib(nibName: "LoadMoreCell", bundle: nil), forCellReuseIdentifier: "LoadMoreCell")
        self.txtAnswer.setNeedsLayout()
        self.txtAnswer.setNeedsDisplay()
        self.title = "Ask To Expert Mom"
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name:  UIResponder.keyboardWillHideNotification, object: nil)
        
        reloadQuestions()
    }
    
    func reloadQuestions(){
        getAnswers()
    }
    
    func getAnswers() {
        offset = 0
        limit = 10
        let param = ["user_id":_user.id,"question_id":"\(dicQuestionList?.id ?? "0")","offset":"\(offset)","limit":"\(limit)"]
        
        self.showCentralSpinner()
        KPWebCall.call.getAnswer(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let qList = AnswerModel(dictionary: dict)
                weakself.arrAnswerList = qList.data
                weakself.offset = weakself.offset + weakself.limit
                weakself.totalCount = qList.answer_count
                weakself.tblAnswers.reloadData()
            }
        }
    }
    
    func getLoadMoreAnswers() {
        let param = ["user_id":_user.id,"question_id":"\(dicQuestionList?.id ?? "0")","offset":"\(offset)","limit":"\(limit)"]
        self.showCentralSpinner()
        KPWebCall.call.getAnswer(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let questionList = AnswerModel(dictionary: dict)
                weakself.arrAnswerList! += questionList.data ?? [AnswerList]()
                weakself.offset = weakself.offset + weakself.limit
                weakself.totalCount = questionList.answer_count
                weakself.tblAnswers.reloadData()
            }
        }
    }
    
    
    @objc func btnEditPress(sender:UIButton) {
        _ = displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupEditQuestion", blockOK: {
            self.dismissPopUp()
            self.reloadQuestions()
        }, blockCancel: {
            self.dismissPopUp()
        }, objData:arrAnswerList?[sender.tag] as AnyObject)
    }
    
    @objc func btnDeletePress(sender:UIButton) {
        _ = displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupDeleteQuestion", blockOK: {
            self.dismissPopUp()
            self.reloadQuestions()
        }, blockCancel: {
            self.dismissPopUp()
        }, objData:arrAnswerList?[sender.tag] as AnyObject)
    }
    
    @IBAction func btnPostAnswerPress(_ sender: Any) {
        if txtAnswer.text?.trimmedString()  == "" {
            self.showErrorView(data: "Please enter Answer", view: self.view)
//            showAlert("Please enter Answer", position: .Center)
        }else {
            let param = ["user_id" : _user.id , "question_id" :dicQuestionList?.id ?? "" , "answer" :txtAnswer.text ?? ""]
            self.showCentralSpinner()
            KPWebCall.call.postAnswer(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    weakself.reloadQuestions()
                    weakself.txtAnswer.text = ""
                }
            }
            
//            callWs_PostAnswer(parms: parms) { sucess in
//                if sucess {
//                    self.reloadQuestions()
//                    self.txtAnswer.text = ""
//                }
//            }
        }
    }
    
    @objc func keyboardShow(notification: NSNotification) {
            if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                 let keyboardHeight = keyboardRectValue.height
                 let safeAreaBottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
                  bottomChatConstant.constant = -(keyboardHeight - safeAreaBottomInset)
        }
    }
   
    @objc func keyboardHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            bottomChatConstant.constant = 0
        }
    }
}


extension QuestionDetailVC {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 132
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell") as! QuestionCell
            //cell.imgQueUser
            cell.lblUserName.text =  dicQuestionList?.firstname
            cell.lblQuestion.text = "Q : \(dicQuestionList?.question ?? "")"
            
            cell.editLine.isHidden = true
            cell.editStackView.isHidden = true
            cell.totalAnsView.isHidden = false
            cell.totalAnswer.isHidden = true
            
            cell.lblTotalAnswer.text = "All answer (\(dicQuestionList?.ans_count ?? 0))"
            return cell
        }
        else{
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (offset < totalCount){
            return 2;
        }
        else{
            return 1;
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return  arrAnswerList?.count ?? 0
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell") as! AnswerCell
            //cell.imgQueUser
            cell.lblUserName.text =  arrAnswerList?[indexPath.row].firstname
            cell.lblAnwser.text = "A : \(arrAnswerList?[indexPath.row].answer ?? "")"
            
            
            if arrAnswerList?[indexPath.row].user_id == _user.id {
                cell.editLine.isHidden = false
                cell.editStackView.isHidden = false
            }
            else{
                cell.editLine.isHidden = true
                cell.editStackView.isHidden = true
            }
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(btnEditPress), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(btnDeletePress), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        } else {
            if (offset < totalCount){
                getLoadMoreAnswers()
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell", for: indexPath) as! LoadMoreCell
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell", for: indexPath) as! LoadMoreCell
                return cell
            }
        }
    }
}
