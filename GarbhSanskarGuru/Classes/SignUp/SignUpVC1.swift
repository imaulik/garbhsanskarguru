//
//  SignUpVC1.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit


class SignUpVC1: ParentViewController {

    var objInfo = UserInfo()
    @IBOutlet var viewAccessory: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension SignUpVC1 {
    
    func prepareUI() {
        objInfo.initModel()
        setKeyboardNotifications()
        viewAccessory.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: 44)
    }
    
    func getEntryCell(_ row: Int) -> SIgnUpCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? SIgnUpCell
        return cell
    }
}

extension SignUpVC1 {
    
    @IBAction func btnDoneTapped(_ sender: UIButton){
        self.view.endEditing(true)
    }
}

extension SignUpVC1 {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objInfo.arrFiled.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return objInfo.arrFiled[indexPath.row].cellType.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let userInfo = objInfo.arrFiled[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: userInfo.cellType.rawValue, for: indexPath) as! SIgnUpCell
        cell.parentVC = self
        cell.setUpCell(userInfo, tag: indexPath.row)
        return cell
    }
}

extension SignUpVC1 {
    
    func checkEmailExist(email: String ,block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.checkEmailExist(param: ["email": email]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, let msg = dict["message"] as? String {
                if status == 200 {
                    _ = ValidationToast.showStatusMessage(message: msg)
                    block(true)
                }else {
                    block(false)
                    self.showError(data: json, view: self.view)
                }
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
}
