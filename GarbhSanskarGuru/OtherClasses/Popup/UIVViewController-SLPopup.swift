//
//  UIVViewController-SLpopup.swift
//  AnonyChat
//
//  Created by Nguyen Duc Hoang on 9/6/15.
//  Copyright © 2015 Home. All rights reserved.
//

import UIKit
import QuartzCore
import ObjectiveC
import MessageUI

enum SLpopupViewAnimationType: Int {
    case bottomTop
    case topBottom
    case bottomBottom
    case topTop
    case leftLeft
    case leftRight
    case rightLeft
    case rightRight
    case fade
}
let kSourceViewTag = 11111
let kpopupViewTag = 22222
let kOverlayViewTag = 22222

var kpopupViewController:UInt8 = 0
var kpopupBackgroundView:UInt8 = 1

let kpopupAnimationDuration = 0.35
let kSLViewDismissKey = "kSLViewDismissKey"

extension UIViewController {
    var popupBackgroundView:UIView? {
        get {
            return objc_getAssociatedObject(self, &kpopupBackgroundView) as? UIView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &kpopupBackgroundView, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    var popupViewController:UIViewController? {
        get {
            return objc_getAssociatedObject(self, &kpopupViewController) as? UIViewController
        }
        set(newValue) {
            objc_setAssociatedObject(self, &kpopupViewController, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
//    var dismissedCallback:UIViewController? {
//        get {
//            return objc_getAssociatedObject(self, kSLViewDismissKey) as? UIViewController
//        }
//        set(newValue) {
//            objc_setAssociatedObject(self, kSLViewDismissKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//        }
//    }
    
    func presentpopupViewController(_ popupViewController: UIViewController, animationType:SLpopupViewAnimationType, completion:() -> Void) {
        let sourceView:UIView = self.getTopView()
        self.popupViewController = popupViewController
        let popupView:UIView = popupViewController.view
        sourceView.tag = kSourceViewTag
        popupView.autoresizingMask = [.flexibleTopMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleBottomMargin]
        popupView.tag = kpopupViewTag
        if(sourceView.subviews.contains(popupView)) {
            return
        }
        popupView.layer.shadowPath = UIBezierPath(rect: popupView.bounds).cgPath
//        popupView.layer.masksToBounds = false
//        popupView.layer.shadowOffset = CGSizeMake(5, 5)
//        popupView.layer.shadowRadius = 5
//        popupView.layer.shadowOpacity = 0.5
        popupView.layer.shouldRasterize = true
        popupView.layer.rasterizationScale = UIScreen.main.scale
        
        let overlayView:UIView = UIView(frame: sourceView.bounds)
        overlayView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlayView.tag = kOverlayViewTag
        overlayView.backgroundColor = UIColor.clear
        
        self.popupBackgroundView = UIView(frame: sourceView.bounds)
        self.popupBackgroundView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.popupBackgroundView!.backgroundColor = UIColor.white
        self.popupBackgroundView?.alpha = 0.0
        if let _ = self.popupBackgroundView {
            overlayView.addSubview(self.popupBackgroundView!)
        }
        //Background is button
        let dismissButton: UIButton = UIButton(type: .custom)
        dismissButton.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        dismissButton.backgroundColor = UIColor.clear
        dismissButton.frame = sourceView.bounds
        overlayView.addSubview(dismissButton)
        
        popupView.alpha = 0.0
        overlayView.addSubview(popupView)
        sourceView.addSubview(overlayView)
        
       // dismissButton.addTarget(self, action: "btnDismissViewControllerWithAnimation:", forControlEvents: .TouchUpInside)
        switch animationType {
        case .bottomTop, .bottomBottom,.topTop,.topBottom, .leftLeft, .leftRight,.rightLeft, .rightRight:
            dismissButton.tag = animationType.rawValue
            self.slideView(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
            
        default:
            dismissButton.tag = SLpopupViewAnimationType.fade.rawValue
            self.fadeView(popupView, sourceView: sourceView, overlayView: overlayView)
        }
        
    }
    func slideView(_ popupView: UIView, sourceView:UIView, overlayView:UIView, animationType: SLpopupViewAnimationType) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        var popupStartRect:CGRect
        switch animationType {
        case .bottomTop, .bottomBottom:
            popupStartRect = CGRect(x: (sourceSize.width - popupSize.width)/2, y: sourceSize.height, width: popupSize.width, height: popupSize.height)
        case .leftLeft, .leftRight:
            popupStartRect = CGRect(x: -sourceSize.width, y: (sourceSize.height - popupSize.height)/2, width: popupSize.width, height: popupSize.height)
        case .topTop, .topBottom:
            popupStartRect = CGRect(x: (sourceSize.width - popupSize.width)/2, y: -sourceSize.height, width: popupSize.width, height: popupSize.height)
        default:
            popupStartRect = CGRect(x: sourceSize.width, y: (sourceSize.height - popupSize.height)/2, width: popupSize.width, height: popupSize.height)
        }
        let popupEndRect:CGRect = CGRect(x: (sourceSize.width - popupSize.width)/2, y: (sourceSize.height - popupSize.height)/2, width: popupSize.width, height: popupSize.height)
        popupView.frame = popupStartRect
        popupView.alpha = 1.0
        UIView.animate(withDuration: kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController?.viewWillAppear(false)
            self.popupBackgroundView?.alpha = 0.5
            popupView.frame = popupEndRect
            }, completion: { (finished) -> Void in
                self.popupViewController?.viewDidAppear(false)
        }) 
        
    }
    func slideViewOut(_ popupView: UIView, sourceView:UIView, overlayView:UIView, animationType: SLpopupViewAnimationType) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        var popupEndRect:CGRect
        switch animationType {
        case .bottomTop, .topTop:
            popupEndRect = CGRect(x: (sourceSize.width - popupSize.width)/2, y: -popupSize.height, width: popupSize.width, height: popupSize.height)
        case .bottomBottom, .topBottom:
            popupEndRect = CGRect(x: (sourceSize.width - popupSize.width)/2, y: popupSize.height, width: popupSize.width, height: popupSize.height)
        case .leftRight, .rightRight:
            popupEndRect = CGRect(x: sourceSize.width, y: popupView.frame.origin.y, width: popupSize.width, height: popupSize.height)
        default:
            popupEndRect = CGRect(x: -popupSize.width, y: popupView.frame.origin.y, width: popupSize.width, height: popupSize.height)
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                self.popupBackgroundView?.backgroundColor = UIColor.clear
            }) { (finished) -> Void in
                UIView.animate(withDuration: kpopupAnimationDuration, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                    self.popupViewController?.viewWillDisappear(false)
                    popupView.frame = popupEndRect
                    }) { (finished) -> Void in
                        popupView.removeFromSuperview()
                        overlayView.removeFromSuperview()
                        self.popupViewController?.viewDidDisappear(false)
                        self.popupViewController = nil
                }
        }
        
        
        
    }
    
    func fadeView(_ popupView: UIView, sourceView:UIView, overlayView:UIView) {
        let sourceSize: CGSize = sourceView.bounds.size
        let popupSize: CGSize = popupView.bounds.size
        popupView.frame = CGRect(x: (sourceSize.width - popupSize.width)/2,
                                                y: (sourceSize.height - popupSize.height)/2,
                                                width: popupSize.width,
                                                height: popupSize.height)
        popupView.alpha = 0.0
        
        UIView.animate(withDuration: kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController!.viewWillAppear(false)
            self.popupBackgroundView!.alpha = 0.5
            popupView.alpha = 1.0
        }, completion: { (finished) -> Void in
           self.popupViewController?.viewDidAppear(false)
        }) 
        
    }
    
    func fadeViewOut(_ popupView: UIView, sourceView:UIView, overlayView:UIView) {
        UIView.animate(withDuration: kpopupAnimationDuration, animations: { () -> Void in
            self.popupViewController?.viewDidDisappear(false)
            self.popupBackgroundView?.alpha = 0.0
            popupView.alpha = 0.0
        }, completion: { (finished) -> Void in
            popupView.removeFromSuperview()
            overlayView.removeFromSuperview()
            self.popupViewController?.viewDidDisappear(false)
            self.popupViewController = nil
        }) 
        
    }
    func btnDismissViewControllerWithAnimation(_ btnDismiss : UIButton) {
        let animationType:SLpopupViewAnimationType = SLpopupViewAnimationType(rawValue: btnDismiss.tag)!
        switch animationType {
        case .bottomTop, .bottomBottom, .topTop, .topBottom, .leftLeft, .leftRight, .rightLeft, .rightRight:
            _appDelegator.window?.rootViewController!.dismissPopupViewController(animationType)
        default:
            _appDelegator.window?.rootViewController!.dismissPopupViewController(SLpopupViewAnimationType.fade)
        }
    }
    func getTopView() -> UIView {
        var recentViewController:UIViewController = self
        if let _ = recentViewController.parent {
           recentViewController = recentViewController.parent!
        }
        return recentViewController.view
    }
    func dismissPopupViewController(_ animationType: SLpopupViewAnimationType) {
        let sourceView:UIView = self.getTopView()
        if sourceView.viewWithTag(kpopupViewTag) != nil {
            for view in sourceView.subviews{
                if view.viewWithTag(kpopupViewTag) != nil {
                    let popupView:UIView = view.viewWithTag(kpopupViewTag)!
                    let overlayView:UIView = view.viewWithTag(kOverlayViewTag)!
                    switch animationType {
                    case .bottomTop, .bottomBottom, .topTop, .topBottom, .leftLeft, .leftRight, .rightLeft, .rightRight:
                        _appDelegator.window?.rootViewController!.slideViewOut(popupView, sourceView: view, overlayView: overlayView, animationType: animationType)
                    default:
                        fadeViewOut(popupView, sourceView: view, overlayView: overlayView)
                        
                        
                    }
                }
            }
        }
    }
    
    // pop method to present
    func displayViewController(_ animationType: SLpopupViewAnimationType,nibName : String, blockOK : (() -> ())?,blockCancel : (() -> ())?, objData : AnyObject?) -> MyPopUpView {
        let myPopupViewController:MyPopUpView = freeAppDashboard.instantiateViewController(withIdentifier: nibName) as! MyPopUpView
        myPopupViewController.pressOK = blockOK
        myPopupViewController.pressCancel = blockCancel
        myPopupViewController.objData = objData
        _appDelegator.window?.rootViewController!.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
        return myPopupViewController
    }
    
    func dismissPopUp() {
        _appDelegator.window?.rootViewController?.dismissPopupViewController(SLpopupViewAnimationType.fade)
    }
    
    func showErrorView(data: Any?, view: UIView?) {
        if let dict = data as? NSDictionary{
            if let msg = dict["message"] as? String{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            } else if let msg = dict["email"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["RESPMSG"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["error"] as? String{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["email"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["password"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict[_appName] as? String {
                if msg != kInternetDown{
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
                }
            }else{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: kInternalError)
            }
        }else{
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kInternalError)
        }
    }
}


extension UIViewController:MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIDocumentPickerDelegate,UIDocumentPickerHandler
{

    //MARK:Activity View Controller
    func shareYourItems(shareItems:Array<Any>,isexcludItems:Bool,excludItems:Array<UIActivity.ActivityType>) -> Void {
        let avc = UIActivityViewController(activityItems:shareItems , applicationActivities: []);
        //  myAppDelegate.setDefaultna@objc vigationAppereance();
        if isexcludItems
        {
            if (!excludItems.isEmpty)
            {
                avc.excludedActivityTypes = excludItems;
            }
        }
         if UIDevice.current.userInterfaceIdiom == .pad {
            avc.modalPresentationStyle  = UIModalPresentationStyle.popover
            avc.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            avc.popoverPresentationController?.sourceView = self.view
            avc.popoverPresentationController?.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
        }
        present(avc, animated: true);
    }
     
    
    
    //MARK:send Email
      func  sendEmail(myRecipients:Array<Any>,mySubject:String,myMessage:String,isAttachment:Bool,Attachments:Array<Any>,viewController:UIViewController) -> Void
      {
          let composer = MFMailComposeViewController()
          if MFMailComposeViewController.canSendMail() {
              composer.mailComposeDelegate = viewController as MFMailComposeViewControllerDelegate
              composer.setToRecipients(myRecipients as? [String])
              composer.setSubject(mySubject)
          
              if isAttachment
              {
                  
              }
      
              composer.setMessageBody(myMessage, isHTML: false)
             
            if UIDevice.current.userInterfaceIdiom == .pad {
                composer.modalPresentationStyle  = UIModalPresentationStyle.popover
                composer.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                composer.popoverPresentationController?.sourceView = self.view
                composer.popoverPresentationController?.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
            }
            present(composer, animated: true, completion: nil)
          }
          else{
               print("mail not avail");
          }
      }
      
      
      public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
          
          switch result {
          case MFMailComposeResult.cancelled:
              print("Mail cancelled")
          case MFMailComposeResult.saved:
              print("Mail saved")
          case MFMailComposeResult.sent:
              print("Mail sent")
          case MFMailComposeResult.failed:
              print("Mail sent failure: \(error?.localizedDescription ?? "")")
          default:
              break
          }
           dismiss(animated: true, completion: nil);
      }
    
    
    
    //MARK: UIDocument Picker View controller
    func openDocumentPickerVC(urViewController:UIViewController,supportsExtentions:[String],PickerMode:UIDocumentPickerMode) -> Void
    {
        let documentPicker = UIDocumentPickerViewController(documentTypes:supportsExtentions, in:PickerMode)
        documentPicker.delegate = urViewController.self as UIDocumentPickerDelegate;
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        getDocumentPickerURL(controller: controller, url: myURL)
    }
    
    func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentPickerURL(controller: UIDocumentPickerViewController, url: URL) {
        
    }
    
    //MARK:send Messagez
    func sendMessage(yourMessage:String,yourRecipients:Array<Any>,viewController:UIViewController) -> Void
    {
//        if !CommonUtils().isPhoneSupport()
//        {
//            print("alert");
//        }
//        else
//        {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController();
                controller.body = yourMessage;
                controller.recipients = yourRecipients as? [String];
                controller.messageComposeDelegate = viewController as MFMessageComposeViewControllerDelegate;
                present(controller, animated: true, completion: nil);
            }
            else
            {
                print("message not avail");
            }
        //}
    }
    
    
    public func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
        dismiss(animated: true, completion: nil);
    }
    
    func configuredMailComposeViewController(email: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([email])
        mailComposerVC.setSubject(_appName)
        mailComposerVC.setMessageBody("Hello this is my message body!", isHTML: false)
        return mailComposerVC
    }

 }

//MARK:  Document picker handler
protocol  UIDocumentPickerHandler {
    func  getDocumentPickerURL(controller:UIDocumentPickerViewController,url:URL) -> Void
}

extension UIViewController {
    
    func showNoDataFound(text:String) {
        let lblNoData = UILabel(frame: CGRect(x: 10, y: 0, width: _screenSize.width - 30, height:20))
        lblNoData.center = self.view.window?.center ?? CGPoint(x: 100, y: 200)
        lblNoData.tag = 456
        lblNoData.text = text
        lblNoData.numberOfLines = 0
        lblNoData.lineBreakMode = .byWordWrapping
        lblNoData.textAlignment = .center
        lblNoData.textColor = .darkGray
        lblNoData.font = UIFont(name: "WorkSans-SemiBold", size: CGFloat(18))
        self.view.window?.addSubview(lblNoData)
    }
    
    func hideNoDataFound() {
        for imgView in self.view.window!.subviews {
            if imgView.tag == 456 {
                imgView.removeFromSuperview()
            }
        }
    }
}

func urlTo(scheme:String) -> Void
 {
     if let url = URL(string: scheme) {
         
             UIApplication.shared.open(url, options: [:], completionHandler: {
                 (success) in
                 print("Open \(scheme): \(success)")
             })
      }
 }
