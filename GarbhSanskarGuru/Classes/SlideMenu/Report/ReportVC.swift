//
//  ReportVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 20/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ReportVC: ParentViewController {
    
    @IBOutlet var viewSummaryActivities: BaseView!
    @IBOutlet var viewCompletedActivities: BaseView!
    @IBOutlet var btnCompletedDay: BaseButton!
    
    var objReportModel:ReportModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareForUI()
        prepareForApicallReport()
    }
    
}

// UI
extension ReportVC{
    
    func prepareForUI(){
        
        viewSummaryActivities.applyCorner(15)
        viewCompletedActivities.applyCorner(15)
        
        let summary_view = UIColor.createGradient(gradientStyle: .leftToRight, colors: [GarbhColor.gradientLeftBlue,GarbhColor.gradientRightBlue], frame: self.viewSummaryActivities.bounds)
        viewSummaryActivities.backgroundColor = summary_view
        
        let complete_activity_view = UIColor.createGradient(gradientStyle: .leftToRight, colors: [GarbhColor.gradientLeftBlue,GarbhColor.gradientRightBlue], frame: self.viewSummaryActivities.bounds)
        viewCompletedActivities.backgroundColor = complete_activity_view
        
    }
    
    func prepareForUpdateUI(){
        btnCompletedDay.setTitle("\(objReportModel!.doneActivityCount!)", for: .normal)
    }
}

// Action
extension ReportVC{
    
    @IBAction func action_SeeReport(_ sender: BaseButton) {
        if sender.tag == 0{
            // Completed activities
            let completed_activities = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "CompletedReportController") as! CompletedReportController
            completed_activities.reportModel = self.objReportModel
            self.navigationController?.pushViewController(completed_activities, animated: true)
        }else{
            // Summary Report.
            let summary_report = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SummaryReportController") as! SummaryReportController
            self.navigationController?.pushViewController(summary_report, animated: true)
        }
    }
}

// Drama
extension ReportVC{
    
    func prepareForApicallReport() {
                
        let param: [String: Any] = ["user_id": _user.id, "day": Int(_user!.currDay)]
        self.showCentralSpinner()
        KPWebCall.call.userDayReport(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
                        
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    weakself.objReportModel = ReportModel(fromDictionary: dict as! NSDictionary)
                    weakself.objReportModel?.getReportData(isFloat: true)
                    weakself.prepareForUpdateUI()
                } else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }
            else {
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
    
}
