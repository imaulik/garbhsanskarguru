//
//  AppDelegate.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Photos
import SDWebImage
import FBSDKCoreKit
import Firebase
 

//YoutubePlayer_in_WKWebView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var languageBundle: Bundle?
    var objInfo = UserInfo()
    //    var allPhotos : PHFetchResult<PHAsset>? = nil
    var isShowPrePaidDashboard:Bool = true // Manage for redirection for below flag.
    var isAllowBackPreDashboard:Bool = true // Manage redirect to PrePaidDashboard to Paid and viceVersa initial Dashboard for paid User, Allow to user redirection.
    var isClickGarbhSanskarInfo:Bool = false // Manage Paid Dashboard to freeDashboard on GSGuru info Menu click from sideMenu.
    var isClickFromDesierBaby:Bool = false // Manage Multiple times Appear FreeDashBoard Popup while user is in planing state and zero days and clicked on later button.(Won't start application later on).
    
    
    var closeSideMenuDetector:((_ isClose: Bool) -> Void)? // Block use for Detect SideMenu close or not. Using in Dashboard data refresh.
    var detectBackFromDashboard:(() -> Void)? // Block use for detect user clicked on back button from FreeAppDashboard which will redirect to PaidDashboard from FreeAppDashboard.
    
    var detectBackFromWebinar:(() -> Void)? // webinar language change
    
    var detectBackFromAboutUsView:(() -> Void)? //  About use language change
    
    var detectBackFromCourseFeedBack:(() -> Void)? //  About use language change
    
    var detectBackFromStatistic:(() -> Void)? //  About use language change
    
    
    var detectLanguageChangeFromPreDashboard:(() -> Void)? // Block use for Refresh language change data in PrePaidDashboard.
    var blockClickHerePreDashBoard:(() -> Void)? // Block use in clickHere button in PrePaidDashbord bottom button, To redirect PaidDashboard.
    var blockFullSubscribeCourse:(()->Void)? // Block use in Dashboard screen on FullCourse button click, on VideoViewPopup for DemoUser.
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //        do {
        //            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        //            try AVAudioSession.sharedInstance().setActive(true)
        //        } catch {
        //            print(error)
        //        }
        self.callHidePayment()
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
       
        Settings.isAutoLogAppEventsEnabled = true
        AppEvents.activateApp()
        
        //Local Notification
        if (application.applicationIconBadgeNumber != 0) {
            application.applicationIconBadgeNumber = 0;
        }
        
        UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            
            let notificationInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            setReminderNotificationHtmlDataToUserDefaults(dictUserInfo: notificationInfo as! [AnyHashable : Any])
            presentAnnouncementVC()
        }
        
        // Configure firebase.
        FirebaseApp.configure()
        registerPushNotification(application: application)
        
        _userDefault.register(defaults: [garbhLanguageKey: AppLanguage.english.rawValue])
        choosenLanguage(lang: AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!)
        UserDefaults.standard.set("1", forKey: "AppfromBackground")
        // var _user: User!
        
        if isUserLoggedIn() {
            createReminderNotificatinsForNextSevenDays()
            KPWebCall.call.setAccesTokenToHeader(token: getAuthorizationToken()!)
            let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
            if isPaymentAllowed {
                if(_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()){
                    self.navigateUserForSkipPaymentOrDemoPayment()
                }
                else {
                    self.navigateUserToHome()
                }
            } else {
                self.navigateDirectlyToHome()
            }
        }else {
            self.popToEntry()
        }
        
        // Settings Version in Device settingsxO
        setAppSettingsBundleInformation()
        
        // Check for internet
        _ = KPValidationToast.shared
        if !KPWebCall.call.isInternetAvailable(){
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                KPWebCall.call.networkManager.listener?(NetworkReachabilityManager.NetworkReachabilityStatus.notReachable)
            })
        }
        if KPWebCall().isInternetAvailable() {
            self.prepareForUpdateAvailable()
        }
        self.navigationAppereance()
        // Jahanvi comment
        //        self.setUpPaypalCredentials()
   
        return true
    }
    
    // Jahanvi comment
    /* func setUpPaypalCredentials() {
     //        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "AZa7u11DwppSexzsvOwK1PSg1lRKNM98ocg7HVQ7CIDRoLAexjzzdUK1OZv5GJRhlC3APw8f-Cm2CYBz", PayPalEnvironmentSandbox:
     //            ""])
     PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentSandbox:"Af2mJBtK9bdPsLpB3D2hGdOD28I1sPk3hRDAjLavWyxigmy_8CyvcltOtZWRhiYtDtN5BMqRny16lPJ6"])
     //        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "AZa7u11DwppSexzsvOwK1PSg1lRKNM98ocg7HVQ7CIDRoLAexjzzdUK1OZv5GJRhlC3APw8f-Cm2CYBz", PayPalEnvironmentSandbox:
     //        "Af2mJBtK9bdPsLpB3D2hGdOD28I1sPk3hRDAjLavWyxigmy_8CyvcltOtZWRhiYtDtN5BMqRny16lPJ6"])
     }*/
    
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {

        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )

    }  
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if (application.applicationIconBadgeNumber != 0) {
            application.applicationIconBadgeNumber = 0;
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppEvents.activateApp()
        SqliteDataManager.shared.openDatabase()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    /*
     // MARK: - Core Data stack
     
     lazy var persistentContainer: NSPersistentContainer = {
     /*
     The persistent container for the application. This implementation
     creates and returns a container, having loaded the store for the
     application to it. This property is optional since there are legitimate
     error conditions that could cause the creation of the store to fail.
     */
     let container = NSPersistentContainer(name: "GarbhSanskarGuru")
     
     container.loadPersistentStores(completionHandler: { (storeDescription, error) in
     if let error = error as NSError? {
     // Replace this implementation with code to handle the error appropriately.
     // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     
     /*
     Typical reasons for an error here include:
     * The parent directory does not exist, cannot be created, or disallows writing.
     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
     * The device is out of space.
     * The store could not be migrated to the current model version.
     Check the error message to determine what the actual problem was.
     */
     fatalError("Unresolved error \(error), \(error.userInfo)")
     }
     })
     return container
     }()
     
     var managedObjectContext: NSManagedObjectContext{
     return persistentContainer.viewContext
     }
     
     // MARK: - Core Data Saving support
     
     func saveContext () {
     
     let context = persistentContainer.viewContext
     if context.hasChanges {
     do {
     try context.save()
     } catch {
     // Replace this implementation with code to handle the error appropriately.
     // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     let nserror = error as NSError
     fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
     }
     }
     }
     */
    
    // MARK: - Core Data stack
    
    private(set) lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        
        return managedObjectContext
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: "GarbhSanskarGuru", withExtension: "momd") else {
            fatalError("Unable to Find Data Model")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to Load Data Model")
        }
        
        return managedObjectModel
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        
        let fileManager = FileManager.default
        let storeName = "GarbhSanskarGuru.sqlite"
        
        let libraryDirectoryURL = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
        let persistentStoreURL = NSURL(fileURLWithPath: libraryDirectoryURL).appendingPathComponent("/Application Support/\(storeName)")
        
        do {
            let options = [ NSInferMappingModelAutomaticallyOption : true,
                            NSMigratePersistentStoresAutomaticallyOption : true]
            
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: options)
        } catch {
            fatalError("Unable to Load Persistent Store")
        }
        
        return persistentStoreCoordinator
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        
        let context : NSManagedObjectContext = self.managedObjectContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

// MARK: - Login And logout
extension AppDelegate {
    
    func navigationAppereance()
    {
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 0.8470588235, green: 0.2117647059, blue: 0.4196078431, alpha: 1)
        //        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:  #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), NSAttributedString.Key.font: UIFont(name: "WorkSans-Medium", size: 20.0)!]
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -2000, vertical: 0), for:UIBarMetrics.default)
        UINavigationBar.appearance().isTranslucent = false
        
        
    }
    
    func isUserLoggedIn() -> Bool {
        let users = User.fetchDataFromEntity(predicate: nil, sortDescs: nil)
        if getAuthorizationToken() != nil && !users.isEmpty{
            _user = users.first
            return true
        }else{
            return false
        }
    }
    
    // commented
    //    func popToEntry() {
    //        let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
    //
    //        //let entVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "EntryVC") as! EntryVC
    //        //abhi comment above line and add below
    //        let loginVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    //
    //        let onBoardingVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingVC") as! OnBoardingVC
    //        if _appDelegator.getLaunchOnBoarding() == nil {
    //            nav.viewControllers = [onBoardingVC]
    //        } else {
    //            nav.viewControllers = [loginVC]//[entVC]
    //        }
    //        _appDelegator.window?.rootViewController = nav
    //    }
    
    func popToEntry() {
        
        let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
        let loginVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let onBoardingVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingVC") as! OnBoardingVC
        
        if _appDelegator.getLaunchOnBoarding() == nil {
            nav.viewControllers = [onBoardingVC]
        } else {
            nav.viewControllers = [onBoardingVC,loginVC]
        }
        _appDelegator.window?.rootViewController = nav
    }
    
    // Created
    func popToEntryRegister() {
        let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
        
        let registerVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        let onBoardingVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingVC") as! OnBoardingVC
        if _appDelegator.getLaunchOnBoarding() == nil {
            nav.viewControllers = [onBoardingVC]
        } else {
            nav.viewControllers = [onBoardingVC,registerVC]
        }
        _appDelegator.window?.rootViewController = nav
    }
    
    func navigateDirectlyToHome() {
        let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
        // add one line
        let onBoardingVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingVC")
        let entVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
        let homeVC = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SlideMenuContainerVC")
        //        nav.viewControllers = [entVC, homeVC] // comment
        nav.viewControllers = [onBoardingVC,entVC, homeVC]
        _appDelegator.window?.rootViewController = nav
    }
    
    func navigateUserToHome() {
        let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
        // add one line
        let onBoardingVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingVC")
        let entVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
        
        // Add new condition
        // Planinig state and current day is zero. redirect to demo dashboard instead of paid.
        //        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
        
        
        print("user current day : \(_user.currDay)")
        print("user pregnancy status : \(_user.pregnancyStatus)")
        print("user payment status : \(_user.paymentStatus)")
      
        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
            
            // DesierBaby status condition
            if _user.desireBabyStatus == .pending {
                let babyCategoryVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BabyCategoryVC") as! BabyCategoryVC
                //            nav.viewControllers = [entVC, babyCategoryVC] // comment
                nav.viewControllers = [onBoardingVC,entVC, babyCategoryVC]
                
            }else{
                let sideMenu = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SlideMenuContainerVC")
                //            nav.viewControllers = [entVC,sideMenu] // comment
                nav.viewControllers = [onBoardingVC,entVC,sideMenu]
            }
        }
        
        else if _user.desireBabyStatus == .pending && _user.paymentStatus == .completed{
            let babyCategoryVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BabyCategoryVC") as! BabyCategoryVC
            //            nav.viewControllers = [entVC, babyCategoryVC] // comment
            nav.viewControllers = [onBoardingVC,entVC, babyCategoryVC]
        }
        else if _user.referralStatus == .pending {
            
            //let introVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
            let introVC = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SlideMenuContainerVC")
            //            nav.viewControllers = [onBoardingVC,entVC, introVC] // comment
            nav.viewControllers = [onBoardingVC,entVC, introVC]
        } else if _user.paymentStatus == .pending {
            
            /*
             let paymentVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
             nav.viewControllers = [entVC, paymentVC] // comment
             */
            
            //            //Abhi changed
            //            if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.full.rawValue || _user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue){
            //
            //                let paymentVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            //                //                nav.viewControllers = [entVC, paymentVC] // comment
            //                nav.viewControllers = [onBoardingVC,entVC, paymentVC]
            //            }
            //            else {
            
            let introVC = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SlideMenuContainerVC")
            //                nav.viewControllers = [entVC, introVC] // comment
            nav.viewControllers = [onBoardingVC,entVC, introVC]
            // }
            
        }
            // commented above else if condition, after change signup flow no need to fill basic info. Commneted.
            //        else if _user.city.isEmpty || _user.country.isEmpty || _user.state.isEmpty {
            //
            //            //Abhi change because now from PaidVC this method call
            //            /*
            //             let paidVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PaidVC") as! PaidVC
            //             nav.viewControllers = [entVC, paidVC]
            //             */
            //
            //            let babyCategoryVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BasicDetailVC") as! BasicDetailVC
            //            //            nav.viewControllers = [entVC, babyCategoryVC] // comment
            //            nav.viewControllers = [onBoardingVC,entVC, babyCategoryVC]
            //
            //        }
        else if _user.desireBabyStatus == .pending {
            let babyCategoryVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BabyCategoryVC") as! BabyCategoryVC
            //            nav.viewControllers = [entVC, babyCategoryVC] // comment
            nav.viewControllers = [onBoardingVC,entVC, babyCategoryVC]
            
        } else {
            let homeVC = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SlideMenuContainerVC")
            //            nav.viewControllers = [entVC, homeVC] // comment
            nav.viewControllers = [onBoardingVC,entVC, homeVC]
        }
        _appDelegator.window?.rootViewController = nav
    }
    
    func isSkipPaymentOrSkipDemoPaymentForLoginUser() -> Bool {
        
        if(_user.isPaymentSkip || (_user.isDemoPaymentSkip && _user.whichUser.isEqual(str: WhichUser.demo_user.rawValue))){
            return true
        }
        return false
    }
    
    func navigateUserForSkipPaymentOrDemoPayment() {
        
          let value  = UserDefaults.standard.bool(forKey: "freeDashboardSkipFromAdmin")
          isfreeDashboard = value
        
        let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
        let onBoardingVC = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingVC")
        let entVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
        // commented above if condition, after change signup flow no need to fill basic info. Commneted .
        //        if _user.city.isEmpty || _user.country.isEmpty || _user.state.isEmpty {
        //
        //            let babyCategoryVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BasicDetailVC") as! BasicDetailVC
        //            nav.viewControllers = [entVC, babyCategoryVC]
        //        } else
        
        if _user.desireBabyStatus == .pending {
            
            let babyCategoryVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BabyCategoryVC") as! BabyCategoryVC
            
            nav.viewControllers = [onBoardingVC,entVC, babyCategoryVC]
        } else {
            
            let homeVC = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SlideMenuContainerVC")
            nav.viewControllers = [onBoardingVC,entVC, homeVC]
        }
        _appDelegator.window?.rootViewController = nav
    }
}

// MARK: - Authorization token
extension AppDelegate{
    
    func storeAuthorizationToken(strToken: String) {
        _userDefault.set(strToken, forKey: garbhAuthTokenKey)
        _userDefault.synchronize()
    }
    
    func getAuthorizationToken() -> String? {
        return _userDefault.value(forKey: garbhAuthTokenKey) as? String
    }
    
    func storePaymentHideShow(isPaymentShown: Bool) {
        _userDefault.set(isPaymentShown, forKey: garbhPaymentKey)
        _userDefault.synchronize()
    }
    
    func getPaymentHideShowStatus() -> Bool {
        return _userDefault.value(forKey: garbhPaymentKey) as! Bool
    }
    
    func storePushToken(strToken: String) {
        _userDefault.set(strToken, forKey: garbhNotificationKey)
    }
    
    func getPushToken() -> String? {
        return _userDefault.value(forKey: garbhNotificationKey) as? String
    }
    
    func storeAppLanguage(lang: AppLanguage) {
        _userDefault.set(lang.rawValue, forKey: garbhLanguageKey)
        _userDefault.synchronize()
        choosenLanguage(lang: lang)
        
        //if user logged in then update notifications language
        if isUserLoggedIn() {
            createReminderNotificatinsForNextSevenDays()
        }
    }
    
    func getAppLanguage() -> Int? {
        return _userDefault.value(forKey: garbhLanguageKey) as? Int
    }
    
    func choosenLanguage(lang: AppLanguage){
        
        var locPath: String
        
        if (lang == .gujarati) {
            locPath = Bundle.main.path(forResource: "gu", ofType: "lproj")!
        }
        else if(lang == .hindi){
            locPath = Bundle.main.path(forResource: "hi", ofType: "lproj")!
        }
        else {
            locPath = Bundle.main.path(forResource: "en", ofType: "lproj")!
        }
        languageBundle = Bundle(path: locPath)
    }
    
    func setLaunchOnBoarding(isOnBoardingShown: Bool) {
        _userDefault.set(isOnBoardingShown, forKey: garbhLaunchBoardingKey)
        _userDefault.synchronize()
    }
    
    func getLaunchOnBoarding() -> Bool? {
        return _userDefault.value(forKey: garbhLaunchBoardingKey) as? Bool
    }
    
    func setOnBoarding(isOnBoardingShown: Bool) {
        _userDefault.set(isOnBoardingShown, forKey: garbhBoardingKey)
        _userDefault.synchronize()
    }
    
    func getOnBoarding() -> Bool? {
        return _userDefault.value(forKey: garbhBoardingKey) as? Bool
    }
    
    func setOnBoardingLanguage(isSetBoardingLanguage: Bool) {
        _userDefault.set(isSetBoardingLanguage, forKey: garbhOnboardingLanguageKey)
        _userDefault.synchronize()
    }
    
    func getOnBoardinglanguage() -> Bool? {
        return _userDefault.value(forKey: garbhOnboardingLanguageKey) as? Bool
    }
    
    //garbhOnboardingLanguage
    
}

extension AppDelegate {
    
    func removeUserInfoAndNavToLogin() {
        UserDefaults.standard.set(false, forKey: "checkdemoUser")
        _userDefault.removeObject(forKey: garbhAuthTokenKey)
        _userDefault.synchronize()
        KPWebCall.call.removeAccessTokenFromHeader()
        UIApplication.shared.unregisterForRemoteNotifications()
        deleteUserObject()
        if let nav = window?.rootViewController as? UINavigationController{
            _ = nav.popToRootViewController(animated: true)
        }
    }
    
    func prepareForLogout(block: @escaping ((Bool, Any?) -> ())) {
        
        KPWebCall.call.logOutUser { (json, status) in
            if status == 200{
                _appDelegator.isShowPrePaidDashboard = true
                removeAllReminderNotificatins()
                self.deleteUserObject()
                block(true, json)
                self.removeUserInfoAndNavToLogin()
            }else{
                block(false, json)
            }
        }
    }
    
    func deleteUserObject() {
        _user = nil
        let users = User.fetchDataFromEntity(predicate: nil, sortDescs: nil)
        for user in users{
            _appDelegator.managedObjectContext.delete(user)
        }
        _appDelegator.saveContext()
    }
}

// MARK: - Push notification.
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func registerPushNotification(application:UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
                if (granted){
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
            //            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            //            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (granted, error) in
            //                kprint(items: "Notification Acccess: \(granted)")
            //                if granted{
            //                    DispatchQueue.main.async {
            //                        UIApplication.shared.registerForRemoteNotifications()
            //                    }
            //                }
            //            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
    }
    
    func removeAllNotification() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        //        if notification.request.content.categoryIdentifier != kLocalNotiIdentifier{
        //            setReminderNotificationHtmlDataToUserDefaults(dictUserInfo: userInfo)
        //        }
        // Change this to your preferred presentation option
        //completionHandler([])
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        if response.notification.request.content.categoryIdentifier != kLocalNotiIdentifier{
            setReminderNotificationHtmlDataToUserDefaults(dictUserInfo: userInfo)
            presentAnnouncementVC()
        }
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        setReminderNotificationHtmlDataToUserDefaults(dictUserInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        setReminderNotificationHtmlDataToUserDefaults(dictUserInfo: userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
}

extension AppDelegate {
    
    func setReminderNotificationHtmlDataToUserDefaults(dictUserInfo: [AnyHashable:Any]){
        
        print("Notification Info: \(dictUserInfo)")
        
        if let type:String = dictUserInfo[STR_Notification_Type] as? String {
            if(type == STR_Local_Reminder){
                UserDefaults.standard.set(dictUserInfo[STR_HTML_Reminder_Body], forKey: garbhUserDefaultLastAnnouncement)
            }
        }                // Change. Push notification, Add else and comment below if condition.
        else{
            UserDefaults.standard.set(dictUserInfo, forKey: garbhUserDefaultLastAnnouncement)
        }
        
        //        if (dictUserInfo[STR_Fcm_Notification_aanouncement] != nil) {
        //            UserDefaults.standard.set(dictUserInfo[STR_Fcm_Notification_aanouncement], forKey: garbhUserDefaultLastAnnouncement)
        //            UserDefaults.standard.set(dictUserInfo, forKey: garbhUserDefaultLastAnnouncement)
        //        }
    }
    
    func presentAnnouncementVC(){
        if isUserLoggedIn() {
            DispatchQueue.main.async {
                
                let reminderVC = UIStoryboard.init(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC")
                reminderVC.modalPresentationStyle = .fullScreen
                self.window?.rootViewController?.present(reminderVC, animated: true, completion: {
                })
            }
        }
    }
    
    func presentPushNotificationVC(){
        if isUserLoggedIn() {
            DispatchQueue.main.async {
                let reminderVC = UIStoryboard.init(name: "FreeAppDashboard", bundle: nil).instantiateViewController(withIdentifier: "PushNotificationVC")
                reminderVC.modalPresentationStyle = .fullScreen
                self.window?.rootViewController?.present(reminderVC, animated: true, completion: {
                })
            }
        }
    }
    
}

extension AppDelegate: MessagingDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString :\(deviceTokenString)")
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
    
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
      print("Firebase registration token: \(String(describing: fcmToken))")

      let dataDict:[String: String] = ["token": fcmToken ?? ""]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      self.storePushToken(strToken: fcmToken ?? "")
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
   
}

extension AppDelegate {
    
    func callHidePayment(){
        self.hidePaymentData(block: { (done) in
            if done {
                self.setOnBoardingLanguage(isSetBoardingLanguage: true)
            }
        })
    }
    
    func hidePaymentData(block: @escaping(Bool) -> ()) {
        //        self.showCentralSpinner()
        KPWebCall.call.getPaymentHideShow { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            //            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["success"] as? Int, status == 200 {
                if let jsonData = dict["data"] as? NSDictionary, let payStatus = jsonData["app_status"] as? String {
                    let paymentStatus = payStatus == "0" ? false : true
                    weakself.storePaymentHideShow(isPaymentShown: paymentStatus)
                    block(true)
                }
            } else {
                //                weakself.showError(data: json, view: self?.view)
                block(false)
            }
        }
    }
    
    func getLanguage() -> String{
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        if currLanguage == .hindi{
            return "hi"
        }else if currLanguage == .english{
            return "en"
        }else{
            return "gu"
        }
    }
    
    func getCountryCode() -> String{
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            var nsDictionary: NSDictionary?
            if let path = Bundle.main.path(forResource: "DiallingCodes", ofType: "plist") {
                nsDictionary = NSDictionary(contentsOfFile: path)
                return "+\(nsDictionary!.value(forKey: countryCode.lowercased())! as! String)"
            }
        }
        return "+91"
    }
    
    func prepareConfirmationPopup(message:String,completionBlock:@escaping (KAction) -> Void){
        let onBoardingView = ConfirmationPopup.instantiateView(with: (self.window?.rootViewController!.view)!)
        onBoardingView.updateUI(keyName: message, isCancelable: false, isSingleButton: true, isShowTitle: true, buttonTitle: "UPDATE")
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            completionBlock(action)
        }
    }
    
    func prepareForUpdateAvailable(){
        KPWebCall.call.updateAvailable { (isUpdateAvailable) in
            if isUpdateAvailable{
                self.prepareConfirmationPopup(message: "appUpdate") { (action) in
                    if action == .done{
                        let url = URL.init(string: "https://apps.apple.com/in/app/garbh-sanskar-guru/id1457418508")
                        if UIApplication.shared.canOpenURL(url!){
                            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                        }
                    }
                }
            }
        }
    }
    
    
    //    func detactTakenScreenShot(){
    //        let mainQueue = OperationQueue.main
    //        NotificationCenter.default.addObserver(forName: UIApplication.userDidTakeScreenshotNotification, object: nil, queue: mainQueue) { (notification) in
    //            print("Detect notification :\(notification)")
    //            self.permissionPhotos(isFirst: false)
    //        }
    //    }
    //
    //    func permissionPhotos(isFirst:Bool){
    //        PHPhotoLibrary.requestAuthorization { (status) in
    //            switch status {
    //            case .authorized:
    //                print("Good to proceed")
    //                if !isFirst{
    //                    let fetchOptions = PHFetchOptions()
    //                    self.allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
    //                    self.performOperation()
    //                }
    //            case .denied, .restricted:
    //                print("Not allowed")
    //            case .notDetermined:
    //                print("Not determined yet")
    //            }
    //        }
    //    }
    //
    //    func performOperation(){
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
    //            PHPhotoLibrary.shared().performChanges({
    //                if self.allPhotos!.count > 0{
    //                    let lastAsset = self.allPhotos?.lastObject!
    //                    var values:[String] = []
    //                    values.append(lastAsset!.localIdentifier)
    //                    let imageAssetToDelete = PHAsset.fetchAssets(withLocalIdentifiers: values, options: nil)
    //                    PHAssetChangeRequest.deleteAssets(imageAssetToDelete)
    //                }
    //            }, completionHandler: {success, error in
    //                print(success ? "Success" : error )
    //            })
    //        }
    //    }
}

/*
 com.garbhsanskar.GarbhSanskarGuru
 Apple Id : mail.jayshree@gmail.com
 pwd : H@rdik12345
 */


