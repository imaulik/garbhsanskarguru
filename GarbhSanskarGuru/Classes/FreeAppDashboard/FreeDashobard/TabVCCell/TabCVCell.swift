//
//  TabCVCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 12/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class TabCVCell: UICollectionViewCell {

    @IBOutlet weak var imgTab: UIImageView!
    @IBOutlet weak var lblTabTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
