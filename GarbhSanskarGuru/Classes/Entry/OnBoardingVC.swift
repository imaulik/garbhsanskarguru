//
//  OnBoardingVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 23/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class OnBoardingCollCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
}

class OnBoardingVC: ParentViewController {

    // Onboarding new flow outlets
    @IBOutlet var animatedViews: [BaseView]!
    
    @IBOutlet var label_titleNewapp: JPWidthLabel!
    @IBOutlet var label_titleRegisterAccount: JPWidthLabel!
    
    @IBOutlet var label_titleAlreadyMember: JPWidthLabel!
    @IBOutlet var label_titleHereLogin: JPWidthLabel!
    
    @IBOutlet var label_desc: JPWidthLabel!
        
    // Onboarding old flow outlets
//    @IBOutlet weak var layout: UICollectionViewFlowLayout!
//    @IBOutlet weak var btnLeft: UIButton!
//    @IBOutlet weak var btnRight: UIButton!
//    @IBOutlet weak var pageControl: UIPageControl!
    
//    var arrImgs: [UIImage]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 5.0) {
            self.prepareUI()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

extension OnBoardingVC {
    
    func prepareUI() {
        if _appDelegator.getOnBoardinglanguage() == nil {
            prepareOnBoarding()
        }
    }
    
    func prepareOnBoarding() {
        let obj = UIStoryboard.init(name: "Entry", bundle: nil).instantiateViewController(withIdentifier: "SelectLanguageVC") as! SelectLanguageVC
        obj.modalPresentationStyle = .overFullScreen
        obj.modalTransitionStyle = .crossDissolve
        obj.actionBlock = { (action) in
            self.hidePaymentData(block: { (done) in
//                if done {
                    obj.dismiss(animated: true, completion: nil)
                    _appDelegator.setOnBoardingLanguage(isSetBoardingLanguage: true)
                    self.prepareLanguage()
//                }
            })
        }
        self.present(obj, animated: true, completion: nil)
    }
    
    func prepareLanguage(){
//        _appDelegator.setLaunchOnBoarding(isOnBoardingShown: true)
        self.label_titleNewapp.text = _appDelegator.languageBundle?.localizedString(forKey: "onboarding_newapp", value: "", table: nil)
        self.label_titleAlreadyMember.text = _appDelegator.languageBundle?.localizedString(forKey: "onboarding_alreadyMember", value: "", table: nil)
        self.label_titleHereLogin.text = _appDelegator.languageBundle?.localizedString(forKey: "onboarding_clickLogin", value: "", table: nil)
        self.label_titleRegisterAccount.text = _appDelegator.languageBundle?.localizedString(forKey: "onboarding_clickRegister", value: "", table: nil)
        self.label_desc.text = _appDelegator.languageBundle?.localizedString(forKey: "onboarding_desc", value: "", table: nil)
    }
    
    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews[tag].transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
        }, completion: { _ in
            _appDelegator.setLaunchOnBoarding(isOnBoardingShown: true)
            if tag == 0{
                // register
                _appDelegator.popToEntryRegister()
            }else{
                // login
                _appDelegator.popToEntry()
            }
        })
    }
    
    @IBAction func btnLoginRegisterTapped(_ sender: UIButton) {
        self.animateView(with: sender.tag)
    }
    
    // comment method. Change onboarding flow.
//    func prepareOnBoarding() {
//        let onBoardingView = LanguageView.instantiateView(with: self.view, animate: false)
//        onBoardingView.actionBlock = { (action) in
//            onBoardingView.removeFromSuperview()
//            self.hidePaymentData(block: { (done) in
//                if done {
//                    self.setOnBoardingScreen()
//                }
//            })
//        }
//    }
    
//    func prepareUI() {
//           if _appDelegator.getLaunchOnBoarding() == nil {
//               prepareOnBoarding()
//           }
//           setLayout()
//       }
    
//    func setLayout(){
//        layout.scrollDirection = .horizontal
//        collectionView.isPagingEnabled = true
//        layout.itemSize = CGSize(width: _screenSize.width, height: collectionView.frame.size.height)
//        layout.minimumInteritemSpacing = 0
//        layout.minimumLineSpacing = 0
//        collectionView.collectionViewLayout = layout
//    }
    
//    func setOnBoardingScreen() {
//        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
//        var arrData:  [UIImage]
//
//        switch currLanguage {
//        case .hindi:
//            arrData = [UIImage(named: "Img1.3")!,UIImage(named: "Img2.1")!,UIImage(named: "Img3.3")!,UIImage(named: "Img4.3")!]
//        case .english:
//            arrData = [UIImage(named: "Img1.1")!,UIImage(named: "Img2.2")!,UIImage(named: "Img3.1")!,UIImage(named: "Img4.1")!]
//        case .gujarati:
//           arrData = [UIImage(named: "Img1.2")!,UIImage(named: "Img2.3")!,UIImage(named: "Img3.2")!,UIImage(named: "Img4.2")!]
//        }
//        self.arrImgs = arrData
//        self.collectionView.reloadData()
//    }
}

//extension OnBoardingVC {
//
//    @IBAction func btnSkipTapped(_ sender: UIButton) {
//        _appDelegator.setLaunchOnBoarding(isOnBoardingShown: true)
//        _appDelegator.popToEntry()
//    }
//
//    @IBAction func btnNextTapped(_ sender: UIButton) {
//        if sender.currentTitle == "Start" {
//            _appDelegator.setLaunchOnBoarding(isOnBoardingShown: true)
//            _appDelegator.popToEntry()
//        } else {
//             collectionView.scrollToItem(at: IndexPath(item: collectionView.currentPage + 1, section: 0), at: .centeredHorizontally, animated: true)
//        }
//    }
//}

//extension OnBoardingVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return arrImgs == nil ? 0 : 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return arrImgs.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell: OnBoardingCollCell
//        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OnBoardingCollCell
//        cell.imgView.image = arrImgs[indexPath.row]
//        return cell
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let currentPage = Int((scrollView.contentOffset.x + (0.5 * scrollView.frame.size.width))/scrollView.frame.width)
//        btnLeft.isHidden = currentPage == 3
//        btnRight.setTitle(currentPage == 3 ? "Start" : "Next", for: .normal)
//        self.pageControl.currentPage = currentPage
//    }
//}

extension OnBoardingVC {
    
    func hidePaymentData(block: @escaping(Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPaymentHideShow { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["success"] as? Int, status == 200 {
                if let jsonData = dict["data"] as? NSDictionary, let payStatus = jsonData["app_status"] as? String {
                    let paymentStatus = payStatus == "0" ? false : true
                    
                    _appDelegator.storePaymentHideShow(isPaymentShown: paymentStatus)
                    block(true)
                }
            } else {
                weakself.showError(data: json, view: self?.view)
                block(false)
            }
        }
    }
}
