
var checkMonth = 0

class PopupMonthVC: MyPopUpView {
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var monthView: UIView!

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkMonth = 0
        monthView.clipsToBounds = true
        monthView.layer.cornerRadius = 30
        monthView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func btnOkClick(_ sender: UIButton) {
        checkMonth = sender.tag
        if (self.pressOK != nil) {
            self.pressOK!()
        }
    }
    
  
    @IBAction func btnCancel(_ sender: UIButton) {
        
        if (self.pressCancel != nil) {
            self.pressCancel!()
        }
    }
    
}


