//
//  PaidVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 26/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PaidVC: ParentViewController {
    
    @IBOutlet weak var btnContinueToDashboard: UIButton!
    @IBOutlet weak var lblWelcomeMsg: UILabel!
    
    var language = _appDelegator.getLanguage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.async {
            if(_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)){
                self.apicallforVideoPopup(videoType: .DemoStart)
            }else{
                self.apicallforVideoPopup(videoType: .SubscribeFullCourse)
            }
        }
    }
    
    func prepareUI() {
        self.lblTitle?.text = "Hey \(_user.name)"
        
        lblWelcomeMsg.text = _appDelegator.languageBundle?.localizedString(forKey: "Hearty welcome to the majestic world of Garbh Sanskar", value: "", table: nil)
        btnContinueToDashboard.setTitle(_appDelegator.languageBundle?.localizedString(forKey: "Begin your journey with smile & prayers", value: "", table: nil), for: .normal)
        
        btnContinueToDashboard.titleLabel?.numberOfLines = 0
    }
    
    func prepareSignOutOnBoarding() {
        let onBoardingView = SignOutView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                self.prepareForLogout()
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    @IBAction func btnPaidTapped(_ sender: UIButton) {
        
        //performSegue(withIdentifier: "basicDetailSegue", sender: nil)
        
        //abhi changed:
        /*
         previously comes in this screen only case when user paid app
         but now, user will come in many cases
         when user paid app,
         user paid for demo app,
         once user paid for demo then re purchase full/paid app
         
         when user once choose country/state/city then no need to open this screen every time
         */
        navigateUser()
    }
    
    @IBAction func btnSignOutTapped(_ sender: UIButton) {
        self.prepareSignOutOnBoarding()
    }
}

extension PaidVC {
    
    func navigateUser() {
        
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        if isPaymentAllowed {
            
            if(_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()){
                _appDelegator.navigateUserForSkipPaymentOrDemoPayment()
            }
            else {
                _appDelegator.navigateUserToHome()
            }
        } else {
            _appDelegator.navigateDirectlyToHome()
        }
    }
}

extension PaidVC {
    
    func prepareforVideoPopup(objPayment : PaymentVideoModel) {
        let onBoardingView = VideoPopupView.instantiateView(with: self.view)
        onBoardingView.setupUI(btnFirst: "  SKIP  ", btnsecond: "", objModel: objPayment)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
        }
    }
    
    func apicallforVideoPopup(videoType:DemoVideoType){
        KPWebCall.call.getVideoForCompleteDemo(param: ["user_id": _user.id, "review":"Yes"]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary {
                let paymentModel = PaymentData(fromDictionary: dict as! [String : Any])
                var paidModel:[PaymentVideoModel] = []
                var typeofVideo = ""
                if videoType == .DemoStart{
                    typeofVideo = "demo_start"
                }else if videoType == .DemoComplete{
                    typeofVideo = "demo_completed"
                }else{
                    typeofVideo = "subscribe_full_course"
                }
                if paymentModel.data.count > 0{
                    if self.language == kEnglish{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == kLongEnglish.lowercased() }
                    }else if self.language == KHindi{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == KLongHindi.lowercased() }
                    }else{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == "gujrati" }
                    }
                    if paidModel.count > 0{
                        self.prepareforVideoPopup(objPayment: paidModel[0])
                    }
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
}
