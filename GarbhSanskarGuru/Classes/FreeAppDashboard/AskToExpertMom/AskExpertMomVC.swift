//
//  AskExpertMomVC.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 15/06/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class AskExpertMomVC: ParentViewController {
    
    var arrQuestionList:[QuestionList]?
    @IBOutlet weak var tblQuestions: UITableView!
    var offset = 0
    var limit = 10
    var totalCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblQuestions.register(UINib(nibName: "QuestionCell", bundle: nil), forCellReuseIdentifier: "QuestionCell")
        self.tblQuestions.register(UINib(nibName: "LoadMoreCell", bundle: nil), forCellReuseIdentifier: "LoadMoreCell")

        self.title = "Ask To Expert"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadQuestions()
    }
    
    
    @IBAction func btnAskQuePress(_ sender: Any) {
        _ = displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupAskQuestion", blockOK: {
            self.dismissPopUp()
            self.reloadQuestions()
            
        }, blockCancel: {
            self.dismissPopUp()
            
        }, objData:nil)
    }
    
    
    func reloadQuestions(){
        getQuestions()
    }
    
    @objc func btnEditPress(sender:UIButton) {
        _ = displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupEditQuestion", blockOK: {
            self.dismissPopUp()
            self.reloadQuestions()
        }, blockCancel: {
            self.dismissPopUp()
        }, objData:arrQuestionList?[sender.tag] as AnyObject)
    }
    
    @objc func btnDeletePress(sender:UIButton) {
        _ = displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupDeleteQuestion", blockOK: {
            self.dismissPopUp()
            self.reloadQuestions()
        }, blockCancel: {
            self.dismissPopUp()
        }, objData:arrQuestionList?[sender.tag] as AnyObject)
    }
    
    func getQuestions() {
        offset = 0
        limit = 10
        let param = ["user_id":_user.id,"offset":"\(offset)","limit":"\(limit)"]
        self.showCentralSpinner()
        KPWebCall.call.getQuestionList(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let questionList = QuestionModel(dictionary: dict)
                weakself.arrQuestionList = questionList.data
                weakself.offset = weakself.offset + weakself.limit
                weakself.totalCount = questionList.question_count
                weakself.tblQuestions.reloadData()
            } else {
                weakself.showError(data: json, view: self?.view)
            }
        }
    }
        
    func getLoadMoreQuestions() {
        
        let param = ["user_id":_user.id,"offset":"\(offset)","limit":"\(limit)"]
        self.showCentralSpinner()
        KPWebCall.call.getQuestionList(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let questionList = QuestionModel(dictionary: dict)
                weakself.arrQuestionList! += questionList.data ?? [QuestionList]()
                weakself.offset = weakself.offset + weakself.limit
                weakself.totalCount = questionList.question_count
                weakself.tblQuestions.reloadData()
            } else {
                weakself.showError(data: json, view: self?.view)
            }
        }
    }
}


extension AskExpertMomVC{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (offset < totalCount){
            return 2;
        }
        else{
            return 1;
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrQuestionList?.count ?? 0
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionCell
            //cell.imgQueUser
            cell.lblUserName.text =  arrQuestionList?[indexPath.row].firstname
            cell.lblQuestion.text = "Q : \(arrQuestionList?[indexPath.row].question ?? "")"
            cell.totalAnswer.text = "Total Answer : \(arrQuestionList?[indexPath.row].ans_count ?? 0)"
            
            if arrQuestionList?[indexPath.row].user_id == _user.id {
                cell.editLine.isHidden = false
                cell.editStackView.isHidden = false
            }
            else{
                cell.editLine.isHidden = true
                cell.editStackView.isHidden = true
            }
            
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(btnEditPress), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(btnDeletePress), for: .touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        }else {
            if (offset < totalCount){
                getLoadMoreQuestions()
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell", for: indexPath) as! LoadMoreCell
                return cell
            }
            else{
              let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell", for: indexPath) as! LoadMoreCell
                         return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let questionDetail = storyboard?.instantiateViewController(withIdentifier: "QuestionDetailVC") as! QuestionDetailVC
            questionDetail.dicQuestionList = arrQuestionList?[indexPath.row]
            navigationController?.pushViewController(questionDetail, animated: true)
        }
    }
    
}
