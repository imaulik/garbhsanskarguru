//
//  PrePaidDashboardVC.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 13/06/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import AVKit

class PrePaidDashboardVC: ParentViewController, SwipeViewDelegate, SwipeViewDataSource {

    @IBOutlet weak var cvCollection: UICollectionView!
    @IBOutlet weak var btnWorkshop: UIButton!
    @IBOutlet weak var btnDashboard: UIButton!
    var dicWebFirstInfo :  WebinarInfo?
    var arrWebinarList : [WebinarInfo]?
    var strLanguage = _appDelegator.getLanguage()
  
    var refresher:UIRefreshControl!

    var shouldScroll = false
    var selectedTabIndex = 0
    var friendTabChangeObserver = "ChangedTab"
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblCurrentPregDay: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet var vwSwipe: SwipeView!
    @IBOutlet weak var lblPregnancyDay: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Dashboard outlets for displayview as per api response.
    @IBOutlet weak var leadingSlider: NSLayoutConstraint!
    @IBOutlet var label_msg: JPWidthLabel!
    @IBOutlet var image_name: UIImageView!
    @IBOutlet var image: UIImageView!
    @IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet var label_image_msg: JPWidthLabel!
    @IBOutlet var label_message: JPWidthLabel!
    @IBOutlet weak var constraintHightVwSwipe: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthVwSwipe: NSLayoutConstraint!
    
    var language = _appDelegator.getLanguage()
    var isAlreadyOpenVideoPopup:Bool = false
    
    var nTodayDayCount = 0
    var dictActivityStatus = [Int: Bool]()
    var statusIndex = 0
    var isOnBoardingShown = false
    var screenType: GarbhScreenType = .todayStory
    var selectedDay:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        startAnimating()
        registerBlock()
        self.refresher = UIRefreshControl()
        self.cvCollection!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(getWebinar), for: .valueChanged)
        self.cvCollection!.addSubview(refresher)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("viewWillAppear")
        prepareForForceLogOut()
    }
    
    @IBAction func action_clickHereTapped(_ sender: KPRoundButton) {
        _appDelegator.isShowPrePaidDashboard = false
        _appDelegator.isAllowBackPreDashboard = true
        if _appDelegator.blockClickHerePreDashBoard != nil{
            _appDelegator.blockClickHerePreDashBoard!()
        }
        self.tabBarController?.selectedIndex = 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != self.cvCollection{
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                  shouldScroll = true
                  scrollSliderView(tag: Int(pageNumber))
        }
    }
}

extension PrePaidDashboardVC {
    
    func prepareUI() {
        
        self.cvCollection.register(UINib(nibName: "PagerCVCell", bundle: .main), forCellWithReuseIdentifier: "PagerCVCell")
    
        cvCollection.register(UINib(nibName: "WebinarView", bundle: nil),
                           forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                           withReuseIdentifier: "WebinarView")

        languageChange()
        
        vwSwipe.bounces = false
        vwSwipe.isPagingEnabled = false
        lblUserName.text = _user.name
        btnNext.titleLabel?.lineBreakMode  = NSLineBreakMode.byWordWrapping
        btnNext.titleLabel?.textAlignment = .center
        btnNext.setTitle("CLICK HERE\nTO START YOUR TODAY'S ACTIVITY", for: .normal)
        
        if (UIDevice.deviceType == .iPad){
            constraintHightVwSwipe.constant = 100
            constraintWidthVwSwipe.constant = (100*5)+(fRightPaddingToSwipeVwItem*5)
        }
        
        if _appDelegator.getOnBoarding() == nil
        {
            let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
            if isPaymentAllowed {
                let getFreePopUp = UserDefaults.standard.bool(forKey: "alreadyOpenPopup")
                if(_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)) && _user.currDay == 0 && !getFreePopUp{
                    prepareOnBoarding()
                }else if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                    self.prepareOnBoardingFreeDashboard()
                }
//                else if _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
//                    prepareOnBoarding()
//                }
                else {
                    self.getToday(block: { (done) in
                        if done {
                            self.setToday(with: _user.currDay)
                            self.getDashboard()
                        }
                    })
                }
            } else {
                if _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                    prepareOnBoarding()
                }else{
                    self.getToday(block: { (done) in
                        if done {
                            self.setToday(with: _user.currDay)
                            self.getDashboard()
                        }
                    })
                }
            }
        } else {
            self.getToday(block: { (done) in
                if done {
                    self.setToday(with: _user.currDay)
                    self.getDashboard()
                }
            })
        }
    }
    
    func prepareForForceLogOut(){
        self.forceLogoutApi()
    }
    
    func prepareOnBoardingFreeDashboard() {
        let onBoardingView = FreeDashBoard.instantiateView(with: self.view)
        onBoardingView.updateUI()
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .later{
                _appDelegator.navigateUserToHome()
            }else{
                self.prepareOnBoarding()
            }
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.actionBlock = { (action) in
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            self.getPregnancyDay(currentDay: currDay, block: { (done) in
                if done {
                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                    onBoardingView.removeFromSuperview()
                    DispatchQueue.main.async {
                        self.getToday(block: { (done) in
                            if done {
                                self.setToday(with: _user.currDay)
                            }
                        })
                    }
                }
            })
        }
    }
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func startAnimating(){
        btnNext.titleLabel?.alpha = 0.0
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(continuousFadeInOut), userInfo: nil, repeats: true)
    }

    @objc func continuousFadeInOut(){
        UIView.animate(withDuration: 2.0, delay: 0.0,options: [.curveEaseIn], animations: {
            self.btnNext.titleLabel?.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 2.0, delay: 0.0, options: [.curveEaseOut], animations: {
                self.btnNext.titleLabel?.alpha = 0.0
            }, completion: { _ in
            })
        })
    }
    
    func registerBlock(){
        _appDelegator.detectLanguageChangeFromPreDashboard = { () in
            self.prepareUI()
        }
    }
    
}

extension PrePaidDashboardVC {
        
    func setToday(with day: Int16) {
        self.lblDay.text = "\(day)"
        let dayDiff = 280 - day
        self.lblCurrentPregDay.text = "Your Pregnancy remaining days is \(dayDiff)"
        self.vwSwipe.reloadData()
    }
    
    func shareApp(sender: UIButton) {
         let title = _appName
         let textToShare = "Garbh Sanskar Guru App is a unique and first of it’s kind mobile application to nurture the values in unborn child. This App contains total 280 days of various activities for pregnant lady. Expecting moms can perform these activities on day to day basis which is going to help overall development of child.\n"
        let appStoreText = "Link for App Store"
        let playStoreText = "Link for Play Store"
        let myWebsite = URL(string: "https://itunes.apple.com/us/app/majestic-garbh-sanskar/id1448235489?ls=1&mt=8")
        let otherWebsite = URL(string: "http://play.google.com/store/apps/details?id=com.gs.garbhsanskarguru")
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [textToShare,appStoreText,myWebsite!,playStoreText,otherWebsite!], applicationActivities: nil)
        activityViewController.setValue(title, forKey: "Subject")
        activityViewController.popoverPresentationController?.sourceView = (sender)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)

        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func languageChange() {
        
        lblPregnancyDay.text = _appDelegator.languageBundle?.localizedString(forKey: "Pregnancy day", value: "", table: nil)
    }
}

extension PrePaidDashboardVC {
    
    @IBAction func btnShareApp(_ sender: UIButton) {
        self.shareApp(sender: sender)
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        _appDelegator.presentPushNotificationVC()
    }
    
    @IBAction func btnAnnouncementTapped(_ sender: UIButton) {
        _appDelegator.presentAnnouncementVC()
    }
    
    
    @IBAction func btnCurrentDayNavigate( _ sender: UIButton) {
        var currDay = _user.currDay
        if sender.tag == 0 {
            guard statusIndex != 3 else {return}
            self.statusIndex += 1
            currDay -= 1
            self.setToday(with: currDay)
        } else {
            guard statusIndex != 0 else {return}
            self.statusIndex -= 1
            currDay += 1
            self.setToday(with: currDay)
        }
        _user.currDay = currDay
        _appDelegator.saveContext()
    }
}

extension PrePaidDashboardVC {
    
    //MARK: SwipeView DataSource & Delegate
    
    func numberOfItems(in swipeView: SwipeView!) -> Int
    {
        if(nTodayDayCount > 0 && nTodayDayCount < 5)
        {
            return nTodayDayCount
        }
        else if(nTodayDayCount >= 5) {
            return 5
        }
        else {
            return 0;
        }
    }
    
    func swipeView(_ swipeView: SwipeView!, viewForItemAt index: Int, reusing view: UIView!) -> UIView! {
                
        let cell = UINib(nibName: "DashboardDaySelectionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DashboardDaySelectionView
        cell.frame = CGRect.init(x: 0, y: 0, width: swipeView.frame.size.height+fRightPaddingToSwipeVwItem, height: swipeView.frame.size.height)

        cell.backgroundColor = GarbhColor.white
        cell.vwDayCircle.backgroundColor = GarbhColor.gray
        cell.lblDay.textColor = GarbhColor.white

        cell.lblDay.font = UIFont.GarbhFontWith(GarbhSanskarFont.Ubuntu_Bold, size: 18.widthRatio)
        
        if(nTodayDayCount > 0 && nTodayDayCount < 5){
            
            cell.tag = index+1
        }
        else if(nTodayDayCount >= 5) {
            
//            cell.tag = (Int(nTodayDayCount)-(swipeView.numberOfItems-(index+1)))
            cell.tag = (Int(nTodayDayCount)-(swipeView.numberOfItems-(index+2)))
        }
        cell.lblDay.text = "\(cell.tag)"

        if(cell.tag == nTodayDayCount){
            cell.vwDayCircle.applyGradientEffects([GarbhColor.gradient1Pink, GarbhColor.gradient2Pink], gradientPoint: .bottomLeftTopRight)
        }
        if(cell.tag != nTodayDayCount && cell.tag == Int(_user.currDay)){
            cell.vwDayCircle.applyGradientEffects([GarbhColor.gradient1Blue, GarbhColor.gradient2Blue], gradientPoint: .bottomLeftTopRight)
        }
        if (nTodayDayCount + 1) == cell.tag {
            cell.vwDayCircle.backgroundColor = GarbhColor.whiteGray
        }
        
        cell.vwDayCircle.clipsToBounds = true
        cell.vwDayCircle.layer.cornerRadius = swipeView.frame.size.height/2

        return cell
    }
    
    func swipeView(_ swipeView: SwipeView!, didSelectItemAt index: Int) {
        let cell:DashboardDaySelectionView = swipeView.itemView(at: index) as! DashboardDaySelectionView
        if (nTodayDayCount + 1) != cell.tag {
            _user.currDay = Int16(swipeView.itemView(at: index).tag)
            _appDelegator.saveContext()
            swipeView.reloadData()
            self.getDashboard()
        }
    }
}

extension PrePaidDashboardVC {
    @objc func getWebinar(){
        getWebinars(type: "Workshop") { json,statusCode in
            self.refresher.endRefreshing()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = dict["data"]
                self.arrWebinarList =  [WebinarInfo](dictionaryArray:res as! [NSDictionary])
                if self.arrWebinarList?.count ?? 0 > 0 {
                    self.dicWebFirstInfo = self.arrWebinarList?.first
                    self.arrWebinarList?.remove(at: 0)
                }
                self.cvCollection.reloadData()
            }
        }
    }
     
    func getDashboard(){
        let param: [String: Any] = ["user_id": _user.id,"current_day":_user.currDay]
        self.showCentralSpinner()
        KPWebCall.call.getAppDashboard(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        let dashboardModel = DashboardDataModel(fromDictionary: dict["data"] as! NSDictionary)
                        weakself.prepareUIForDashboard(modelData: dashboardModel)
                    }
                }
            }
        }
    }
    
    func prepareUIForDashboard(modelData : DashboardDataModel){

        var msg_value = ""
        var image_msg_value = ""
        var url_value = "" // for Type = video
        var image_value = "" // for Type = image
        var message_value = "" // for Type = Text
        
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        switch currLanguage {
        case .hindi:
            msg_value = modelData.hindiMsg
            image_msg_value = modelData.hindiImageMsg
            url_value = modelData.hindiUrl
            image_value = modelData.hindiImage
            message_value = modelData.hindiMessage
        case .english:
            msg_value = modelData.engMsg
            image_msg_value = modelData.engImageMsg
            url_value = modelData.engUrl
            image_value = modelData.engImage
            message_value = modelData.engMessage
        case .gujarati:
            msg_value = modelData.gujMsg
            image_msg_value = modelData.gujImageMsg
            url_value = modelData.gujUrl
            image_value = modelData.gujImage
            message_value = modelData.gujMessage
        }

        label_msg.text = msg_value
        let imageNameURL = URL.init(string: modelData.imageName)
        image_name.kf.setImage(with: imageNameURL, placeholder: _placeImage)
        
        if image_msg_value == ""{
            label_image_msg.isHidden = true
        }else{
            label_image_msg.isHidden = false
            label_image_msg.attributedText = image_msg_value.htmlToAttributedString
            let range = (label_image_msg.text! as NSString).range(of: label_image_msg.text!)
            let attributedString = NSMutableAttributedString(string:label_image_msg.text!)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black , range: range)
            label_image_msg.attributedText = attributedString
        }
        
        if modelData.type == "Video"{
            playerView.isHidden = false
            image.isHidden = true
            label_message.isHidden = true
            self.setUPlayer(with: url_value.getYoutubeId())
        }else if modelData.type == "Image"{
            playerView.isHidden = true
            image.isHidden = false
            label_message.isHidden = true
            let imageURL = URL.init(string: image_value)
            image.kf.setImage(with: imageURL, placeholder: _placeImage)
        }else if modelData.type == "Text"{
            playerView.isHidden = true
            image.isHidden = true
            label_message.isHidden = false
            label_message.attributedText = message_value.htmlToAttributedString
            
            let range = (label_message.text! as NSString).range(of: label_image_msg.text!)
            let attributedString = NSMutableAttributedString(string:label_message.text!)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: GarbhColor.pink , range: range)
            label_message.attributedText = attributedString

        }
    }
    
    func getToday(block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        
                        if(!deviceId.isEqual(str: _deviceID)){
                            
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            block(false)
                        }
                        else {
                            
                            let demoDay = dict.getInt16Value(key: "demo_day")
                            let whichUser = dict.getStringValue(key: "which_user ")

                            print("\(demoDay) \(whichUser)")
                            _user.setCurrentDay(dict: dict)
                            _user.setWhichUserType(strType: whichUser)
                            _user.setChangeDay(dict: dict)
                            _user.setappstop(dict: dict)
                            _appDelegator.saveContext()
                            weakself.nTodayDayCount = Int(_user.currDay)
                            
                            if(weakself.nTodayDayCount <= 4){
                                weakself.constraintWidthVwSwipe.constant = (weakself.constraintHightVwSwipe.constant * CGFloat(weakself.nTodayDayCount)) + (fRightPaddingToSwipeVwItem * CGFloat(weakself.nTodayDayCount))
                            }
                            
                            if(_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)){
                                if(demoDay > 2){
                                    weakself.apicallforVideoPopup(videoType: .DemoComplete)
                                }
                            }
                            block(true)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func apicallforVideoPopup(videoType:DemoVideoType){
        KPWebCall.call.getVideoForCompleteDemo(param: ["user_id": _user.id, "review":"Yes"]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary {
                let paymentModel = PaymentData(fromDictionary: dict as! [String : Any])
                var paidModel:[PaymentVideoModel] = []
                var typeofVideo = ""
                if videoType == .DemoStart{
                    typeofVideo = "demo_start"
                }else if videoType == .DemoComplete{
                    typeofVideo = "demo_completed"
                }else{
                    typeofVideo = "subscribe_full_course"
                }
                if paymentModel.data.count > 0{
                    if self.language == kEnglish{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == kLongEnglish.lowercased() }
                    }else if self.language == KHindi{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == KLongHindi.lowercased() }
                    }else{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == "gujrati" }
                    }
                    if paidModel.count > 0{
                        if !self.isAlreadyOpenVideoPopup{
                            self.prepareforVideoPopup(objPayment: paidModel[0])
                        }
                    }
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func prepareforVideoPopup(objPayment : PaymentVideoModel) {
        let onBoardingView = VideoPopupView.instantiateView(with: self.view)
        onBoardingView.setupUI(btnFirst: "   OK   ", btnsecond: " Subscribe Full Course ", objModel: objPayment, isDemoComplete: true)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        self.isAlreadyOpenVideoPopup = true
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            self.isAlreadyOpenVideoPopup = false
            DispatchQueue.main.async {
                self.reappPurchase(block: { (done) in
                    _appDelegator.navigateUserToHome()
                    if done {
                        //done means k click on subscribe full course button, Should redirect to Payment action click.
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            if _appDelegator.blockFullSubscribeCourse != nil{
                                _appDelegator.blockFullSubscribeCourse!()
                            }
                        }
                    }
                })
            }
        }
    }
    
    func reappPurchase(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        
        let param: [String: Any] = ["user_id": _user.id]
        KPWebCall.call.setReappPurchase(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let userInfo = dict["data"] as? NSDictionary{
                    DispatchQueue.main.async {
                        _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
                        _user.initWith(entry: userInfo)
                        _appDelegator.saveContext()
                        block(true)
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            } else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
    
    func forceLogoutApi() {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        
                        if(!deviceId.isEqual(str: _deviceID)){
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
    
}

extension PrePaidDashboardVC{
    
    func setUPlayer(with playerID: String?) {
        guard let playeID = playerID else {return}
        playerView.setPlaybackQuality(.auto)
        playerView.setShuffle(false)
        playerView.setLoop(false)
        let playerVars = ["playsinline": 1, "autohide": 1, "showinfo": 0, "controls":1, "origin" : "http://youtube.com"] as [String : Any] // 0: will play video in fullscreen
        playerView.load(withVideoId: playeID, playerVars: playerVars)
//str.getYoutubeId()
    }
}

 
extension PrePaidDashboardVC {
  
     @IBAction func btnTabClicked(_ sender: UIButton) {
         shouldScroll = true
         scrollSliderView(tag: sender.tag)
     }
    
    
    func scrollSliderView(tag: Int) {
         //delegate?.tabDidChange(index: tag)
         if shouldScroll {
             NotificationCenter.default.post(name: NSNotification.Name(friendTabChangeObserver), object: nil, userInfo: ["index" : tag])
             shouldScroll = false

         }
         let btnArr = [btnDashboard,btnWorkshop]
        
         UIView.animate(withDuration: 0.33, animations: {
             self.leadingSlider.constant = ((btnArr.filter{$0?.tag == tag}.first!?.frame.origin.x)!)
             let scrollx = (((btnArr.filter{$0?.tag == tag}.first!?.frame.origin.x)!))
             self.scrollView!.setContentOffset(CGPoint(x: CGFloat(self.scrollView.frame.size.width*CGFloat(tag)), y: 0.0), animated: true)
         })
         self.view.layoutIfNeeded()
                
        if tag == 1{
            getWebinar()
        }
     }
    
    
   
}
 
// functions
extension PrePaidDashboardVC :UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        // Get the view for the first header
        let indexPath = IndexPath(row: 0, section: section)
        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)

        // Use this view to calculate the optimal size based on the collection view's width
        return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
                                                  withHorizontalFittingPriority: .required, // Width is fixed
                                                  verticalFittingPriority: .fittingSizeLevel) // Height can be as large as needed
    }
   
    @objc func btnPlayVideo(sender: UIButton) {
        var strVideo = ""
        if self.strLanguage == kEnglish {
            strVideo = dicWebFirstInfo?.eng_video ?? ""
        }else if self.strLanguage == kGujrati {
            strVideo = dicWebFirstInfo?.guj_video ?? ""
        }else if self.strLanguage == KHindi {
            strVideo = dicWebFirstInfo?.hindi_video ?? ""
        }
        
        if strVideo != "" {
            videoPlay(strVideo: strVideo)
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let friendHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WebinarView", for: indexPath) as! WebinarView
        if let dic = dicWebFirstInfo {
            friendHeader.lblNoData.isHidden =  true
            friendHeader.imgVideo.isHidden = false
            friendHeader.btnPlay.isHidden = false
            var strImage : String?
            if self.strLanguage == kEnglish {
                strImage = dic.eng_image
            }else if self.strLanguage == kGujrati {
                strImage = dic.guj_image
            }else if self.strLanguage == KHindi {
                strImage = dic.hindi_image
            }
           
            friendHeader.btnPlay.addTarget(self, action: #selector(btnPlayVideo), for:.touchUpInside)
            
            friendHeader.imgVideo.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
            if arrWebinarList?.count ?? 0 > 0 {
                friendHeader.noWebinarView.isHidden = true
            }else {
                friendHeader.noWebinarView.isHidden = false
            }
            
        }else{
            friendHeader.lblNoData.isHidden = false
            friendHeader.imgVideo.isHidden = true
            friendHeader.btnPlay.isHidden = true
        }
        
        return friendHeader
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2) - 5  , height: UIScreen.width / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWebinarList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"PagerCVCell",    for: indexPath) as! PagerCVCell
        let dic  = arrWebinarList?[indexPath.row]
        
        var strImage : String?
        if self.strLanguage == kEnglish {
            strImage = dic?.eng_image
        }else if self.strLanguage == kGujrati {
            strImage = dic?.guj_image
        }else if self.strLanguage == KHindi {
            strImage = dic?.hindi_image
        }
        
        cell.imgPage.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        var strVideo = ""
        let dic = arrWebinarList?[indexPath.row]
        if self.strLanguage == kEnglish {
            strVideo = dic?.eng_video ?? ""
        }else if self.strLanguage == kGujrati {
            strVideo = dic?.guj_video ?? ""
        }else if self.strLanguage == KHindi {
            strVideo = dic?.hindi_video ?? ""
        }
        
        if strVideo != "" {
            videoPlay(strVideo: strVideo)
        }
    }
}



