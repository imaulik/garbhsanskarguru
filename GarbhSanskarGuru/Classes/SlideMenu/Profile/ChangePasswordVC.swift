//
//  ChangePasswordVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 17/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct ChangePasswordData {
    var currPassword = ""
    var newPassword = ""
    var cnfrmPassword = ""
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["user_id"] = _user.id
        dict["old_password"] = currPassword
        dict["new_password"] = newPassword
        dict["re_password"] = cnfrmPassword
        return dict
    }
    
    
    func isValidData() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: currPassword){
            result.isValid = false
            result.error = kEnterPassword
            return result
        }
        
        if String.validateStringValue(str: newPassword){
            result.isValid = false
            result.error = "Enter New Password"
            return result
        }
        
        if String.validateStringValue(str: cnfrmPassword){
            result.isValid = false
            result.error = "Enter Confirm Password"
            return result
        }else if !newPassword.isEqual(str: cnfrmPassword) {
            result.isValid = false
            result.error = kPasswordNotMatch
            return result
        }
        return result
    }
}

class ChangePasswordCell: ConstrainedTableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var tfInput: UITextField!
    
    weak var parent: ChangePasswordVC!
    
    func setUpCellUI(with tag: Int, and data: ChangePasswordData) {
        tfInput.tag = tag
        tfInput.isSecureTextEntry = true
        switch tag {
        case 1:
            tfInput.placeholder = "Current Password"
            tfInput.text = data.currPassword
            break
        case 2:
            tfInput.placeholder = "New Password"
            tfInput.text = data.newPassword
            break
        case 3:
            tfInput.placeholder = "Confirm New Password"
            tfInput.text = data.cnfrmPassword
            tfInput.returnKeyType = .done
            break
        default:
            break
        }
    }
    
    @IBAction func tfEditingChange(_ textfield: UITextField) {
        if textfield.tag == 1 {
            parent.data.currPassword = textfield.text!.trimmedString()
        }else if textfield.tag == 2{
            parent.data.newPassword = textfield.text!.trimmedString()
        }else if textfield.tag == 3{
            parent.data.cnfrmPassword = textfield.text!.trimmedString()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next{
            if let index = parent.tableView.indexPath(for: self){
                if let cell = parent.getEntryCell(index.row + 1){
                    cell.tfInput.becomeFirstResponder()
                }
            }else{
                textField.resignFirstResponder()
            }
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}

class ChangePasswordVC: ParentViewController {

    var data = ChangePasswordData()
    
    var block: ((_ isHidden: Bool) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setKeyboardNoti()
    }
    
    func getEntryCell(_ row: Int) -> ChangePasswordCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? ChangePasswordCell
        return cell
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        let validate = data.isValidData()
        if validate.isValid {
            self.changePassword { (done) in
                if done {
                    self.block?(true)
                }
            }
        }else {
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        block?(true)
    }
}

extension ChangePasswordVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 || indexPath.row == 4 ? 50.widthRatio : 60.widthRatio
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChangePasswordCell
        let cellID = indexPath.row == 0 ? "titleCell" : indexPath.row == 4 ? "btnCell" : "textCell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! ChangePasswordCell
        if indexPath.row != 0 && indexPath.row != 4 {
            cell.parent = self
            cell.setUpCellUI(with: indexPath.row, and: data)
        }
        return cell
    }
    
}

extension ChangePasswordVC{
    
    func setKeyboardNoti() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardShow(_ notification: Notification) {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        tableView.contentInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
}

extension ChangePasswordVC {
    
    func changePassword(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.changePassword(param: data.paramDict()) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, let msg = dict["message"] as? String {
                if status == 200 {
                    _ = ValidationToast.showStatusMessage(message: msg)
                    block(true)
                } else {
                    _ = ValidationToast.showStatusMessage(message: msg)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
}
