//
//  FreeAppDashboardVC.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 15/06/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class DashboardList {
    var title: String?
    var description: String?
    var bottomDescription : String?
    var imagName: String?
    var category_name : String?
    
    
    convenience init(title: String, description:String,bottomDescription:String,imagName:String,categoryName:String){
        self.init()
        self.title = title
        self.description = description
        self.bottomDescription = bottomDescription
        self.imagName = imagName
        self.category_name = categoryName
    }
}

class FreeAppDashboardVC: UIViewController {
 
    var timer = Timer()
    var date =  NSDate()
    var is_Notify = 0
    var joinNow = false
    

    
    @IBOutlet weak var tblDashboard: UITableView!
    var strLanguage = _appDelegator.getLanguage()
    var arrDashboardList = [String:[DashboardList]]()
    
    var smallImage = ""
    
    var eventDetail = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tblDashboard.tableHeaderView = UIView(frame: frame)
         
        self.prepareForArrayData()

        self.tblDashboard.register(UINib(nibName: "DashboardListCell", bundle: nil), forCellReuseIdentifier: "DashboardListCell")
        
        self.tblDashboard.register(UINib(nibName: "PageViewCell", bundle: nil), forCellReuseIdentifier: "PageViewCell")
        
        var headerNib = UINib.init(nibName: "OpenURLView", bundle: Bundle.main)
        self.tblDashboard.register(headerNib, forHeaderFooterViewReuseIdentifier: "OpenURLView")
        
        headerNib = UINib.init(nibName: "BannerView", bundle: Bundle.main)
        self.tblDashboard.register(headerNib, forHeaderFooterViewReuseIdentifier: "BannerView")

    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.registerBlock()
        print("get back action")
        
        getEventDetail()
        self.navigationController?.isNavigationBarHidden = false
        self.strLanguage = _appDelegator.getLanguage()
        self.tblDashboard.reloadData()
    }
    
    func registerBlock(){
        _appDelegator.detectBackFromDashboard = { () in
            self.strLanguage = _appDelegator.getLanguage()
            self.tblDashboard.reloadData()
        }
        
        _appDelegator.blockFullSubscribeCourse = { () in
            let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PackagesVC") as! PackagesVC
            self.navigationController?.pushViewController(packagesVC, animated: true)
        }
    }
    
    func prepareForArrayData(){
        //arrDashboardList
        if !_appDelegator.getPaymentHideShowStatus(){
            // Free User
            arrDashboardList =
                [
                    "0":arraySection0, "1":arraySection1, "2":arraySection2,
                    "3":arraySection3, "4":arraySection4,   "5":arraySection5
            ]
        }
        else if ((_user.pregnancyStatus == .planning && _user.paymentStatus == .completed) || (_user.app_stop == "Pause")) || _appDelegator.isClickGarbhSanskarInfo ||  _appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser(){
            // Paid User
            arrDashboardList =
                [
                    "0":arraySection01, "1":[], "2":arraySection2, "3":arraySection3,
                    "4":arraySection4, "5":arraySection5
            ]
        }else{
            // Free User
            arrDashboardList =
                [
                    "0":arraySection0, "1":arraySection1, "2":arraySection2,
                    "3":arraySection3, "4":arraySection4,   "5":arraySection5
            ]
        }
    }
    
    func pushToVideo(obj:DashboardList){
        //GalleryViewController
        let videoVC = freeAppDashboard.instantiateViewController(withIdentifier: "VideoVC") as! VideoVC
        videoVC.dashboardSelItem = obj
        self.navigationController?.pushViewController(videoVC, animated: true)
    }
    
    func pushTOGallery(obj: DashboardList) {
        //GalleryViewController
        let gallery = freeAppDashboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
        gallery.dashboardSelItem = obj
        self.navigationController?.pushViewController(gallery, animated: true)
        
    }
    
    func pushToLibrary(obj: DashboardList) {
        //LibraryVC
        let libVC = freeAppDashboard.instantiateViewController(withIdentifier: "LibraryVC") as! LibraryVC
        libVC.dashboardSelItem = obj
        self.navigationController?.pushViewController(libVC, animated: true)
    }
    
    func gotoAskToExpert(){
        let  askWithExpertVC = freeAppDashboard.instantiateViewController(withIdentifier:"AskExpertMomVC") as! AskExpertMomVC
        self.navigationController?.pushViewController(askWithExpertVC, animated: true)
    }
    
    func gotoQuiz(){
        let quizVC = freeAppDashboard.instantiateViewController(withIdentifier:"QuizVC") as! QuizVC
        self.navigationController?.pushViewController(quizVC, animated: true)
    }
    
    func gotoChatWithUs(){
        let  chatWithusVC = freeAppDashboard.instantiateViewController(withIdentifier:"ChatWithusVC") as! ChatWithusVC
        chatWithusVC.isFromDashboard = true
        self.navigationController?.pushViewController(chatWithusVC, animated: true)
    }
    
    func gotoLiveFBGS(){
        isFromLiveFB = true
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
    
    @objc func gotoFollow() {
        let  chatWithusVC = freeAppDashboard.instantiateViewController(withIdentifier:"FollowDetailVC") as! FollowDetailVC
        self.navigationController?.pushViewController(chatWithusVC, animated: true)
    }
    
    @objc func getEventDetail() {
        self.eventDetailAPi { json,statusCode in
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
              print(dict)
                
                let data = dict["data"] as? NSArray
                if let data = data {
                    if data.count > 0 {
                        self.eventDetail = data.firstObject as! NSDictionary
                        print(self.eventDetail)
                        self.smallImage = self.eventDetail["small_image"] as? String ?? ""
                        self.is_Notify = self.eventDetail["is_notify"] as? Int ?? 0
                        
                        let startdate = self.eventDetail["start_date"] as? String ?? ""
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        self.date = dateFormatter.date(from:startdate)! as NSDate
                      
                        let calendar = Calendar.current
                        let newDate = calendar.date(byAdding: .minute, value: -15, to: self.date as Date, wrappingComponents:false)
                        self.date = newDate! as NSDate
                        
                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.offsetFrom), userInfo: nil, repeats: true)
                        self.tblDashboard.reloadData()
                    }
                }
               
            }
        }
    }
    
    
    @objc func openEveentDetailView(){
        let loginVC = UIStoryboard.init(name: "FreeAppDashboard", bundle: nil).instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
        loginVC.eventDetail = self.eventDetail
        self.navigationController?.pushViewController(loginVC, animated: false)
    }
}


extension FreeAppDashboardVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return UITableView.automaticDimension
        }
        
        else if section == arrDashboardList.count - 1  || section == 1 || section == 2 || section == 3 || section == 4{
            return UITableView.automaticDimension
        }

        return 0
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        
        if  section == 0 && self.smallImage != ""{
            return 175
        }
        else if section == arrDashboardList.count - 1  || section == 1 || section == 3 || section == 2{
            return 175
        }
        else if  section == 4 {
            return 58
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == arrDashboardList.count - 1 {
            let pageCell = tableView.dequeueReusableCell(withIdentifier: "PageViewCell") as! PageViewCell
            //            pageCell.arrPageImages = ["ic_ChatBan","ic_QuizBan","ic_AskExpertBan","ic_LiveGSBan"]
            pageCell.arrPageImages = ["ic_QuizBan","ic_AskExpertBan","ic_LiveGSBan"]
            pageCell.pagerIndex = { index in
                //                if index == 0 {
                //                    self.gotoChatWithUs()
                //                }else
                if index == 0 {
                    self.gotoQuiz()
                }else if index == 1 {
                    self.gotoAskToExpert()
                }else if index == 2 {
                    self.gotoLiveFBGS()
                }
            }
            return pageCell
        }else if section  == 0 && self.smallImage != ""{
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BannerView") as! BannerView
            headerView.hidebanner.isHidden = false
            headerView.bannerInfoView.isHidden = true
            headerView.btnFollow.isHidden = true
            headerView.imgDashPoster.isUserInteractionEnabled = true
            
            headerView.hideTimerView.isHidden = false
            
            let strDate = Date.convertStringToDate(strDate: self.eventDetail ["start_date"] as? String ?? "2021-10-18 14:00:00", dateFormate: "yyyy-MM-dd HH:mm:ss")
            let datetoStr = strDate.convertDateToString(strDateFormate: "dd-MM-yyyy")
            
            let time = self.eventDetail ["start_time"] ?? ""
            
            headerView.lblDate.text = datetoStr
            headerView.lblTime.text = "\(time)"
          
            if joinNow{
                headerView.lblStatus.isHidden = false
                headerView.lblStatus.text = "Join Now"
                headerView.lblStatus.backgroundColor = #colorLiteral(red: 0, green: 0.4991469979, blue: 0.001404395094, alpha: 1)
             
                UIView.animate(withDuration: 2.0, delay: 0.0,options: [.curveLinear, .repeat, .autoreverse], animations: {
                    headerView.lblStatus.alpha = 0.1
                }) { finished in
                    UIView.animate(withDuration: 2.0, delay: 0.0,options: [.curveLinear, .repeat, .autoreverse], animations: {
                        headerView.lblStatus.alpha = 1.0
                    })
                }
        
            }else if is_Notify == 0 {
                headerView.lblStatus.isHidden = false
                headerView.lblStatus.text = "Book Seat"
                headerView.lblStatus.backgroundColor = .systemPink
               
                UIView.animate(withDuration: 2.0, delay: 0.0,options: [.curveLinear, .repeat, .autoreverse], animations: {
                    headerView.lblStatus.alpha = 0.1
                }) { finished in
                    UIView.animate(withDuration: 2.0, delay: 0.0,options: [.curveLinear, .repeat, .autoreverse], animations: {
                        headerView.lblStatus.alpha = 1.0
                    })
                }
            }
            else if is_Notify > 0 {
                headerView.lblStatus.isHidden = false
                headerView.lblStatus.text = "Seat Booked"
                headerView.lblStatus.backgroundColor = .gray
            }
            
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openEveentDetailView))
            tap.cancelsTouchesInView = false
            headerView.imgDashPoster.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openEveentDetailView)))
            headerView.imgDashPoster.addGestureRecognizer(tap)
            
            
            headerView.imgDashPoster.sd_setImage(with:URL(string: smallImage), placeholderImage: nil)
            return headerView
        }
        else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PageViewCell") as! PageViewCell
            if !_appDelegator.getPaymentHideShowStatus(){
                cell.isGIFImage = false
                cell.arrPageImages = ["english_for IOS"]
                cell.pagerIndex = { index in
                    if index == 0{
                    }
                }
            }
            else if ((_user.pregnancyStatus == .planning && _user.paymentStatus == .completed) || (_user.app_stop == "Pause")) || _appDelegator.isClickGarbhSanskarInfo  || _appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser(){
                cell.arrPageImages = ["ic_AskExpertBan","ic_LiveGSBan","ic_QuizBan"]
                cell.pagerIndex = { index in
                    if index == 0 {
                        self.gotoAskToExpert()
                    }else if index == 1 {
                        self.gotoLiveFBGS()
                    }
                    else if index == 2 {
                        self.gotoQuiz()
                    }
                }
                cell.backgroundColor = .clear
                cell.selectionStyle = .none
            }else{
                cell.isGIFImage = true
                var imageary = [String]()
             
                // condition
                if _user.paymentStatus == .pending{
                    cell.pageview.isHidden = false
                    if self.strLanguage == KHindi{
                        imageary = ["App-Banner-Hindi","App-Banner-Hindi2","App-Banner-Hindi3","App-Banner-Hindi4","App-Banner-Hindi5","App-Banner-Hindi6"]
                    }else if self.strLanguage == kGujrati{
                        imageary = ["App-Banner-Gujarati","App-Banner-Gujarati2","App-Banner-Gujarati3","App-Banner-Gujarati4","App-Banner-Gujarati5","App-Banner-Gujarati6"]
                    }else{
                        imageary = ["App-Banner-English","App-Banner-English2","App-Banner-English3","App-Banner-English4","App-Banner-English5","App-Banner-English6"]
                    }
                    cell.pagerIndex = { index in
                        if _user.paymentStatus == .pending{
                            // call api.
                            self.viewbanner { (json, statusCode) in
                                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                                    print("success")
                                }
                            }
                            let activitiesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SubscribeContainer") as! SubscribeContainer
                            activitiesVC.isShowBackButton = false
                            self.navigationController?.pushViewController(activitiesVC, animated: true)
                        }
                    }
                }
                else {
                    if self.strLanguage == KHindi{
                        imageary = ["App-Banner-Hindi"]
                    }else if self.strLanguage == kGujrati{
                        imageary = ["App-Banner-Gujarati"]
                    }else{
                        imageary = ["App-Banner-English"]
                    }
                    cell.pageview.isHidden = true
                }

                cell.arrPageImages = imageary
                cell.backgroundColor = .clear
                cell.selectionStyle = .none
            }
            return cell
        }
        else if section  == 2 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BannerView") as! BannerView
            headerView.hidebanner.isHidden = true
            
            //            let strImageName = "ic_DashList".localized
            //            //print(strImageName)
            //            headerView.imgDashPoster.image =  UIImage(named:strIm'ageName)
            
            headerView.btnFollow.addTarget(self, action: #selector(gotoFollow), for: .touchUpInside)
            headerView.lblTitle.text = "Special Literature on GarbhSanskar".localized
            headerView.lblDescription.text = "Literature to be referred and practice for in-depth understanding".localized
            return headerView
        }else if section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PageViewCell") as! PageViewCell
            //            cell.arrPageImages = ["ic_LiveGSBan","ic_ChatBan","ic_QuizBan","ic_AskExpertBan"]
            cell.arrPageImages = ["ic_LiveGSBan","ic_QuizBan","ic_AskExpertBan"]
            cell.pagerIndex = { index in
                if index == 0 {
                    self.gotoLiveFBGS()
                }
                    //                else if index == 1 {
                    //                    self.gotoChatWithUs()
                    //                }
                else if index == 1 {
                    self.gotoQuiz()
                }else if index == 2 {
                    self.gotoAskToExpert()
                }
            }
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        } else if section  == 4 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BannerView") as! BannerView
            headerView.hidebanner.isHidden = true
            headerView.lblTitle.text = "Garbhsanskar Material".localized
            headerView.lblDescription.text = "Read, Understand and Practice".localized
            return headerView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDashboardList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let strSection = String(section)
        return arrDashboardList[strSection]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardListCell", for: indexPath) as! DashboardListCell
        let strSection = String(indexPath.section)
        let arrList = arrDashboardList[strSection]
        let obj = arrList?[indexPath.row]
        cell.lblName.text = obj?.title?.localized
        cell.lblDesc.text = obj?.description?.localized
        cell.lblBottomDesc.text = obj?.bottomDescription?.localized
        cell.imgDashList.image = UIImage(named:obj?.imagName ?? "")
        cell.selectionStyle = .none
        return cell
    }
    
//    @objc func offsetFrom() {
//
//        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
//        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: Date() as Date, to: date as Date)
//
//        if difference.second ?? 0 <= 0  && difference.minute ?? 0  <= 0   && difference.hour ?? 0 <= 0 && difference.day ?? 0 <= 0 {
//            self.timer.invalidate()
//
//            joinNow = true
//            self.tblDashboard.reloadData()
//        }
//    }
//
    @objc func offsetFrom() {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: Date() as Date, to: date as Date)
        
    
        if difference.second ?? 0 <= 0  && difference.minute ?? 0  <= 0   && difference.hour ?? 0 <= 0 && difference.day ?? 0 <= 0 {
            self.timer.invalidate()
          
            joinNow = true
            self.tblDashboard.reloadData()
     
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if (NetworkState().isInternetAvailable) {
        let strSection = String(indexPath.section)
        let arrList = arrDashboardList[strSection]
        let obj = arrList?[indexPath.row]
        
        if obj?.category_name == DashBoardCategoryList.GS9Months.rawValue {
            _ = displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupMonthVC", blockOK: {
                self.dismissPopUp()
                self.pushTOGallery(obj: obj ?? DashboardList())
            }, blockCancel: {
                checkMonth = 0
                self.dismissPopUp()
            }, objData:nil)
        }else if obj?.category_name == DashBoardCategoryList.GSGame.rawValue{
            _ = KPValidationToast.shared.showToastOnStatusBar(message: "comming soon")
        }
        else if obj?.category_name == DashBoardCategoryList.GSArticle.rawValue{
            let articleVC = freeAppDashboard.instantiateViewController(withIdentifier: "ArticleVC") as! ArticleVC
            self.navigationController?.pushViewController(articleVC, animated: true)
        }
        else if obj?.category_name == DashBoardCategoryList.GSYoga.rawValue || obj?.category_name == DashBoardCategoryList.GSGarbhSavand.rawValue {
            pushToVideo(obj: obj ?? DashboardList())
        }
        else if obj?.category_name == DashBoardCategoryList.GSLibrary.rawValue || obj?.category_name == DashBoardCategoryList.GSTips.rawValue  || obj?.category_name == DashBoardCategoryList.GSVideo.rawValue  || obj?.category_name == DashBoardCategoryList.GSAudio.rawValue  {
            pushToLibrary(obj: obj ?? DashboardList())
        }
            
        else {
            pushTOGallery(obj: obj ?? DashboardList())
        }
        //        }else {
        //            NoInternet()
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if  section ==  arrDashboardList.count - 1 {
            return 70
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        if  section ==  arrDashboardList.count - 1 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "OpenURLView") as! OpenURLView

            return headerView
        }
        return UIView()
    }
}


//var arrDashboardList = [
//    "0" :[
//        DashboardList(title: "What is GarbhSanskar?", description: "To the point information on GarbhSanskar", bottomDescription: "90 Years Work Done in 9 Months", imagName: "ic_WGS",categoryName:DashBoardCategoryList.GS.rawValue),
//        DashboardList(title: "GarbhSanskar Benefits", description: "Why one should practice GarbhSanskar?", bottomDescription: "Seeds Can be Rectified, Not The Tree", imagName: "ic_GSBenefit",categoryName:DashBoardCategoryList.GSBenefit.rawValue),
//        DashboardList(title: "Actual understanding of GarbhSanskar", description: "Complete Process of GarbhSanskar", bottomDescription: "Journey From Pre-Planning to Delivery", imagName: "ic_GSUnderStanding",categoryName:DashBoardCategoryList.GSunderStanding.rawValue)
//    ],
//    "1":[
//        DashboardList(title: "Receiving Desired Baby Through GarbhSanskar", description: "Inculcating the best qualities in unborn", bottomDescription: "Mother’s Womb is a University", imagName: "ic_DesiredBaby",categoryName:DashBoardCategoryList.GSDesired.rawValue),
//        DashboardList(title: "Garbhyatra", description: "9 month’s journey of pregnancy", bottomDescription: "Month Wise Guideline", imagName: "ic_garbhyatra",categoryName:DashBoardCategoryList.GS9Months.rawValue),
//        DashboardList(title: "How to Practice GarbhSanskar?", description: "Complete guideline to practice GarbhSanskar", bottomDescription: "Activities for Overall Development of Unborn", imagName: "ic_practice_garbh",categoryName:DashBoardCategoryList.PracticeGarbhsanskar.rawValue)
//    ],
//    "2":[
//        DashboardList(title: "GarbhSamvad", description: "Talk of Parent's with unborn", bottomDescription: "Yes, Baby Can Listen You", imagName: "ic_garbhsamvad",categoryName:DashBoardCategoryList.GSGarbhSavand.rawValue),
//        DashboardList(title: "Yoga and Meditation", description: "Pregnancy yoga and meditation", bottomDescription: "Guidance by Experts", imagName: "ic_GSYoga",categoryName:DashBoardCategoryList.GSYoga.rawValue),
//        DashboardList(title: "Diet", description: "What to Eat in Pregnancy", bottomDescription: "Ayurveda based guidance", imagName: "ic_diet",categoryName:DashBoardCategoryList.GSDiet.rawValue),
//        DashboardList(title:"5 Sense Activities", description: "For the development of 5 senses", bottomDescription: "Activities Can Be Perform by Own", imagName: "ic_fiveSense",categoryName:DashBoardCategoryList.GS5SenseActivity.rawValue)
//    ],
//    "3":[
//        DashboardList(title: "Colour, Raga and Ornaments", description: "Which colour and ornaments are appropriate?", bottomDescription: "Month wise Guide", imagName: "ic_garbh_sharir",categoryName:DashBoardCategoryList.ColorRagaOrnament.rawValue),
//        DashboardList(title: "Garbh Sharir Bhav", description: "6 elements of foetus creation", bottomDescription: "Complete Understanding", imagName: "ic_garbh_sharir",categoryName:DashBoardCategoryList.GarbhSharirBhav.rawValue),
//        DashboardList(title: "Tips", description: "Pregnancy Tips", bottomDescription:"Beauty, Baby Care, General and GS Tips", imagName: "ic_tips",categoryName:DashBoardCategoryList.GSTips.rawValue),
//        DashboardList(title: "Video", description: "Pregnancy Videos", bottomDescription: "Motivational, Yoga, Spiritual, Facts etc...", imagName: "ic_video",categoryName:DashBoardCategoryList.GSVideo.rawValue),
//        DashboardList(title: "Audio", description:"Pregnancy Music", bottomDescription: "Mantras, Meditation, Songs, Prayers etc...", imagName: "ic_audio",categoryName:DashBoardCategoryList.GSAudio.rawValue)
//    ],
//    "4":[
//        DashboardList(title: "Library", description: "Must read books during pregnancy", bottomDescription: "Garbhsanskar Books, reference books", imagName: "ic_library",categoryName:DashBoardCategoryList.GSLibrary.rawValue),
//        DashboardList(title: "Calendar", description: "Calendar with quote", bottomDescription: "To share with family and friends", imagName: "ic_calender",categoryName:DashBoardCategoryList.GSCalender.rawValue),
//        DashboardList(title: "Games", description: "Brain Activity", bottomDescription: "Games to develop IQ", imagName: "ic_games",categoryName:DashBoardCategoryList.GSGame.rawValue),
//        DashboardList(title: "Daily Quote", description: "Motivational Quotes", bottomDescription: "Get one quote daily", imagName: "ic_qoutes",categoryName:DashBoardCategoryList.DailyQuote.rawValue)
//    ],
//    "5":[
//        DashboardList(title: "Article", description: "Pregnancy and GarbhSanskar Articles", bottomDescription: "Inspirational and informative articles", imagName: "ic_article_dashboard",categoryName:DashBoardCategoryList.GSArticle.rawValue),
//        DashboardList(title: "Posters", description: "Poster to visualize", bottomDescription: "To inculcate qualities from great personalities", imagName: "ic_poster",categoryName:DashBoardCategoryList.GSposters.rawValue),
//        DashboardList(title: "Baby image", description: "Attractive baby images", bottomDescription: "Visualization Activity", imagName: "ic_BabyImage",categoryName:DashBoardCategoryList.GSbabyimage.rawValue),
//        DashboardList(title: "Shloka", description: "Shlokas from Scriptures", bottomDescription: "Explore the meaning of life", imagName: "shloksakhi",categoryName:DashBoardCategoryList.GSShloka.rawValue),
//        DashboardList(title: "Halarda", description: "Lullabies for your new-born", bottomDescription: "Sleep aid for infants", imagName: "ic_halrada",categoryName:DashBoardCategoryList.GSHalarda.rawValue)
//    ]
//]

var arraySection01 = [
    DashboardList(title: "What is GarbhSanskar?", description: "To the point information on GarbhSanskar", bottomDescription: "90 Years Work Done in 9 Months", imagName: "ic_WGS",categoryName:DashBoardCategoryList.GS.rawValue),
    DashboardList(title: "GarbhSanskar Benefits", description: "Why one should practice GarbhSanskar?", bottomDescription: "Seeds Can be Rectified, Not The Tree", imagName: "ic_GSBenefit",categoryName:DashBoardCategoryList.GSBenefit.rawValue),
    DashboardList(title: "Actual understanding of GarbhSanskar", description: "Complete Process of GarbhSanskar", bottomDescription: "Journey From Pre-Planning to Delivery", imagName: "ic_GSUnderStanding",categoryName:DashBoardCategoryList.GSunderStanding.rawValue),
    DashboardList(title: "Receiving Desired Baby Through GarbhSanskar", description: "Inculcating the best qualities in unborn", bottomDescription: "Mother’s Womb is a University", imagName: "ic_DesiredBaby",categoryName:DashBoardCategoryList.GSDesired.rawValue),
    DashboardList(title: "Garbhyatra", description: "9 month’s journey of pregnancy", bottomDescription: "Month Wise Guideline", imagName: "ic_garbhyatra",categoryName:DashBoardCategoryList.GS9Months.rawValue),
    DashboardList(title: "How to Practice GarbhSanskar?", description: "Complete guideline to practice GarbhSanskar", bottomDescription: "Activities for Overall Development of Unborn", imagName: "ic_practice_garbh",categoryName:DashBoardCategoryList.PracticeGarbhsanskar.rawValue)
]

var arraySection0 = [
    DashboardList(title: "What is GarbhSanskar?", description: "To the point information on GarbhSanskar", bottomDescription: "90 Years Work Done in 9 Months", imagName: "ic_WGS",categoryName:DashBoardCategoryList.GS.rawValue),
    DashboardList(title: "GarbhSanskar Benefits", description: "Why one should practice GarbhSanskar?", bottomDescription: "Seeds Can be Rectified, Not The Tree", imagName: "ic_GSBenefit",categoryName:DashBoardCategoryList.GSBenefit.rawValue),
    DashboardList(title: "Actual understanding of GarbhSanskar", description: "Complete Process of GarbhSanskar", bottomDescription: "Journey From Pre-Planning to Delivery", imagName: "ic_GSUnderStanding",categoryName:DashBoardCategoryList.GSunderStanding.rawValue)
]

var arraySection1 = [
    DashboardList(title: "Receiving Desired Baby Through GarbhSanskar", description: "Inculcating the best qualities in unborn", bottomDescription: "Mother’s Womb is a University", imagName: "ic_DesiredBaby",categoryName:DashBoardCategoryList.GSDesired.rawValue),
    DashboardList(title: "Garbhyatra", description: "9 month’s journey of pregnancy", bottomDescription: "Month Wise Guideline", imagName: "ic_garbhyatra",categoryName:DashBoardCategoryList.GS9Months.rawValue),
    DashboardList(title: "How to Practice GarbhSanskar?", description: "Complete guideline to practice GarbhSanskar", bottomDescription: "Activities for Overall Development of Unborn", imagName: "ic_practice_garbh",categoryName:DashBoardCategoryList.PracticeGarbhsanskar.rawValue)
]

var arraySection2 = [
    DashboardList(title: "GarbhSamvad", description: "Talk of Parent's with unborn", bottomDescription: "Yes, Baby Can Listen You", imagName: "ic_garbhsamvad",categoryName:DashBoardCategoryList.GSGarbhSavand.rawValue),
    DashboardList(title: "Yoga and Meditation", description: "Pregnancy yoga and meditation", bottomDescription: "Guidance by Experts", imagName: "ic_GSYoga",categoryName:DashBoardCategoryList.GSYoga.rawValue),
    DashboardList(title: "Diet", description: "What to Eat in Pregnancy", bottomDescription: "Ayurveda based guidance", imagName: "ic_diet",categoryName:DashBoardCategoryList.GSDiet.rawValue),
    DashboardList(title:"5 Sense Activities", description: "For the development of 5 senses", bottomDescription: "Activities Can Be Perform by Own", imagName: "ic_fiveSense",categoryName:DashBoardCategoryList.GS5SenseActivity.rawValue)
]

var arraySection3 = [
    DashboardList(title: "Colour, Raga and Ornaments", description: "Which colour and ornaments are appropriate?", bottomDescription: "Month wise Guide", imagName: "ic_garbh_sharir",categoryName:DashBoardCategoryList.ColorRagaOrnament.rawValue),
    DashboardList(title: "Garbh Sharir Bhav", description: "6 elements of foetus creation", bottomDescription: "Complete Understanding", imagName: "ic_garbh_sharir",categoryName:DashBoardCategoryList.GarbhSharirBhav.rawValue),
    DashboardList(title: "Tips", description: "Pregnancy Tips", bottomDescription:"Beauty, Baby Care, General and GS Tips", imagName: "ic_tips",categoryName:DashBoardCategoryList.GSTips.rawValue),
    DashboardList(title: "Video", description: "Pregnancy Videos", bottomDescription: "Motivational, Yoga, Spiritual, Facts etc...", imagName: "ic_video",categoryName:DashBoardCategoryList.GSVideo.rawValue),
    DashboardList(title: "Audio", description:"Pregnancy Music", bottomDescription: "Mantras, Meditation, Songs, Prayers etc...", imagName: "ic_audio",categoryName:DashBoardCategoryList.GSAudio.rawValue)
]

var arraySection4 = [
    DashboardList(title: "Library", description: "Must read books during pregnancy", bottomDescription: "Garbhsanskar Books, reference books", imagName: "ic_library",categoryName:DashBoardCategoryList.GSLibrary.rawValue),
    DashboardList(title: "Calendar", description: "Calendar with quote", bottomDescription: "To share with family and friends", imagName: "ic_calender",categoryName:DashBoardCategoryList.GSCalender.rawValue),
    DashboardList(title: "Games", description: "Brain Activity", bottomDescription: "Games to develop IQ", imagName: "ic_games",categoryName:DashBoardCategoryList.GSGame.rawValue),
    DashboardList(title: "Daily Quote", description: "Motivational Quotes", bottomDescription: "Get one quote daily", imagName: "ic_qoutes",categoryName:DashBoardCategoryList.DailyQuote.rawValue)
]

var arraySection5 = [
    DashboardList(title: "Article", description: "Pregnancy and GarbhSanskar Articles", bottomDescription: "Inspirational and informative articles", imagName: "ic_article_dashboard",categoryName:DashBoardCategoryList.GSArticle.rawValue),
    DashboardList(title: "Posters", description: "Poster to visualize", bottomDescription: "To inculcate qualities from great personalities", imagName: "ic_poster",categoryName:DashBoardCategoryList.GSposters.rawValue),
    DashboardList(title: "Baby image", description: "Attractive baby images", bottomDescription: "Visualization Activity", imagName: "ic_BabyImage",categoryName:DashBoardCategoryList.GSbabyimage.rawValue),
    DashboardList(title: "Shloka", description: "Shlokas from Scriptures", bottomDescription: "Explore the meaning of life", imagName: "shloksakhi",categoryName:DashBoardCategoryList.GSShloka.rawValue),
    DashboardList(title: "Halarda", description: "Lullabies for your new-born", bottomDescription: "Sleep aid for infants", imagName: "ic_halrada",categoryName:DashBoardCategoryList.GSHalarda.rawValue)
]
