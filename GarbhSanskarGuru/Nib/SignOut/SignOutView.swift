//
//  SignOutView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 06/02/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SignOutView: ConstrainedView {

    var actionBlock: (KAction)->Void = {_ in}
    @IBOutlet weak var baseView: UIView!

    class func instantiateView(with view: UIView) -> SignOutView {
        let objView = UINib(nibName: "SignOutView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SignOutView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
}

extension SignOutView {
    
    @IBAction func btnYesTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func btnLaterTapped(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
}
