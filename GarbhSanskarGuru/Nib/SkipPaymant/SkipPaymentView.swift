//
//  SkipPaymentView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 25/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SkipPaymentView: ConstrainedView {

    var actionBlock: (KAction)->Void = {_ in}
    
    @IBOutlet weak var baseView: UIView!
    
    class func instantiateView(with view: UIView) -> SkipPaymentView {
        let objView = UINib(nibName: "SkipPaymentView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SkipPaymentView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
}

extension SkipPaymentView {
    
    @IBAction func btnYesTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func btnLaterTapped(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
}

