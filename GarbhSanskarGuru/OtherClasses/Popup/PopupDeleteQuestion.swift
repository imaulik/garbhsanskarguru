import SearchTextField


class PopupDeleteQuestion: MyPopUpView {
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    
    var myQuestion: QuestionList?
    var myAnswer: AnswerList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let question = objData as? QuestionList{
            myQuestion = question
            lblQuestion.text = "Are you sure want to delete this question?"
        }
        else {
            myAnswer = objData as? AnswerList
            lblQuestion.text = "Are you sure want to delete this answer?"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnOkClick(_ sender: UIButton) {
        
        if let _ = objData as? QuestionList{
            let param = ["user_id" :_user.id, "question_id":myQuestion?.id ?? "0"]
            
            self.showCentralSpinner()
            KPWebCall.call.deleteQuestion(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    if (weakself.pressOK != nil) {
                        weakself.pressOK!()
                    }
                }
            }
        }
        else {
            let param = ["user_id" : _user.id, "answer_id":myAnswer?.id ?? "0"]
            
            self.showCentralSpinner()
            KPWebCall.call.deleteAnswer(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    if (weakself.pressOK != nil) {
                        weakself.pressOK!()
                    }
                }
            }
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        
        if (self.pressCancel != nil) {
            self.pressCancel!()
        }
    }
    
}


