//
//  PushListCell.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 04/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PushListCell: UITableViewCell {
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubDetail: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

