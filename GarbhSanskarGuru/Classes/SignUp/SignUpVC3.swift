//
//  SignUpVC3.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 27/04/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SignUpVC3: ParentViewController {
    
    var arrDetails: [BasicDetailCell] = [.spaceCell, .textCell, .textCell, .textCell, .spaceCell]
    var basicInfoData = BasicInfoData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //prepareUI()
    }
}

extension SignUpVC3 {
    func prepareUI(with status: PregnancyStatus) {
        setKeyboardNotifications()
        tableView.reloadData()
    }
    
    func getEntryCell(_ row: Int) -> BasicInfoCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? BasicInfoCell
        return cell
    }
}

extension SignUpVC3 {
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        let validate = basicInfoData.isValidData()
        if validate.isValid {
            self.updateProfile { (done) in
                if done {
                    //self.performSegue(withIdentifier: "desiredSegue", sender: nil)
                    /*let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BabyCategoryVC") as! BabyCategoryVC
                    self.navigationController?.pushViewController(packagesVC, animated: true)*/
                    _appDelegator.navigateDirectlyToHome()
                }
            }
        }else {
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
}

extension SignUpVC3 {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDetails.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrDetails[indexPath.row].cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BasicInfoCell
        let info = arrDetails[indexPath.row]
        cell = tableView.dequeueReusableCell(withIdentifier: info.rawValue, for: indexPath) as! BasicInfoCell
        cell.isFromSignUp = true
        cell.signupParentVC = self
        cell.screenType = .basicInfoVC
        cell.setUpBasicUI(tag: indexPath.row, data: info)
        return cell
    }
}

extension SignUpVC3 {
    
    func updateProfile(block: @escaping (Bool) -> ()) {
        let param = basicInfoData.paramDict()
        self.showCentralSpinner()
        KPWebCall.call.updateUserProfile(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    DispatchQueue.main.async {
                        _user.setRegionData(dict: data)
                        _appDelegator.saveContext()
                    }
                    block(true)
                }else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}
