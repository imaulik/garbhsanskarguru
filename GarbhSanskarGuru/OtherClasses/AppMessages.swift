

let _appName        = "GarbhSanskarGuru"

// MARK: Permission 
let kCameraAccessTitle   = "no camera access"
let kCameraAccessMsg     = "please go to settings and switch on your Camera. settings -> \(_appName) -> switch on camera"
let kPhotosAccessTitle   = "no photos access"
let kPhotosAccessMsg     = "please go to settings and switch on your photos. settings -> \(_appName) -> switch on photos"
let kCameraNotAvailable  = "this device does not have a camera"
let kUnknowedErrorFound  = "an unknown error occurred"

// MARK: Web Operation
let kInternetDown  = "no internet connection"
let kConnecting    = "connecting..."
let kHostDown      = "your host seems to be down"
let kTimeOut       = "the request timed out"
let kInternalError      = "internal error"
let kTokenExpire        = "token expire please login again"

let kTaskCompleted      = "Your today task has been completed"

// MARK: Login Registration
let kInvalidEmail           = "Enter valid email address"
let kInvalidName            = "Enter a valid name"
let kInvalidPassword        = "Enter valid password"
let kMobileInvalid          = "Enter valid mobile number"
let kVerificationCodeSent   = "verification code sent"
let kUsernameSmaller        = "username must contain at lease three characters"
let kUsernameBigger         = "15 characters max"
let kPasswordSmall          = "8 characters min"
let kEnterSubject           = "Enter subject"
let kEnterMessage           = "Enter message"
let kEnterWhoAmI            = "please select your profession"
let kEnterUsername          = "enter username"
let kEnterName              = "Enter your name"
let kSelectPlaningStatus    = "Select your current stage"
let kEnterCompanyName       = "Enter your company name"
let kPasswordBig            = "15 characters max"
let kEnterPassword          = "please enter a password"
let kEnterCity              = "please select city"
let kEnterCountryCode       = "please enter country code"
let kPasswordNotMatch       = "password not same"
let kAppId                  = "1457418508"
let kShareAppURL            = "https://itunes.apple.com/us/app/\(_appName)/\(kAppId)?ls=1&mt=8"
let kFromDay                = "please enter From day"
let kToDay                  = "please enter TO day"
let kValidDayReport         = "please enter different from day"

// MARK: Create Party
let kPartyNameBigger        = "25 characters max"
let kPartyDescBigger        = "190 characters max"
let kErrorInLocation        = "error in location fetch"

// MARK: Profile
let kEnterEmail             = "Enter your email"
let kEnterMobile            = "Enter mobile number"

//MARK : Update Profile 
let kpartyStatus    = "150 characters max"

var websURL = "http://webserviceparentingapp.garbhsanskar.co/"
var baseURL = "\(websURL)garbhsanskarapp/api/"
let baseURL2 = "http://webserviceforapp.garbhsanskar.co/gswebserviceV2/"
let baseURI = "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/"

let fbURL = "https://www.facebook.com/MAJESTICGARBHSANSKAR"
let ytURL = "https://www.youtube.com/channel/UCrQ2-hzImdJ4A3BD0yIYm-A?sub_confirmation=1"
let instaURL = "https://www.instagram.com/mgarbhsanskar"
let website = "http://www.garbhsanskar.co/"
let licenceAgreement  = "https://www.garbhsanskar.co/LICENSEDAGREEMENT.pdf"
let whatsapp = "https://api.whatsapp.com/send?phone=919727006001"
let appProURL = "https://apps.apple.com/in/app/garbh-sanskar-guru/id1457418508"

var isFromLiveFB = false

