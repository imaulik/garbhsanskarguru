//
//  GarbhSanskarInfoVC.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 08/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct ModelGSInfo {
    var id = 0
    var image = ""
}

class GarbhSanskarInfoCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
}

class GarbhSanskarInfoVC: ParentViewController {

    var arrData: [ModelGSInfo]!
    var isForDivineMystery:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "introDetailSegue"{
            let destVC = segue.destination as! InfoContentVC
            destVC.objRahasya = (sender as? ModelRahasyaTitle)
        }
        else if segue.identifier == "segueInfoContentStatic"{
            let destVC = segue.destination as! InfoContentStaticVC
            destVC.nTypeForStaticData = kGSInfo_HowToFollow
        }
    }
}

extension GarbhSanskarInfoVC {
    
    func prepareUI() {
        
        if(isForDivineMystery){
            lblTitle?.text = _appDelegator.languageBundle?.localizedString(forKey: "Garbhsanskar – A Divine Mystery", value: "", table: nil)
        }
        else {
            lblTitle?.text = "Garbh Sanskar Info"
        }
        
        initModel()
    }
}

extension GarbhSanskarInfoVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,              UICollectionViewDelegate  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrData == nil ? 0 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GarbhSanskarInfoCell
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GarbhSanskarInfoCell
        cell.imgView.image = UIImage(named: arrData[indexPath.row].image)
        
        if(UIDevice.deviceType == .iPad) {
            cell.imgView.layer.cornerRadius = 50;
        }
        else {
            cell.imgView.layer.cornerRadius = 20;
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
            if (self.arrData[indexPath.row].id == kGSInfo_Garbhyatra) {
                self.performSegue(withIdentifier: "segueJourney", sender: nil)
            }
            else if(self.arrData[indexPath.row].id == kGSInfo_HowToFollow){
                self.performSegue(withIdentifier: "segueInfoContentStatic", sender: nil)
            }
            else {
            
                let objRahasya: ModelRahasyaTitle = ModelRahasyaTitle.init(dict: ["cat_id": "\(self.arrData[indexPath.row].id)"])
                self.performSegue(withIdentifier: "introDetailSegue", sender: objRahasya)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collViewWidth = collectionView.frame.size.width - 45
        return CGSize(width: collViewWidth/2, height: ((collViewWidth/2)*2.3)/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}

extension GarbhSanskarInfoVC {
    
    func initModel() {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        
        switch currLanguage {
        case .hindi:
            arrData = [ModelGSInfo(id: kGSInfo_WhatIsGarbhsanskar, image: "GS_WhatIs_h"), ModelGSInfo(id: kGSInfo_GarbhsanskarBenefits, image: "GS_Benefits_h"), ModelGSInfo(id: kGSInfo_GarbhsanskarHowTo, image: "GS_HowTo_h"), ModelGSInfo(id: kGSInfo_RecevingDesiredBaby, image: "RecevingDesiredBaby_h"), ModelGSInfo(id: kGSInfo_Garbhyatra, image: "GarbhYatra_h"), ModelGSInfo(id: kGSInfo_HowToFollow, image: "GS_HowToFollow_h")]
        case .english:
            arrData = [ModelGSInfo(id: kGSInfo_WhatIsGarbhsanskar, image: "GS_WhatIs_e"), ModelGSInfo(id: kGSInfo_GarbhsanskarBenefits, image: "GS_Benefits_e"), ModelGSInfo(id: kGSInfo_GarbhsanskarHowTo, image: "GS_HowTo_e"), ModelGSInfo(id: kGSInfo_RecevingDesiredBaby, image: "RecevingDesiredBaby_e"), ModelGSInfo(id: kGSInfo_Garbhyatra, image: "GarbhYatra_e"), ModelGSInfo(id: kGSInfo_HowToFollow, image: "GS_HowToFollow_e")]
        case .gujarati:
            arrData = [ModelGSInfo(id: kGSInfo_WhatIsGarbhsanskar, image: "GS_WhatIs_g"), ModelGSInfo(id: kGSInfo_GarbhsanskarBenefits, image: "GS_Benefits_g"), ModelGSInfo(id: kGSInfo_GarbhsanskarHowTo, image: "GS_HowTo_g"), ModelGSInfo(id: kGSInfo_RecevingDesiredBaby, image: "RecevingDesiredBaby_g"), ModelGSInfo(id: kGSInfo_Garbhyatra, image: "GarbhYatra_g"), ModelGSInfo(id: kGSInfo_HowToFollow, image: "GS_HowToFollow_g")]
        }
        collectionView.reloadData()
    }
}
