//
//  GiftAppVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct GiftInfoData {
    var fullName = ""
    var email = ""
    var people = ""
    var contactNumber = ""
    var referral = ""
    
    func isValidData() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: fullName){
            result.isValid = false
            result.error = kEnterName
            return result
        }
        
        if String.validateStringValue(str: email) {
            result.isValid = false
            result.error = kEnterEmail
            return result
        }else if !email.isValidEmailAddress() {
            result.isValid = false
            result.error = kInvalidEmail
            return result
        }
        
       if String.validateStringValue(str: contactNumber){
            result.isValid = false
            result.error = kEnterMobile
            return result
       }
//       else if !contactNumber.replacingOccurrences(of: "+", with: "").trimmingCharacters(in:.whitespacesAndNewlines).validateContact(withCode: true) {
//            result.isValid = false
//            result.error = kMobileInvalid
//            return result
//        }
        
        if String.validateStringValue(str: people){
            result.isValid = false
            result.error = "Enter the number of people"
            return result
        }
        
        return result
    }
    
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["name"] = fullName
        dict["email"] = email
        dict["number_of_person"] = people
        dict["coupon_code"] = referral
        dict["phone_number"] = contactNumber.replacingOccurrences(of: "+", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        return dict
    }
}

class GiftAppVC: ParentViewController {
    
    @IBOutlet weak var accessoryView: UIView!

    var arrDetails: [BasicDetailCell] = [.dataCell, .textCell, .textCell, .phonecell, .textCell, .textCell, .spaceCell]
    var basicInfoData = GiftInfoData()
    
    override func viewDidLoad() {
            super.viewDidLoad()
        prepareUI()
    }
}

extension GiftAppVC {
    
    func prepareUI() {
        setKeyboardNotifications()
        accessoryView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: 44.heightRatio)
    }
    
    func getEntryCell(_ row: Int) -> BasicInfoCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? BasicInfoCell
        return cell
    }
}

extension GiftAppVC {
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        self.view.endEditing(true)
    }
}

extension GiftAppVC {
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        let valid = basicInfoData.isValidData()
        if valid.isValid {
            self.postGiftDetails { (done) in
                if done {
                    self.performSegue(withIdentifier: "disclaimerSegue", sender: nil)
                }
            }
        }else {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: valid.error)
        }
    }
}

extension GiftAppVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDetails.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return UITableView.automaticDimension
        }else{
            return arrDetails[indexPath.row].cellHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BasicInfoCell
        let info = arrDetails[indexPath.row]
        cell = tableView.dequeueReusableCell(withIdentifier: info.rawValue, for: indexPath) as! BasicInfoCell
        cell.parentGift = self
        cell.screenType = .giftVC
        cell.setUpBasicUI(tag: indexPath.row, data: info)
        return cell
    }
}


extension GiftAppVC {
    
    func postGiftDetails(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.postGiftDetails(param: basicInfoData.paramDict()) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let userInfo = dict["data"] as? NSDictionary {
                    _giftUser = GiftUser(dict: userInfo)
                    block(true)
                }else if status == 401, let msg = dict["message"] as? NSDictionary {
                    block(false)
                    self.showError(data: msg, view: self.view)
                }else {
                    block(false)
                    self.showError(data: json, view: self.view)
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
}
