//
//  InfoContentStaticVC.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 08/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class InfoContentStaticVC: ParentViewController {

    var arrData: [String]!
    var nTypeForStaticData: Int = 0
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        initModel()
    }
}

extension InfoContentStaticVC {
    
    func prepareUI() {
        
        if(nTypeForStaticData == kGSInfo_HowToFollow) {
            self.lblTitle?.text = "How to follow Garbhsanskar?"
        }
        else if(nTypeForStaticData == kBasicInfo_AppBenefits) {
            self.lblTitle?.text = _appDelegator.languageBundle?.localizedString(forKey: "Benefits of the App", value: "", table: nil)
        }
        else {
            self.lblTitle?.text = ""
        }
        
        btnLeft.isHidden = true
        setLayout()
    }
    
    func setLayout(){
    
        layout.scrollDirection = .horizontal
        collectionView.isPagingEnabled = true
        layout.itemSize = CGSize(width: _screenSize.width, height: _screenSize.height - _navigationHeight
        )
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
}

extension InfoContentStaticVC {
    
    @IBAction func btnLeftTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath - 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnRightTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath + 1, section: 0), at: .centeredHorizontally, animated: true)
    }
}

extension InfoContentStaticVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrData == nil ? 0 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GarbhSamvadCollCell
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GarbhSamvadCollCell
        cell.imgView.image = UIImage(named: arrData[indexPath.row])
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView{
            let currPage = scrollView.currentPage
            if currPage == self.arrData.count - 1{
                self.btnRight.isHidden = true
            }else if currPage == 0{
                self.btnLeft.isHidden = true
            }else{
                btnRight.isHidden = false
                btnLeft.isHidden = false
            }
        }
    }
}

extension InfoContentStaticVC {
    
    func initModel() {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        
        if(nTypeForStaticData == kGSInfo_HowToFollow) {

            switch currLanguage {
            case .hindi:
                arrData = ["Follow_Step1_h.jpg", "Follow_Step2_h.jpg", "Follow_Step3_h.jpg", "Follow_Step4_h.jpg", "Follow_Step5_h.jpg", "Follow_Step6_h.jpg", "Follow_Step7_h.jpg", "Follow_Step8_h.jpg", "Follow_Step9_h.jpg", "Follow_Step10_h.jpg"]
            case .english:
                arrData = ["Follow_Step1_e.jpg", "Follow_Step2_e.jpg", "Follow_Step3_e.jpg", "Follow_Step4_e.jpg", "Follow_Step5_e.jpg", "Follow_Step6_e.jpg", "Follow_Step7_e.jpg", "Follow_Step8_e.jpg", "Follow_Step9_e.jpg", "Follow_Step10_e.jpg"]
            case .gujarati:
                arrData = ["Follow_Step1_g.jpg", "Follow_Step2_g.jpg", "Follow_Step3_g.jpg", "Follow_Step4_g.jpg", "Follow_Step5_g.jpg", "Follow_Step6_g.jpg", "Follow_Step7_g.jpg", "Follow_Step8_g.jpg", "Follow_Step9_g.jpg", "Follow_Step10_g.jpg"]
            }
        }
        else if(nTypeForStaticData == kBasicInfo_AppBenefits) {
            
            switch currLanguage {
            case .hindi:
                arrData = ["AppBenefit1_h.jpg", "AppBenefit2_h.jpg", "AppBenefit3_h.jpg", "AppBenefit4_h.jpg", "AppBenefit5_h.jpg", "AppBenefit6_h.jpg"]
            case .english:
                arrData = ["AppBenefit1_e.jpg", "AppBenefit2_e.jpg", "AppBenefit3_e.jpg", "AppBenefit4_e.jpg", "AppBenefit5_e.jpg", "AppBenefit6_e.jpg"]
            case .gujarati:
                arrData = ["AppBenefit1_g.jpg", "AppBenefit2_g.jpg", "AppBenefit3_g.jpg", "AppBenefit4_g.jpg", "AppBenefit5_g.jpg", "AppBenefit6_g.jpg"]
            }
        }
        
        collectionView.reloadData()
    }
}
