//
//  Disclaimer.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class Disclaimer: ParentViewController {

    @IBOutlet weak var btnDisclaimer: UIButton!
    @IBOutlet weak var lblDisclaimer: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    func prepareUI() {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        lblDisclaimer.setLineSpacing(lineSpacing: 0.5, lineHeightMultiple: 0.0)
        switch currLanguage {
        case .hindi:
            lblDisclaimer.text = """
            - गिफ्ट ऐप का ऑप्शन एप को गिफ्ट करने के लिए लिए है।
            - ऐप की पूरी कीमत उपहार देने वाले को देना होगा।
            - रिफर एन्ड अर्न के वॉलेट बिंदु को यहां जारी नहीं किया जा सकता है।
            - आपको भुगतान के बाद प्राप्त उपहार कोड को बनाए रखना होगा।
            - उपहार कोड केवल एक वर्ष के लिए मान्य होगा।
            - एक बार भुगतान की गई राशि किसी भी स्थिति में उपलब्ध नहीं होगी।
            - अगर आपने यूजर को ऐप गिफ्ट किया है, तो Sign Up करते समय उसे यह गिफ्ट कोड एप में
            डालना पड़ेगा
            """
            break
        case .english:
            lblDisclaimer.text = """
            - The “Gift App” option is to Gift the App.
            - The full cost of the app will have to be borne by the person who wants to gift it.
            - The wallet points of the Refer and Earn cannot be redeemed here.
            - You will have to retain the gift code received after successful payment.
            - This gift code is valid for one year only. After one year, this gift code will be lapsed.
            - Paid amount for “Gift App” is non-refundable in any circumstances.
            - The person who is receiving the app as a gift will have to enter gift code provided by you at
            the time of Sign up.
            """
            break
        case .gujarati:
            lblDisclaimer.text = """
            - “ગીફ્ટ એપ” વિકલ્પ એપ ને ગીફ્ટ કરવા માટે છે.
            - એપ ની સંપૂર્ણ કિંમત ગીફ્ટ આપવા વાળા ઉપભોગતા એ આપવાની રહેશે.
            - રીફર એન્ડ અર્ન ના વોલેટ પોઈન્ટ અહી અદા કરી શકાશે નહિ.
            - પેમેન્ટ કર્યા પછી મળેલ ગીફ્ટ કોડ સાચવી રાખવો પડશે.
            - Gift Code એક વરસ માટે જ માન્ય રહેશે.
            - એકવાર ચૂકવેલ રકમ કોઈ પણ સંજોગોમાં પછી મળશે નહિ.
            - જે ઉપભોગતા ને આપે એપ ગીફ્ટ કરી હોય, તેણે, આ ગીફ્ટ કોડ SignUP કરતી વખતે નાખવો પડશે.
            """
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "payNowSegue" {
            let destVC = segue.destination as! PaymentVC
            destVC.isFromEntry = true
        }
    }
    
    @IBAction func btnAgreementTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        if btnDisclaimer.isSelected {
            if _giftUser.amount == 0 {
                self.setPaymentStatus { (completed) in
                    if completed {
                        self.performSegue(withIdentifier: "paidSegue", sender: nil)
                    }
                }
            } else {
                performSegue(withIdentifier: "payNowSegue", sender: nil)
            }
        } else {
           _ = KPValidationToast.shared.showToastOnStatusBar(message: "Please accept the disclaimers")
        }
    }
}

extension Disclaimer {
    
    func setPaymentStatus(block: @escaping (Bool) -> ()) {
       
        let randomNumber = arc4random_uniform(55555)
        kprint(items: "Random Number is \(randomNumber)")
        
        let param: [String: Any] = ["id": _giftUser.id,"unique_id": _giftUser.uniqueId, "payment_mode": "COUPAN_100", "transaction_id": randomNumber, "amount": _giftUser.amount,"email": _giftUser.email]
        
        self.showCentralSpinner()
        KPWebCall.call.setPaymentStatus(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    _giftUser.setPaymentStatus(dict: data)
                    _ = ValidationToast.showStatusMessage(message: "Payment \(data.getStringValue(key: "payment_status"))")
                    block(true)
                }else if let msg = dict["message"] as? NSDictionary{
                    self.showError(data: msg, view: self.view)
                    block(false)
                }else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}
