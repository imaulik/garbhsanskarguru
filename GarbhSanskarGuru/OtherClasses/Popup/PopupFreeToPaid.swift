
class PopupFreeToPaid: MyPopUpView {
    
    let strLanguage = _appDelegator.getLanguage()
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var imgAd: UIImageView!
    
    var popUpInfo: PopupInfo?
    var bannerid  = 0
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let info = objData as? PopupInfo{
            if self.strLanguage == KHindi{
                self.imgAd.image = info.hindi_Uimage
            }else if self.strLanguage == kGujrati{
                self.imgAd.image = info.guj_Uimage
            }else{
                self.imgAd.image = info.eng_Uimage
            }
            bannerid = info.id
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnOkClick(_ sender: UIButton) {
        if (self.pressOK != nil) {
            self.viewbanner(banner_id: "\(bannerid)"){ (json, statusCode) in
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    print("success")
                }
            }
            self.pressOK!()
        }
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        
        if (self.pressCancel != nil) {
            self.pressCancel!()
        }
    }
    
}


