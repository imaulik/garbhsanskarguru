//
//  ContactVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 19/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

class ContactCell: ConstrainedTableViewCell {
    
    override func awakeFromNib() {
        
    }
}

class ContactVC: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ContactVC {
    func openEmail(email: String){
        let mailComposeViewController = configuredMailComposeViewController(email: email)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    func openWebsite(website: String){
        var url = website
        if !url.lowercased().hasPrefix("http://"){
            url = "http://" + website
        }
        if let websiteUrl = URL(string: url) {
            let vc = SFSafariViewController(url: websiteUrl)
            present(vc, animated: true)
        }
    }
    
    func openFaceBook() {
        let username =  "MAJESTICGARBHSANSKAR"
        let appURL = NSURL(string: "fb://page/\(username)")!
        let webURL = NSURL(string: "http://www.facebook.com/\(username)")!
        let application = UIApplication.shared
        if application.canOpenURL(appURL as URL){
            application.open(appURL as URL)
        }else{
            application.open(webURL as URL)
        }
    }
    
    func openInstagram() {
        let username =  "mgarbhsanskar" // Your Instagram Username here
        let appURL = NSURL(string: "instagram://user?username=\(username)")!
        let webURL = NSURL(string: "https://instagram.com/\(username)")!
        let application = UIApplication.shared
        
        if application.canOpenURL(appURL as URL){
            application.open(appURL as URL)
        }else{
            application.open(webURL as URL)
        }
    }

}

extension ContactVC {
    
    @IBAction func btnEmailTapped(_ sender: UIButton) {
        openEmail(email: "mgarbhsanskar@gmail.com")
    }
    
    @IBAction func btnMobileTapped(_ sender: UIButton) {
        makeCall(number: "9727006001")
    }
    
    @IBAction func btnWebsiteTapped(_ sender: UIButton) {
        openWebsite(website: "www.garbhsanskar.co")
    }
    
    @IBAction func btnFacebookTapped(_ sender: UIButton) {
        openFaceBook()
    }
    
    @IBAction func btnInstagramTapped(_ sender: UIButton) {
        openInstagram()
    }
    
}

extension ContactVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContactCell
        cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactCell
        return cell
    }
    
}

////MARK:- MailComposer Methods
//extension ContactVC: MFMailComposeViewControllerDelegate{
//    func configuredMailComposeViewController(email: String) -> MFMailComposeViewController {
//        let mailComposerVC = MFMailComposeViewController()
//        mailComposerVC.mailComposeDelegate = self
//        mailComposerVC.setToRecipients([email])
//        mailComposerVC.setSubject(_appName)
//        mailComposerVC.setMessageBody("Hello this is my message body!", isHTML: false)
//        return mailComposerVC
//    }
//
//    override func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        controller.dismiss(animated: true, completion: nil)
//    }
//}

