//
//  PrayerVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PrayerVC: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension PrayerVC {
    
    func prepareOnBoarding() {
        let onBoardingView = TaskCompleteView.instantiateView(with: self.view, with: "Dear Mom")
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                onBoardingView.removeFromSuperview()
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
}

extension PrayerVC {
    
    @IBAction func btnTaskTapped(_ sender: UIButton) {
        self.prepareOnBoarding()
    }
}


extension PrayerVC {
    
    func getPrayer() {
        
    }
    
}
