//  Created by Tom Swindell on 09/12/2015.
//  Copyright © 2015 The App Developers. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

//MARK: - Graphics
extension UIView {
    
    func makeRound() {
        layer.cornerRadius = (self.frame.height * _widthRatio) / 2
        clipsToBounds = true
    }
    
    func makeCircle() {
        layer.cornerRadius = self.frame.height / 2
        clipsToBounds = true
    }
    
    func fadeAlpha(toAlpha: CGFloat, duration time: TimeInterval) {
        UIView.animate(withDuration: time) { () -> Void in
            self.alpha = toAlpha
        }
    }
    
    // Will add mask to given image
    func mask(maskImage: UIImage) {
        let mask: CALayer = CALayer()
        mask.frame = CGRect(x: 0, y: 0, width: maskImage.size.width, height: maskImage.size.height)//CGRectMake( 0, 0, maskImage.size.width, maskImage.size.height)
        mask.contents = maskImage.cgImage
        layer.mask = mask
        layer.masksToBounds = true
    }
    
    func bounceView(animateView: UIView) {
        animateView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, animations: {
            animateView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        }) { (finished) in
            UIView.animate(withDuration: 0.15, animations: {
                animateView.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    animateView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                }, completion: { (finished) in
                    UIView.animate(withDuration: 0.15, animations: {
                        animateView.transform = CGAffineTransform.identity
                    }, completion: nil)
                })
            })
        }
    }
    
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.04
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: self.center.x - 8, y: self.center.y)
        animation.toValue = CGPoint(x: self.center.x + 8, y: self.center.y)
        self.layer.add(animation, forKey: "position")
    }
}

// MARK: - Constraints
extension UIView {

    func addConstraintToSuperView(lead: CGFloat, trail: CGFloat, top: CGFloat, bottom: CGFloat) {
        guard self.superview != nil else {
            return
        }
        let top = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.superview!, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: top)
        let bottom = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.superview!, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: bottom)
        let lead = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.superview!, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: lead)
        let trail = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.superview!, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: trail)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview!.addConstraints([top,bottom,lead,trail])
    }
}

//MARK: - Apply gradient on view
extension UIView {
    
    func applyGradientEffects(_ colours: [UIColor], gradientPoint: VBGradientPoint, removeFirstLayer: Bool = true) {
        layoutIfNeeded()
        if let subLayers = layer.sublayers, subLayers.count > 1, removeFirstLayer {
            subLayers.first?.removeFromSuperlayer()
        }
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = gradientPoint.draw().startPoint
        gradient.endPoint = gradientPoint.draw().endPoint
        layer.insertSublayer(gradient, at: 0)
    }
    
    func applyCorner(_ radius: CGFloat? = nil)
    {
        if let validradius = radius {
            self.layer.cornerRadius = validradius
        } else {
            let newradius = min(self.frame.size.height, self.frame.size.width)
            self.applyCorner(newradius / 2)
        }
        self.layer.masksToBounds = true
    }
}

extension UIView {
  
    // Will take screen shot of whole screen and return image. It's working on main thread and may lag UI.
    func takeScreenShot() -> UIImage {
        UIGraphicsBeginImageContext(self.bounds.size)
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        let rec = self.bounds
        self.drawHierarchy(in: rec, afterScreenUpdates: true)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }

    // To give parellex effect on any view.
    func ch_addMotionEffect() {
        let axis_x_motion: UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffect.EffectType.tiltAlongHorizontalAxis)
        axis_x_motion.minimumRelativeValue = NSNumber(value: -10)
        axis_x_motion.maximumRelativeValue = NSNumber(value: 10)
        
        let axis_y_motion: UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffect.EffectType.tiltAlongVerticalAxis)
        axis_y_motion.minimumRelativeValue = NSNumber(value: -10)
        axis_y_motion.maximumRelativeValue = NSNumber(value: 10)
        
        let motionGroup : UIMotionEffectGroup = UIMotionEffectGroup()
        motionGroup.motionEffects = [axis_x_motion, axis_y_motion]
        self.addMotionEffect(motionGroup)
    }
    
    func inAnimate(){
        self.alpha = 1.0
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.values = [0.01,1.2,0.9,1]
        animation.keyTimes = [0,0.4,0.6,1]
        animation.duration = 0.5
        self.layer.add(animation, forKey: "bounce")
    }
    
    func OutAnimation(comp:@escaping ((Bool)->())){
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            comp(true)
        })
        
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.values = [1,1.2,0.9,0.01]
        animation.keyTimes = [0,0.4,0.6,1]
        animation.duration = 0.2
        self.layer.add(animation, forKey: "bounce")
        CATransaction.commit()
    }

    
    //Applying gradient effect
    func applyGradient(colours: [UIColor] = [UIColor.hexStringToUIColor(hexStr: "EB6809"),UIColor.hexStringToUIColor(hexStr: "F3A046")],locations location:[NSNumber] = [0.5,1.0],opacity opc:Float = 1) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.colors = colours.map { $0.cgColor }
//        gradient.locations = location
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.opacity = opc
        self.layer.insertSublayer(gradient, at: 0)
        self.layoutIfNeeded()
    }
}

/// Gradient view use to apply gradient effects
class GradientView: ConstrainedView {
    
    var gradientColor1: UIColor = .appGrediantColor1
    var gradientColor2: UIColor = .appGrediantColor2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyGradient(colours: [gradientColor1, gradientColor2])
    }
}

//Gradient type
typealias GradientType = (startPoint: CGPoint, endPoint: CGPoint)

//Enum for gradient
enum VBGradientPoint {
    case leftRight
    case rightLeft
    case topBottom
    case bottomTop
    case topLeftBottomRight
    case bottomRightTopLeft
    case topRightBottomLeft
    case bottomLeftTopRight
    func draw() -> GradientType {
        switch self {
        case .leftRight:
            return (startPoint: CGPoint(x: 0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        case .rightLeft:
            return (startPoint: CGPoint(x: 1, y: 0.5), endPoint: CGPoint(x: 0, y: 0.5))
        case .topBottom:
            return (startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1))
        case .bottomTop:
            return (startPoint: CGPoint(x: 0.5, y: 1), endPoint: CGPoint(x: 0.5, y: 0))
        case .topLeftBottomRight:
            return (startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1))
        case .bottomRightTopLeft:
            return (startPoint: CGPoint(x: 1, y: 1), endPoint: CGPoint(x: 0, y: 0))
        case .topRightBottomLeft:
            return (startPoint: CGPoint(x: 1, y: 0), endPoint: CGPoint(x: 0, y: 1))
        case .bottomLeftTopRight:
            return (startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
        }
    }
}

extension UIView {
    // Note  : Corner radius and shadow not work both side by side so you need to outlet and set layer radius
    // other wise you can set layer.cornerradius in user defines
    func blink() {
        self.alpha = 0.2
        UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
    }
    
    
    //MARK: Border COLOR
        @IBInspectable open var borderColorw: UIColor? {
            get {
                return self.borderColorw
            }
            set {
                self.layer.borderColor = newValue?.cgColor
            }
        }

    //MARK: Border Width
      @IBInspectable open var borderWidthw: CGFloat {
        get {
            return self.borderWidthw
        }
        set {
            layer.borderWidth = newValue
        }
      }
    
    
  
 
    //MARK: corner Radius
    @IBInspectable open var cornerRadiusw: CGFloat{
        get {
            return self.cornerRadiusw
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var shadowRadiusw: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacityw: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffsetw: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColorw: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    //MARK: set bottom border
    @IBInspectable var bottomBorderColor:UIColor {
        get {
            return self.bottomBorderColor
        }
        set {
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.masksToBounds = false
            self.layer.shadowColor = newValue.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }
    
    
    var nibName: String {
        return type(of:self).description().components(separatedBy: ".").last!
    }
        
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options:nil)[0] as! UIView
    }
   
}



@IBDesignable class DesignableImageView: UIImageView {
}
