//
//  ParentViewController.swift
//  manup
//
//  Created by iOS Development Company on 08/01/16.
//  Copyright © 2016 The App Developers. All rights reserved.
//

import UIKit

@objc protocol RefreshProtocol: NSObjectProtocol{
    @objc optional func refreshController()
    @objc optional func refreshNotification(noti: Notification)
}

class ParentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var viewNavigation: UIView?
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?
    @IBOutlet var verticalConstraints: [NSLayoutConstraint]?
    @IBOutlet var btnView: [UIButton]?

    // MARK: - Actions
    @IBAction func parentBackAction(_ sender: UIButton!) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func parentBackActionAnim(_ sender: UIButton!) {
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func parentDismissAction(_ sender: UIButton!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Variables for Pull to Referesh
    let refresh = UIRefreshControl()
    var isRefereshing = false
   
    // MARK: - iOS Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintUpdate()
        setDefaultUI()
        setLoaderUI()
        prepareGradientView()
        kprint(items: "Allocated: \(self.classForCoder)")
    }
    
    deinit{
        _defaultCenter.removeObserver(self)
        kprint(items: "Deallocated: \(self.classForCoder)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func prepareGradientView() {
        btnView?.forEach({ (btn) in
            btn.applyGradientEffects([GarbhColor.gradient1Pink, GarbhColor.gradient2Pink], gradientPoint: .leftRight)
        })
    }
    
    // Set Default UI
    func setDefaultUI() {
        tableView?.scrollsToTop = true;
        tableView?.tableFooterView = UIView()
    }
    
    func setTabBar(hidden: Bool){
        self.tabBarController?.tabBar.isHidden = hidden
        self.tabBarController?.tabBar.isTranslucent = hidden
    }
    
    func setLoaderUI() {
        activityIndicator.hidesWhenStopped = false
        activityIndicator.isHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    
    // This will update constaints and shrunk it as device screen goes lower.
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
        if let vConst = verticalConstraints {
            for const in vConst {
                let v1 = const.constant
                let v2 = v1 * _heightRatio
                const.constant = v2
            }
        }
    }
    
    // MARK: - Lazy Variables
    lazy internal var activityIndicator : UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        act.color = UIColor.white
        return act
    }()
    
    lazy internal var smallActivityIndicator : CustomActivityIndicatorView = {
        let image : UIImage = UIImage(named: kActivitySmallImageName)!
        return CustomActivityIndicatorView(image: image)
    }()
    
    lazy internal var centralActivityIndicator : NVActivityIndicatorView = {
        let act = NVActivityIndicatorView(frame: CGRect.zero, type: NVActivityIndicatorType.ballSpinFadeLoader, color: #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1), padding: nil)
        return act
    }()
}

extension ParentViewController{
    
    func setKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let kbSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height + 10, right: 0)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        tableView.contentInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
}


//MARK:- Uitility Methods
extension ParentViewController {
    
    func tableViewCell(index: Int , section : Int = 0) -> UITableViewCell {
        let cell = tableView.cellForRow(at: NSIndexPath(row: index, section: section) as IndexPath)
        return cell!
    }
    
    func scrollToIndex(index: Int, animate: Bool = false){
        if index >= 0{
            let indexPath = NSIndexPath(row: index, section: 0)
            tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.none, animated: animate)
        }
    }
    
    func scrollToIndexChat(section: Int, index: Int, animate: Bool = false){
        if index >= 0{
            let indexPath = NSIndexPath(row: index, section: section)
            tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.top, animated: animate)
        }
    }
    
    func scrollToTop(animate: Bool = false) {
        let point = CGPoint(x: 0, y: -tableView.contentInset.top)
        tableView.setContentOffset(point, animated: animate)
    }
    
    func scrollToBottom(animate: Bool = false)  {
        let point = CGPoint(x: 0, y: tableView.contentSize.height + tableView.contentInset.bottom - tableView.frame.height)
        if point.y >= 0{
            tableView.setContentOffset(point, animated: animate)
        }
    }
    
    func customPresentationTransition() {
        let transition = CATransition()
        transition.duration = _vcTransitionTime
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        view.window?.layer.add(transition, forKey: kCATransition)
    }
}

//MARK: - TableView
extension ParentViewController{
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
}

//MARK:- Activity Indicator
extension ParentViewController{
    
    // This will show and hide spinner. In middle of container View
    // You can pass any view here, Spinner will be placed there runtime and removed on hide.
    func showSpinnerIn(container: UIView, control: UIButton, isCenter: Bool) {
        container.addSubview(activityIndicator)
        let xConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint])
        activityIndicator.alpha = 0.0
        view.layoutIfNeeded()
        self.view.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.activityIndicator.alpha = 1.0
            if isCenter{
                control.alpha = 0.0
            }
        }
    }
    
    func hideSpinnerIn(container: UIView, control: UIButton) {
        self.view.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
        control.isSelected = false
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.activityIndicator.alpha = 0.0
            control.alpha = 1.0
        }
    }
    
    func showSmallSpinnerIn(container: UIView, control: UIControl, isCenter: Bool) {
        container.addSubview(smallActivityIndicator)
        let xConstraint = NSLayoutConstraint(item: smallActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: -8.5)
        let yConstraint = NSLayoutConstraint(item: smallActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: -8.5)
        smallActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint])
        smallActivityIndicator.alpha = 0.0
        view.layoutIfNeeded()
        self.view.isUserInteractionEnabled = false
        smallActivityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.smallActivityIndicator.alpha = 1.0
            if isCenter{
                control.alpha = 0.0
            }
        }
    }
    
    func hideSmallSpinnerIn(container: UIView, control: UIControl) {
        self.view.isUserInteractionEnabled = true
        smallActivityIndicator.stopAnimating()
        control.isSelected = false
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.smallActivityIndicator.alpha = 0.0
            control.alpha = 1.0
        }
    }
    
    func showCentralSpinner() {
        self.view.addSubview(centralActivityIndicator)
        let xConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let hei = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 40)
        let wid = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 40)
        centralActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint, hei, wid])
        centralActivityIndicator.alpha = 0.0
        self.view.layoutIfNeeded()
        self.view.isUserInteractionEnabled = false
        _appDelegator.window?.isUserInteractionEnabled = false
        centralActivityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.centralActivityIndicator.alpha = 1.0
        }
    }
    
    func hideCentralSpinner() {
        self.view.isUserInteractionEnabled = true
        _appDelegator.window?.isUserInteractionEnabled = true
        centralActivityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.centralActivityIndicator.alpha = 0.0
        }
    }
}


// MARK: - Validation Message.
extension ParentViewController {
    
    // Show API Error
    /// Desc
    ///
    /// - Parameters:
    ///   - data: json object
    ///   - yPos: Banner possition
    func showError(data: Any?, view: UIView?) {
        if let dict = data as? NSDictionary{
            if let msg = dict["message"] as? String{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            } else if let msg = dict["email"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["RESPMSG"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["error"] as? String{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["email"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["password"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict[_appName] as? String {
                if msg != kInternetDown{
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
                }
            }else{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: kInternalError)
            }
        }else{
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kInternalError)
        }
    }
    
    func showSucMsg(data: Any?,view: UIView? = nil,yPos: CGFloat = 64) {
        if let dict = data as? NSDictionary{
            if let msg = dict["message"] as? String{
                let _ = ValidationToast.showStatusMessage(message: msg, yCord: yPos, inView: view, withColor: GarbhColor.gray)
            }else if let msg = dict["error_description"] as? String{
                let _ = ValidationToast.showStatusMessage(message: msg, yCord: yPos, inView: view, withColor: GarbhColor.gray)
            }else if let msg = dict[_appName] as? String {
                if msg != kInternetDown{
                    let _ = ValidationToast.showStatusMessage(message: msg, yCord: yPos, inView: view, withColor: GarbhColor.gray)
                }
            }
        }
    }
}

//MARK: - Custom SlideMenu
extension ParentViewController {
    
    @IBAction func shutterAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        _appDelegator.isAllowBackPreDashboard = false
        if let containerController = self.findContainerController(){
            containerController.animatedDrawerEffect()
        }
    }
    
    func findContainerController() -> SlideMenuContainerVC? {
        let navCon = _appDelegator.window!.rootViewController as! UINavigationController
        for vc in navCon.viewControllers {
            if let con = vc as? SlideMenuContainerVC {
                guard con.menuActionType == .open else {return con}
                if let dashVC = con.chilTabBar.children.first as? DashboardVC {
                    dashVC.initModel()
                }
                if let introVC = con.chilTabBar.children[1] as? IntroVC {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        introVC.initModel()
                    }
                }
                return con
            }
        }
        return nil
    }
    
    func updateUserProfile(){
        if let containerController = self.findContainerController(){
            containerController.tableView.reloadData()
        }
    }
    
    func prepareForLogout() {
        self.showCentralSpinner()
        
        _appDelegator.prepareForLogout { (success, json) in
            self.hideCentralSpinner()
            if let dict = json as? NSDictionary {
                self.showError(data: dict, view: self.view)
            }
        }
    }
    
    func getActivityDone(with Module: String, activityId:String, dailyActivityId:String, block: @escaping(Bool) -> ()) {
        let params: [String: Any] = ["user_id": _user.id, "day": _user.currDay, "module": Module, "activity_id":activityId, "daily_activity_id":dailyActivityId]
        self.showCentralSpinner()
        KPWebCall.call.postModuleInfo(param: params) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                weakself.showSucMsg(data: dict)
                block(true)
            } else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
}

