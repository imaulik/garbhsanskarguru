//
//  CityListViewController.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 10/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import PureLayout

class CityListModel {
    var cityName : String!
    init(dict: NSDictionary) {
        cityName = dict.getStringValue(key: "city_name")
    }
}

class cityListCell : UITableViewCell{
    @IBOutlet var lblCityname: JPWidthLabel!
}

class CityListViewController: UIViewController {
    
    // MARK:- Outlet Declaration
    
    @IBOutlet var tfsearchCity: UITextField!
    @IBOutlet var tableview: UITableView!
    
    // MARK:- Vriable Declaration
    var arrayCityList: [CityListModel] = []
    var isloadMore: Bool = false {
        didSet {
            self.isloadMorePending = false
        }
    }
    var isloadMorePending: Bool = false
    var timer: Timer?
    var searchedText = ""
    
    lazy internal var centralActivityIndicator : NVActivityIndicatorView = {
        let act = NVActivityIndicatorView(frame: CGRect.zero, type: NVActivityIndicatorType.ballSpinFadeLoader, color: #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1), padding: nil)
        return act
    }()
    
    // MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getallCityList(name: "", limit: 10, offsetvalue: 0)
        setupTable()
        setupTimer()
    }
    
    // MARK:- Private Methods
    
    static let loadMoreCellIdentifier: String = "loadMoreCell"
    func retriveLoadmoreCell() -> UITableViewCell {
        var cell: UITableViewCell? = tableview.dequeueReusableCell(withIdentifier: "loadMoreCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "loadMoreCell")
            
            let activityindicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .gray)
            activityindicator.tag = 1005
            cell?.contentView.addSubview(activityindicator)
            activityindicator.autoAlignAxis(toSuperviewAxis: .horizontal)
            activityindicator.autoAlignAxis(toSuperviewAxis: .vertical)
        }
        let activityindicator: UIActivityIndicatorView = cell?.contentView.viewWithTag(1005) as! UIActivityIndicatorView
        activityindicator.startAnimating()
        return cell!
    }
    
    func setupTable() {
        
        tableview.register(UINib(nibName: "LoadMoreCell", bundle: nil), forCellReuseIdentifier: String(describing: "LoadMoreCell"))
        tableview.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hidekeyboard))
        tapGesture.cancelsTouchesInView = false
        tableview.addGestureRecognizer(tapGesture)
    }
    
    func setupTimer(){
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(getUpdatedCityApicall), userInfo: nil, repeats: true)
    }
    
    @objc func getUpdatedCityApicall(){
        if tfsearchCity.text! != searchedText{
            getallCityList(name: tfsearchCity.text!, limit: 10, offsetvalue: 0)
            searchedText = tfsearchCity.text!
        }
    }
    
    
    @objc fileprivate func hidekeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK:- Button Action Methods
    
    @IBAction func action_backTapped(_ sender: UIButton) {
        timer!.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
    
}

// API CALL

extension CityListViewController {
    
    func getallCityList(name:String, limit:Int, offsetvalue:Int){
        
        let param: [String: Any] = ["city_name": name, "offset": offsetvalue, "limit": limit]
        self.showCentralSpinner()
        KPWebCall.call.getClityList(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200,let arrResult = dict["data"] as? [NSDictionary]{
                    var cityModel:[CityListModel]? = []
                    for result in arrResult{
                        cityModel?.append(CityListModel(dict: result))
                    }
                    if offsetvalue == 0 {
                        weakself.arrayCityList.removeAll()
                    }
                    weakself.isloadMore = (cityModel!.count == limit) ? true : false
                    weakself.arrayCityList.append(contentsOf: cityModel!)
                    weakself.tableview.reloadData()
                }else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }
        }
    }
    
}

extension CityListViewController : UITableViewDelegate , UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.isloadMore ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrayCityList.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && !self.isloadMorePending {
            self.isloadMorePending = true
            getallCityList(name: tfsearchCity.text!, limit: 10, offsetvalue: arrayCityList.count)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        timer!.invalidate()
        _appDelegator.objInfo.city = arrayCityList[indexPath.row].cityName!
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            return self.retriveLoadmoreCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityListCell") as! cityListCell
        cell.lblCityname.text = arrayCityList[indexPath.row].cityName
        return cell
    }
}

extension CityListViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
//        tfsearchCity.text = newString
        print("search :\(newString)")
//        getallCityList(name: newString, limit: 10, offsetvalue: 0)
        return true
    }
}

extension CityListViewController {
    
    func showCentralSpinner() {
        self.view.addSubview(centralActivityIndicator)
        let xConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let hei = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 40)
        let wid = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 40)
        centralActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint, hei, wid])
        centralActivityIndicator.alpha = 0.0
        self.view.layoutIfNeeded()
        self.view.isUserInteractionEnabled = false
        _appDelegator.window?.isUserInteractionEnabled = false
        centralActivityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.centralActivityIndicator.alpha = 1.0
        }
    }
    
    func hideCentralSpinner() {
        self.view.isUserInteractionEnabled = true
        _appDelegator.window?.isUserInteractionEnabled = true
        centralActivityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.centralActivityIndicator.alpha = 0.0
        }
    }
    
    func showError(data: Any?, view: UIView?) {
        if let dict = data as? NSDictionary{
            if let msg = dict["message"] as? String{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["RESPMSG"] as? String {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict["error"] as? String{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
            }else if let msg = dict[_appName] as? String {
                if msg != kInternetDown{
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
                }
            }else{
                _ = KPValidationToast.shared.showToastOnStatusBar(message: kInternalError)
            }
        }else{
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kInternalError)
        }
    }
    
}
