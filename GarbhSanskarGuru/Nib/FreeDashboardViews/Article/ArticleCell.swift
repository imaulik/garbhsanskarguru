//
//  ArticleCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 08/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class ArticleCell: UICollectionViewCell {
    @IBOutlet weak var imgArticle: UIImageView!
    @IBOutlet weak var lblArticle: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgArticle.layer.cornerRadius = 12
        self.imgArticle.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
}


 
