//
//  SubscribeContainer.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 01/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
 
class SubscribeContainer: UIViewController {
    var shouldScroll = true
    var friendTabChangeObserver = "FriendTabObserver"
    @IBOutlet weak var btnBack: UIButton!
    var isShowBackButton:Bool = false
   
    @IBOutlet weak var scrollViewFriendsContainer: UIScrollView!
    
    @IBOutlet weak var btnAboutCourse: UIButton! //0
    @IBOutlet weak var btnCourseFeedBack: UIButton! //1
    @IBOutlet weak var btnCourseStatstics: UIButton! //2
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ConstraintsliderViewLeading: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnBack.isHidden = !self.isShowBackButton
        topConstraint.constant =  self.btnBack.isHidden ?  0 : 60
        // Do any additional setup after loading the view.
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        if btnBack.isHidden {
                self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
}


extension SubscribeContainer : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        shouldScroll = true
        scrollSliderView(tag: Int(pageNumber))
    }
}


extension SubscribeContainer {
     @IBAction func btnBackPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
     @IBAction func btnTabClicked(_ sender: UIButton) {
         shouldScroll = true
         scrollSliderView(tag: sender.tag)
     }
    
    
    func scrollSliderView(tag: Int) {
         //delegate?.tabDidChange(index: tag)
         if shouldScroll {
             NotificationCenter.default.post(name: NSNotification.Name(friendTabChangeObserver), object: nil, userInfo: ["index" : tag])
             shouldScroll = false

         }
        
         let btnArr = [btnAboutCourse,btnCourseFeedBack,btnCourseStatstics]
        
         UIView.animate(withDuration: 0.33, animations: {
             self.ConstraintsliderViewLeading.constant = ((btnArr.filter{$0?.tag == tag}.first!?.frame.origin.x)!)
             let scrollx = (((btnArr.filter{$0?.tag == tag}.first!?.frame.origin.x)!))
             self.scrollViewFriendsContainer!.setContentOffset(CGPoint(x: CGFloat(self.scrollViewFriendsContainer.frame.size.width*CGFloat(tag)), y: 0.0), animated: true)
         })
         self.view.layoutIfNeeded()
     }
     
}
