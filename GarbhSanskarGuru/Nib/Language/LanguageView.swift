//
//  LanguageView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class LanguageView: ConstrainedView {
    
    @IBOutlet var animatedViews: [UIView]!
    @IBOutlet var imgSelectedLanguage: [UIImageView]!
    @IBOutlet weak var baseView: UIView!

    var actionBlock: (KAction)->Void = {_ in}
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> LanguageView {
        let objView = UINib(nibName: "LanuageView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LanguageView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        if _appDelegator.getAppLanguage() != nil && animate{
            objView.animateTick(_appDelegator.getAppLanguage()!)
        }
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
    
    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews[tag].transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
            
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.animatedViews[tag].transform = .identity
            }, completion: { _ in
                _appDelegator.storeAppLanguage(lang: AppLanguage(rawValue: tag)!)
                self.animateTick(_appDelegator.getAppLanguage()!)
                self.actionBlock(.done)
            })
        })
    }
    
    func animateTick(_ ind: Int) {
        for(idx,view) in imgSelectedLanguage.enumerated() {
            if ind == idx {
                view.isHidden = false
            } else {
                view.isHidden = true
            }
        }
    }
    
    @IBAction func onBtnOkAction(_ sender: UIButton) {
        self.animateView(with: sender.tag)
    }
}
