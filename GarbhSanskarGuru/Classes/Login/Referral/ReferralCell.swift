//
//  ReferralCell.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

enum ReferenceCellType: String {
    
    case txtcell = "txtcell"
    case infoCell = "infoCell"
    case separatorCell = "separatorCell"
    
    var cellHeight:CGFloat {
        switch self {
        case .txtcell:
            return 90.widthRatio
        case .infoCell:
            return 50.widthRatio
        case .separatorCell:
            return 60.widthRatio
        }
    }
}


class ReferralCell: ConstrainedTableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfInput: UITextField!
    
    weak var parent: ReferalCodeVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(field: ReferenceCellType, tag: Int) {
        switch field{
        case .txtcell:
            if tag == 0 {
                lblTitle.text = "Do you have referral code ?"
                tfInput.placeholder = "Enter Refferal code"
                tfInput.returnKeyType = .next
                tfInput.tag = tag
            }else {
                lblTitle.text = "Do you have Gift app code ?"
                tfInput.placeholder = "Enter Gift App code"
                tfInput.returnKeyType = .done
                tfInput.tag = tag
            }
        case .infoCell:
            break
        case .separatorCell:
            break
        }
    }
}

extension ReferralCell: UITextFieldDelegate {
    
    @IBAction func tfEditingChange(_ sender: UITextField) {
        if sender.tag == 0 {
            parent.referralData.referralCode = sender.text!.trimmedString()
        }else {
            parent.referralData.giftAppCode = sender.text!.trimmedString()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            if let index = parent.tableView.indexPath(for: self){
                if let cell = parent.getEntryCell(index.row + 2){
                    cell.tfInput.becomeFirstResponder()
                }
            }
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
