//  Created by Maulik shah on 4/9/19.
//  Copyright © 2019 Ganesh. All rights reserved.




class WebViewController: ParentViewController , WKNavigationDelegate{
    
    @IBOutlet weak var webview: WKWebView!
  
    var strUrl : String!
    var strTile: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webview.scrollView.showsVerticalScrollIndicator = false
        self.webview.scrollView.bounces = false
        self.strUrl = "https://www.facebook.com/MAJESTICGARBHSANSKAR/"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Live Facebook GS"
        loadWebView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        webview.load(URLRequest(url: URL(string:"about:blank")!))
    }
    
    func loadWebView(){
        let url = URL(string: strUrl)
        var request: URLRequest? = nil
        if let url = url {
            self.showCentralSpinner()
            request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30.0)
        }
        if let request = request {
            webview.navigationDelegate = self
            webview.load(request)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- WKNavigationDelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void){
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        self.hideCentralSpinner()
        //print("WebView content loaded.")
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        self.hideCentralSpinner()
        let nserror = error as NSError
        if nserror.code != NSURLErrorCancelled {
            webView.loadHTMLString("404 - Page Not Found", baseURL: URL(string: ""))
        }
    }
    
}
