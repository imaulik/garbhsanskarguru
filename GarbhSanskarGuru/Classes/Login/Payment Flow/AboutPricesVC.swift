//
//  AboutPricesVC.swift
//  GarbhSanskarGuru
//
//  Created by Jahanvi Trivedi on 23/01/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class AboutPricesVC: UIViewController {
    
    @IBOutlet weak var lblTop0: UILabel!
    @IBOutlet weak var lblTop1: UILabel!
    @IBOutlet weak var lblTop2: UILabel!
    @IBOutlet weak var lblTop3: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel!
    @IBOutlet weak var lbl11: UILabel!
    @IBOutlet weak var lbl12: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setAboutPricesVC()
    }
    
    // MARK: Functions
    func setAboutPricesVC()
    {
        lblTop0.text = _appDelegator.languageBundle?.localizedString(forKey: "Garbhsanskar workshop recording", value: "", table: nil)
        
        lblTop1.text = _appDelegator.languageBundle?.localizedString(forKey: "Is this course worth the price?", value: "", table: nil)
        lblTop2.text = _appDelegator.languageBundle?.localizedString(forKey: "Material", value: "", table: nil)
        lblTop3.text = _appDelegator.languageBundle?.localizedString(forKey: "Approximate Cost (INR)", value: "", table: nil)
        lbl1.text = _appDelegator.languageBundle?.localizedString(forKey: "Biographies of great personalities", value: "", table: nil)
        lbl2.text = _appDelegator.languageBundle?.localizedString(forKey: "Diet chart and additional recipes suggested by an expert", value: "", table: nil)
        lbl3.text = _appDelegator.languageBundle?.localizedString(forKey: "Pregnancy yoga class", value: "", table: nil)
        lbl4.text = _appDelegator.languageBundle?.localizedString(forKey: "The Book of Brain Activity", value: "", table: nil)
        lbl5.text = _appDelegator.languageBundle?.localizedString(forKey: "Ramayana, Gita, Four Vedas and other scriptures", value: "", table: nil)
        lbl6.text = _appDelegator.languageBundle?.localizedString(forKey: "Worth of the time to find stories and videos online", value: "", table: nil)
        lbl7.text = _appDelegator.languageBundle?.localizedString(forKey: "TOTAL COST", value: "", table: nil)
        lbl8.text = _appDelegator.languageBundle?.localizedString(forKey: "In addition…", value: "", table: nil)
        lbl9.text = _appDelegator.languageBundle?.localizedString(forKey: "• After finding so many books and materials, where can you find motivation to follow it daily?", value: "", table: nil)
        lbl10.text = _appDelegator.languageBundle?.localizedString(forKey: "• How reliable is the material found on the Internet?", value: "", table: nil)
        lbl11.text = _appDelegator.languageBundle?.localizedString(forKey: "• Can this material be referred anytime and anywhere?", value: "", table: nil)
        lbl12.text = _appDelegator.languageBundle?.localizedString(forKey: "Priceless", value: "", table: nil)
    }
    
    // MARK: IBAction
    @IBAction func btnBack(_ sender: AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
