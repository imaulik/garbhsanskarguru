//
//  SignUpFirstStep.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 10/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SignUpFirstStep: ParentViewController {

    // MARK: - Outlet Declaration
    @IBOutlet var label_Desc: JPWidthLabel!
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        prepareUI()
    }

    // MARK: - Private Methods
    
    func prepareUI(){
        label_Desc.text = _appDelegator.languageBundle?.localizedString(forKey: "signup_sp1_desc", value: "", table: nil)
    }
    
}
