//
//  ImageviewVC.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 09/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import SDWebImage
class ImageviewVC: UIViewController {
    @IBOutlet weak var imgImageview: UIImageView!
    @IBOutlet weak var btnSharePress: UIButton!
    var urlImage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.imgImageview.sd_setImage(with:URL(string:urlImage ?? "") , placeholderImage: nil)
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGesture(sender:)))
        imgImageview.addGestureRecognizer(pinchGesture)
        // Do any additional setup after loading the view.
    }
    

    @objc func pinchGesture(sender : UIPinchGestureRecognizer) {
        sender.view?.transform = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale) ?? CGAffineTransform()
        sender.scale = 1.0
    }
    @IBAction func btnBackPress(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSharePress(_ sender: Any) {
        if let image = SDImageCache.shared.imageFromDiskCache(forKey:(urlImage ?? "")) {
            let activityItem: [AnyObject] = [image as AnyObject]
            shareYourItems(shareItems: activityItem, isexcludItems:false , excludItems:[])
        }
    }
}
