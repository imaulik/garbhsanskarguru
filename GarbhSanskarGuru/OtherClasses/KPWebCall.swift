//  Created by iOS Development Company on 12/12/16.
//  Copyright © 2016 iOS Development Company. All rights reserved.
//

import Foundation

// MARK: Web Operation
class AccessTokenAdapter: RequestAdapter {
    private let accessToken: String
    
    init(accessToken: String) {
        self.accessToken = accessToken
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        return urlRequest
    }
}


#if DEBUG
let _baseUrl = "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/" // Dev URL
let _baseUrlFile = "http://18.217.155.109:3000/uploads/" // Dev File URL
let _baseUrlFreeDask = "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/freedesk/v1/" // Dev FreeDashboard - Inner screens
#else
let _baseUrl = "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/" // Live Url
let _baseUrlFile = "http://18.217.155.109:3000/uploads/" // Live File URL
let _baseUrlFreeDask = "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/freedesk/v1/" // Live FreeDashboard - Inner screens
#endif

typealias WSBlock = (_ json: Any?, _ flag: Int) -> ()
typealias WSBlockYouTube = ( kYoutubeResponse ) -> ()
typealias WSBlockGet = ( Bool ) -> ()
typealias WSProgress = (Progress) -> ()?
typealias WSFileBlock = (_ path: String?, _ error: Error?) -> ()

struct NetworkState {
    var isInternetAvailable:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class KPWebCall:NSObject{
    static var call: KPWebCall = KPWebCall()
    
    let manager: SessionManager
    var networkManager: NetworkReachabilityManager = NetworkReachabilityManager()!
    var headers: HTTPHeaders = ["Content-Type" : "application/x-www-form-urlencoded","Accept" : "application/json"]
    var toast: ValidationToast!
    var paramEncode: ParameterEncoding = URLEncoding.default
    let timeOutInteraval: TimeInterval = 60
    var successBlock: (String, HTTPURLResponse?, AnyObject?, WSBlock) -> Void
    var errorBlock: (String, HTTPURLResponse?, NSError, WSBlock) -> Void
    
    override init() {
        manager = Alamofire.SessionManager.default

        // Will be called on success of web service calls.
        successBlock = { (relativePath, res, respObj, block) -> Void in
            // Check for response it should be there as it had come in success block
            if let response = res{
                kprint(items: "Response Code: \(response.statusCode)")
                kprint(items: "Response(\(relativePath)): \(String(describing: respObj))")
                if response.statusCode == 200 {
                    block(respObj, response.statusCode)
                } else {
                    if response.statusCode == 401 {
                       if _user != nil{
                            _appDelegator.removeUserInfoAndNavToLogin()
                            if let msg = respObj?["error"] as? String{
                                _ = KPValidationToast.shared.showToastOnStatusBar(message: msg)
                            }else{
                               _ = KPValidationToast.shared.showToastOnStatusBar(message: kTokenExpire)
                            }
                       }
                        block([_appName: kInternetDown] as AnyObject, response.statusCode)
                    } else {
                        block(respObj, response.statusCode)
                    }
                }
            } else {
                // There might me no case this can get execute
                block(nil, 404)
            }

        }
        
        // Will be called on Error during web service call
        errorBlock = { (relativePath, res, error, block) -> Void in
            // First check for the response if found check code and make decision
            if let response = res {
                kprint(items: "Response Code: \(response.statusCode)")
                kprint(items: "Error Code: \(error.code)")
                if let data = error.userInfo["com.alamofire.serialization.response.error.data"] as? NSData {
                    let errorDict = (try? JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary
                    if errorDict != nil {
                        kprint(items: "Error(\(relativePath)): \(errorDict!)")
                        block(errorDict!, response.statusCode)
                    } else {
                        let code = response.statusCode
                        block(nil, code)
                    }
                } else if response.statusCode == 401{
                    if _user != nil{
                       _appDelegator.removeUserInfoAndNavToLogin()
                        _ = KPValidationToast.shared.showToastOnStatusBar(message: kTokenExpire)
                    }
                    block([_appName: kInternetDown] as AnyObject, response.statusCode)
                }else {
                    block(nil, response.statusCode)
                }
                // If response not found rely on error code to find the issue
            } else if error.code == -1009  {
                kprint(items: "Error(\(relativePath)): \(error)")
                block([_appName: kInternetDown] as AnyObject, error.code)
                return
            } else if error.code == -1003  {
                kprint(items: "Error(\(relativePath)): \(error)")
                block([_appName: kHostDown] as AnyObject, error.code)
                return
            } else if error.code == -1001  {
                kprint(items: "Error(\(relativePath)): \(error)")
                block([_appName: kTimeOut] as AnyObject, error.code)
                return
            } else {
                kprint(items: "Error(\(relativePath)): \(error)")
                block(nil, error.code)
            }
        }
        super.init()
        addInterNetListner()
    }
    
    deinit {
        networkManager.stopListening()
    }
}

// MARK: Other methods
extension KPWebCall{
    func getFullUrl(forFreeDaskAPI:Bool = false, relPath : String) throws -> URL{
        do{
            if relPath.lowercased().contains("http") || relPath.lowercased().contains("www"){
                return try relPath.asURL()
            }else{
                if forFreeDaskAPI{
                    return try (_baseUrlFreeDask+relPath).asURL()
                }
                else{
                    return try (_baseUrl+relPath).asURL()
                }
            }
        }catch let err{
            throw err
        }
    }
    
    func setAccesTokenToHeader(token:String){
        manager.adapter = AccessTokenAdapter(accessToken: token)
    }
    
    func removeAccessTokenFromHeader(){
        manager.adapter = nil
    }
}

// MARK: - Request, ImageUpload and Dowanload methods
extension KPWebCall{
    
    func getRequest(forDask:Bool = false, relPath: String, param: [String: Any]?, headerParam: HTTPHeaders?, timeout: TimeInterval? = nil, block: @escaping WSBlock)-> DataRequest?{
        do{
//            kprint(items: "Url: \(try getFullUrl(relPath: relPath))")
            kprint(items: "Url: \(try getFullUrl(forFreeDaskAPI: forDask, relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
    
            var req = try URLRequest(url: getFullUrl(forFreeDaskAPI:forDask, relPath: relPath), method: HTTPMethod.get, headers: (headerParam ?? headers))
            req.timeoutInterval = timeout ?? timeOutInteraval
            let encodedURLRequest = try paramEncode.encode(req, with: param)
            return Alamofire.request(encodedURLRequest).responseJSON { (resObj) in
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            
                            guard let object = try? JSONSerialization.jsonObject(with: resData, options: []),
                                let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
                                let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                                    return
                            }
                            
                            print("dictResponse \(prettyPrintedString)")
                            
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            kprint(items: errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            kprint(items: error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }
    
    func postRequest(forDask:Bool = false, relPath: String, param: [String: Any]?, headerParam: HTTPHeaders?, timeout: TimeInterval? = nil, block: @escaping WSBlock)-> DataRequest?{
        do{
            kprint(items: "Url: \(try getFullUrl(forFreeDaskAPI: forDask, relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
            
            var req = try URLRequest(url: getFullUrl(forFreeDaskAPI: forDask, relPath: relPath), method: HTTPMethod.post, headers: (headerParam ?? headers))
            req.timeoutInterval = timeout ?? timeOutInteraval
            let encodedURLRequest = try paramEncode.encode(req, with: param)
            return Alamofire.request(encodedURLRequest).responseJSON { (resObj) in
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            
                            guard let object = try? JSONSerialization.jsonObject(with: resData, options: []),
                                let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
                                let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                                    return
                            }
                            
                            print("dictResponse \(prettyPrintedString)")
                            
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            kprint(items: errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            kprint(items: error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }
    
    func putRequest(relPath: String, param: [String: Any]?, headerParam: HTTPHeaders?, block: @escaping WSBlock)-> DataRequest?{
        do{
            kprint(items: "Url: \(try getFullUrl(relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
            return manager.request(try getFullUrl(relPath: relPath), method: HTTPMethod.put, parameters: param, encoding: paramEncode, headers: (headerParam ?? headers)).responseJSON { (resObj) in
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            kprint(items: errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            kprint(items: error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }

    
    func deleteRequest(relPath: String, param: [String: Any]?, headerParam: HTTPHeaders?, block: @escaping WSBlock)-> DataRequest?{
        do{
            kprint(items: "Url: \(try getFullUrl(relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
            return manager.request(try getFullUrl(relPath: relPath), method: HTTPMethod.delete, parameters: param, encoding: paramEncode, headers: (headerParam ?? headers)).responseJSON { (resObj) in
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            kprint(items: errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            kprint(items: error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }
    
    func mylatestPostRequest(relPath: String, param: [String: Any]?,headerParam: HTTPHeaders?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            kprint(items: "Url: \(try getFullUrl(relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
            manager.upload(multipartFormData: { (formData) in
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: (headerParam ?? headers), encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint( items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint( items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint( items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    
    func uploadVideo(relPath: String, vidFileUrl: URL, param: [String: Any]?, name : String ,headerParam: HTTPHeaders?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            kprint(items: "Url: \(try getFullUrl(relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
            manager.upload(multipartFormData: { (formData) in
                formData.append(vidFileUrl, withName: name, fileName: "video.mp4", mimeType: "video/mp4")
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: (headerParam ?? headers), encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint( items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint( items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint( items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    func uploadAudio(relPath: String, audioFileUrl: URL, param: [String: Any]?, name : String ,headerParam: HTTPHeaders?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            kprint(items: "Url: \(try getFullUrl(relPath: relPath))")
            kprint(items: "Param: \(String(describing: param))")
            manager.upload(multipartFormData: { (formData) in
                formData.append(audioFileUrl, withName: name, fileName: "audio.m4a", mimeType: "audio/m4a")
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: (headerParam ?? headers), encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint( items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint( items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint( items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    func uploadUserImages(relPath: String,imgs: [UIImage?],param: [String: String]?, keyStr : [String], headerParam: HTTPHeaders?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            manager.upload(multipartFormData: { (formData) in
                for (idx,img) in imgs.enumerated(){
                    if let _ = img{
                        let imageData = img!.jpegData(compressionQuality: 0.4)
                        formData.append(imageData!, withName: keyStr[idx], fileName: "image.jpeg", mimeType: "image/jpeg")
                    }
                }
                if let _ = param{
                    for (key, value) in param!{
                        formData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: (headerParam ?? headers), encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint(items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint(items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    func dowanloadFile(relPath : String, saveFileUrl: URL, progress: WSProgress?, block: @escaping WSFileBlock){
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (saveFileUrl, [.removePreviousFile, .createIntermediateDirectories])
        }
        do{
            manager.download(try getFullUrl(relPath: relPath), to: destination).downloadProgress { (prog) in
                progress?(prog)
                }.response { (responce) in
                    if let path = responce.destinationURL?.path{
                        block(path, responce.error)
                    }else{
                        block(nil, responce.error)
                    }
                }.resume()
        }catch{
            block(nil, nil)
        }
    }
}


// MARK: - Internet Availability
extension KPWebCall{
    func addInterNetListner(){
        networkManager.startListening()
        networkManager.listener = { (status) -> Void in
            if status == NetworkReachabilityManager.NetworkReachabilityStatus.notReachable{
                print("No InterNet")
                if self.toast == nil{
                    self.toast = KPValidationToast.shared.showStatusMessageForInterNet(message: kInternetDown)
                }
            }else{
                print("Internet Avail")
                if self.toast != nil{
                    self.toast.animateOut(duration: 0.2, delay: 0.2, completion: { () -> () in
                        self.toast.removeFromSuperview()
                        self.toast = nil
                    })
                }
            }
        }
    }
    
    func isInternetAvailable() -> Bool {
        if networkManager.isReachable{
            return true
        }else{
            return false
        }
    }
}

// MARK: - Dashboard
extension KPWebCall{
    
    func getUserNotification(block: @escaping WSBlock) {
         kprint(items: "------------  get user notification -------------")
        let parms = ["user_id" : _user.id]
        let relPath = "getusernotification"
        _ = postRequest(relPath: relPath, param: parms, headerParam: nil, block: block)
    }
    
    func getUserNotificationDetail(announcementid : String,block: @escaping WSBlock) {
        kprint(items: "------------  get user notification Detail-------------")
        let parms = ["user_id" : _user.id,"announcement_id" : announcementid]
        let relPath = "getusernotification"
        _ = postRequest(relPath: relPath, param: parms, headerParam: nil, block: block)
    }
    
    func viewbanner(param: [String: Any], block: @escaping WSBlock) {
         kprint(items: "------------  viewbanner -------------")
         let relPath = "viewbanner"
         _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
     }
    
    func getEventDetail(block: @escaping WSBlock) {
        kprint(items: "------------  GetEventDetail -------------")
        let relPath = "event_detail/\(_appDelegator.getLanguage())"
        
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    
    func getStatisticsData(block: @escaping WSBlock) {
        kprint(items: "------------- Get Statistics Data  -----------")
        let relPath = "getstatisticsdata"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getCourseFeedback(block: @escaping WSBlock) {
        kprint(items: "------------- Get Statistics Data  -----------")
        let relPath = "coursefeedback"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getAboutFeedback(block: @escaping WSBlock) {
        kprint(items: "------------- Get Statistics Data  -----------")
        let relPath = "aboutfeedback"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getPopupBanner(block: @escaping WSBlock) {
        kprint(items: "------------- Get Popup Banner Image -----------")
        let relPath = "getPopupBanner"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func loginUser(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------ Login User -------------")
        let relPath = "login"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func getworkshopwebinar(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------  work shop webinar -------------")
        let relPath = "getworkshopwebinar"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func forceLogout(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------ Force Logout User -------------")
        let relPath = "force_logout"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func registerUser(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------ Register User -------------")
        let relPath = "signup"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func checkEmailExist(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Check Email Exist -----------")
        let relPath = "checkEmailExist"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func checkGiftCode(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Check Gift Code -----------")
        let relPath = "checkgiftcode"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func skipGiftCode(block: @escaping WSBlock) {
        kprint(items: "------------- Skip Gift Code -----------")
        let relPath = "skipgiftcode"
        _ = postRequest(relPath: relPath, param: nil , headerParam: nil, block: block)
    }
    
    func setReappPurchase(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Reapp Purchase -----------")
        let relPath = "reapp_perchase"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func setPaymentStatus(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Set Payment Status -----------")
        let relPath = "paymentsuccess"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func checkPaymentStatus(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Check Payment Status -----------")
        let relPath = "repayment_inquiry"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func updatePaymentStatus(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Update Payment Status -----------")
        let relPath = "user_payment_success"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func postGiftDetails(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Post Gift Details -----------")
        let relPath = "giftuserdetail"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getBabyCategory(block: @escaping WSBlock) {
        kprint(items: "------------- Get Baby Category -----------")
        let relPath = "getcategory"
        _ = postRequest(relPath: relPath, param: nil , headerParam: nil, block: block)
    }
    
    //http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/getcategory
      func getConfigPrice(block: @escaping WSBlock) {
          kprint(items: "------------- Get Price -----------")
          let relPath = "getconfig"
          _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
      }
    
    func getPregnancyDay(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Pregnancy Day -----------")
        let relPath = "selectday"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func resetPassword(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Forgot Password -----------")
        let relPath = "forgotPassword"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func changePassword(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Change Password -----------")
        let relPath = "changepassword"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func verifyPayment(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Verify Payment -----------")
        let relPath = "check_payment_success"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func updateUserProfile(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Update Profile -----------")
        let relPath = "updateprofile"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func eventBooking(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Update Profile -----------")
        let relPath = "event_notify"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func logOutUser(block: @escaping WSBlock) {
        kprint(items: "------------ Log Out User -------------")
        let relPath = "logout"
        _ = postRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func generateCheckSum(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Generate CheckSum -----------")
        let relPath = "generatechecksum"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func selectDesireBaby(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Desire Baby -----------")
        let relPath = "selectdesirebabby"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getToday(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Today -----------")
        let relPath = "getToday"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getDashboardAllActivityStatus(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Activity Status -----------")
        let relPath = "getupdatemodule"//"getnewmodule"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getMomData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Mom Data -----------")
        let relPath = "getmomactivity"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getDietActivityData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Diet Activity Data -----------")
        let relPath = "getDietreciepe"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getMonthlyActivityData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Diet Activity Data -----------")
        let relPath = "getdietactivity"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getGarbhSamad(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Garbh Samvad -----------")
        let relPath = "garbhsamvaad"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getGarbhSamvadSuggestion(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Garbh Samvad Suggestion -----------")
        let relPath = "garbhsamvaadauto"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getGarbhActivityData(relPath: String, param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Activity Data -----------")
        let relPath = relPath
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func getRahasyaTitle(block: @escaping WSBlock){
        kprint(items: "-------------- Get Rahasya Title ----------")
        let relPath = "http://webserviceforapp.garbhsanskar.co/gswebserviceV2/getRahasyaTitle.php"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getRandomVideo(block: @escaping WSBlock){
        kprint(items: "-------------- Get Rahasya Title ----------")
        let relPath = "getrandomvideo"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getRahasyaContent(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "-------------- Get Rahasya Content ----------")
        let relPath = "http://webserviceforapp.garbhsanskar.co/gswebserviceV2/getRahasyaContent.php"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getGarbhYatraContent(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "-------------- Get Rahasya Content ----------")
        let relPath = "http://webserviceforapp.garbhsanskar.co/gswebserviceV2/get_garbhyatra_content.php"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getDietData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Desire Baby -----------")
        let relPath = "selectdesirebabby"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func postModuleInfo(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Post Module Data -----------")
        let relPath = "doneActivity"
        _ = postRequest(relPath: relPath, param: param, headerParam: nil, block: block)
    }
    
    func getBannerImage(block: @escaping WSBlock) {
        kprint(items: "------------- Get Banner Image -----------")
        let relPath = "getbannerimage/1"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
//    func getPaymentHideShow(block: @escaping WSBlock) {
//        let relPath = //"http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/userstatusnext"
////        "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/usernext/23"
//            "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/usernext/24"
//        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
//    }
    
    func getPaymentHideShow(block: @escaping WSBlock) {
        let relPath = "http://webserviceforapp.garbhsanskar.co/garbhsanskar/public/api/v1/usernext/37"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getAppInformationVideo(block: @escaping WSBlock) {
        kprint(items: "------------- App Information video -----------")
        let relPath = "demo_video"
        _ = getRequest(relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getBrainActivityData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Brain Activity Data -----------")
        let relPath = "puzzle"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getClityList(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Brain Activity Data -----------")
        let relPath = "getcity"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func googleApiforYoutubeVideoStatus(youtubeID:String,block: @escaping WSBlockYouTube){
        //AIzaSyBYXXjYtM+_IBLQoiIpky+KJhvlun+2UVhMIk - Old Key
        let relPath = "https://www.googleapis.com/youtube/v3/videos?id=\(youtubeID)&part=status&key=AIzaSyA2pUP3Sip2-6YACGZDLlv0n7fG2XS4qS8"
        
        let task = URLSession.shared.dataTask(with: URL.init(string: relPath)!) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
            do {
                if let dictResponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] {
                    print("Response : \(dictResponse)")
                    let youTubeModel = YoutubeVideoDetail(fromDictionary: dictResponse as NSDictionary)
                    if youTubeModel.items.count > 0{
                        if youTubeModel.items[0].status.embeddable == true{
                            block(.success)
                        }else{
                            block(.fail)
                        }
                    }
                    else{
                        block(.error)
                    }
                }
//                else{
//                    block(false)
//                }
            }
            catch let error as NSError {
                print("error :\(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    func sendVideoFailedEmail(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- play video failed email -----------")
        let relPath = "sendMail"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func userDayReport(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- user Day Report -----------")
        let relPath = "userdayreport"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }

    func userMainReport(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- user Main Report -----------")
        let relPath = "user_main_report"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func pauseApplication(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- pause application -----------")
        let relPath = "stopapp"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func changePregnancyDay(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- change pregnancy day -----------")
        let relPath = "changeday"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func updateAvailable(block: @escaping WSBlockGet){
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                return
        }
        
        do{
            if NetworkState().isInternetAvailable  {
                let data = try? Data(contentsOf: url)
                if let json = try? JSONSerialization.jsonObject(with: data ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]{
                    if let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
                        if version > currentVersion{
                            block(true)
                        }else{
                            block(false)
                        }
                    }
                }
            }
        }
    }
    
    func getVideoForCompleteDemo(param: [String: Any], block: @escaping WSBlock){
        kprint(items: "------------- get video for complete demo -----------")
        let relPath = "getpaymentvideo"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getAppDashboard(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- getApp Dashboard -----------")
        let relPath = "getappdashboard"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    // FreeDashboard new Design
    
    func getQuestionList(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Question List -----------")
        let relPath = "getquestion"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func postAnswer(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Post Question Answer -----------")
        let relPath = "answer"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getAnswer(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- get Answer -----------")
        let relPath = "getanswer"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func askQuestion(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Ask Question -----------")
        let relPath = "askquestion"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func editQuestion(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Edit posted Question -----------")
        let relPath = "editquestion"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func editAnswer(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Edit posted Answer -----------")
        let relPath = "editanswer"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func deleteAnswer(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- delete posted Answer -----------")
        let relPath = "deleteanswer"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func deleteQuestion(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- delete posted Question -----------")
        let relPath = "deletequestion"
        _ = postRequest(relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    // Dashboard - Inner screen api's
    
    func categoryTips(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Category Tips -----------")
        let relPath = "tips"
        _ = postRequest(forDask:true, relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getPageImage(forDashBoard:Bool = true, endPoint:String,param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Page Image -----------")
        let relPath = endPoint//"month"
        _ = postRequest(forDask:forDashBoard,relPath: relPath, param: param , headerParam: nil, block: block)
    }
        
    func getCategoryList(endPoint:String,param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Category List -----------")
        let relPath = endPoint
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getAppInformation(block: @escaping WSBlock) {
        kprint(items: "------------- Application information video -----------")
        let relPath = "demo_video"
        _ = getRequest(forDask:true, relPath: relPath, param: nil, headerParam: nil, block: block)
    }
    
    func getVideosOfGS(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- get video of GS -----------")
        let relPath = "uploadvideo"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getVideoList(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Video list -----------")
        let relPath = "video"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getLibraryData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Video list -----------")
        let relPath = "library"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getArticalList(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Article list -----------")
        let relPath = "article"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getQuizData(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Quiz Api call -----------")
        let relPath = "quiz"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func checkAnswer(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- CheckAnswer Api call -----------")
        let relPath = "checkaswer"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
 
    func getAudioList(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Get Audio List Api call -----------")
        let relPath = "audio"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
    
    func getAboutUs(param: [String: Any], block: @escaping WSBlock) {
        kprint(items: "------------- Aboutus Api call -----------")
        let relPath = "get_about_us.php"
        _ = postRequest(forDask:true,relPath: relPath, param: param , headerParam: nil, block: block)
    }
}
