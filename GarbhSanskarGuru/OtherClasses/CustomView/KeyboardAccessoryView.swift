//
//  KeyboardAccessoryView.swift
//  DeepContract
//
//  Created by Yudiz Solutions on 20/07/18.
//  Copyright © 2018 Yudiz. All rights reserved.
//

import UIKit

//MARK:============================= Enums ===================================
enum KAction {
    case cancel, done, share , other, later
}

enum kReminderAction {
    case cancel, done, delete, update
}

enum kApicallType {
    case resumeApp, changeDay, none
}

enum kYoutubeResponse {
    case success, fail, error
}

class KeyboardAccessoryView: UIView {
    var actionBlock: (KAction)->Void = {_ in}
    override func awakeFromNib() {
        
    }
    
    @IBAction func doneBtnClicked(_ sender: UIButton) {
        actionBlock(.done)
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        actionBlock(.cancel)
    }

}
