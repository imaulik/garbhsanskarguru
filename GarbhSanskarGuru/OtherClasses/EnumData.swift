//
//  EnumData.swift
//  GroupNgrab
//
//  Created by alpesh on 09/02/17.
//  Copyright © 2017 alpesh. All rights reserved.
//

import Foundation


enum DashBoardCategoryList : String {
    case GS                    = "Garbhsanskar"
    case GSBenefit             = "Garbhsanskar Benefit"
    case GSunderStanding       = "Garbhsanskar Understanding"
    case GSDesired             = "Desired Baby though Garbhsanskar"
    case GS9Months             = "MyMonth"
    case PracticeGarbhsanskar  = "Practice Garbhsanskar"
    case GSGarbhSavand         = "Garbhsamvad"
    case GSYoga                = "Yoga & Meditation"
    case GSDiet                = "Diat"
    case GS5SenseActivity      = "5 Sense Activity"
    case ColorRagaOrnament     = "Color,Raga & Ornament"
    case GarbhSharirBhav       = "Garbh Sharir Bhav"
    case GSTips                = "Tips"
    case GSVideo               = "Video"
    case GSAudio               = "Audio"
    case GSLibrary             = "Library"
    case GSCalender            = "Calender"
    case DailyQuote            = "DailyQuote"
    case GSArticle             = "Article"
    case GSposters             = "GSposters"
    case GSbabyimage           = "GSbabyimage"
    case GSShloka              = "GSshloka"
    case GSHalarda             = "GShalarda"
    case GSGame                = "GSGame"
}

enum ImageList : String {
    case AppInfo1 = "ic_appinfo1"
    case AppInfo2 = "ic_appinfo2"
    case AppInfo3 = "ic_appinfo3"
    case AppInfo4 = "ic_appinfo4"
    case AppInfo5 = "ic_appinfo5"
    case AppInfo6 = "ic_appinfo6"
}

enum DemoVideoType : String{
    
    case DemoComplete = "demo_completed"
    case SubscribeFullCourse = "subscribe_full_course"
    case DemoStart = "demo_start"
    
}
