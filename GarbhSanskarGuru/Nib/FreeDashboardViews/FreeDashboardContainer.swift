//
//  FreeDashboardContainer.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 27/08/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import SDWebImage

class FreeDashboardContainer: UIViewController {
    let strLanguage = _appDelegator.getLanguage()
    var selectedTabIndex = 0
    var friendTabChangeObserver = "FriendTabObserver"
    @IBOutlet weak var ConstraintsliderViewLeading: NSLayoutConstraint!

    var shouldScroll = true
    @IBOutlet weak var lblSlider: UILabel!
    @IBOutlet weak var btnDashboard: UIButton!
    @IBOutlet weak var btnOurWebinars: UIButton!
    @IBOutlet weak var scrollViewFriendsContainer: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}



extension FreeDashboardContainer : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        shouldScroll = true
        scrollSliderView(tag: Int(pageNumber))
    }
}




extension FreeDashboardContainer {
  
     @IBAction func btnTabClicked(_ sender: UIButton) {
         shouldScroll = true
         scrollSliderView(tag: sender.tag)
     }
    
    
    func scrollSliderView(tag: Int) {
         //delegate?.tabDidChange(index: tag)
         if shouldScroll {
             NotificationCenter.default.post(name: NSNotification.Name(friendTabChangeObserver), object: nil, userInfo: ["index" : tag])
             shouldScroll = false

         }
         let btnArr = [btnDashboard,btnOurWebinars]
        
         UIView.animate(withDuration: 0.33, animations: {
             self.ConstraintsliderViewLeading.constant = ((btnArr.filter{$0?.tag == tag}.first!?.frame.origin.x)!)
             let scrollx = (((btnArr.filter{$0?.tag == tag}.first!?.frame.origin.x)!))
             self.scrollViewFriendsContainer!.setContentOffset(CGPoint(x: CGFloat(self.scrollViewFriendsContainer.frame.size.width*CGFloat(tag)), y: 0.0), animated: true)
         })
         self.view.layoutIfNeeded()
     }
     
}
