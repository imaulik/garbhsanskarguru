//
//  CustomTextField.swift
//  Laminate
//
//  Created by Yudiz Solutions Pvt. Ltd. on 13/08/18.
//  Copyright © 2018 Yudiz. All rights reserved.
//

import Foundation
import UIKit

class CustomTextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
