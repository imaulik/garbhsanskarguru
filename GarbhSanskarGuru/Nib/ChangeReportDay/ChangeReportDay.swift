//
//  ChangeReportDay.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 29/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ChangeReportDay: UIView {
    
    @IBOutlet var textfield_FromDay: UITextField!
    @IBOutlet var textfield_ToDay: UITextField!
    
    var actionBlock: (String?,String?,kReminderAction)->Void = {_,_,_  in}
    @IBOutlet var animatedViews: UIView!
    
}

extension ChangeReportDay{
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> ChangeReportDay {
        let objView = UINib(nibName: "ChangeReportDay", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ChangeReportDay
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }

    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews.transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.animatedViews.transform = .identity
            }, completion: { _ in
            })
        })
    }
    
}

extension ChangeReportDay {
    
    func isValidData() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: textfield_FromDay.text){
            result.isValid = false
            result.error = kFromDay
            return result
        }
        if String.validateStringValue(str: textfield_ToDay.text) {
            result.isValid = false
            result.error = kToDay
            return result
        }
        
        let fromDay = Int(textfield_FromDay.text!)!
        let toDay = Int(textfield_ToDay.text!)!
        if fromDay < (Int(_user.currDay) - 7) || fromDay > Int(_user.currDay) {
            result.isValid = false
            result.error = kValidDayReport
            return result
        }
        if toDay < fromDay {
            result.isValid = false
            result.error = kValidDayReport
            return result
        }
        return result
    }
    
}

extension ChangeReportDay{
    
    @IBAction func action_GetReport(_ sender: BaseButton) {
        self.endEditing(true)
        let validate = self.isValidData()
        if validate.isValid{
            self.actionBlock(textfield_FromDay.text!,textfield_ToDay.text!,.done)
        }else{
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
    
    @IBAction func action_Cancel(_ sender: UIControl) {
        self.endEditing(true)
        self.actionBlock(nil,nil,.cancel)
    }
    
}
