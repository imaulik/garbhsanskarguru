//
//  DashboardDataModel.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 13/06/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class DashboardDataModel: NSObject {

    var engImage : String!
    var engImageMsg : String!
    var engMessage : String!
    var engMsg : String!
    var engUrl : String!
    var engUrlImage : String!
    var gujImage : String!
    var gujImageMsg : String!
    var gujMessage : String!
    var gujMsg : String!
    var gujUrl : String!
    var gujUrlImage : String!
    var hindiImage : String!
    var hindiImageMsg : String!
    var hindiMessage : String!
    var hindiMsg : String!
    var hindiUrl : String!
    var hindiUrlImage : String!
    var imageName : String!
    var type : String!
    
    override init() {
    }
    
    init(fromDictionary dictionary: NSDictionary){
        engImage = dictionary.getStringValue(key: "eng_image") //dictionary["eng_image"] as? String
        engImageMsg = dictionary.getStringValue(key: "eng_image_msg") //dictionary["eng_image_msg"] as? String
        engMessage = dictionary.getStringValue(key: "eng_message") //dictionary["eng_message"] as? String
        engMsg = dictionary.getStringValue(key: "eng_msg")//dictionary["eng_msg"] as? String
        engUrl = dictionary.getStringValue(key: "eng_url")//dictionary["eng_url"] as? String
        engUrlImage = dictionary.getStringValue(key: "eng_url_image")//dictionary["eng_url_image"] as? String
        gujImage = dictionary.getStringValue(key: "guj_image")//dictionary["guj_image"] as? String
        gujImageMsg = dictionary.getStringValue(key: "guj_image_msg")//dictionary["guj_image_msg"] as? String
        gujMessage = dictionary.getStringValue(key: "guj_message")//dictionary["guj_message"] as? String
        gujMsg = dictionary.getStringValue(key: "guj_msg")//dictionary["guj_msg"] as? String
        gujUrl = dictionary.getStringValue(key: "guj_url")//dictionary["guj_url"] as? String
        gujUrlImage = dictionary.getStringValue(key: "guj_url_image")//dictionary["guj_url_image"] as? String
        hindiImage = dictionary.getStringValue(key: "hindi_image")//dictionary["hindi_image"] as? String
        hindiImageMsg = dictionary.getStringValue(key: "hindi_image_msg")//dictionary["hindi_image_msg"] as? String
        hindiMessage = dictionary.getStringValue(key: "hindi_message")//dictionary["hindi_message"] as? String
        hindiMsg = dictionary.getStringValue(key: "hindi_msg")//dictionary["hindi_msg"] as? String
        hindiUrl = dictionary.getStringValue(key: "hindi_url")//dictionary["hindi_url"] as? String
        hindiUrlImage = dictionary.getStringValue(key: "hindi_url_image")//dictionary["hindi_url_image"] as? String
        imageName = dictionary.getStringValue(key: "image_name")//dictionary["image_name"] as? String
        type = dictionary.getStringValue(key: "type")//dictionary["type"] as? String
    }

    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if engImage != nil{
            dictionary["eng_image"] = engImage
        }
        if engImageMsg != nil{
            dictionary["eng_image_msg"] = engImageMsg
        }
        if engMessage != nil{
            dictionary["eng_message"] = engMessage
        }
        if engMsg != nil{
            dictionary["eng_msg"] = engMsg
        }
        if engUrl != nil{
            dictionary["eng_url"] = engUrl
        }
        if engUrlImage != nil{
            dictionary["eng_url_image"] = engUrlImage
        }
        if gujImage != nil{
            dictionary["guj_image"] = gujImage
        }
        if gujImageMsg != nil{
            dictionary["guj_image_msg"] = gujImageMsg
        }
        if gujMessage != nil{
            dictionary["guj_message"] = gujMessage
        }
        if gujMsg != nil{
            dictionary["guj_msg"] = gujMsg
        }
        if gujUrl != nil{
            dictionary["guj_url"] = gujUrl
        }
        if gujUrlImage != nil{
            dictionary["guj_url_image"] = gujUrlImage
        }
        if hindiImage != nil{
            dictionary["hindi_image"] = hindiImage
        }
        if hindiImageMsg != nil{
            dictionary["hindi_image_msg"] = hindiImageMsg
        }
        if hindiMessage != nil{
            dictionary["hindi_message"] = hindiMessage
        }
        if hindiMsg != nil{
            dictionary["hindi_msg"] = hindiMsg
        }
        if hindiUrl != nil{
            dictionary["hindi_url"] = hindiUrl
        }
        if hindiUrlImage != nil{
            dictionary["hindi_url_image"] = hindiUrlImage
        }
        if imageName != nil{
            dictionary["image_name"] = imageName
        }
        if type != nil{
            dictionary["type"] = type
        }
        return dictionary
    }
}
