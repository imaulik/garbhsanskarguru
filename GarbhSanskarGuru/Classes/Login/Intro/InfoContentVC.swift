//
//  InfoContentVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 24/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class InfoContentVC: ParentViewController {

    var objRahasya: ModelRahasyaTitle!
    var arrData: [ModelRahasyaContent]!
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        getRahasyaContentDetail()
    }
}

extension InfoContentVC {
    
    func prepareUI() {
        btnLeft.isHidden = true
        setLayout()
    }
    
    func setLayout(){
        layout.scrollDirection = .horizontal
        collectionView.isPagingEnabled = true
        layout.itemSize = CGSize(width: _screenSize.width, height: _screenSize.height - _navigationHeight
        )
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
}

extension InfoContentVC {
    
    @IBAction func btnLeftTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath - 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnRightTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath + 1, section: 0), at: .centeredHorizontally, animated: true)
    }
}


extension InfoContentVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrData == nil ? 0 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GarbhSamvadCollCell
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GarbhSamvadCollCell
        cell.setUpImgData(data: arrData[indexPath.row])
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView{
            let currPage = scrollView.currentPage
            if currPage == self.arrData.count - 1{
                self.btnRight.isHidden = true
            }else if currPage == 0{
                self.btnLeft.isHidden = true
            }else{
                btnRight.isHidden = false
                btnLeft.isHidden = false
            }
        }
    }
}

extension InfoContentVC {
    
    func getRahasyaContentDetail() {
        let param: [String: Any] = ["cat_id" : objRahasya.id]
        self.showCentralSpinner()
        KPWebCall.call.getRahasyaContent(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
             self.arrData = []
            
            if statusCode == 200, let dict = json as? NSDictionary {
                if let arrResult = dict["results"] as? [NSDictionary] {
                    for result in arrResult{
                        let obj = ModelRahasyaContent(dict: result)
                        self.arrData.append(obj)
                    }
                }
                self.collectionView.reloadData()
            } else {
                 self.showError(data: json, view: self.view)
            }
        }
    }
}
