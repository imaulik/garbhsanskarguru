//
//  PregnancyStatusView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PregnancyStatusView: ConstrainedView {

    @IBOutlet var view_label_calculator: UIControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtDay: UITextField!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var label_pregnency: JPWidthLabel!
    
    @IBOutlet var button_calculctor_ok: UIButton!
    @IBOutlet var button_calculator_cancel: UIButton!
    @IBOutlet var labelDate: JPWidthLabel!{
        didSet{
            self.labelDate.text = getPregnancyDate(date: Date())
        }
    }
    @IBOutlet var datePicker: UIDatePicker!{
        didSet{
            datePicker.maximumDate = Date()
            datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        }
    }
    @IBOutlet var viewCalculator: BaseView!{
        didSet{
            viewCalculator.isHidden = true
        }
    }
    
    var actionBlock: (KAction)->Void = {_ in}
    var selectedDate = Date()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setKeyboardNotifications()
    }
    
    deinit{
        _defaultCenter.removeObserver(self)
        kprint(items: "Deallocated: \(self.classForCoder)")
    }
    
    class func instantiateView(with view: UIView) -> PregnancyStatusView {
        let objView = UINib(nibName: "PregnancyStatusView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PregnancyStatusView
//        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: _screenSize.height)
        objView.layoutIfNeeded()
//        view.addSubview(objView)
        _appDelegator.window?.rootViewController?.view.addSubview(objView)
        return objView
    }
}

extension PregnancyStatusView {
    
    func setupUI(isCancelable:Bool = false){
        txtDay.isUserInteractionEnabled = true
        if isCancelable{
            baseView.isUserInteractionEnabled = true
        }else{
            baseView.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
      
        self.actionBlock(.done)
    }
    
    @IBAction func pregnencyCalculator_Tapped(_ sender: UIControl) {
        let minDate = Calendar.current.date(byAdding: .day, value: -280, to: Date())!
        datePicker.minimumDate = minDate
        datePicker.maximumDate = Date()
        self.labelDate.text = getPregnancyDate(date: Date())
        self.viewCalculator.isHidden = false
    }
}

extension PregnancyStatusView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtDay.resignFirstResponder()
        return true
    }
}

extension PregnancyStatusView {
    
    func setKeyboardNotifications() {
        view_label_calculator.layer.cornerRadius = view_label_calculator.frame.size.height / 2
        view_label_calculator.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let kbSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            
            var aRect = self.baseView.frame
            aRect.size.height -= kbSize.height

            if aRect.contains(txtDay.frame.origin) {
                let yInset = kbSize.height - txtDay.frame.origin.y
                print(yInset)
                let scrollPoint = CGPoint(x: 0, y: yInset)
                scrollView.setContentOffset(scrollPoint, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
}


extension PregnancyStatusView {
    
    @IBAction func btnCalculator_cancle(_ sender: UIButton) {
        if baseView.isUserInteractionEnabled{
            self.actionBlock(.cancel)
        }else{
            self.viewCalculator.isHidden = true
        }
    }
    
    @IBAction func btnCalculator_Ok(_ sender: UIControl) {
        self.viewCalculator.isHidden = true
        self.txtDay.text = selectedDate.getTotalDayofPregnent()
    }
}

extension PregnancyStatusView : UIPickerViewDelegate
{
    @objc func dateChanged(_ sender: UIDatePicker) {
        self.selectedDate = sender.date
        print("Pregnenet Day :\(selectedDate.getTotalDayofPregnent())")
        self.labelDate.text = getPregnancyDate(date: sender.date)
    }
    
    func getPregnancyDate(date : Date) -> String?{
        let formatter = datePickerFormat
        let current_dateString = formatter.string(from: date)
        let current_date = formatter.date(from: current_dateString)
        formatter.dateFormat = "EEE, MMM dd, yyyy"
        let format_string = formatter.string(from: current_date!)
        print(format_string)
        return format_string
    }

}
