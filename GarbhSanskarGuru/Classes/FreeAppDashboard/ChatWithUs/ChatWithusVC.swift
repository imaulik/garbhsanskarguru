//
//  ChatWithusVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 28/03/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit
import SearchTextField

class AutoQuestionTextFieldItem: SearchTextFieldItem {
    var id: String?
    
    init(id: String,title: String, subtitle: String?) {
        super.init(title: title, subtitle: subtitle)
        self.id = id
    }
}
 
struct QuestionAnswerList {
    var Title : String?
    var Flag : String?
}

class ChatWithusVC: UIViewController {
    
    let arrChatMessage = ["hi","Hiee","hello","good morning","good afternoon","good evening"]
    
    let kSender = "Sender"
    let kReciver = "Reciver"
    
    var isFromDashboard = false
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtQuestion: SearchTextField!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    var arrSearchQuestion:[ChatAutoQuestionList]?
    var item : AutoQuestionTextFieldItem?
    
    var arrQuestionAnswer : [QuestionAnswerList]?
    var existCV = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblChat.register(UINib(nibName: "SenderCell", bundle: nil), forCellReuseIdentifier: "SenderCell")
        self.tblChat.register(UINib(nibName: "ReciverCell", bundle: nil), forCellReuseIdentifier: "ReciverCell")
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:  UIResponder.keyboardWillHideNotification, object: nil)

        self.title = "Chat With Us"
        self.txtQuestion.text = ""
        var qaList = QuestionAnswerList()
        qaList.Title = "Please call/WhatsApp us on 9727006001 or write us at garbhsanskarguru@gmail.com"
        qaList.Flag = kReciver
        arrQuestionAnswer = [qaList]
        self.tblChat.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
           
    }
    
    @IBAction func btnSendPress(_ sender: Any) {
        if txtQuestion.text?.trimmedString() != "" {
            let searchText = txtQuestion.text ?? ""
            var qaList = QuestionAnswerList()
            qaList.Title = searchText
            qaList.Flag = kSender
            self.arrQuestionAnswer?.append(qaList)
            let filteredStrings = arrChatMessage.filter({(item: String) -> Bool in
                
                let stringMatch = item.lowercased().range(of: searchText.lowercased())
                return stringMatch != nil ? true : false
            })
            
            qaList = QuestionAnswerList()
            if (filteredStrings as NSArray).count > 0{
                qaList.Title = "Hello, How may I Help you?"
            }else{
                qaList.Title = "Please call/WhatsApp us on 9727006001 or write us at : garbhsanskarguru@gmail.com"
            }
            
            qaList.Flag = self.kReciver
            self.arrQuestionAnswer?.append(qaList)
            txtQuestion.text = ""
            self.tblChat.reloadData()
            self.scrollToBottom()
        }
        else {
            self.showErrorView(data: "please enter question", view: self.view)
//            showAlert("please enter question")
        }
    }
 
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardRectValue.height
            if isFromDashboard == true{
                let safeAreaBottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
                bottomHeight.constant = keyboardHeight - safeAreaBottomInset
            }
            else {
                let tabBarController = UITabBarController()
                let tabBarHeight = tabBarController.tabBar.frame.size.height
                var bottomSafeAreaHeight: CGFloat = 0
                let window = UIApplication.shared.windows[0]
                let safeFrame = window.safeAreaLayoutGuide.layoutFrame
                bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
                bottomHeight.constant = keyboardHeight - tabBarHeight - bottomSafeAreaHeight
            }
        }
        
}
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            bottomHeight.constant = 5
        }
    }
    
    
    func scrollToBottom()  {
        let indexPath = NSIndexPath(item: (arrQuestionAnswer?.count ?? 0) - 1, section: 0)
        self.tblChat.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
    }
    
    func searchQuestion(){
        var results = [AutoQuestionTextFieldItem]()
        for result in arrSearchQuestion!{
            results.append(AutoQuestionTextFieldItem(id: result.id, title: result.question, subtitle:""))
            
        }
        
        txtQuestion.theme.bgColor = UIColor.white
        txtQuestion.theme.borderColor = UIColor.lightGray
        txtQuestion.theme.separatorColor = UIColor.black.withAlphaComponent(0.5)
        txtQuestion.theme.cellHeight = 50
        
        txtQuestion.comparisonOptions = [.caseInsensitive,.anchored]
        txtQuestion.forceRightToLeft = false
        
        txtQuestion.highlightAttributes = [NSAttributedString.Key.backgroundColor: UIColor.clear, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12)]
        txtQuestion.filterItems(results)
        txtQuestion.itemSelectionHandler = { filteredResults, itemPosition in
            
            self.item = filteredResults[itemPosition] as? AutoQuestionTextFieldItem
            self.txtQuestion.text = self.item?.title
            self.view.endEditing(true)
            var qaList = QuestionAnswerList()
            qaList.Title =  self.item?.title
            qaList.Flag = self.kSender
            self.arrQuestionAnswer?.append(qaList)
            self.tblChat.reloadData()
            let parm = ["user_id":_user.id,"question_id":self.item?.id ?? ""]
//            self.callWs_GetChatAnswer(parms:parm) { myAnswer in
//                guard let  answer  = myAnswer else {
//                    return
//                }
//                var qaList = QuestionAnswerList()
//                qaList.Title = answer.data?[0].answer
//                qaList.Flag = self.kReciver
//                self.arrQuestionAnswer?.append(qaList)
//                self.tblChat.reloadData()
//                self.scrollToBottom()
//                self.txtQuestion.text = ""
//            }
        }
    }
}


extension ChatWithusVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestionAnswer?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = arrQuestionAnswer?[indexPath.row]
        
        if obj?.Flag == kReciver {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReciverCell", for: indexPath) as! ReciverCell
            cell.lblReciver.text = obj?.Title
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! SenderCell
            cell.lblSenderMsg.text =  obj?.Title
            cell.selectionStyle = .none
            return cell
        }
    }
}


extension ChatWithusVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.trimmedString() != "" {
            let parms = ["user_id" :_user.id ,"question_text" : textField.text ?? ""]
//            callWs_GetChatQuestion(parms: parms) { userQuestions in
//                guard let  questionList  = userQuestions else {
//                    return
//                }
//                self.arrSearchQuestion = questionList
//                self.searchQuestion()
//            }
        }
        return true
    }
}
