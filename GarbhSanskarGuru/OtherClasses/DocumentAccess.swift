//
//  DocumentAccess.swift
//  Cherish
//
//  Created by Yudiz Solutions Pvt. Ltd. on 03/03/15.
//  Copyright (c) 2015 Yudiz Pvt Solution Ltd. All rights reserved.
//

import UIKit

class DocumentAccess: NSObject {
    
    static var shared = DocumentAccess()
    
    private var fileManager: FileManager {
        return FileManager.default
    }
    
    private var audioFolderPath: URL{
        return URL(fileURLWithPath: self.cacheFolderPath("audio"))
    }
    
    private var tempVidPath: String {
        return self.tempFolderPath("temp_media")
    }
    
    // MARK: Paths For Directory
    private func cacheFolderPath(_ foldername: String) -> String {
        let paths = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        let cPath = paths.first!.appendingPathComponent(foldername).path
        kprint(items: cPath)
        if !fileManager.fileExists(atPath: cPath) {
            do {
                try fileManager.createDirectory(atPath: cPath, withIntermediateDirectories: true, attributes: nil)
            } catch let err {
                kprint(items: err.localizedDescription)
            }
        }
        return cPath
    }
    
    private func tempFolderPath(_ foldername: String) -> String {
        let tempDirectory = URL(fileURLWithPath: NSTemporaryDirectory())
        let tPath = tempDirectory.appendingPathComponent(foldername).path
        kprint(items: tPath)
        if !fileManager.fileExists(atPath: tPath) {
            do {
                try fileManager.createDirectory(atPath: tPath, withIntermediateDirectories: true, attributes: nil)
            } catch let err {
                kprint(items: err.localizedDescription)
            }
        }
        return tPath
    }
    
    // MARK : Audio File
    // It will remove old file will give new file path
    func createAudioFileUrl(_ fileName: String) -> URL {
        let fullPath = audioFolderPath.appendingPathComponent(fileName).path
        kprint(items: fullPath)
        if fileManager.fileExists(atPath: fullPath) {
            do {
                try fileManager.removeItem(atPath: fullPath)
            } catch let err {
                kprint(items: err.localizedDescription)
            }
        }
        return URL(fileURLWithPath: fullPath)
    }

    func removeAudioFromPath(_ fileName: String) -> Bool {
        let fullPath = audioFolderPath.appendingPathComponent(fileName).path
        do {
            try fileManager.removeItem(atPath: fullPath)
            return true
        } catch let err {
            kprint(items: err.localizedDescription)
            return false
        }
    }

    // If NSURL is nil then file is stored on cached directory.
    func getAudioUrl(_ fileName: String) -> URL? {
        let fullPath = audioFolderPath.appendingPathComponent(fileName).path
        if fileManager.fileExists(atPath: fullPath){
            return URL(fileURLWithPath: fullPath)
        }else{
            return nil
        }
    }
    
    func randomAudioFileName() -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyyMMddhhmmssSSSS"
        let strDate = dateFormat.string(from: Date())
        return "\(strDate).m4a"
    }
    
    func removeZeroSizeFileFromApp(){
        var fileSize : UInt64 = 0
        let files = fileManager.enumerator(atPath: audioFolderPath.path)
        while let file = files?.nextObject() {
            do {
                let filePath = audioFolderPath.appendingPathComponent(file as! String).absoluteString
                let attr: NSDictionary? = try fileManager.attributesOfItem(atPath: filePath) as NSDictionary
                if let _attr = attr {
                    fileSize = _attr.fileSize();
                    if fileSize == 0{
                        kprint(items: "file Size \(fileSize)")
                        _ = removeAudioFromPath(file as! String)
                    }
                }
            } catch let err {
                kprint(items: err.localizedDescription)
            }
        }
    }

    //Rename Audio File
    func renameAudioFile(fileFrom: String, fileTo: String){
        let fullPathTo = audioFolderPath.appendingPathComponent(fileTo).path
        let fullPathFrom = audioFolderPath.appendingPathComponent(fileFrom).path
        do {
            try fileManager.moveItem(atPath: fullPathFrom, toPath: fullPathTo)
        }catch let err {
            kprint(items: err.localizedDescription)
        }
    }
   
//    private func documentFolderPath(foldername: String) -> String {
//        // Changing directory to cache .DocumentDirectory
//        var paths =  NSSearchPathForDirectoriesInDomains(.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true)
//        let cacheDirectory = paths[0] as NSString
//        let dPath = cacheDirectory.stringByAppendingPathComponent(foldername)
//        if !fileManager.fileExistsAtPath(dPath) {
//            do {
//                try fileManager.createDirectoryAtPath(dPath, withIntermediateDirectories: true, attributes: nil)
//            } catch _ {
//            }
//        }
//        return dPath
//    }
//
//    // MARK : Temp Directory Video Store
//    func flushTheTempDirectory() -> Bool {
//        let success: Bool
//        do {
//            try fileManager.removeItemAtPath(tempVidPath as String)
//            success = true
//        } catch _ {
//            success = false
//        }
//        return success
//    }
//
//    func randomVideoUrlofTempDirectory() -> NSURL {
//        let fileName = randomVideoFileName()
//        let fullpath = tempVidPath.stringByAppendingPathComponent(fileName)
//        return NSURL(fileURLWithPath: fullpath)
//    }
//
//    // MARK: Store Media To Cache
//    func setMedia(medData: NSData, forName filename: String) -> Bool {
//        let fullpath = imageFolderPath.stringByAppendingPathComponent(filename)
//        return fileManager.createFileAtPath(fullpath, contents: medData, attributes: nil)
//    }
//
//    func mediaUrlForName(filename: String) -> NSURL? {
//        let fullpath = imageFolderPath.stringByAppendingPathComponent(filename)
//        if fileManager.fileExistsAtPath(fullpath) {
//            return NSURL(fileURLWithPath: fullpath)
//        } else {
//            return nil
//        }
//    }
//
//    func removeMediaForName(filename: String) -> Bool {
//        let fullpath = imageFolderPath.stringByAppendingPathComponent(filename)
//        do {
//            try fileManager.removeItemAtPath(fullpath)
//            return true
//        } catch _ {
//            return false
//        }
//    }
//
//    func renameMediaForName(filename: String) -> Bool {
//        let fullpath = audioFolderPath.stringByAppendingPathComponent(filename)
//        do {
//            try fileManager.moveItemAtPath(fullpath, toPath:filename)
//            return true
//        } catch _ {
//            return false
//        }
//    }
//
//    // MARK: Store Image to Cache
//    /* we will append _thumb next to its name that way we can store both thumb and full image */
//    func imageForName(filename: String, isthumb: Bool) -> UIImage? {
//        let fullpath = imageFolderPath.stringByAppendingPathComponent(filename + (isthumb ? "_thumb" : ""))
//        return UIImage(contentsOfFile: fullpath)
//    }
//
//    func setImage(img: UIImage, isthumb: Bool, forName name: String) -> Bool {
//        let fullpath = imageFolderPath.stringByAppendingPathComponent(name + (isthumb ? "_thumb" : ""))
//        return fileManager.createFileAtPath(fullpath, contents: UIImageJPEGRepresentation(img, 1), attributes: nil)
//    }
//
//
//    // MARK : Audio File
//    // It will remove old file will give new file path
//    func createAudioFileUrl(fileName: String) -> NSURL {
//        let fullPath = audioFolderPath.stringByAppendingPathComponent(fileName) as String
//        if fileManager.fileExistsAtPath(fullPath) {
//            do {
//                try fileManager.removeItemAtPath(fullPath)
//            } catch _ {
//            }
//        }
//        return  NSURL(fileURLWithPath: fullPath)
//    }
//
//    //Rename Audio File
//    func renameAudioFile(fileTo: String, fileFrom: String){
//        let fullPathTo = audioFolderPath.stringByAppendingPathComponent(fileTo) as String
//        let fullPathFrom = audioFolderPath.stringByAppendingPathComponent(fileFrom) as String
//        do{
//            try fileManager.moveItemAtPath(fullPathTo, toPath: fullPathFrom)
//        }catch let err{
//            jprint(err)
//        }
//    }
//
//    //It will give path
//    func getAudioFileURL(fileName: String) -> NSURL {
//        let fullPath = audioFolderPath.stringByAppendingPathComponent(fileName) as String
//        return NSURL(fileURLWithPath: fullPath)
//    }
//
//    func setAudio(audio: NSData, forName name:String) -> NSURL? {
//        let fullPath = audioFolderPath.stringByAppendingPathComponent(name) as String
//        if audio.writeToFile(fullPath, atomically: true) {
//            return NSURL(fileURLWithPath: fullPath)
//        } else {
//            return nil;
//        }
//    }
//
//    func removeZeroSizeFileFromApp(){
//
//        var fileSize : UInt64 = 0
//        let files = fileManager.enumeratorAtPath(audioFolderPath as String)
//        while let file = files?.nextObject() {
//            do {
//                let filePath = audioFolderPath.stringByAppendingPathComponent(file as! String) as String
//                let attr : NSDictionary? = try fileManager.attributesOfItemAtPath(filePath)
//
//                if let _attr = attr {
//                    fileSize = _attr.fileSize();
//                    if fileSize == 0{
//                        jprint("file Size \(fileSize)")
//                        removeAudioFromPath(file as! String)
//                    }
//                }
//            } catch {
//                jprint("Error: \(error)")
//            }
//        }
//    }
//
//    func removeAudioFromPath(name: String) -> Bool {
//        let fullPath = audioFolderPath.stringByAppendingPathComponent(name) as String
//        do {
//            try fileManager.removeItemAtPath(fullPath)
//            return true
//        } catch _ {
//            return false
//        }
//    }
//
//    // If NSURL is nil then file is stored on cached directory.
//    func getAudioForName(name: String) -> NSURL? {
//        let fullPath = audioFolderPath.stringByAppendingPathComponent(name) as String
//        if NSFileManager.defaultManager().fileExistsAtPath(fullPath) {
//           return  NSURL(fileURLWithPath: fullPath)
//        } else {
//            return nil
//        }
//    }
//
//    //MARK: for images
//    func getImageForName(name: String) -> NSURL? {
//        let fullPath = imageFolderPath.stringByAppendingPathComponent(name) as String
//        if NSFileManager.defaultManager().fileExistsAtPath(fullPath) {
//            return  NSURL(fileURLWithPath: fullPath)
//        } else {
//            return nil
//        }
//    }
//
//    func saveImage (image: UIImage, name: String) -> NSURL?{
//        let fullPath = imageFolderPath.stringByAppendingPathComponent(name) as String
//        //let pngImageData = UIImagePNGRepresentation(image)
//        let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
//        if jpgImageData!.writeToFile(fullPath, atomically: true){
//            return NSURL(fileURLWithPath: fullPath)
//        }else{
//            return nil
//        }
//    }
//
//    //Rename Audio File
//    func renameImageFile(fileTo: String, fileFrom: String){
//        let fullPathTo = imageFolderPath.stringByAppendingPathComponent(fileTo) as String
//        let fullPathFrom = imageFolderPath.stringByAppendingPathComponent(fileFrom) as String
//        do{
//            try fileManager.moveItemAtPath(fullPathTo, toPath: fullPathFrom)
//        }catch let err{
//            jprint(err)
//        }
//    }
//
//    //It will give path
//    func getImageFileURL(fileName: String) -> NSURL {
//        let fullPath = imageFolderPath.stringByAppendingPathComponent(fileName) as String
//        return NSURL(fileURLWithPath: fullPath)
//    }
//
//    // MARK : Randome Names
//    func randomFileName() -> String {
//        let dateFormat = NSDateFormatter()
//        dateFormat.dateFormat = "yyyyMMddhhmmssSSSS"
//        let strDate = dateFormat.stringFromDate(NSDate())
//        return strDate
//    }
//
//    func randomAudioFileName() -> String {
//        let dateFormat = NSDateFormatter()
//        dateFormat.dateFormat = "yyyyMMddhhmmssSSSS"
//        let strDate = dateFormat.stringFromDate(NSDate())
//        return "\(strDate).m4a"
//    }
//
//    func randomVideoFileName() -> String {
//        let dateFormat = NSDateFormatter()
//        dateFormat.dateFormat = "yyyyMMddhhmmssSSSS"
//        let strDate = dateFormat.stringFromDate(NSDate())
//        return "\(strDate).mov"
//    }
}

