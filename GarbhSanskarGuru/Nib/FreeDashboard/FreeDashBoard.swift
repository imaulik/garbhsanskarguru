//
//  FreeDashBoard.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 29/04/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class FreeDashBoard: UIView {
    
    var actionBlock: (KAction)->Void = {_ in}
    
    @IBOutlet var animatedViews: UIView!
    
    @IBOutlet var buttonYes: UIButton!
    @IBOutlet var buttonLater: UIButton!
    
    @IBOutlet var label_title: JPWidthLabel!
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> FreeDashBoard {
        let objView = UINib(nibName: "FreeDashBoard", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FreeDashBoard
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
    
    func updateUI(){
        let yes_button_title = _appDelegator.languageBundle?.localizedString(forKey: "title_button_yes", value: "", table: nil)
        let later_button_title = _appDelegator.languageBundle?.localizedString(forKey: "title_button_later", value: "", table: nil)
        buttonYes.setTitle(yes_button_title, for: .normal)
        buttonLater.setTitle(later_button_title, for: .normal)
        label_title.text = _appDelegator.languageBundle?.localizedString(forKey: "fressdashboard_view_title", value: "", table: nil)
    }
    
    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews.transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
            
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.animatedViews.transform = .identity
            }, completion: { _ in
            })
        })
    }
    
    
    @IBAction func onBtnYesAction(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func onBtnLaterAction(_ sender: UIButton) {
        self.actionBlock(.later)
    }
    
}
