//
//  CourseFeedBack.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 01/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import AVKit

class CourseFeedBackVC: UIViewController {
   
    @IBOutlet weak var cvCourseFeedBack: UICollectionView!
    var dicWebFirstInfo :  WebinarInfo?
    var arrWebinarList : [WebinarInfo]?
    var strLanguage = _appDelegator.getLanguage()
    var refresher:UIRefreshControl!
    var arrFinalWebinarList : [WebinarInfo]?
    var strCourseTrusedby = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvCourseFeedBack.register(UINib(nibName: "PagerCVCell", bundle: .main), forCellWithReuseIdentifier: "PagerCVCell")
        
        cvCourseFeedBack.register(UINib(nibName: "WebinarView", bundle: nil),
                           forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                           withReuseIdentifier: "WebinarView")
        
        self.refresher = UIRefreshControl()
        self.cvCourseFeedBack!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(getCourseFeed), for: .valueChanged)
        self.cvCourseFeedBack!.addSubview(refresher)
        // Do any additional setup after loading the view.
        registerBlock()

    }
    
    override func viewWillAppear(_ animated: Bool) {
         strLanguage = _appDelegator.getLanguage()
        getCourseFeed()
        getStatisticsData()
    }
    
    func registerBlock(){
        _appDelegator.detectBackFromCourseFeedBack = { () in
             self.removeBlankValuefromWebinar()
        }
    }
   
    @IBAction func btnCall(_ sender: Any) {
        makeCall(number: "919727006001")
    }

    @IBAction func btnSubscribe(_ sender: UIButton)
    {
        let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PackagesVC") as! PackagesVC
        self.navigationController?.pushViewController(packagesVC, animated: true)
    }
}

extension CourseFeedBackVC  {
    
    func getStatisticsData() {
           KPWebCall.call.getStatisticsData {[weak self] (json, statusCode) in
              guard let weakself = self else {return}
             
              if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                  let data  = dict ["data"] as? NSDictionary
                if self?.strLanguage == kEnglish {
                    self?.strCourseTrusedby  = "Course is trusted by\n\(data?["mother"] ?? "2000")+ mothers"
                }else if self?.strLanguage == kGujrati {
                    self?.strCourseTrusedby = "\(data?["mother"] ?? "2000")+ માતાઓએ ભરોસો કરેલ કોર્સ"
                }else if self?.strLanguage == KHindi {
                    self?.strCourseTrusedby = "\(data?["mother"] ?? "2000")+ माताओ द्वारा भरोसा किया हुआ कोर्स"
                }
                self?.cvCourseFeedBack.reloadData()
              }
          }
      }
    
    @objc func getCourseFeed() {
        KPWebCall.call.getCourseFeedback {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                self?.refresher.endRefreshing()
                let res = dict["data"]
                self?.arrWebinarList =  [WebinarInfo](dictionaryArray:res as! [NSDictionary])
                if self?.arrWebinarList?.count ?? 0 > 0 {
                    self?.dicWebFirstInfo = self?.arrWebinarList?.first
                    self?.arrWebinarList?.remove(at: 0)
                }
                self?.removeBlankValuefromWebinar()
            }
        }
    }

    func removeBlankValuefromWebinar (){
         strLanguage = _appDelegator.getLanguage()
         if self.strLanguage == kEnglish {
             self.arrFinalWebinarList =  self.arrWebinarList?.filter({ $0.eng_image != ""})
         }else if self.strLanguage == kGujrati {
             self.arrFinalWebinarList =  self.arrWebinarList?.filter({ $0.guj_image != ""})
         }else if self.strLanguage == KHindi {
             self.arrFinalWebinarList =  self.arrWebinarList?.filter({ $0.hindi_image != ""})
         }
         self.cvCourseFeedBack.reloadData()
     }
}

 // functions
 extension CourseFeedBackVC :UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

         // Get the view for the first header
         let indexPath = IndexPath(row: 0, section: section)
         let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)

         // Use this view to calculate the optimal size based on the collection view's width
         return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
                                                   withHorizontalFittingPriority: .required, // Width is fixed
                                                   verticalFittingPriority: .fittingSizeLevel) // Height can be as large as needed
     }
    
     @objc func btnPlayVideo(sender: UIButton) {
         var strVideo = ""
         if self.strLanguage == kEnglish {
             strVideo = dicWebFirstInfo?.eng_video ?? ""
         }else if self.strLanguage == kGujrati {
             strVideo = dicWebFirstInfo?.guj_video ?? ""
         }else if self.strLanguage == KHindi {
             strVideo = dicWebFirstInfo?.hindi_video ?? ""
         }
         
         if strVideo != "" {
             videoPlay(strVideo: strVideo)
         }
     }
     
     
     
     func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
         
         let friendHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WebinarView", for: indexPath) as! WebinarView
        
       //  કોર્સ યુઝર્સના અનુભવ
        
        friendHeader.lblHeaderTitle.text = strCourseTrusedby
        
        friendHeader.lblHeaderTitle.textAlignment = .center
        friendHeader.lblHeaderTitle.font = FontBook.Avenir_Medium.of(25)
        
        friendHeader.lblHeaderDescription.textAlignment = .center
        friendHeader.lblHeaderDescription.font = FontBook.Avenir_Light.of(15)
      
        if strLanguage == kEnglish {
            friendHeader.lblHeaderDescription.text = "Few experiences of course users"
        }else if strLanguage == kGujrati {
            friendHeader.lblHeaderDescription.text = "કોર્સ યુઝર્સના અનુભવ"
        }else if strLanguage == KHindi {
            friendHeader.lblHeaderDescription.text = "कोर्स के यूजर्स के अनुभव"
        }
    
        
        if let dic = dicWebFirstInfo {
            
             friendHeader.lblNoData.isHidden =  true
             friendHeader.imgVideo.isHidden = false
             friendHeader.btnPlay.isHidden = false
             var strImage : String?
             
             if self.strLanguage == kEnglish {
                 strImage = dic.eng_image
             }else if self.strLanguage == kGujrati {
                 strImage = dic.guj_image
             }else if self.strLanguage == KHindi {
                 strImage = dic.hindi_image
             }
            
            
             if strImage == "" {
                 friendHeader.lblNoData.isHidden =  false
                 friendHeader.imgVideo.isHidden = true
                 friendHeader.btnPlay.isHidden = true
             }else {
                 friendHeader.btnPlay.addTarget(self, action: #selector(btnPlayVideo), for:.touchUpInside)
                 friendHeader.imgVideo.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
             }
             
             
             if arrWebinarList?.count ?? 0 > 0 {
                 friendHeader.noWebinarView.isHidden = true
             }else {
                 friendHeader.noWebinarView.isHidden = false
             }
             
         }else{
             friendHeader.lblNoData.isHidden = false
             friendHeader.imgVideo.isHidden = true
             friendHeader.btnPlay.isHidden = true
         }

        return friendHeader
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2) - 5  , height: UIScreen.width / 3)
     }
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return arrFinalWebinarList?.count ?? 0
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"PagerCVCell",    for: indexPath) as! PagerCVCell
         let dic  = arrFinalWebinarList?[indexPath.row]
         
         var strImage : String?
         if self.strLanguage == kEnglish {
             strImage = dic?.eng_image
         }else if self.strLanguage == kGujrati {
             strImage = dic?.guj_image
         }else if self.strLanguage == KHindi {
             strImage = dic?.hindi_image
         }
         
         cell.imgPage.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
         cell.layoutIfNeeded()
         return cell
     }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
         var strVideo = ""
         let dic = arrFinalWebinarList?[indexPath.row]
         if self.strLanguage == kEnglish {
             strVideo = dic?.eng_video ?? ""
         }else if self.strLanguage == kGujrati {
             strVideo = dic?.guj_video ?? ""
         }else if self.strLanguage == KHindi {
             strVideo = dic?.hindi_video ?? ""
         }
         
         if strVideo != "" {
             videoPlay(strVideo: strVideo)
         }
     }
 }



