//
//  AudioCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 08/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class AudioCell: UITableViewCell {
    @IBOutlet weak var lblAudio: UILabel!
    @IBOutlet weak var imgAudio: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
