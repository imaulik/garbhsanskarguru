//
//  BasicInfoCell.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

enum ScreenType {
    case basicInfoVC
    case giftVC
}

enum BasicDetailCell: String {
    
    case dataCell = "dataCell"
    case spaceCell = "spaceCell"
    case textCell = "txtcell"
    case phonecell = "txtPhoneNumber"
    
    var cellHeight: CGFloat {
        switch self {
        case .dataCell:
            return 0.heightRatio
        case .spaceCell:
            return 100.heightRatio
        case .textCell:
            return 60.heightRatio
        case .phonecell:
            return 60.heightRatio
        }
    }
}

class BasicInfoCell: UITableViewCell {

    @IBOutlet weak var tfInput: UITextField!
    @IBOutlet var labelDesc: JPWidthLabel!
    @IBOutlet var labelTitle: JPWidthLabel!
    
    var screenType: ScreenType = .basicInfoVC
    
    var isFromSignUp:Bool? = false
    weak var signupParentVC: SignUpVC3!
    weak var parentVC: BasicDetailVC!
    weak var parentGift: GiftAppVC!
    
    var code:String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        code = "\(_appDelegator.getCountryCode())"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpBasicUI(tag: Int, data: BasicDetailCell) {
        switch screenType {
            
        case .basicInfoVC:
            switch data {
            case .dataCell: break
            case .spaceCell: break
            case .textCell:
                tfInput.tag = tag
                tfInput.autocorrectionType = .no
                tfInput.autocapitalizationType = .words
                tfInput.keyboardType = .asciiCapable
                tfInput.returnKeyType = .next
                tfInput.isSecureTextEntry = false
                tfInput.inputAccessoryView = nil
                switch tag {
                case 1:
                    tfInput.placeholder = "Country"
                    tfInput.text = isFromSignUp! ? signupParentVC.basicInfoData.country : parentVC.basicInfoData.country
                    break
                case 2:
                    tfInput.placeholder = "State"
                    tfInput.text = isFromSignUp! ? signupParentVC.basicInfoData.state : parentVC.basicInfoData.state
                    break
                case 3:
                    tfInput.placeholder = "City"
                    tfInput.text = isFromSignUp! ? signupParentVC.basicInfoData.city : parentVC.basicInfoData.city
                    tfInput.returnKeyType = .done
                    break
                default:
                    break
                }
            case .phonecell: break
                
            }
        case .giftVC:
            switch data {
            case .dataCell:
                labelTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "gift_desc", value: "", table: nil)
                labelDesc.text = _appDelegator.languageBundle?.localizedString(forKey: "gift_title", value: "", table: nil)
            case .spaceCell:
                break
            case .textCell:
                tfInput.tag = tag
                tfInput.autocorrectionType = .no
                tfInput.autocapitalizationType = .none
                tfInput.keyboardType = .asciiCapable
                tfInput.returnKeyType = .next
                tfInput.isSecureTextEntry = false
                tfInput.inputAccessoryView = nil
                switch tag {
                case 1:
                    tfInput.placeholder = "Full Name of Gift Purchaser"
                    tfInput.text = parentGift.basicInfoData.fullName
                    tfInput.autocapitalizationType = .words
                    break
                case 2:
                    tfInput.placeholder = "Email of Gift Purchaser"
                    tfInput.text = parentGift.basicInfoData.email
                    tfInput.keyboardType = .emailAddress
                    break
                case 4:
                    tfInput.placeholder = "How many gift code you wants to buy?"
                    tfInput.text = parentGift.basicInfoData.people
                    tfInput.keyboardType = .numberPad
                    tfInput.inputAccessoryView = parentGift.accessoryView
                    break
                case 5:
                    tfInput.placeholder = "Coupon code (if any)"
                    tfInput.text = parentGift.basicInfoData.referral
                    tfInput.returnKeyType = .done
                    break
                default:
                    break
                }
            case .phonecell:
                tfInput.tag = tag
                tfInput.autocorrectionType = .no
                tfInput.autocapitalizationType = .none
                tfInput.keyboardType = .phonePad
                tfInput.returnKeyType = .next
                tfInput.isSecureTextEntry = false
                tfInput.inputAccessoryView = parentGift.accessoryView
                switch tag {
                case 3:
                    tfInput.placeholder = "Your mobile number without country code"
                    tfInput.text = code + parentGift.basicInfoData.contactNumber
                    tfInput.returnKeyType = .done
                    break
                default:
                    break
                }
            }
        }
    }
}

extension BasicInfoCell: UITextFieldDelegate {
    
    @IBAction func tfEditing(_ textfield: UITextField) {
        switch screenType {
        case .basicInfoVC:
            if textfield.tag == 1 {
                if isFromSignUp!{
                    signupParentVC.basicInfoData.country = textfield.text!.trimmedString()
                }else{
                    parentVC.basicInfoData.country = textfield.text!.trimmedString()
                }
            }else if textfield.tag == 2 {
                if isFromSignUp!{
                    signupParentVC.basicInfoData.state = textfield.text!.trimmedString()
                }else{
                    parentVC.basicInfoData.state = textfield.text!.trimmedString()
                }
            }else {
                if isFromSignUp!{
                    signupParentVC.basicInfoData.city = textfield.text!.trimmedString()
                }else{
                    parentVC.basicInfoData.city = textfield.text!.trimmedString()
                }
            }
        case .giftVC:
            if textfield.tag == 1 {
                parentGift.basicInfoData.fullName = textfield.text!.trimmedString()
            }else if textfield.tag == 2 {
                parentGift.basicInfoData.email = textfield.text!.trimmedString()
            }else if textfield.tag == 3 {
                parentGift.basicInfoData.contactNumber = textfield.text!.trimmedString()
            }else if textfield.tag == 4 {
                parentGift.basicInfoData.people = textfield.text!.trimmedString()
            }else {
                parentGift.basicInfoData.referral = textfield.text!.trimmedString()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 3{
            let fullText = (textField.text! as NSString).replacingCharacters(in: range, with: string);
            return fullText.count >= 1
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch screenType {
        case .basicInfoVC:
            if textField.returnKeyType == .next{
                if isFromSignUp!{
                    if let index = signupParentVC.tableView.indexPath(for: self){
                        if let cell = signupParentVC.getEntryCell(index.row + 1){
                            cell.tfInput.becomeFirstResponder()
                        }
                    }else{
                        textField.resignFirstResponder()
                    }
                }else{
                    if let index = parentVC.tableView.indexPath(for: self){
                        if let cell = parentVC.getEntryCell(index.row + 1){
                            cell.tfInput.becomeFirstResponder()
                        }
                    }else{
                        textField.resignFirstResponder()
                    }
                }
            }else{
                textField.resignFirstResponder()
            }
        case .giftVC:
            if textField.returnKeyType == .next{
                if let index = parentGift.tableView.indexPath(for: self){
                    if let cell = parentGift.getEntryCell(index.row + 1){
                        cell.tfInput.becomeFirstResponder()
                    }
                }else{
                    textField.resignFirstResponder()
                }
            }else{
                textField.resignFirstResponder()
            }
        }
        return true
    }
}


