//
//  TaskCompleteView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class TaskCompleteView: ConstrainedView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var baseView: UIView!
    
    var actionBlock: (KAction)->Void = {_ in}
    
    class func instantiateView(with view: UIView, with title: String) -> TaskCompleteView {
        let objView = UINib(nibName: "TaskCompleteView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TaskCompleteView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        objView.lblTitle.text = "Are you sure want to complete \(title) Task ?"
        view.addSubview(objView)
        return objView
    }
    
    class func instantiatePuzzleView(with view: UIView, with title: String) -> TaskCompleteView {
        let objView = UINib(nibName: "TaskCompleteView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TaskCompleteView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        objView.lblTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "We are sure that you have solved this puzzle / question by yourself. Do you want to check the answer?", value: "", table: nil)
        view.addSubview(objView)
        return objView
    }
}

extension TaskCompleteView {
    
    @IBAction func btnYesTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func btnLaterTapped(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
}
