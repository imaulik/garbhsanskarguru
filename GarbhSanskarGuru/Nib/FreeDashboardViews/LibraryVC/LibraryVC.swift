//
//  LibraryVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 06/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class LibraryVC: ParentViewController {

    @IBOutlet weak var libCV: UICollectionView!

    var arrTipsCategory = [TipsCategoryList]()
    var dashboardSelItem : DashboardList?
    var strLanguage = kEnglish
    
    @IBOutlet weak var warningView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.libCV.register(UINib(nibName: "LibrayCell", bundle: .main), forCellWithReuseIdentifier: "LibrayCell")
        
        strLanguage = _appDelegator.getLanguage()
        if dashboardSelItem?.category_name == DashBoardCategoryList.GSLibrary.rawValue{
            self.title = "Select Library Category"
            getCategyList(api: "libraryCategory")
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSVideo.rawValue{
            self.navigationItem.title = "Select Video Category"
            warningView.isHidden = false
            getCategyList(api: "videoCategory")
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSAudio.rawValue{
            self.title = "Select Audio Category"
            warningView.isHidden = false
            getCategyList(api: "audiocategory")
        }
        else  {
            self.title = "Tips".localized
             getCategyList(api: "tipsCategory")
        }
       
    }
    
    func getCategyList(api: String) {
        let param = ["user_id" : _user.id]
                
        self.showCentralSpinner()
        KPWebCall.call.getCategoryList(endPoint: api, param: param) {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let result = TipsViewModel(dictionary: dict)
                guard let arrList = result.data else { return }
                weakself.arrTipsCategory = arrList
                weakself.libCV.reloadData()
            }
        }
        
//        callWS_CategoryList(endPoint: api, parms: parms) { data in
//            guard let arrList = data else {
//                return
//            }
//
//            self.arrTipsCategory = arrList
//            self.libCV.reloadData()
//        }
    }
}
 
extension LibraryVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if dashboardSelItem?.category_name == DashBoardCategoryList.GSLibrary.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSAudio.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSVideo.rawValue{
            return CGSize(width: _screenSize.width / 2 - 10, height: _screenSize.width / 2)
        }
        else  {
            return CGSize(width: _screenSize.width - 10 , height: 150)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             return arrTipsCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"LibrayCell", for: indexPath) as! LibrayCell
         let obj = arrTipsCategory[indexPath.row]
        if dashboardSelItem?.category_name == DashBoardCategoryList.GSAudio.rawValue{
            cell.lblLib.text = obj.name
        }
        else if strLanguage == KHindi {
            cell.lblLib.text = obj.hindi_name
        }else if strLanguage != kGujrati {
            cell.lblLib.text = obj.guj_name
        }else{
            cell.lblLib.text = obj.eng_name
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrTipsCategory[indexPath.row]
        if dashboardSelItem?.category_name == DashBoardCategoryList.GSTips.rawValue{
            let gallery = freeAppDashboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            gallery.arrSelectedTips = obj
            gallery.isFromTips = true
            self.navigationController?.pushViewController(gallery, animated: true)
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSLibrary.rawValue{
            let libVC = freeAppDashboard.instantiateViewController(withIdentifier: "LibraryDetailVC") as! LibraryDetailVC
            libVC.arrSelectedTips = obj
            self.navigationController?.pushViewController(libVC, animated: true)
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSVideo.rawValue{
            let videoVC = freeAppDashboard.instantiateViewController(withIdentifier: "VideoVC") as! VideoVC
            videoVC.arrSelectedTips = obj
            self.navigationController?.pushViewController(videoVC, animated: true)
        } else if dashboardSelItem?.category_name == DashBoardCategoryList.GSAudio.rawValue{
            let audioVC = freeAppDashboard.instantiateViewController(withIdentifier: "AudioVC") as! AudioVC
            audioVC.arrSelectedTips = obj
            self.navigationController?.pushViewController(audioVC, animated: true)
        }
        else {
            
        }
    }
}
