//
//  EventDetailVC.swift
//  GarbhSanskarGuru
//
//  Created by Maulik Shah on 10/12/21.
//  Copyright © 2021 Jayesh Tejwani. All rights reserved.
//

import UIKit
import SDWebImage
class EventDetailVC: UIViewController {

    let strLanguage = _appDelegator.getLanguage()
    var is_Notify = 0
    var eventid = ""
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblhrs: UILabel!
    @IBOutlet weak var lblMins: UILabel!
    @IBOutlet weak var lblSec: UILabel!
    @IBOutlet weak var lblNotifyMessage: UILabel!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblLiveNow: UILabel!
    
    var eventDetail = NSDictionary()
  
    
    var timer = Timer()
    var date =  NSDate()
    
    var joinNow = false
    
    var eventLink = ""
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setdata()
        
        // Do any additional setup after loading the view.
        self.colorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(isnotifycheck)))
        NotificationCenter.default .addObserver(self, selector:  #selector(setAnimation), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        getEventDetail()
    }
    
    
    
   @objc func setAnimation() {
        if joinNow{
            self.lblStatus.startBlink()
            self.lblLiveNow.startBlink()
        }else if is_Notify == 0 {
            self.lblStatus.startBlink()
        }
    }
    
    @objc func isnotifycheck(){
        if joinNow{
            //eventLink open
            openURL(url: eventLink)
        }
        else if is_Notify == 0 {
            //open pop
            let obj = UIStoryboard.init(name: "FreeAppDashboard", bundle: nil).instantiateViewController(withIdentifier: "NotifyMePopup") as! NotifyMePopup
            obj.eventid = eventid
            obj.completionBlock = { info in
                self.dismiss(animated: false, completion: nil)
                self.getEventDetail()
            }
            obj.modalPresentationStyle = .overCurrentContext
            self.present(obj, animated: true, completion: nil)
        }
    }
    
    
    @objc func offsetFrom() {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: Date() as Date, to: date as Date)
        let seconds = "\(difference.second ?? 0 )"
        let minutes = "\(difference.minute ?? 0 )"
        let hours = "\(difference.hour ?? 0)"
        let days = "\(difference.day ?? 0)"
        
       
        self.lblhrs.text = hours
        self.lblMins.text = minutes
        self.lblDays.text = days
        self.lblSec.text = seconds
        
        if difference.second ?? 0 <= 0  && difference.minute ?? 0  <= 0   && difference.hour ?? 0 <= 0 && difference.day ?? 0 <= 0 {
            self.timer.invalidate()
            self.lblhrs.text =  "0"
            self.lblMins.text = "0"
            self.lblDays.text = "0"
            self.lblSec.text = "0"
           
            self.lblStatus.text = "Join Now"
            
            self.lblLiveNow.isHidden = false
            self.lblStatus.startBlink()
            self.lblLiveNow.startBlink()
          
            self.colorView.backgroundColor = #colorLiteral(red: 0, green: 0.4991469979, blue: 0.001404395094, alpha: 1)
            self.colorView.isUserInteractionEnabled = true
            
            //event_link
            self.lblNotifyMessage.text = ""
         
            joinNow = true
        }
    }
    
    func setdata(){
        self.imgBanner.sd_setImage(with: URL(string: "\(self.eventDetail["large_image"] ?? "")"), placeholderImage: nil)
        is_Notify = self.eventDetail["is_notify"] as? Int ?? 0
        lblTitle.text = self.title
        if is_Notify == 0 {
           // i am interested
            self.lblNotifyMessage.text = "I am Intrested"
            self.lblStatus.text = "Book My Seat"
            self.colorView.isUserInteractionEnabled = true
            self.colorView.backgroundColor = UIColor(named: "pink")
            self.lblStatus.startBlink()
        }else if is_Notify > 0 {
            self.lblNotifyMessage.text = "you will be notify with the link"
            self.lblStatus.text = "Seat Booked"
            self.colorView.backgroundColor = .gray
            self.colorView.isUserInteractionEnabled = false
        }
        eventid = "\(self.eventDetail["id"] as? Int ?? 0)"
        eventLink = "\(self.eventDetail["event_link"] as?  String ?? "")"
        let startdate = self.eventDetail["start_date"] as? String ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        date = dateFormatter.date(from:startdate)! as NSDate
      
        let calendar = Calendar.current
        let newDate = calendar.date(byAdding: .minute, value: -15, to: date as Date, wrappingComponents:false)
        date = newDate! as NSDate
        
        self.title = self.eventDetail["title"] as? String ?? ""
        
        let description = self.eventDetail["description"] as? String ?? ""
        var headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'></header>"
        headerString.append(description)
        self.webView.loadHTMLString("\(headerString)", baseURL: nil)
     
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(offsetFrom), userInfo: nil, repeats: true)
     }
    
    @IBAction func btnBackPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func btnInviteFriend(_ sender: Any) {
        
        // large image with text 
        var image  : UIImage?
        
        if let ldimage = SDImageCache.shared.imageFromDiskCache(forKey: "\(self.eventDetail["large_image"] ?? "")") {
        //use image
            
            image = ldimage
            
            var message = ""
        
            let strDate = Date.convertStringToDate(strDate: self.eventDetail ["start_date"] as? String ?? "2021-10-18 14:00:00", dateFormate: "yyyy-MM-dd HH:mm:ss")
            let datetoStr = strDate.convertDateToString(strDateFormate: "dd-MM-yyyy")
           
            let time = self.eventDetail ["start_time"] ?? ""
          
            if strLanguage == kEnglish{
               
                message = "Hey Mommies ! Interesting session for Happy and Healthy Child by Garbhsanskar will be live on Garbh Sanskar Guru App from \(datetoStr),\(time).\n\nGarbhsanskar Guru app is most trusted Garbhsanskar App for Planners and Pregnant. Download it from http://guru.garbhsanskar.co  "
                
            }else if strLanguage == kGujrati{
                message = "Hey Mommies !! ગર્ભસંસ્કાર દ્વારા હેપ્પી, હેલ્ધી અને શ્રેષ્ઠ ગુણોવાળા સંતાન પ્રાપ્તિ માટે રસપ્રદ વેબિનર \(datetoStr),\(time) પર ગર્ભ સંસ્કાર ગુરુ એપ પર લાઇવ થશે.\n\nગર્ભસંસ્કાર ગુરુ એપ્લિકેશન પ્રેગ્નેન્સી પ્લાનર અને સગર્ભા માતાઓ માટે સૌથી વિશ્વસનીય સાથી છે. તેને http://guru.garbhsanskar.co પરથી ડાઉનલોડ કરો."
            }else{
                message =   "Hey Mommies ! गर्भसंस्कार के माध्यमसे हेप्पी, हेल्धी और श्रेष्ठ गुणो वाले संतानकी प्राप्ति के लिए दिलचस्प वेबिनार \(datetoStr),\(time) को गर्भ संस्कार गुरु ऐप पर लाइव होगा।\n\nगर्भसंस्कार गुरु ऐप प्लानर्स और गर्भस्थ माताओ के लिए सबसे भरोसे मंद साथी है। इसे http://guru.garbhsanskar.co  से डाउनलोड करें |"
            }
        
            shareYourItems(shareItems:  [message,image ?? UIImage()], isexcludItems:false , excludItems:[])
        }
 
    }
    
    @objc func getEventDetail() {
        self.eventDetailAPi { json,statusCode in
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
              print(dict)
                
                let data = dict["data"] as? NSArray
                if let data = data {
                    self.eventDetail = data.firstObject as! NSDictionary
                    self.setdata()
                }
               
            }
        }
    }
}


extension UIViewController {
    func openURL(url : String) {
        if let url = URL(string: url) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }

}


extension UILabel {

    func startBlink() {
        UIView.animate(withDuration: 0.8,
              delay:0.0,
              options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
              animations: { self.alpha = 0 },
              completion: nil)
    }

    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}


extension Date {
    static func convertStringToDate(strDate:String, dateFormate strFormate:String) -> Date{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = strFormate
        dateFormate.timeZone = TimeZone.init(abbreviation: "UTC")
        let dateResult:Date = dateFormate.date(from: strDate) ?? Date()
        
        return dateResult
    }
   
    //Convert Date to String
    func convertDateToString(strDateFormate:String) -> String{
        let dateFormatter = DateFormatter()
        let loc = Locale(identifier: "en")
        dateFormatter.locale = loc
        dateFormatter.dateFormat = strDateFormate
        let strDate = dateFormatter.string(from: self)
        return strDate
    }
}
