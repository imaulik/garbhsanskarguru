//
//  OurWebinarsVC.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 27/08/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import AVKit
import XCDYouTubeKit



class OurWebinarsVC: UIViewController {
    @IBOutlet weak var cvWebinar: UICollectionView!
    var dicWebFirstInfo :  WebinarInfo?
    var arrWebinarList : [WebinarInfo]?
    var strLanguage = _appDelegator.getLanguage()
    var refresher:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvWebinar.register(UINib(nibName: "PagerCVCell", bundle: .main), forCellWithReuseIdentifier: "PagerCVCell")
        cvWebinar.register(UINib(nibName: "WebinarView", bundle: nil),
                          forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                          withReuseIdentifier: "WebinarView")
        
        self.refresher = UIRefreshControl()
        self.cvWebinar!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(getWebinar), for: .valueChanged)
        self.cvWebinar!.addSubview(refresher)

         // Do any additional setup after loading the view.
     }
    
    
    override func viewWillAppear(_ animated: Bool) {
        registerBlock()
        getWebinar()
    }
    
    @objc func getWebinar(){
        getWebinars(type: "Webinar") { json,statusCode in
            self.refresher.endRefreshing()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = dict["data"]
                self.arrWebinarList =  [WebinarInfo](dictionaryArray:res as! [NSDictionary])
                if self.arrWebinarList?.count ?? 0 > 0 {
                    self.dicWebFirstInfo = self.arrWebinarList?.first
                    self.arrWebinarList?.remove(at: 0)
                }
                self.cvWebinar.reloadData()
            }
        }
    }
    
    func registerBlock(){
        _appDelegator.detectBackFromWebinar = { () in
            self.strLanguage = _appDelegator.getLanguage()
            self.cvWebinar.reloadData()
        }
    }
}

// functions
extension OurWebinarsVC :UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        // Get the view for the first header
        let indexPath = IndexPath(row: 0, section: section)
        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)

        // Use this view to calculate the optimal size based on the collection view's width
        return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
                                                  withHorizontalFittingPriority: .required, // Width is fixed
                                                  verticalFittingPriority: .fittingSizeLevel) // Height can be as large as needed
    }
   
    @objc func btnPlayVideo(sender: UIButton) {
        var strVideo = ""
        if self.strLanguage == kEnglish {
            strVideo = dicWebFirstInfo?.eng_video ?? ""
        }else if self.strLanguage == kGujrati {
            strVideo = dicWebFirstInfo?.guj_video ?? ""
        }else if self.strLanguage == KHindi {
            strVideo = dicWebFirstInfo?.hindi_video ?? ""
        }
        
        if strVideo != "" {
            videoPlay(strVideo: strVideo)
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let friendHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WebinarView", for: indexPath) as! WebinarView
        if let dic = dicWebFirstInfo {
            friendHeader.lblNoData.isHidden =  true
            friendHeader.imgVideo.isHidden = false
            friendHeader.btnPlay.isHidden = false
            var strImage : String?
            
            if self.strLanguage == kEnglish {
                strImage = dic.eng_image
            }else if self.strLanguage == kGujrati {
                strImage = dic.guj_image
            }else if self.strLanguage == KHindi {
                strImage = dic.hindi_image
            }
           
           
            if strImage == "" {
                friendHeader.lblNoData.isHidden =  false
                friendHeader.imgVideo.isHidden = true
                friendHeader.btnPlay.isHidden = true
            }else {
                friendHeader.btnPlay.addTarget(self, action: #selector(btnPlayVideo), for:.touchUpInside)
                friendHeader.imgVideo.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
            }
            
            
            if arrWebinarList?.count ?? 0 > 0 {
                friendHeader.noWebinarView.isHidden = true
            }else {
                friendHeader.noWebinarView.isHidden = false
            }
            
        }else{
            friendHeader.lblNoData.isHidden = false
            friendHeader.imgVideo.isHidden = true
            friendHeader.btnPlay.isHidden = true
        }
        
        return friendHeader
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2) - 5  , height: UIScreen.width / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWebinarList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"PagerCVCell",    for: indexPath) as! PagerCVCell
        let dic  = arrWebinarList?[indexPath.row]
        
        var strImage : String?
        if self.strLanguage == kEnglish {
            strImage = dic?.eng_image
        }else if self.strLanguage == kGujrati {
            strImage = dic?.guj_image
        }else if self.strLanguage == KHindi {
            strImage = dic?.hindi_image
        }
        
        cell.imgPage.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        var strVideo = ""
        let dic = arrWebinarList?[indexPath.row]
        if self.strLanguage == kEnglish {
            strVideo = dic?.eng_video ?? ""
        }else if self.strLanguage == kGujrati {
            strVideo = dic?.guj_video ?? ""
        }else if self.strLanguage == KHindi {
            strVideo = dic?.hindi_video ?? ""
        }
        
        if strVideo != "" {
            videoPlay(strVideo: strVideo)
        }
    }
}




extension UIViewController {
   
    func videoPlay(strVideo :String){
        var ytVideo = strVideo
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: ytVideo , options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, ytVideo.lengthOfBytes(using: String.Encoding.utf8)))
            let range = match?.range(at: 0)
            if range == nil {
                  _ = KPValidationToast.shared.showToastOnStatusBar(message: "Somthing went wrong")
                return
            }
            ytVideo = (strVideo as NSString).substring(with: range!)
        } catch {
        }
        
        let playerViewController = AVPlayerViewController()
        self.present(playerViewController, animated: true, completion: nil)
  
        
        XCDYouTubeClient.default().getVideoWithIdentifier(ytVideo) { [weak self] (video, error) in

           if video != nil {
               let streamURLs = video?.streamURLs
             let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
             if let streamURL = streamURL {
                 playerViewController.player = AVPlayer(url: streamURL)
             }
             playerViewController.player?.play()

           } else {
               
              self?.dismiss(animated: true)
           }
        }
    }
}

