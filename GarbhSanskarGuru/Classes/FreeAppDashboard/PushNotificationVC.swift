//
//  NotificationVC.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 04/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PushNotificationVC: ParentViewController {
    var arrNotificationInfo = [NotificationInfo]()
    
    @IBOutlet weak var lblNoNotification: UILabel!
    @IBOutlet weak var tblPushNotification: UITableView!
    var notificationURL =  ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tblPushNotification.register(UINib(nibName: PushListCell.className, bundle: nil), forCellReuseIdentifier: PushListCell.className)

        getNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tblPushNotification.reloadData()
    }

    fileprivate func noDataFound() {
        self.lblNoNotification.isHidden = false
        self.tblPushNotification.reloadData()
    }
    
    func getNotification() {
        self.showCentralSpinner()
        KPWebCall.call.getUserNotification { info , status in
            self.hideCentralSpinner()
             if  let dict = info as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let data = dict["data"]
                self.arrNotificationInfo =  [NotificationInfo](dictionaryArray:data as! [NSDictionary])
                if self.arrNotificationInfo.count == 0 {
                    self.noDataFound()
                    return
                }
                self.notificationURL = dict["image_url"] as! String
                self.tblPushNotification.reloadData()
                self.lblNoNotification.isHidden = true
             }else {
                self.noDataFound()
            }
        }
    }
    
   
    
    @IBAction func btnBackPress(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        self.tblPushNotification.reloadData()
    }
}



extension PushNotificationVC {
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrNotificationInfo.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:PushListCell.className, for: indexPath) as! PushListCell
        let dic = arrNotificationInfo[indexPath.row]
      
       // cell.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(prepereForAnimation), userInfo: nil, repeats: true)
        
        
        cell.btnView.titleLabel?.blink()
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(btnViewPress), for: .touchUpInside)
        
        cell.lblHeader.text = dic.announcement_title
 
            let linkTextAttributes : [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.foregroundColor: UIColor.blue,
                NSAttributedString.Key.underlineColor: UIColor.clear,
                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.patternDot.rawValue,
                NSAttributedString.Key.font: FontBook.Avenir_Medium
            ]
 
        cell.lblSubDetail.linkTextAttributes = linkTextAttributes
        cell.lblSubDetail.attributedText = dic.announcement_text.convertHtmlToAttributedStringWithCSS(font: FontBook.Avenir_Heavy.of(20), csscolor: "#D8366B", lineheight: 2, csstextalign: "")
        cell.selectionStyle = .none
        return cell
    }
    
    @objc func btnViewPress (id : UIButton) {
        //  NotificationVC
        let reminderVC = UIStoryboard.init(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        reminderVC.modalPresentationStyle = .fullScreen
        reminderVC.notificationData = arrNotificationInfo[id.tag]
        reminderVC.notificationURL = self.notificationURL
        self.present(reminderVC, animated: true, completion: {
        })
    }
}

 


extension String {
    private var convertHtmlToNSAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data,options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func convertHtmlToAttributedStringWithCSS(font: UIFont? , csscolor: String , lineheight: Int, csstextalign: String) -> NSAttributedString? {
        guard let font = font else {
            return convertHtmlToNSAttributedString
        }
        let modifiedString = "<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px; color: \(csscolor); line-height: \(lineheight)px; text-align: \(csstextalign); }</style>\(self)";
        guard let data = modifiedString.data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error)
            return nil
        }
    }
}


 
 class UnselectableTappableTextView: UITextView {

     // required to prevent blue background selection from any situation
     override var selectedTextRange: UITextRange? {
         get { return nil }
         set {}
     }

     override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {

         if let tapGestureRecognizer = gestureRecognizer as? UITapGestureRecognizer,
             tapGestureRecognizer.numberOfTapsRequired == 1 {
             // required for compatibility with links
             return super.gestureRecognizerShouldBegin(gestureRecognizer)
         }

         return false
     }

 }
