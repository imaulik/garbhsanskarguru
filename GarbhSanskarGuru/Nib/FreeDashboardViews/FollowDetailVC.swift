//
//  FollwDetailVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 10/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class FollowDetailVC: UIViewController {
    @IBOutlet weak var cvFollowDetail: UICollectionView!
    let arrFollowImg = ["ic_AppInfo","ic_Faq","ic_Appbenefit"]
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvFollowDetail.register(UINib(nibName: "FollowCell", bundle: .main), forCellWithReuseIdentifier: "FollowCell")
        self.title = "Follow Garbhsanskar"
    }
}


extension FollowDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 2 - 5 , height: _screenSize.width / 2 - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrFollowImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"FollowCell", for: indexPath) as! FollowCell
        cell.imgFollow.image = UIImage(named: arrFollowImg[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            let  videoVC = freeAppDashboard.instantiateViewController(withIdentifier:"VideoVC") as! VideoVC
            videoVC.isFromAppInformation = true
            self.navigationController?.pushViewController(videoVC, animated: true)
        } else if indexPath.item == 1 {
            isFromAboutUs = false
            let  aboutUsVC = freeAppDashboard.instantiateViewController(withIdentifier:"AboutUsVC") as! AboutUsVC
            self.navigationController?.pushViewController(aboutUsVC, animated: true)
        }else {
            let gallery = freeAppDashboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            gallery.isFromAppBenefit = true
            self.navigationController?.pushViewController(gallery, animated: true)
        }
    }

}
