//
//  IntroTableCell.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 24/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class IntroTableCell: ConstrainedTableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTitle(data: ModelRahasyaTitle){
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        imgView.image = #imageLiteral(resourceName: "ic_article")
        imgView.tintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        switch currLanguage{
        case .hindi:
            lblTitle.text = data.titleHindi
            break
        case .english:
            lblTitle.text = data.titleEng
            break
        case .gujarati:
            lblTitle.text = data.titleGuj
            break
        }
    }

}
