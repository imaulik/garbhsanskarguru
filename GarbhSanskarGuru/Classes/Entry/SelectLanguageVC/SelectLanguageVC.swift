//
//  SelectLanguageVC.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 09/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SelectLanguageVC: ParentViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var animatedViews: [UIView]!
    
    //MARK:- Variable Declaration
    var actionBlock: (KAction)->Void = {_ in}
    
    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- Private Methods
    
    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews[tag].transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
            
        }, completion: { _ in
            _appDelegator.storeAppLanguage(lang: AppLanguage(rawValue: tag)!)
            self.actionBlock(.done)
//            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
//                self.animatedViews[tag].transform = .identity
//            }, completion: { _ in
//                _appDelegator.storeAppLanguage(lang: AppLanguage(rawValue: tag)!)
//                self.actionBlock(.done)
//            })
        })
    }
    
    //MARK:- Button Action Method

    @IBAction func action_language(_ sender: UIButton) {
        self.animateView(with: sender.tag)
    }
}
