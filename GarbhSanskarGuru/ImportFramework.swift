//
//  ImportFramework.swift
//
//  Created by Maulik on 18/05/20.
//  Copyright © 2021 Lets Explore. All rights reserved.
// maulik 

@_exported import Foundation
@_exported import UIKit
@_exported import AVFoundation
@_exported import PhotosUI
@_exported import MessageUI
@_exported import MobileCoreServices
@_exported import WebKit
@_exported import CoreTelephony
@_exported import Contacts
@_exported import UserNotifications
@_exported import EventKit
@_exported import SafariServices // safri view
@_exported import AuthenticationServices // apple login


//Thirdparty

@_exported import Kingfisher
@_exported import Alamofire
@_exported import NVActivityIndicatorView
@_exported import YoutubePlayer_in_WKWebView

@_exported import Firebase

//@_exported import SDWebImage
//@_exported import GoogleSignIn
//@_exported import GooglePlaces

//@_exported import Quickblox
//@_exported import QuickbloxWebRTC
//@_exported import FacebookCore
//@_exported import FacebookLogin
