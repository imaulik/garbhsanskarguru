//
//  PackagesVC.swift
//  GarbhSanskarGuru
//
//  Created by Jahanvi Trivedi on 20/01/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PackagesVC: ParentViewController {

    @IBOutlet weak var lbldemoPrice: UILabel!
    @IBOutlet weak var lbldemoDiscountprice: UILabel!
    @IBOutlet weak var lblfullCoursePrice: UILabel!
    @IBOutlet weak var lblfullDiscountprice: UILabel!
    @IBOutlet weak var lblDemoSave: UILabel!
    @IBOutlet weak var lblSaveFullPrice: UILabel!
    
    @IBOutlet weak var lblFullGST: UILabel!
    @IBOutlet weak var lblTrailGST: UILabel!
    @IBOutlet weak var viewFull: UIView!
    @IBOutlet weak var viewTrial: UIView!
    @IBOutlet weak var viewFullSelect: UIView!
    @IBOutlet weak var viewTrialSelect: UIView!
    var strStatus = "Demo"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        getPrice()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        let arysubviews = self.view.subviews
         for scrview in arysubviews{
             if scrview is UIScrollView && scrview.tag == 1{
                 let scrollView = scrview as! UIScrollView
                 let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.height + scrollView.adjustedContentInset.bottom)
                 scrollView.setContentOffset(bottomOffset, animated: true)
             }
         }
    }
    
    @IBAction func btnAboutPrices(_ sender: UIButton)
    {
        let aboutPricesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "AboutPricesVC") as! AboutPricesVC
        self.navigationController?.pushViewController(aboutPricesVC, animated: true)
    }
    
    @IBAction func btnChoosePackage(_ sender: UIButton) {
        if sender.tag == 0 {
            strStatus = "Demo"
            viewTrial.backgroundColor = UIColor.init(red: 226/255, green: 210/255, blue: 229/255, alpha: 1)
            viewFull.backgroundColor = UIColor.white
            viewTrialSelect.backgroundColor = UIColor.init(red: 83/255, green: 185/255, blue: 73/255, alpha: 1)
            viewFullSelect.backgroundColor = UIColor.black
        }
        else
        {
            strStatus = "Full"
            viewFull.backgroundColor = UIColor.init(red: 226/255, green: 210/255, blue: 229/255, alpha: 1)
            viewTrial.backgroundColor = UIColor.white
            viewFullSelect.backgroundColor = UIColor.init(red: 83/255, green: 185/255, blue: 73/255, alpha: 1)
            viewTrialSelect.backgroundColor = UIColor.black
        }
    }
 

 
    @IBAction func btnPayNowTapped(_ sender: UIButton) {
        let vcReferalNew = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ReferalCodeNewVC") as! ReferalCodeNewVC
        if strStatus == "Demo" {
           vcReferalNew.paymentType = .trial
        }
        else {
           vcReferalNew.paymentType = .full
        }
        self.navigationController?.pushViewController(vcReferalNew, animated: true)
    }
    
    @IBAction func btnbackTapped(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}


extension PackagesVC {
    
    func getToday() {
          let param: [String: Any] = ["user_id": _user.id]
          self.showCentralSpinner()
          KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
              guard let weakself = self else {return}
              weakself.hideCentralSpinner()
              if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                  if status == 200 {
                    let checkfreedemoUser =  dict["free_demo_payment"]
                    let checkuser = checkfreedemoUser as? String == "No" ? false  : true
                    //let  checkuser = true
                    UserDefaults.standard.set(checkuser, forKey: "checkdemoUser") //Bool
                    if checkuser{
                        self?.lbldemoPrice.text = "₹ 0"
                        self?.lblTrailGST.text = ""
                    }
                  }
                  else {
                      weakself.showError(data: json, view: weakself.view)
                  }
              }else {
                  weakself.showError(data: json, view: weakself.view)
              }
          }
      }
    
    
    func getPrice() {
        self.showCentralSpinner()
        KPWebCall.call.getConfigPrice { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int , status == 200{

                if let data = dict["data"] as? NSDictionary{
                    print(data)
                    self.lbldemoPrice.text = "₹ \(data["demo_price"] ?? "")"
                    self.lblfullCoursePrice.text = "₹ \(data["price"] ?? "")"
                   
                    self.lbldemoDiscountprice.text = "₹ \(data["demo_full_price"] ?? "")"
                    self.lblfullDiscountprice.text = "₹ \(data["full_price"] ?? "")"
                    self.lblDemoSave.text = "Save ₹ \(data["demo_save_price"] ?? "")"
                    self.lblSaveFullPrice.text =  "Save ₹ \(data["save_price"] ?? "")"
                    self.lblTrailGST.text = "+\(data["demo_gst_per"] ?? "") % GST"
                    self.lblFullGST.text = "+\(data["demo_gst_per"] ?? "") % GST"
                    _user.amountDemoApp = data.getInt32Value(key: "demo_price")
                    _user.amount = data.getInt32Value(key: "price")
                    _user.gstPrice =  data.getDoubleValue(key:"demo_gst_price")
                    _user.gstPresntage = data.getDoubleValue(key:"demo_gst_per")
                    _user.gstFullPrice = data.getDoubleValue(key:"full_gst_price")
 
                    _appDelegator.saveContext()
                    self.getToday()
                }
            }
        }
    }
}
 
