//
//  UserDataSpecifier.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import Foundation

enum EnumTxtCellType:String {
    
    case txtcell = "txtcell"
    case infoCell = "infoCell"
    case none = "none"
    
    var cellHeight:CGFloat {
        switch self {
        case .txtcell:
            return 60.heightRatio
        case .infoCell:
            return 100.heightRatio
        case .none:
            return 0
        }
    }
}

struct UserFiled {
    var cellType = EnumTxtCellType.none
    var placeHolder = ""
    var text = ""
}

class UserInfo: NSObject{
    
    // New design flow.
    
    var planingStatus:String! = "planing" //planing
    var name:String!
    var email:String!
    var phone:String!
    var password:String!
    var city:String!
    var countryCode:String!
    var currentDay:Int!
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["full_name"] = name
        dict["email"] = email
        dict["password"] = password
        dict["phone_number"] = phone
        dict["section_type"] = planingStatus
        dict["device_type"] = _deviceType
        dict["device_id"] = _deviceID
        dict["login_with"] = "app"
        dict["is_login"] = "Yes"
        dict["device_token"] = _appDelegator.getPushToken()
        dict["current_day"] = planingStatus != "planing" ? currentDay : 0
        dict["city"] = city
        return dict
    }
        
    func isValidData(validFor:Int) -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if validFor == 1{
            if String.validateStringValue(str: planingStatus){
                result.isValid = false
                result.error = kSelectPlaningStatus
                return result
            }
        }else{
            if String.validateStringValue(str: name){
                result.isValid = false
                result.error = kEnterName
                return result
            }
            
            if String.validateStringValue(str: email) {
                result.isValid = false
                result.error = kEnterEmail
                return result
            }else if !email.isValidEmailAddress() {
                result.isValid = false
                result.error = kInvalidEmail
                return result
            }
            
            if String.validateStringValue(str: password){
                result.isValid = false
                result.error = kEnterPassword
                return result
            }
            
            if String.validateStringValue(str: phone){
                result.isValid = false
                result.error = kEnterMobile
                return result
            }else if !phone.validateContact() {
                result.isValid = false
                result.error = kMobileInvalid
                return result
            }
//            if String.validateStringValue(str: countryCode){
//                result.isValid = false
//                result.error = kEnterCountryCode
//                return result
//            }
            if String.validateStringValue(str: city){
                result.isValid = false
                result.error = kEnterCity
                return result
            }
        }
        return result
    }
    
    // Old Design flow.
    
    var arrFiled: [UserFiled] = []
    
    func initModel() {
        var name = UserFiled()
        name.cellType = .txtcell
        name.placeHolder = "Full Name"
        arrFiled.append(name)
        
        var companyName = UserFiled()
        companyName.cellType = .txtcell
        companyName.placeHolder = "Email"
        arrFiled.append(companyName)
        
        var email = UserFiled()
        email.cellType = .txtcell
        email.placeHolder = "Password"
        arrFiled.append(email)
        
        var contact = UserFiled()
        contact.cellType = .txtcell
        contact.placeHolder = "Mobile number"
        arrFiled.append(contact)
        
        var submitButton = UserFiled()
        submitButton.cellType = .infoCell
        submitButton.text = "planing"
        arrFiled.append(submitButton)
    }
    
    func paramDict1() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["full_name"] = arrFiled[0].text
        dict["email"] = arrFiled[1].text
        dict["password"] = arrFiled[2].text
        dict["phone_number"] = arrFiled[3].text
        dict["section_type"] = arrFiled[4].text
        dict["device_type"] = _deviceType
        dict["device_id"] = _deviceID
        dict["login_with"] = "app"
        dict["is_login"] = "Yes"
        dict["device_token"] = _appDelegator.getPushToken()
        return dict
    }
    
    
    func isValidData1() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: arrFiled[0].text){
            result.isValid = false
            result.error = kEnterName
            return result
        }
        
        if String.validateStringValue(str: arrFiled[1].text) {
            result.isValid = false
            result.error = kEnterEmail
            return result
        }else if !arrFiled[1].text.isValidEmailAddress() {
            result.isValid = false
            result.error = kInvalidEmail
            return result
        }
        
        if String.validateStringValue(str: arrFiled[2].text){
            result.isValid = false
            result.error = kEnterPassword
            return result
        }
        
        if String.validateStringValue(str: arrFiled[3].text){
            result.isValid = false
            result.error = kEnterMobile
            return result
        }else if !arrFiled[3].text.validateContact() {
            result.isValid = false
            result.error = kMobileInvalid
            return result
        }
        return result
    }
}
