//
//  User.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 26/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import Foundation
import CoreData

enum PregnancyStatus: String {
    case planning = "planing"
    case pregnant = "pregnent"
    case none = "none"
}

enum PaymentStatus: String {
    case pending = "Pending"
    case completed = "Completed"
}

enum ActivityStatus: String {
    case pending = "Pending"
    case completed = "Completed"
}

enum ReferralStatus: String {
    case pending = "Pending"
    case completed = "Completed"
}

enum Gender: String {
    case male = "Male"
    case female = "Female"
    case none = "none"
}

enum AppLanguage: Int {
    case hindi = 0
    case english = 1
    case gujarati = 2
}

enum DownloadLanguage: String {
    case hindi = "hindi"
    case gujarati = "guj"
    case english = "eng"
}

enum DownloadGarbhsatsangLanguage: String {
    case hindi = "Hindi"
    case gujarati = "Gujarati"
    case english = "English"
}

enum DisplayType: String {
    case image = "Image"
    case video = "Video"
}

enum DisplayBrainActivityType: String {
    case image = "Image"
    case text = "Text"
}

enum PaymentTypeLocalSelected: Int16 {
    case notSelected    = 0
    case trial          = 1
    case full           = 2
}

enum WhichUser: String {
    case reg_user = "reg_user"
    case demo_user = "demo_user"
}

enum appStopType: String {
    case pause = "Pause"
    case completed = "Completed"
    case no = "No"
}

enum commonType: String {
    case yes = "Yes"
    case no = "No"
}

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

class User: NSManagedObject, ParentManagedObject{
    
    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var email: String
    @NSManaged var mobile: String
    @NSManaged var refCode: String
    @NSManaged var country: String
    @NSManaged var state: String
    @NSManaged var city: String
    @NSManaged var gender: String
    
    @NSManaged var pregStatus: String
    @NSManaged var payStatus: String
    @NSManaged var uniqueId: String
    @NSManaged var desireStatus: String
    @NSManaged var referredStatus: String
    
    @NSManaged var profile: String
    @NSManaged var giftCode: String
    @NSManaged var isLogin: String
    @NSManaged var amount: Int32
    @NSManaged var usdPrice: Int32
    @NSManaged var amountDemoApp: Int32
    @NSManaged var usdPriceDemoApp: Int32
    @NSManaged var gstPresntage : Double
    @NSManaged var gstPrice : Double
    @NSManaged var gstFullPrice : Double
    
    
    @NSManaged var currDay: Int16
    @NSManaged var lastLoginDate: Date?
    
    @NSManaged var localSelectedPaymentType: Int16
    @NSManaged var whichUser: String
    @NSManaged var isPaymentSkip: Bool
    @NSManaged var isDemoPaymentSkip: Bool
    @NSManaged var changeDay: String
    @NSManaged var app_stop: String
    
    
    var imgUrl: URL? {
        return URL(string: profile)
    }
    
//    var appPauseStatus: appStopType {
//        return appStopType(rawValue: app_stop) ?? .no
//    }
//    
//    var changeDayStatus: commonType {
//        return commonType(rawValue: changeDay) ?? .no
//    }
    
    var pregnancyStatus: PregnancyStatus {
        return PregnancyStatus(rawValue: pregStatus) ?? .none
    }
    
    var paymentStatus: PaymentStatus {
        return PaymentStatus(rawValue: payStatus)!
    }
    
    var desireBabyStatus: ActivityStatus {
        return ActivityStatus(rawValue: desireStatus)!
    }
    
    var referralStatus: ReferralStatus {
        return ReferralStatus(rawValue: referredStatus)!
    }
    
    var userGender: Gender {
        return Gender(rawValue: gender) ?? .none
    }
    
    var isUserLogin: Bool {
        return isLogin == "Yes"
    }
    
    func setReferral(dict: NSDictionary) {
        uniqueId = dict.getStringValue(key: "unique_id")
        amount = dict.getInt32Value(key: "amount")
        usdPrice = dict.getInt32Value(key: "usd_price")
        payStatus = dict.getStringValue(key: "payment_status")
        referredStatus = dict.getStringValue(key: "referrer_status")
        
        amountDemoApp = dict.getInt32Value(key: "demo_app_price_inr")
        usdPriceDemoApp = dict.getInt32Value(key: "demo_app_price_usd")
        gstPresntage = dict.getDoubleValue(key: "demo_gst_per")
        gstPrice  =  dict.getDoubleValue(key: "demo_gst_price")
        gstFullPrice = dict.getDoubleValue(key: "full_gst_price")
    }
    
    func setRegionData(dict: NSDictionary) {
        country = dict.getStringValue(key: "country")
        state = dict.getStringValue(key: "state")
        city = dict.getStringValue(key: "city")
    }
    
    func setCurrentDay(dict: NSDictionary) {
        currDay = dict.getInt16Value(key: "current_day")
    }
    
    func setCurrentDay(currentDay: String) {
        currDay = NSDictionary().getInt16Value(key: currentDay)
    }
    
    func setPaymentStatus(dict: NSDictionary) {
        payStatus = dict.getStringValue(key: "payment_status")
    }
    
    func setDesiredBaby(dict: NSDictionary) {
        desireStatus = dict.getStringValue(key: "desirebabby_status")
    }

    func setLocalSelectedPaymentType(type: PaymentTypeLocalSelected) {
        localSelectedPaymentType = type.rawValue
    }
    
    func setWhichUserType(strType: String) {
        whichUser = strType
    }
    
    func setFullName(fName: String) {
        name = fName
    }
    
    func setPhoneNumber(number: String) {
        mobile = number
    }
    
    func setChangeDay(dict: NSDictionary) {
        changeDay = dict.getStringValue(key: "change_day")
    }
    
    func setappstop(dict: NSDictionary) {
        app_stop = dict.getStringValue(key: "app_stop")
    }

    func setAddressData(scity: String, stateName: String, countryName: String) {
        country = countryName
        state = stateName
        country = scity
    }
    
    func initForGift(entry: NSDictionary) {
        id = entry.getStringValue(key: "id")
        name = entry.getStringValue(key: "name")
        email = entry.getStringValue(key: "email")
        giftCode = entry.getStringValue(key: "gift_code")
        amount = entry.getInt32Value(key: "amount")
        usdPrice = entry.getInt32Value(key: "usd_price")
        payStatus = entry.getStringValue(key: "payment_status")
        uniqueId = entry.getStringValue(key: "unique_id")
    }
    
    func initWith(entry: NSDictionary) {
        id = entry.getStringValue(key: "user_id")
        name = entry.getStringValue(key: "full_name")
        email = entry.getStringValue(key: "email")
        mobile = entry.getStringValue(key: "phone_number")
        
        country = entry.getStringValue(key: "country")
        state = entry.getStringValue(key: "state")
        city = entry.getStringValue(key: "city")
        
        refCode = entry.getStringValue(key: "ref_code")

        pregStatus = entry.getStringValue(key: "section_type")
        isLogin = entry.getStringValue(key: "is_login")
        lastLoginDate = Date.dateFromServerFormat(from: entry.getStringValue(key: "last_login_date"))
        
        usdPrice = entry.getInt32Value(key: "usd_price")
        amount = entry.getInt32Value(key: "amount")
        usdPriceDemoApp = entry.getInt32Value(key: "demo_app_price_usd")
        gstPresntage = Double(entry.getInt32Value(key: "demo_gst_per"))
        gstPrice  = Double(entry.getInt32Value(key: "demo_gst_price"))
        gstFullPrice = Double(entry.getInt32Value(key: "full_gst_price"))
        amountDemoApp = entry.getInt32Value(key: "demo_app_price_inr")
        payStatus = entry.getStringValue(key: "payment_status")
        uniqueId = entry.getStringValue(key: "unique_id")
        
        gender = entry.getStringValue(key: "gender")
        profile = entry.getStringValue(key: "profile_picture")
        
        desireStatus = entry.getStringValue(key: "desirebabby_status")
        referredStatus = entry.getStringValue(key: "referrer_status")

        currDay = entry.getInt16Value(key: "current_day")
        whichUser = entry.getStringValue(key: "which_user")
        isPaymentSkip = entry.getBooleanValue(key: "skip_payment")
        isDemoPaymentSkip = entry.getBooleanValue(key: "demo_skip_payment")
        changeDay = entry.getStringValue(key: "change_day")
        app_stop = entry.getStringValue(key: "app_stop")
        
    }
}
