//
//  BabyCategoryVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ModelBabyCategory {
    
    let id: String
    let activityID: String
    let guj_lang: String
    let hindi_lang: String
    let eng_lang: String
    
    var isSelected = false
    
    init(dict: NSDictionary) {
        id = dict.getStringValue(key: "id")
        activityID = dict.getStringValue(key: "activity_id")
        guj_lang = dict.getStringValue(key: "guj_lan")
        hindi_lang = dict.getStringValue(key: "hindi_lang")
        eng_lang = dict.getStringValue(key: "eng_lang")
    }
}


var isfreeDashboard = false

class BabyCategoryCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTick: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func prepareCellUI(with data: ModelBabyCategory) {
        lblTitle.text = data.eng_lang
    }
}

class BabyCategoryVC: ParentViewController {
    
    var arrData: [ModelBabyCategory]!
    let checkUserFreeDemo =  UserDefaults.standard.bool(forKey: "checkdemoUser")
    var whichUser  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
}

extension BabyCategoryVC {
    
    func prepareUI() {
        //if self.checkUserFreeDemo  {
            self.getToday(block: { (done) in
           
            })
       //  }
        self.getBabyCategory()
    }
    
    func removeDuplicates(arr: [String]) -> [String] {
        var result = [String]()
        for value in arr {
            if !result.contains(value) {
                result.append(value)
            }
        }
        return result
    }
}

extension BabyCategoryVC {
    
    @IBAction func btnTickSelected(_ sender: UIButton) {
        if let idx = IndexPath.indexPathForCellContainingView(view: sender, inTableView: tableView) {
            let isSelected = arrData[idx.row].isSelected
            arrData[idx.row].isSelected = !isSelected
            tableView.reloadData()
        }
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        let selectedActivities = arrData.filter{$0.isSelected}
        let activityId = selectedActivities.map{$0.id}.filter{!$0.isEmpty}.joined(separator: ",")
        
        let arrActivity = selectedActivities.map{$0.activityID.components(separatedBy: ",")}.flatMap{$0}
        let arrRemoveDuplicates = self.removeDuplicates(arr: arrActivity)
        let selectedGun = arrRemoveDuplicates.joined(separator: ",")
        
        guard !activityId.isEmpty && !arrActivity.isEmpty else {
            _ = ValidationToast.showStatusMessage(message: "Please choose your desire baby category ")
            return
        }
        
        let param: [String: Any] = ["activity": activityId, "activity_gun": selectedGun, "user_id": _user.id]
      
        self.selectDesireBaby(param: param) { (done,dicInfo) in
            if done {
                // Add new condition.
                // For status planning and payment completed and reg_user.
                //                if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
                //free_demo_payment
               
                if self.checkUserFreeDemo && self.whichUser == "demo_user" {
                    self.prepareForFreeUserDialog(dicInfo: dicInfo)
                }else {
                    if(_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()){
                      
                        if _user.isDemoPaymentSkip {
                            if _user.pregnancyStatus == .planning {
                                self.prepareOnBoarding()
                            }else {
                                self.prepareOnBoardingPregnancyStatus()
                            }
                        }else {
                            if _user.pregnancyStatus == .planning {
                                self.prepareOnBoarding()
                            }else {
                                self.prepareOnBoardingPregnancyStatus()
                            }
                        }
                    }
                    else {
                        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                            self.prepareOnBoarding()
                        }else if _user.currDay == 0 && _user.paymentStatus == .completed && (_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)){
                            self.prepareOnBoarding()
                        }else if _user.currDay == 0 && _user.paymentStatus == .completed && (_user.whichUser.isEqual(str: WhichUser.reg_user.rawValue)){
                            self.prepareOnBoarding()
                        }else{
                            self.performSegue(withIdentifier: "dashboardSegue", sender: nil)
                        }
                    }
                }
                
            }
        }
    }
}

extension BabyCategoryVC {
    
    func prepareOnBoardingPregnancyStatus() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.actionBlock = { (action) in
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            self.getPregnancyDay(currentDay: currDay, block: { (done) in
                if done {
                    _user.currDay = Int16(currDay)
                    _appDelegator.saveContext()
                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                    onBoardingView.removeFromSuperview()
                    self.performSegue(withIdentifier: "dashboardSegue", sender: nil)
                }
            })
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = FreeDashBoard.instantiateView(with: self.view)
        onBoardingView.updateUI()
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .later{
                isfreeDashboard = true
                UserDefaults.standard.setValue(isfreeDashboard, forKey: "freeDashboardSkipFromAdmin")
                UserDefaults.standard.setValue(true, forKey: "alreadyOpenPopup")
                UserDefaults.standard.synchronize()
                _user.currDay = 0
                _appDelegator.saveContext()
                
                // show free dashboard.
                _appDelegator.isClickFromDesierBaby = true
                _appDelegator.navigateUserToHome()
            }else{
                isfreeDashboard = false
                UserDefaults.standard.setValue(isfreeDashboard, forKey: "freeDashboardSkipFromAdmin")
                UserDefaults.standard.synchronize()
                // show paid dashboard with set today date in pregnent day as first.
                self.prepareOnBoardingPregnancyStatus()
            }
        }
    }
    
    
    func prepareForFreeUserDialog(dicInfo : NSDictionary) {
        let onBoardingView = FreeDemoView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .done{
                 self.getPregnancyDay(currentDay: 1, block: { (done) in
                     if done {
                        self.getToday(block: { (done) in
                            if done {
                                _user.setDesiredBaby(dict: dicInfo)
                                _appDelegator.saveContext()
                                
                                _appDelegator.setOnBoarding(isOnBoardingShown: true)
                                _appDelegator.navigateDirectlyToHome()
                            }
                        })
                     }
                 })
            }
        }
    }
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
    
}

extension BabyCategoryVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrData == nil ? 0 : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.widthRatio
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BabyCategoryCell
        let data = arrData[indexPath.row]
        cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BabyCategoryCell
        cell.prepareCellUI(with: data)
        cell.btnTick.isSelected = data.isSelected
        return cell
    }
}

extension BabyCategoryVC {
    
    func getBabyCategory() {
        self.showCentralSpinner()
        KPWebCall.call.getBabyCategory { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let arrData = dict["data"] as? [NSDictionary] {
                self.arrData = []
                for dataDict in arrData {
                    let objDict = ModelBabyCategory(dict: dataDict)
                    self.arrData.append(objDict)
                }
                self.tableView.reloadData()
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func getToday(block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                            self?.whichUser = dict.getStringValue(key: "which_user ")
                            let demoDay = dict.getInt16Value(key: "demo_day")
                            let whichUser = dict.getStringValue(key: "which_user ")
                            
                            print("\(demoDay) \(whichUser)")
                            _user.setCurrentDay(dict: dict)
                            _user.setWhichUserType(strType: whichUser)
                            _user.setChangeDay(dict: dict)
                            _user.setappstop(dict: dict)
                            _appDelegator.saveContext()
                            block(true)
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    
//    func getToday(block: @escaping (Bool) -> ()) {
//   // func getToday() {
//        let param: [String: Any] = ["user_id": _user.id]
//        self.showCentralSpinner()
//        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
//            guard let weakself = self else {return}
//            weakself.hideCentralSpinner()
//            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
//                if status == 200 {
//                    DispatchQueue.main.async {
//                        self?.whichUser = dict.getStringValue(key: "which_user ")
//
//                    }
//                }
//                else {
//                    weakself.showError(data: json, view: weakself.view)
//
//                }
//            }else {
//                weakself.showError(data: json, view: weakself.view)
//            }
//        }
//    }
//
    
    func selectDesireBaby(param: [String: Any], block: @escaping (Bool,NSDictionary) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.selectDesireBaby(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary {
               
                //if self.checkUserFreeDemo == false && self.whichUser != "demo_user" {
                    _user.setDesiredBaby(dict: dict)
                    _appDelegator.saveContext()
               // }
                _ = ValidationToast.showStatusMessage(message: dict.getStringValue(key: "message"))
                block(true,dict)
            } else {
                self.showError(data: json, view: self.view)
                block(false,NSDictionary())
            }
        }
    }
}
