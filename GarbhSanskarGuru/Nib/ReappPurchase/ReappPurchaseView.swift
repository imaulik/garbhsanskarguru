//
//  ReappPurchaseView.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 18/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ReappPurchaseView: ConstrainedView {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var btnYesProceed: UIButton!

    var actionBlock: (KAction)->Void = {_ in}

    class func instantiateView(with view: UIView) -> ReappPurchaseView {
        
        let objView = UINib(nibName: "ReappPurchaseView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ReappPurchaseView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        
        objView.btnYesProceed.layer.cornerRadius = 10
        
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
}

extension ReappPurchaseView {
    
    @IBAction func btnYesProceedPopUpTapped(_ sender: UIButton){
        self.actionBlock(.done)
    }
    
    @IBAction func btnClosePopUpTapped(_ sender: UIButton){
        self.actionBlock(.cancel)
    }
}

