//
//  DietVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class DietVC: ParentViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollConst: NSLayoutConstraint!
    @IBOutlet weak var btnSpecial: UIButton!
    @IBOutlet weak var btnMonthly: UIButton!
    @IBOutlet weak var roundView: UIView!
    
    var isActivityDone: Bool = false
    var selected_DashboardId:Int? = nil

    var childSpecial: TodaySpecialVC {
        return (self.children[0] as? TodaySpecialVC)!
    }
    
    var childMonthly: MonthlyVC {
        return (self.children[1] as? MonthlyVC)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension DietVC {
   
    func prepareUI() {
        
        self.roundView.backgroundColor = self.isActivityDone ? GarbhColor.green : GarbhColor.pink

        guard childSpecial.data != nil else {return}
        //self.roundView.backgroundColor = self.childSpecial.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
    }
    
    func prepareOnBoarding() {
        let onBoardingView = TaskCompleteView.instantiateView(with: self.view, with: "Diet")
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                //diet_receipe - old name comment. New name refrence by Android app from developer.
                let activity_id = self.scrollView.currentPage == 0 ? self.childSpecial.data.activityId : self.childMonthly.data.activityId
                let daily_activity_id = self.scrollView.currentPage == 0 ? self.childSpecial.data.id : self.childMonthly.data.id
                self.getActivityDone(with: "diet", activityId: String(activity_id), dailyActivityId: "\(daily_activity_id)") { (completed) in
                    if completed {
                        onBoardingView.removeFromSuperview()
                        NotificationCenter.default.post(name: Notification.Name(garbhNotificationUpdateActicityStatus), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    func openDisaplyInfoView() {
        let onBoardingView = DisplayInfoView.instantiateView(with: self.view)
        onBoardingView.setupUI()
        onBoardingView.setupValues(valueIndex: self.selected_DashboardId!)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
        }
    }
}

extension DietVC {
    
    @IBAction func btnSpecialTapped(_ sender: UIButton) {
        scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height), animated: true)
    }
    
    @IBAction func btnMonthlyTapped(_ sender: UIButton) {
        scrollView.scrollRectToVisible(CGRect(x: scrollView.frame.width, y: 0, width: scrollView.frame.width, height: scrollView.frame.height), animated: true)
    }
    
    @IBAction func btnTaskTapped(_ sender: UIButton) {
        
        if(isActivityDone){
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kTaskCompleted)
        }
        else {
            self.prepareOnBoarding()
        }
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
        self.openDisaplyInfoView()
    }
}

extension DietVC {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currPage = scrollView.currentPage
        if currPage == 0 {
            //self.roundView.backgroundColor = self.childSpecial.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
            btnSpecial.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .medium)
            btnMonthly.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .regular)
            scrollConst.constant = 0
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }else {
            //self.roundView.backgroundColor = self.childMonthly.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
            btnMonthly.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .medium)
            btnSpecial.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .regular)
            scrollConst.constant = scrollView.frame.width / 2
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension DietVC {
    
    func getDietActivity() {
        let param: [String: Any] = ["day": _user.currDay, "type": "month"]
        self.showCentralSpinner()
        KPWebCall.call.getDietData(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    kprint(items: data)
                }else {
                    self.showError(data: json, view: self.view)
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
}
