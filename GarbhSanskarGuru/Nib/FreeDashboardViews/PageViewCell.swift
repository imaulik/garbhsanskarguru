//
//  PageViewCell.swift
//  deal
//
//  Created by LN-iMAC-001 on 27/12/19.
//  Copyright © 2019 LN-iMAC-001. All rights reserved.
//

import UIKit

class PageViewCell: UITableViewCell {
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageview: UIView!
    
    var isGIFImage:Bool = false
    
    var arrPageImages = [String]()
    
    var pagerIndex: ((Int) -> Void)?
    @IBOutlet weak var pagerCV: UICollectionView!
    @IBOutlet var cvLayoutConstant: NSLayoutConstraint!
  
    var animationTimer = Timer()
  //  @IBOutlet weak var pageControl: UIPageControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codess
        self.pagerCV.register(UINib(nibName: "PagerCVCell", bundle: .main), forCellWithReuseIdentifier: "PagerCVCell")
        animationTimer.invalidate()
     }

    

    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func addPageControl(){
        if(arrPageImages.count > 0){
            animationTimer.invalidate()
            startTimer()
            pageControl.numberOfPages = arrPageImages.count
        }
    }
  
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = pagerCV {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)! < arrPageImages.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
            }
        }
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    @objc func startTimer() {
        animationTimer =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    func timerActivate(){
        startTimer()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}


extension PageViewCell: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.size.width, height:collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrPageImages.count > 0 {
            addPageControl()
        }
         return arrPageImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"PagerCVCell", for: indexPath) as! PagerCVCell
        if self.isGIFImage{
            cell.imgPage.image = UIImage.gif(name: arrPageImages[indexPath.item])
        }else{
            cell.imgPage.image = UIImage(named: arrPageImages[indexPath.item])
        }
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      if arrPageImages.count - 1 == self.pageControl.currentPage {
          self.pageControl.currentPage = 0
      } else {
          self.pageControl.currentPage = indexPath.item
      }
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let handler = pagerIndex {
            handler(indexPath.item)
        }
    }
   
}


