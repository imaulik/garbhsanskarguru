//
//  SignupThirdStep.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 10/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import EasyTipView

class SignupThirdStep: ParentViewController {
    
    // MARK: - Outlet Declaration
    @IBOutlet var viewAccessory: UIView!
    
    @IBOutlet var label_Desc: JPWidthLabel!
    @IBOutlet var label_title: JPWidthLabel!
    
    @IBOutlet var tfName: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfNumber: UITextField!
    @IBOutlet var tfCity: UITextField!
    @IBOutlet var tfcountryCode: UITextField!
    @IBOutlet var button_info: [UIButton]!
    @IBOutlet var buttonEye: JPWidthButton!{
        didSet{
            buttonEye.isSelected = true
        }
    }
    
    weak var tipView: EasyTipView?
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("country code : \(_appDelegator.getCountryCode())")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        prepareUI()
    }
    
    override func viewDidLayoutSubviews() {
        DispatchQueue.main.async {
            self.button_info?.forEach({ (btn) in
                btn.makeCircle()
            })
        }
    }
    
    // MARK: - Private Methods
    func prepareUI(){
        label_Desc.text = _appDelegator.languageBundle?.localizedString(forKey: "signup_sp3_desc", value: "", table: nil)
        label_title.text = _appDelegator.languageBundle?.localizedString(forKey: "signup_sp3_title", value: "", table: nil)
        
        viewAccessory.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: 44)
        tfName.inputAccessoryView = viewAccessory
        tfEmail.inputAccessoryView = viewAccessory
        tfNumber.inputAccessoryView = viewAccessory
        tfPassword.inputAccessoryView = viewAccessory
       
        tfCity.text = _appDelegator.objInfo.city
        tfcountryCode.text = _appDelegator.getCountryCode()
    }
    
    func addValues(){
        _appDelegator.objInfo.name = tfName.text!
        _appDelegator.objInfo.email = tfEmail.text!
        _appDelegator.objInfo.phone = tfNumber.text!
        _appDelegator.objInfo.password = tfPassword.text!
        _appDelegator.objInfo.city = tfCity.text!
//        _appDelegator.objInfo.countryCode = tfcountryCode.text?.replacingOccurrences(of: "+", with: "")
    }
    
    func prepareToolTip(clickSender: UIButton, dispalyText:String){
        var preferences = EasyTipView.globalPreferences
        preferences.drawing.arrowPosition = .right
        preferences.drawing.font = UIFont.GarbhFontWith(.Ubuntu_Bold, size: 12.0)
        preferences.drawing.textAlignment = .center
        let bgColor = UIColor.colourWith(red: 239, green: 65, blue: 114, alpha: 1.0)
        preferences.drawing.backgroundColor = bgColor
        preferences.drawing.foregroundColor = .white
        
        preferences.positioning.maxWidth = _screenSize.width * 0.75
        
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 100, y: 0)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 100, y: 0)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1
        preferences.animating.dismissDuration = 1
        
        //EasyTipView.show(forView: button_info[clickSender.tag], text: dispalyText, preferences: preferences)
        let tip = EasyTipView(text: dispalyText, preferences: preferences, delegate: nil)
        tip.show(forView: button_info[clickSender.tag])
        tipView = tip
    }
    
    
    
    // MARK: - Button Action Methods
    
    @IBAction func action_infoTapped(_ sender: UIButton) {
        print("tag : \(sender.tag)")
        let textvalue = _appDelegator.languageBundle?.localizedString(forKey: "signup_sp3_info\(sender.tag)", value: "", table: nil)
        prepareToolTip(clickSender: sender, dispalyText: textvalue!)
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { _ in
            if let tipView = self.tipView {
                tipView.dismiss(withCompletion: {
                    print("Completion called!")
                })
            }
        }
    }
    
    @IBAction func action_eyeTapped(_ sender: JPWidthButton) {
        guard !tfPassword.text!.isEmpty else {return}
        sender.isSelected = !sender.isSelected
        tfPassword.isSecureTextEntry = !tfPassword.isSecureTextEntry
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton){
        self.view.endEditing(true)
    }
    
    @IBAction func btnCityTapped(_ sender: UIButton){
        self.view.endEditing(true)
        let obj = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "CityListViewController") as! CityListViewController
        obj.modalPresentationStyle = .fullScreen
        self.present(obj, animated: true, completion: nil)
    }
}

extension SignupThirdStep : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var shouldReturn: Bool = false
       
        if textField == tfcountryCode{
            let fullText = (textField.text! as NSString).replacingCharacters(in: range, with: string);
            shouldReturn = fullText.count >= 1
        }else{
            shouldReturn = true
        }
        
        if textField == tfNumber {
            guard let preText = textField.text as NSString?,
                preText.replacingCharacters(in: range, with: string).count <= 13 else {
                    return false
            }
        }
        
        return shouldReturn
    }
    
}



