//
//  DashboardVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

let fRightPaddingToSwipeVwItem:CGFloat = 15.0

struct ModelDashBoard {
    var id:Int = 0
    var title = ""
    var image = ""
}

class DashBoardCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgVwBtnBg: UIImageView!
}

class DashboardVC: ParentViewController, SwipeViewDelegate, SwipeViewDataSource {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblCurrentPregDay: UILabel!
    
    @IBOutlet var btnPrevious: JPWidthButton!
    @IBOutlet var vwSwipe: SwipeView!
    @IBOutlet weak var lblPregnancyDay: UILabel!
    @IBOutlet weak var lblDaysLeft: UILabel!
    @IBOutlet weak var lblActivitiesCompleted: UILabel!
    
    @IBOutlet weak var constraintHightVwSwipe: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthVwSwipe: NSLayoutConstraint!

    var isAlreadyOpenVideoPopup:Bool = false
    var onBoardingDoneActivitiesView: DoneAllActivitiesView!

    var nTodayDayCount = 0
    var arrData: [ModelDashBoard]!
    var dictActivityStatus = [Int: Bool]()
    var statusIndex = 0
    var isOnBoardingShown = false
    var screenType: GarbhScreenType = .todayStory
    
    var language = _appDelegator.getLanguage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receivedNotificationUpdateAllActivityStatus(notification:)), name: Notification.Name(garbhNotificationUpdateActicityStatus), object: nil)
        
        registerBlock()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("viewWillAppear")
        prepareForForceLogOut()
        DispatchQueue.main.async {
            self.vwSwipe.reloadData()
        }
        // condition only for manage previous button on header for allow to see again PreDashboard.
        if _appDelegator.isAllowBackPreDashboard{
            btnPrevious.isHidden = false
        }else{
            btnPrevious.isHidden = true
        }
    }
    
    func registerBlock(){
        _appDelegator.closeSideMenuDetector = { (close) in
            if close{
                if _appDelegator.isAllowBackPreDashboard{
                    self.btnPrevious.isHidden = false
                }else{
                    self.btnPrevious.isHidden = true
                }
            }
        }
        
        _appDelegator.blockClickHerePreDashBoard = { () in
//            self.prepareUI()
//            _user.currDay = Int16(swipeView.itemView(at: index).tag)
//            _appDelegator.saveContext()
            self.getAllActivitiesStatus()
            self.vwSwipe.reloadData()
        }
        
    }
    
}

extension DashboardVC {
    
    func prepareforVideoPopup(objPayment : PaymentVideoModel) {
        let onBoardingView = VideoPopupView.instantiateView(with: self.view)
        onBoardingView.setupUI(btnFirst: "   OK   ", btnsecond: " Subscribe Full Course ", objModel: objPayment, isDemoComplete: true)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        self.isAlreadyOpenVideoPopup = true
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            self.isAlreadyOpenVideoPopup = false
            DispatchQueue.main.async {
                self.reappPurchase(block: { (done) in
                    _appDelegator.navigateUserToHome()
                    if done {
                        //done means k click on subscribe full course button, Should redirect to Payment action click.
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            if _appDelegator.blockFullSubscribeCourse != nil{
                                _appDelegator.blockFullSubscribeCourse!()
                            }
                        }
                    }
                })
            }
        }
    }
    
    func apicallforVideoPopup(videoType:DemoVideoType){
        KPWebCall.call.getVideoForCompleteDemo(param: ["user_id": _user.id, "review":"Yes"]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary {
                let paymentModel = PaymentData(fromDictionary: dict as! [String : Any])
                var paidModel:[PaymentVideoModel] = []
                var typeofVideo = ""
                if videoType == .DemoStart{
                    typeofVideo = "demo_start"
                }else if videoType == .DemoComplete{
                    typeofVideo = "demo_completed"
                }else{
                    typeofVideo = "subscribe_full_course"
                }
                if paymentModel.data.count > 0{
                    if self.language == kEnglish{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == kLongEnglish.lowercased() }
                    }else if self.language == KHindi{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == KLongHindi.lowercased() }
                    }else{
                        paidModel = paymentModel.data.filter { $0.type == typeofVideo && $0.language == "gujrati" }
                    }
                    if paidModel.count > 0{
                        if !self.isAlreadyOpenVideoPopup{
                            self.prepareforVideoPopup(objPayment: paidModel[0])
                        }
                    }
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
}

extension DashboardVC {
    
    func prepareUI() {
        initModel()

        lblUserName.text = _user.name
        vwSwipe.bounces = false
        vwSwipe.isPagingEnabled = false

        if (UIDevice.deviceType == .iPad)
        {
            constraintHightVwSwipe.constant = 100
            constraintWidthVwSwipe.constant = (100*4)+(fRightPaddingToSwipeVwItem*4)
        }
        
        if _appDelegator.getOnBoarding() == nil
        {
            let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
            if isPaymentAllowed {
                // Add new condition
//                if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
                if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                    self.prepareOnBoardingFreeDashboard()
                }
                    // comment conditon . Open popup while payment complete.
//                else if _user.pregnancyStatus == .planning {
//                else if _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
//                    prepareOnBoarding()
//                }
                else {
                    self.getToday(block: { (done) in
                        if done {
                            self.setToday(with: _user.currDay)
                        }
                    })
                }
            } else {
                if _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                    prepareOnBoarding()
                }else{
                    self.getToday(block: { (done) in
                        if done {
                            self.setToday(with: _user.currDay)
                        }
                    })
                }
                // comment line .
//                prepareOnBoarding()
            }
        } else {
            self.getToday(block: { (done) in
                if done {
                    self.setToday(with: _user.currDay)
                }
            })
        }
    }
    
    func prepareOnBoardingDoneActivities() {
        onBoardingDoneActivitiesView = DoneAllActivitiesView.instantiateView(with: self.view)
        onBoardingDoneActivitiesView.bounceView(animateView: onBoardingDoneActivitiesView.baseView)
        onBoardingDoneActivitiesView.actionBlock = { (action) in
            switch action {
            case .cancel:
                break
            case .done:
                self.onBoardingDoneActivitiesView.removeFromSuperview()
                break
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    func prepareOnBoardingTrialOverPopup() {
    
        let onBoardingView = TrialOverView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            
            switch action {
            case .done:
                DispatchQueue.main.async {
                    self.reappPurchase(block: { (done) in
                        if done {
                            _appDelegator.navigateUserToHome()
                        }
                    })
                }
                break
            default:
                break
            }
            onBoardingView.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var isActivityDone:Bool = false
        if let index = sender as? Int {
            if(dictActivityStatus[arrData[index].id] != nil){
                isActivityDone = dictActivityStatus[arrData[index].id] ?? false
            }
        }
        var currentId = 0
        if let index = sender as? Int {
            currentId = arrData[index].id
        }
        
        if segue.identifier == "videoImageSegue", let destVC = segue.destination as? TodayStoryVC{
            if let index = sender as? Int {
                if index == 2 {
                    self.screenType = .todayStory
                }else if index == 3 {
                    self.screenType = .activityOfDay
                }else if index == 4 {
                    self.screenType = .prayer
                }else if index == 5 {
                    self.screenType = .yoga
                }else if index == 6 {
                    self.screenType = .biography
                }else if index == 8 {
                    self.screenType = .shloka
                }else if index == 9 {
                    self.screenType = .video
                }else if index == 10 {
                    self.screenType = .brainActivity
                }else if index == 11 {
                    self.screenType = .garbhSatsang
                }
            }
            destVC.screenType = self.screenType
            destVC.isActivityDone = isActivityDone
            destVC.selected_DashboardId = currentId
        }
        else if segue.identifier == "dearMomSegue", let destVC = segue.destination as? DearMomVC {
            destVC.isActivityDone = isActivityDone
            destVC.selected_DashboardId = currentId
        }
        else if segue.identifier == "garbhSamvadSegue", let destVC = segue.destination as? GarbhSamvadVC {
            destVC.isActivityDone = isActivityDone
            destVC.selected_DashboardId = currentId
        }
        else if segue.identifier == "dietSegue", let destVC = segue.destination as? DietVC {
            destVC.isActivityDone = isActivityDone
            destVC.selected_DashboardId = currentId
        }
        else if segue.identifier == "brainActivitySegue", let destVC = segue.destination as? BrainActivityVC {
            destVC.isActivityDone = isActivityDone
            destVC.selected_DashboardId = currentId
        }
    }
    
    func prepareForForceLogOut(){
        self.forceLogoutApi()
    }
    
}

extension DashboardVC {
    
    func initModel() {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        
        switch currLanguage {
        case .hindi:
            arrData = [ModelDashBoard(id: 1, title: "प्यारी माँ..!", image: "dearmom"),ModelDashBoard(id: 2, title: "गर्भसंवाद", image: "garbhsamvad"),ModelDashBoard(id: 3, title: "आज की कहानी", image: "story"),ModelDashBoard(id: 4, title: "प्रवृत्ति", image: "activity"),ModelDashBoard(id: 5, title: "प्रार्थना", image: "prayers"),ModelDashBoard(id: 6, title: "योग", image: "yoga"),ModelDashBoard(id: 7, title: "जीवनचरित्र में से ..", image: "frombiography"),ModelDashBoard(id: 8, title: "आहार", image: "diet"),ModelDashBoard(id: 9, title: "श्लोक-साखी", image: "shloksakhi"),ModelDashBoard(id: 10, title: "वीडियो", image: "vdo"),ModelDashBoard(id: 11, title: "ब्रेइन एक्टिविटी", image: "Puzzle"),ModelDashBoard(id: 12, title: "गर्भ सत्संग", image: "GarbhSastsang")]
        case .english:
            arrData = [ModelDashBoard(id: 1, title: "Dear Mom", image: "dearmom"),ModelDashBoard(id: 2, title: "Garbh Samvad", image: "garbhsamvad"),ModelDashBoard(id: 3, title: "Today's Story", image: "story"),ModelDashBoard(id: 4, title: "Activity of the day", image: "activity"),ModelDashBoard(id: 5, title: "Prayer", image: "prayers"),ModelDashBoard(id: 6, title: "Yoga", image: "yoga"),ModelDashBoard(id: 7, title: "From Biography", image: "frombiography"),ModelDashBoard(id: 8, title: "Diet", image: "diet"),ModelDashBoard(id: 9, title: "Shloka-Sakhi", image: "shloksakhi"),ModelDashBoard(id: 10, title: "Video", image: "vdo"), ModelDashBoard(id: 11, title: "Brain Activity", image: "Puzzle"),ModelDashBoard(id: 12, title: "Garbh Satsang", image: "GarbhSastsang")]
        case .gujarati:
            arrData = [ModelDashBoard(id: 1, title: "વ્હાલી માં ..!", image: "dearmom"),ModelDashBoard(id: 2, title: "ગર્ભસંવાદ", image: "garbhsamvad"),ModelDashBoard(id: 3, title: "આજ ની વાર્તા", image: "story"),ModelDashBoard(id: 4, title: "પ્રવૃત્તિ", image: "activity"),ModelDashBoard(id: 5, title: "પ્રાર્થના", image: "prayers"),ModelDashBoard(id: 6, title: "યોગ", image: "yoga"),ModelDashBoard(id: 7, title: "જીવનચરિત્ર માંથી ..", image: "frombiography"),ModelDashBoard(id: 8, title: "આહાર", image: "diet"),ModelDashBoard(id: 9, title: "શ્લોક-સાખી", image: "shloksakhi"),ModelDashBoard(id: 10, title: "વિડીઓ", image: "vdo"),ModelDashBoard(id: 11, title: "બ્રેઈન એક્ટીવીટી", image: "Puzzle"),ModelDashBoard(id: 12, title: "ગર્ભ સત્સંગ", image: "GarbhSastsang")]
        }
        collectionView.reloadData()
        
        languageChange()
    }
    
    func setToday(with day: Int16) {
        self.lblDay.text = "\(day)"
        let dayDiff = 280 - day
        self.lblCurrentPregDay.text = "Your Pregnancy remaining days is \(dayDiff)"
        self.vwSwipe.reloadData()
    }
    
    func shareApp(sender: UIButton) {
         let title = _appName
         let textToShare = "Garbh Sanskar Guru App is a unique and first of it’s kind mobile application to nurture the values in unborn child. This App contains total 280 days of various activities for pregnant lady. Expecting moms can perform these activities on day to day basis which is going to help overall development of child.\n"
        let appStoreText = "Link for App Store"
        let playStoreText = "Link for Play Store"
        let myWebsite = URL(string: "https://itunes.apple.com/us/app/majestic-garbh-sanskar/id1448235489?ls=1&mt=8")
        let otherWebsite = URL(string: "http://play.google.com/store/apps/details?id=com.gs.garbhsanskarguru")
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [textToShare,appStoreText,myWebsite!,playStoreText,otherWebsite!], applicationActivities: nil)
        activityViewController.setValue(title, forKey: "Subject")
        activityViewController.popoverPresentationController?.sourceView = (sender)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.actionBlock = { (action) in
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            self.getPregnancyDay(currentDay: currDay, block: { (done) in
                if done {
                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                    onBoardingView.removeFromSuperview()
                    DispatchQueue.main.async {
                        self.getToday(block: { (done) in
                            if done {
                                self.setToday(with: _user.currDay)
                            }
                        })
                    }
                }
            })
        }
    }
    
    func prepareOnBoardingFreeDashboard() {
        let onBoardingView = FreeDashBoard.instantiateView(with: self.view)
        onBoardingView.updateUI()
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .later{
                // show free dashboard.
//                _appDelegator.setOnBoarding(isOnBoardingShown: true)
                _appDelegator.navigateUserToHome()
            }else{
                // show paid dashboard with set today date in pregnent day as first.
                self.prepareOnBoarding()
            }
        }
    }
    
    func languageChange() {
        language = _appDelegator.getLanguage()
        lblPregnancyDay.text = _appDelegator.languageBundle?.localizedString(forKey: "Pregnancy day", value: "", table: nil)
    }
    
    @objc func receivedNotificationUpdateAllActivityStatus(notification: Notification) {
        getAllActivitiesStatus()
    }
}

extension DashboardVC {
    
    @IBAction func btnShareApp(_ sender: UIButton) {
        self.shareApp(sender: sender)
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        
        /*
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "notiSegue", sender: nil)
        }
        */
        
        _appDelegator.presentAnnouncementVC()
    }
    
    @IBAction func btnCurrentDayNavigate( _ sender: UIButton) {
        var currDay = _user.currDay
        if sender.tag == 0 {
            guard statusIndex != 3 else {return}
            self.statusIndex += 1
            currDay -= 1
            self.setToday(with: currDay)
        } else {
            guard statusIndex != 0 else {return}
            self.statusIndex -= 1
            currDay += 1
            self.setToday(with: currDay)
        }
        _user.currDay = currDay
        _appDelegator.saveContext()
    }
    
    @IBAction func btnPreviousTapped(_ sender: UIButton) {
        _appDelegator.isShowPrePaidDashboard = true
        self.tabBarController?.selectedIndex = 2
        if _appDelegator.detectBackFromDashboard != nil{
            _appDelegator.detectBackFromDashboard!()
        }
    }
    
}

extension DashboardVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,              UICollectionViewDelegate  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrData == nil ? 0 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DashBoardCell
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DashBoardCell
        cell.lblTitle.text = arrData[indexPath.row].title
        cell.imgView.image = UIImage(named: arrData[indexPath.row].image)
        
        if(dictActivityStatus[arrData[indexPath.row].id] != nil){
            cell.imgVwBtnBg.image = (dictActivityStatus[arrData[indexPath.row].id] == false) ? UIImage(named: "gradient-pink.png") : UIImage(named: "gradient-green.png")
        }
        else
        {
            cell.imgVwBtnBg.image = UIImage(named: "gradient-pink.png")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if indexPath.item == 0 {
                self.performSegue(withIdentifier: "dearMomSegue", sender: indexPath.item)
            }else if indexPath.item == 1 {
                self.performSegue(withIdentifier: "garbhSamvadSegue", sender: indexPath.item)
            }else if indexPath.item == 7 {
                self.performSegue(withIdentifier: "dietSegue", sender: indexPath.item)
            }else if indexPath.item == 10 {
                self.performSegue(withIdentifier: "brainActivitySegue", sender: indexPath.item)
            }else{
                self.performSegue(withIdentifier: "videoImageSegue", sender: indexPath.item)
            }
        } 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collViewWidth = collectionView.frame.size.width - 40
        return CGSize(width: collViewWidth / 2, height: 160.widthRatio)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension DashboardVC {
    
    //MARK: SwipeView DataSource & Delegate
    
    func numberOfItems(in swipeView: SwipeView!) -> Int
    {
        if(nTodayDayCount > 0 && nTodayDayCount < 4)
        {
            return nTodayDayCount
        }
        else if(nTodayDayCount >= 4) {
            return 4
        }
        else {
            return 0;
        }
    }
    
    func swipeView(_ swipeView: SwipeView!, viewForItemAt index: Int, reusing view: UIView!) -> UIView! {
                
        let cell = UINib(nibName: "DashboardDaySelectionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DashboardDaySelectionView
        cell.frame = CGRect.init(x: 0, y: 0, width: swipeView.frame.size.height+fRightPaddingToSwipeVwItem, height: swipeView.frame.size.height)

        cell.backgroundColor = GarbhColor.white
        cell.vwDayCircle.backgroundColor = GarbhColor.gray
        cell.lblDay.textColor = GarbhColor.white

        cell.lblDay.font = UIFont.GarbhFontWith(GarbhSanskarFont.Ubuntu_Bold, size: 18.widthRatio)
        
        if(nTodayDayCount > 0 && nTodayDayCount < 4){
            
            cell.tag = index+1
        }
        else if(nTodayDayCount >= 4) {
            
            cell.tag = (Int(nTodayDayCount)-(swipeView.numberOfItems-(index+1)))
        }
        cell.lblDay.text = "\(cell.tag)"

        if(cell.tag == nTodayDayCount){
            cell.vwDayCircle.applyGradientEffects([GarbhColor.gradient1Pink, GarbhColor.gradient2Pink], gradientPoint: .bottomLeftTopRight)
        }
        if(cell.tag != nTodayDayCount && cell.tag == Int(_user.currDay)){
            cell.vwDayCircle.applyGradientEffects([GarbhColor.gradient1Blue, GarbhColor.gradient2Blue], gradientPoint: .bottomLeftTopRight)
        }
        
        cell.vwDayCircle.clipsToBounds = true
        cell.vwDayCircle.layer.cornerRadius = swipeView.frame.size.height/2

        return cell
    }
    
    func swipeView(_ swipeView: SwipeView!, didSelectItemAt index: Int) {
        
        _user.currDay = Int16(swipeView.itemView(at: index).tag)
        _appDelegator.saveContext()

        getAllActivitiesStatus()
        
        swipeView.reloadData()
        
    }
}

extension DashboardVC {
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func getToday(block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        
                        if(!deviceId.isEqual(str: _deviceID)){
                            
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            block(false)
                        }
                        else {
                            
                            let demoDay = dict.getInt16Value(key: "demo_day")
                            let whichUser = dict.getStringValue(key: "which_user ")

                           
                            
                            print("\(demoDay) \(whichUser)")
                            _user.setCurrentDay(dict: dict)
                            _user.setWhichUserType(strType: whichUser)
                            _user.setChangeDay(dict: dict)
                            _user.setappstop(dict: dict)
                            _appDelegator.saveContext()
                            weakself.nTodayDayCount = Int(_user.currDay)
                            
                            if(weakself.nTodayDayCount <= 3){
                                weakself.constraintWidthVwSwipe.constant = (weakself.constraintHightVwSwipe.constant * CGFloat(weakself.nTodayDayCount)) + (fRightPaddingToSwipeVwItem * CGFloat(weakself.nTodayDayCount))
                            }
                            
                            if(_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)){
                                
                                if(demoDay == 0){
                                    weakself.lblDaysLeft.text = "Trial expires in 3 days"
                                }
                                else if(demoDay == 1) {
                                    weakself.lblDaysLeft.text = "Trial expires in 2 days"
                                }
                                else if(demoDay == 2) {
                                    
                                    weakself.lblDaysLeft.text = "Trial expires in 1 days"
                                }
                                else {
                                    weakself.lblDaysLeft.text = "Trial expired"
                                    // comment one line of code.
//                                    weakself.prepareOnBoardingTrialOverPopup()
//                                     Demo video api call while complete demo, On click button call another api for remove user credentials data and                                     redirect to FreeAppDashboard.
                                    weakself.apicallforVideoPopup(videoType: .DemoComplete)
                                }
                            }
                            else {
                                weakself.lblDaysLeft.text = "\(280-Int(_user.currDay)) Days left"
                            }
                            
                            if(_user.currDay > 0){
                                weakself.getAllActivitiesStatus()
                            }
                            if _appDelegator.getOnBoarding() == nil{
                                if(_user.currDay == 0){
                                    let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
                                    if isPaymentAllowed {
                                        // Add new condition
//                                        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
                                        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                                            weakself.prepareOnBoardingFreeDashboard()
                                        }
                                    }
                                }
                            }
                            block(true)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func forceLogoutApi() {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        
                        if(!deviceId.isEqual(str: _deviceID)){
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
    
    @objc func getAllActivitiesStatus() {
        
        let nActiDataForDay:Int = Int(_user!.currDay)
        
        let param: [String: Any] = ["user_id": _user.id, "day": nActiDataForDay]
        self.showCentralSpinner()
        KPWebCall.call.getDashboardAllActivityStatus(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            self!.dictActivityStatus.removeAll()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["success"] as? Int {
                if status == 200 {
                
                    if let arrResult = dict["data"] as? [NSDictionary]{
                        for result in arrResult
                        {
                        
                            let module_name:String  = result.getStringValue(key: "description")
                            let activity_status:String  = result.getStringValue(key: "activity_status")

                            var status:Bool = true
                            if(activity_status.lowercased().isEqual(str: "undone"))
                            {
                                status = false
                            }
                            
                            switch(module_name.lowercased()){
                            case "dear_mom":
                                self!.dictActivityStatus[1] = status
                                break
                            case "garbhsamvad":
                                self!.dictActivityStatus[2] = status
                                break
                            case "today_story":
                                self!.dictActivityStatus[3] = status
                                break
                            case "activity_day":
                                self!.dictActivityStatus[4] = status
                                break
                            case "prayer":
                                self!.dictActivityStatus[5] = status
                                break
                            case "yoga":
                                self!.dictActivityStatus[6] = status
                                break
                            case "biography":
                                self!.dictActivityStatus[7] = status
                                break
                            case "diet":
                                self!.dictActivityStatus[8] = status
                                break
                            case "shloka":
                                self!.dictActivityStatus[9] = status
                                break
                            case "video":
                                self!.dictActivityStatus[10] = status
                                break
                            case "puzzle":
                                self!.dictActivityStatus[11] = status
                                break
                            case "garbhsatsang":
                                self!.dictActivityStatus[12] = status
                                break
                            default:
                                break
                            }
                        }
                        
                        var nFinishedActivity: Int = 0
                        
                        for i in 0...self!.dictActivityStatus.count-1{
                            
                            let nKey = Array(self!.dictActivityStatus)[i].key
                            
                            if(self!.dictActivityStatus[nKey]!){
                                nFinishedActivity += 1
                            }
                        }
                        
                        if(nActiDataForDay == self?.nTodayDayCount){
                            
                            _nTodaysCompletedActivities = nFinishedActivity
                            if(nFinishedActivity == 12){
                                //Today's All 10/12 activity finished
                                updateReminderNotificatinsForToday()
                            }
                            else if(nFinishedActivity > 0 && nFinishedActivity < 10){
                                //Today's some x/12 activity finished
                                updateReminderNotificatinsForToday()
                            }
                        }
                        
                        self!.lblActivitiesCompleted.text = "\(nFinishedActivity)/12 Activities completed"
                        
                        if(nFinishedActivity == 12){
                            
                            //All activity finished
                            self!.prepareOnBoardingDoneActivities()
                        }
                    }
                    
                } else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }
            else {
                weakself.showError(data: json, view: weakself.view)
            }
            self!.collectionView.reloadData()
        }
    }
    
    func reappPurchase(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        
        let param: [String: Any] = ["user_id": _user.id]
        KPWebCall.call.setReappPurchase(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let userInfo = dict["data"] as? NSDictionary{
                    DispatchQueue.main.async {
                        _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
                        _user.initWith(entry: userInfo)
                        _appDelegator.saveContext()
                        block(true)
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            } else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}
