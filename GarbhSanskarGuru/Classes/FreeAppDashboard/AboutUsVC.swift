//
//  AboutUsVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 26/04/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class AboutUsVC: ParentViewController {
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtGujarati: UITextView!
    @IBOutlet weak var txtHindi: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromAboutUs {
            self.txtView.text = ""
            self.txtGujarati.isHidden = true
            self.txtHindi.isHidden = true
            self.title = "About US"
            var selectedLang = ""
            let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
            
            if currLanguage == .hindi{
                selectedLang = "hi"
            }else if currLanguage == .english{
                selectedLang = "en"
            }else{
                selectedLang = "gu"
            }
            let param = ["lang":selectedLang]
            
            self.showCentralSpinner()
            KPWebCall.call.getAboutUs(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let res = AboutUsModel(dictionary: dict)
                    guard let response = res.results else { return }
                    
                    var htmlString = ""
                    if selectedLang == kEnglish {
                        htmlString = response[0].about_contenten
                    }else if  selectedLang == kGujrati {
                        htmlString = response[0].about_contentgu
                    }else if selectedLang == KHindi{
                        htmlString = response[0].about_contenthi
                    }
                    
                    guard let data = htmlString.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return  }
                    guard let html = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else { return}
                    html.addAttribute(NSAttributedString.Key.font, value:UIFont(name: "WorkSans-Medium", size: 18)! , range: NSMakeRange(0, html.length))
                    html.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray , range: NSMakeRange(0, html.length))
                    weakself.txtView.attributedText = html

                }
            }
            
            
//            callWs_AboutUS(parms:parms) { response  in
//                 var htmlString = ""
//                 if selectedLang == kEnglish {
//                     htmlString = response?[0].about_contenten ?? ""
//                 }else if  selectedLang == kGujrati {
//                     htmlString = response?[0].about_contentgu ?? ""
//                 }else if selectedLang == KHindi{
//                     htmlString = response?[0].about_contenthi ?? ""
//                 }
//
//                 guard let data = htmlString.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return  }
//                 guard let html = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else { return}
//                 html.addAttribute(NSAttributedString.Key.font, value:UIFont(name: "WorkSans-Medium", size: 18)! , range: NSMakeRange(0, html.length))
//                 html.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray , range: NSMakeRange(0, html.length))
//                 self.txtView.attributedText = html
//             }
        }
        else {
            self.title = "FAQ"
            let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
            if currLanguage == .hindi{
                self.txtView.isHidden = true
                self.txtHindi.isHidden = false
            }else{
                self.txtView.isHidden = true
                self.txtGujarati.isHidden = false
            }
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        isFromAboutUs = false
    }
}
