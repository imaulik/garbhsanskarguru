//
//  ExploreView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 15/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ExploreView: ConstrainedView {

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var baseView: UIView!
    
    var actionBlock: (KAction)->Void = {_ in}

    class func instantiateView(with view: UIView, with price: String) -> ExploreView {
        let objView = UINib(nibName: "ExploreView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ExploreView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        objView.lblPrice.text = "₹ \(price)"
        view.addSubview(objView)
        return objView
    }

}

extension ExploreView {
    
    @IBAction func btnProTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func btnFreeTapped(_ sender: UIButton) {
        self.actionBlock(.other)
    }
}

