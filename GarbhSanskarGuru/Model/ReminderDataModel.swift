//
//  ReminderDataModel.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 31/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ReminderDataModel: NSObject {
    
    static let shared: ReminderDataModel = ReminderDataModel()
    
    var id : Int!
    var message : String! = ""
    var weekDays : String!
    var indetifier_day_1 : String! = ""
    var indetifier_day_2 : String! = ""
    var indetifier_day_3 : String! = ""
    var indetifier_day_4 : String! = ""
    var indetifier_day_5 : String! = ""
    var indetifier_day_6 : String! = ""
    var indetifier_day_7 : String! = ""
    var reminder_date : String!
    
    
    // Get all from Reminder Table.
    
    func loadReminder(lastrecord:Bool = false, byId:Int = 0) -> [ReminderDataModel]! {
        var reminderdata = [ReminderDataModel]()
        
        if SqliteDataManager.shared.openDatabase() {
            var query = "select * from tbl_reminder"
            
            if lastrecord{
                query = "select * FROM tbl_reminder ORDER by id DESC"
            }else if byId != 0{
                query = "select * FROM tbl_reminder where id ='\(byId)'"
            }
            
            do {
                print(SqliteDataManager.shared.database)
                let results = try SqliteDataManager.shared.database.executeQuery(query, values: nil)
                
                while results.next() {
                    let reminder = ReminderDataModel()
                    reminder.id = Int(results.int(forColumn: "id"))
                    reminder.message = results.string(forColumn: "message")
                    reminder.reminder_date = results.string(forColumn: "reminder_date")
                    reminder.weekDays = results.string(forColumn: "weekdays")?.replacingOccurrences(of: "&", with: ",")
                    reminder.indetifier_day_1 = results.string(forColumn: "identifier_day_1")
                    reminder.indetifier_day_2 = results.string(forColumn: "identifier_day_2")
                    reminder.indetifier_day_3 = results.string(forColumn: "identifier_day_3")
                    reminder.indetifier_day_4 = results.string(forColumn: "identifier_day_4")
                    reminder.indetifier_day_5 = results.string(forColumn: "identifier_day_5")
                    reminder.indetifier_day_6 = results.string(forColumn: "identifier_day_6")
                    reminder.indetifier_day_7 = results.string(forColumn: "identifier_day_7")
                    
                    reminderdata.append(reminder)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            SqliteDataManager.shared.database.close()
        }
        return reminderdata
    }
    
    // Add new record into Reminder Table.
    
    func insertReminderData(reminder:ReminderDataModel) -> Bool {
        var result = false
        if SqliteDataManager.shared.openDatabase() {
            let query = "insert into tbl_reminder ('message','reminder_date','weekdays','identifier_day_1','identifier_day_2','identifier_day_3','identifier_day_4','identifier_day_5','identifier_day_6','identifier_day_7') values ('\(reminder.message!)', '\(reminder.reminder_date!)', \(reminder.weekDays!.replacingOccurrences(of: ",", with: "&")), '\(reminder.indetifier_day_1!)', '\(reminder.indetifier_day_2!)','\(reminder.indetifier_day_3!)','\(reminder.indetifier_day_4!)','\(reminder.indetifier_day_5!)','\(reminder.indetifier_day_6!)','\(reminder.indetifier_day_7!)');"
            
            if !SqliteDataManager.shared.database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(SqliteDataManager.shared.database.lastError(), SqliteDataManager.shared.database.lastErrorMessage())
            }else{
                result = true
            }
            SqliteDataManager.shared.database.close()
        }
        return result
    }
    
    // Update Existing record into Reminder Table.
    
    func updateReminder(reminder:ReminderDataModel) -> Bool{
        var result = false
        if SqliteDataManager.shared.openDatabase() {
            let query = "update tbl_reminder set message=?, reminder_date=?, weekdays=?, identifier_day_1=?, identifier_day_2=?, identifier_day_3=?, identifier_day_4=?,identifier_day_5=?,identifier_day_6=?,identifier_day_7=? where id=?"
            
            do {
                try SqliteDataManager.shared.database.executeUpdate(query, values: [reminder.message!, reminder.reminder_date!,reminder.weekDays!.replacingOccurrences(of: ",", with: "&"),reminder.indetifier_day_1!,reminder.indetifier_day_2!,reminder.indetifier_day_3!,reminder.indetifier_day_4!,reminder.indetifier_day_5!,reminder.indetifier_day_6!,reminder.indetifier_day_7!, reminder.id!])
                result = true
            }
            catch {
                print(error.localizedDescription)
            }
            
            SqliteDataManager.shared.database.close()
        }
        return result
    }
    
    // Delete Existing record from Reminder Table.
    
    func deleteReminder(withID ID: Int) -> Bool {
        var deleted = false
        
        if SqliteDataManager.shared.openDatabase() {
            
            let query = "delete from tbl_reminder where id =?"
            do {
                try SqliteDataManager.shared.database.executeUpdate(query, values: [ID])
                deleted = true
            }catch {
                print(error.localizedDescription)
            }
            SqliteDataManager.shared.database.close()
        }
        return deleted
    }
    
}
