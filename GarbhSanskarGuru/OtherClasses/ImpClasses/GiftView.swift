//
//  GiftView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 29/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class GiftView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //let duration: TimeInterval = 7.0
        
       // self.transform = CGAffineTransform.identity
        
        // initial transform
        //self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        // start rotation
        //rotate(duration: duration)
        
        // scaling and fading
        //scaleUpAndDown(desiredRepetitions: 30, initalDuration: duration)
    }
    
    func startAnimation() {
        let duration: TimeInterval = 7.0
        self.transform = CGAffineTransform.identity
        
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        scaleUpAndDown(desiredRepetitions: 30, initalDuration: duration)
    }
    
    
    func stopAnimation() {
        UIView.animate(withDuration: 0.1) {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func rotate(duration: TimeInterval) {
        UIView.animate(withDuration: duration/2.0,
                       delay: 0.0,
                       options: [.curveLinear], animations: {
                        let angle = Double.pi
                        self.transform = self.transform.rotated(by: CGFloat(angle))
        }, completion: {[weak self] finished in
            guard let strongSelf = self else {
                return
            }
            if finished &&
                strongSelf.transform != CGAffineTransform.identity {
                strongSelf.rotate(duration: duration)
            } else {
                // rotation ending
            }
        })
    }
    
    func scaleUpAndDown(timesRepeated: Int = 0, desiredRepetitions: Int, initalDuration: TimeInterval) {
        
        guard timesRepeated < desiredRepetitions,
            desiredRepetitions > 0, initalDuration > 0 else {
                UIView.animate(withDuration: 0.5) {
                    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }
                return
        }
        let repeatedCount = timesRepeated + 1
        //let scalingDuration = initalDuration/2.0/Double(desiredRepetitions)
        
        UIView.animate(withDuration: 1.16,
                       delay: 0.0,
                       animations: {
                        let desiredOriginalScale: CGFloat = 1.0
                        
                        let scaleX = abs(CGAffineTransform.identity.a / self.transform.a) * desiredOriginalScale
                        let scaleY = abs(CGAffineTransform.identity.d / self.transform.d) * desiredOriginalScale
                        
                        self.transform = self.transform.scaledBy(x: scaleX, y: scaleY)
        }) { (true) in
            UIView.animate(withDuration: 1.16,
                           delay: 0.0,
                           animations: {
                            
                            let desiredOriginalScale: CGFloat = 0.5
                            
                            let scaleX = abs(CGAffineTransform.identity.a / self.transform.a) * desiredOriginalScale
                            let scaleY = abs(CGAffineTransform.identity.d / self.transform.d) * desiredOriginalScale
                            self.transform = self.transform.scaledBy(x: scaleX, y: scaleY)
            }) { finshed in
                self.scaleUpAndDown(timesRepeated: repeatedCount, desiredRepetitions: desiredRepetitions, initalDuration: initalDuration);
            }
        }
        
    }
    
}
