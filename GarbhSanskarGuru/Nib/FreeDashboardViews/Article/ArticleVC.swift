//
//  ArticleVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 08/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class ArticleVC: ParentViewController {
    
    var arrArticleList : [ArticleList]?
    let strLanguage = _appDelegator.getLanguage()
    var image_url = ""
    @IBOutlet weak var cvArticle: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvArticle.register(UINib(nibName: "ArticleCell", bundle: .main), forCellWithReuseIdentifier: "ArticleCell")
        self.title = "Article".localized
        getArticle()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        hideNoDataFound()
    }
    
    func getArticle(){
        let param = ["user_id":_user.id]
        
        self.showCentralSpinner()
        KPWebCall.call.getArticalList(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let arList = ArticleModel(dictionary: dict)
                
                var searchText = ""
                if weakself.strLanguage == kEnglish {
                    searchText = kLongEnglish
                }else if weakself.strLanguage == kGujrati {
                    searchText = kLongGujrati
                }else if weakself.strLanguage == KHindi {
                    searchText = KLongHindi
                }
                
                weakself.arrArticleList = arList.data?.filter { catalogName in
                    return catalogName.language ==  searchText
                }
                
                if weakself.arrArticleList?.count == 0 {
                    weakself.showNoDataFound(text: "Data will be available soon")
                }else{
                    weakself.hideNoDataFound()
                }
                
                weakself.image_url = arList.image_url
                weakself.cvArticle.reloadData()
            }
        }
        
        
//        callWS_GetGSArticle(parms:parm) {result  in
//            guard let arList = result else  { return }
//
//            var searchText = ""
//            if self.strLanguage == kLongEnglish {
//                searchText = kLongEnglish
//            }else if self.strLanguage == kLongGujrati {
//                searchText = kLongGujrati
//            }else if self.strLanguage == KLongHindi {
//                searchText = KLongHindi
//            }
//
//            self.arrArticleList = arList.data?.filter { catalogName in
//                return catalogName.language ==  searchText
//            }
//
//            if self.arrArticleList?.count == 0 {
//                self.showNoDataFound(text: "Data will be available soon")
//            }else{
//                self.hideNoDataFound()
//            }
//
//            self.image_url = arList.image_url
//            self.cvArticle.reloadData()
//        }
    }
}


extension ArticleVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 2 - 5 , height: _screenSize.width / 2 - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrArticleList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"ArticleCell", for: indexPath) as! ArticleCell
        let obj = arrArticleList?[indexPath.item]
        cell.lblArticle.text = obj?.title
//        cell.imgArticle.sd_setImage(with:URL(string: self.image_url + (obj?.image ?? "")) , placeholderImage: nil)
        cell.imgArticle.kf.setImage(with: URL(string: self.image_url + (obj?.image ?? "")))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isFromArticle = true
        
        webViewVC.strTile = arrArticleList?[indexPath.item].title
        webViewVC.strUrl = arrArticleList?[indexPath.item].url
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }

}
