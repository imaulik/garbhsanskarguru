//
//  LoginVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct LoginData {
    
    var email = ""
    var password = ""
    
    func paramDict(isForceLogout: Bool) -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["email"] = email
        dict["password"] = password
        dict["login_with"] = "app"

        if !isForceLogout {
            dict["device_type"] = _deviceType
            dict["device_id"] = _deviceID
            dict["device_token"] = _appDelegator.getPushToken()
            dict["is_login"] = "Yes"
        }
        return dict
    }
    
    
    func isValidData() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: email) {
            result.isValid = false
            result.error = kEnterEmail
            return result
        }else if !email.isValidEmailAddress() {
            result.isValid = false
            result.error = kInvalidEmail
            return result
        }
        
        if String.validateStringValue(str: password){
            result.isValid = false
            result.error = kEnterPassword
            return result
        }
        
        return result
    }
}


class LoginVC: ParentViewController {
    
    // comment
//    @IBOutlet weak var imgBannerView: UIImageView!
//    @IBOutlet weak var giftView: UIView!
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var tfForceEmail: UITextField!
    @IBOutlet weak var tfForcePassword: UITextField!
    
    @IBOutlet weak var forceLogoutView: UIView!
    @IBOutlet weak var baseView: UIView!

    // comment
//    @IBOutlet var btnSignUp: [UIButton]?
//    @IBOutlet weak var constraintHeightImgVwLogo: NSLayoutConstraint!
    
    var data = LoginData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // comment
//        prepareBtnGradientView()
        prepareUI()
    }
    
    // comment
//    func prepareBtnGradientView() {
//        btnSignUp?.forEach({ (btn) in
//            btn.applyGradientEffects([GarbhColor.gradient1Pink, GarbhColor.gradient2Pink], gradientPoint: .leftRight)
//        })
//    }
}

extension LoginVC {

    func prepareUI() {
        
        tfEmail.setAttributedPlaceHolder(text: "email address", font: tfEmail.font!, color: UIColor.white, spacing: 0)
        tfPassword.setAttributedPlaceHolder(text: "password", font: tfPassword.font!, color: UIColor.white, spacing: 0)
        
        //abhi comment below line because put this in didFinidhLaunching
        //_appDelegator.registerPushNotification()
        
        // comment
//        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
//        self.giftView.isHidden = !isPaymentAllowed
//        if isPaymentAllowed {
//            self.getBannerImage()
//        }
//        if (UIDevice.deviceType == .iPad){
//
//            constraintHeightImgVwLogo.constant = 120
//        }
        
        //abhi comment
        /*
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        switch currLanguage {
        case .hindi:
            self.lblTitle?.text = "गर्भसंस्कारकी अदभुत दुनियामे आपका हार्दिक स्वागत हैं।"
        case .english:
            self.lblTitle?.text = "Hearty welcome to the majestic world of Garbh Sanskar"
        case .gujarati:
            self.lblTitle?.text = "ગર્ભસંસ્કારની અદભુત દુનિયામાં આપનું હાર્દિક સ્વાગત છે."
        }
        */
    }
    
    // comment
//    func setImage(imgUrl: URL?) {
//        imgBannerView.kf.setImage(with: imgUrl)
//    }
    
    func prepareForceLogout() {
        self.forceLogoutView.isHidden = false
        self.baseView.bounceView(animateView: self.baseView)
    }
    
    
    fileprivate func makeLoginCall() {
        let validate = data.isValidData()
        if validate.isValid {
            self.view.endEditing(true)
            self.loginUser()
        }else {
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = ForgotPassView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.animateView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                let emailStr = onBoardingView.tfEmail.text!
                kprint(items: "Email is \(emailStr)")
                
                let isValidEmail = emailStr.isValidEmailAddress()
                if emailStr.isEmpty {
                    _ = ValidationToast.showStatusMessage(message: kEnterEmail)
                } else if !isValidEmail {
                    _ = ValidationToast.showStatusMessage(message: kInvalidEmail)
                } else {
                    self.forgotPassword(email: emailStr, block: { (done) in
                        if done{
                            onBoardingView.removeFromSuperview()
                        }else {
                            kprint(items: "Issue Happened")
                        }
                    })
                }
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
}

extension LoginVC {
    
    // comment
//    @IBAction func btnGiftApp(_ sender: UIButton) {
//        performSegue(withIdentifier: "giftSegue", sender: nil)
//    }
    
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        guard !tfPassword.text!.isEmpty else {return}
        sender.isSelected = !sender.isSelected
        tfPassword.isSecureTextEntry = !tfPassword.isSecureTextEntry
    }
    
    
    
    @IBAction func btnShowForgotPassword(_ sender: UIButton) {
        guard !tfForcePassword.text!.isEmpty else {return}
        sender.isSelected = !sender.isSelected
        tfForcePassword.isSecureTextEntry = !tfForcePassword.isSecureTextEntry
    }
    
    @IBAction func btnForgotPassTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.prepareOnBoarding()
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.forceLogoutView.isHidden = true
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.forceLogoutUser { (complete) in
            self.forceLogoutView.isHidden = complete
        }
    }
    
    // comment
//    @IBAction func btnSignUpTapped(_ sender: UIButton) {
//        performSegue(withIdentifier: "signUpSegue", sender: nil)
//    }
   
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        makeLoginCall()
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}

extension LoginVC: UITextFieldDelegate {
    
    @IBAction func tfEditingChange(_ textField: UITextField) {
        if forceLogoutView.isHidden {
            if textField == tfEmail {
                let str = textField.text!.trimmedString()
                self.data.email = str
            } else if textField == tfPassword {
                let str = textField.text!.trimmedString()
                self.data.password = str
            }
        } else {
            if textField == tfForceEmail {
                let str = textField.text!.trimmedString()
                self.data.email = str
            } else if textField == tfForcePassword {
                let str = textField.text!.trimmedString()
                self.data.password = str
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if forceLogoutView.isHidden {
            if textField == tfEmail {
                tfPassword.becomeFirstResponder()
            } else if textField == tfPassword{
                makeLoginCall()
            }
        } else {
            if textField == tfForceEmail {
                tfForcePassword.becomeFirstResponder()
            } else if textField == tfForcePassword {
                tfForcePassword.resignFirstResponder()
            }
        }
        return true
    }
}

extension LoginVC {
    
    func navigateUser() {
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        if isPaymentAllowed
        {
         //   _appDelegator.navigateUserToHome()
            
            // comment code. Redirect to Home screen insted of PaidVC.
            if(_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser())
            {
                if _user.desireBabyStatus == .pending {
                    let paidVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PaidVC") as! PaidVC
                    self.navigationController?.pushViewController(paidVC, animated: true)
                }else {
                    _appDelegator.navigateUserToHome()
                }
            }
            else
            {
                _appDelegator.navigateUserToHome()
            }
        }
        else
        {
            _appDelegator.navigateDirectlyToHome()
        }
    }
    
    
    func forgotPassword(email: String ,block: @escaping (Bool) -> ()) {
        
        view.endEditing(true)
        
        self.showCentralSpinner()
        KPWebCall.call.resetPassword(param: ["email": email]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String{
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func forceLogoutUser(block: @escaping(Bool) -> ()) {
        
        view.endEditing(true)
        
        let paramDict = data.paramDict(isForceLogout: true)
        self.showCentralSpinner()
        KPWebCall.call.forceLogout(param: paramDict) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    //weakself.showSucMsg(data: dict)
                    self?.loginUser()
                    block(true)
            } else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func loginUser() {
        
        view.endEditing(true)
        let paramDict = data.paramDict(isForceLogout: false)
        self.showCentralSpinner()
        KPWebCall.call.loginUser(param: paramDict) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    if let userInfo = dict["data"] as? NSDictionary, let token = userInfo["token"] as? String {
                        _appDelegator.storeAuthorizationToken(strToken: token)
                        KPWebCall.call.setAccesTokenToHeader(token: token)
                        DispatchQueue.main.async {
                            _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
                            _user.initWith(entry: userInfo)
                            _appDelegator.saveContext()
                           
                            let checkfreedemoUser =  userInfo["free_demo_payment"]
                            let checkuser = checkfreedemoUser as? String == "No" ? false  : true
                            UserDefaults.standard.set(checkuser, forKey: "checkdemoUser")
                            
                            createReminderNotificatinsForNextSevenDays()
                            
                            weakself.navigateUser()
                        }
                    }
                }else if status == 401 {
                    if let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                        if msg.contains(find: "You are login other device.please logout first other device") {
                            weakself.prepareForceLogout()
                        } else {
                            weakself.showError(data: dict, view: weakself.view)
                        }
                    }
                } else {
                    weakself.showError(data: dict, view: self?.view)
                }
            } else {
                weakself.showError(data: json, view: self?.view)
            }
        }
    }
    
    // comment
//    func getBannerImage() {
//        self.showCentralSpinner()
//        KPWebCall.call.getBannerImage { [weak self] (json, statusCode) in
//            guard let weakself = self else {return}
//            weakself.hideCentralSpinner()
//            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
//                if status == 200, let dictData = dict["data"] as? NSDictionary {
//                    guard let imgUrl = dictData["banner_image_url"] as? String else {
//                        return weakself.showError(data: dictData, view: weakself.view)
//                    }
//                    weakself.setImage(imgUrl: URL(string: imgUrl))
//                } else {
//                    weakself.showError(data: json, view: weakself.view)
//                }
//            }
//        }
//    }
}
