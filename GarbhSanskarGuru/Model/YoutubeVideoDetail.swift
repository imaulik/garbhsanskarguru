//
//  YoutubeVideoDetail.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 16/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class YoutubeVideoDetail: NSObject {
    
    var etag : String!
    var items : [Item]!
    var kind : String!
    var pageInfo : PageInfo!
    
    override init() {
        super.init()
    }
    
    init(fromDictionary dictionary: NSDictionary){
        etag = dictionary.getStringValue(key: "etag")
        kind = dictionary.getStringValue(key: "kind")
//        etag = dictionary["etag"] as? String
//        kind = dictionary["kind"] as? String
        if let pageInfoData = dictionary["pageInfo"] as? [String:Any]{
            pageInfo = PageInfo(fromDictionary: pageInfoData as NSDictionary)
        }
        items = [Item]()
        if let itemsArray = dictionary["items"] as? [[String:Any]]{
            for dic in itemsArray{
                let value = Item(fromDictionary: dic as NSDictionary)
                items.append(value)
            }
        }
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if etag != nil{
            dictionary["etag"] = etag
        }
        if kind != nil{
            dictionary["kind"] = kind
        }
        if pageInfo != nil{
            dictionary["pageInfo"] = pageInfo.toDictionary()
        }
        if items != nil{
            var dictionaryElements = [[String:Any]]()
            for itemsElement in items {
                dictionaryElements.append(itemsElement.toDictionary())
            }
            dictionary["items"] = dictionaryElements
        }
        return dictionary
    }
    
}


class PageInfo : NSObject {
    
    var resultsPerPage : Int!
    var totalResults : Int!
    
    init(fromDictionary dictionary: NSDictionary){
        resultsPerPage = dictionary.getIntValue(key:"resultsPerPage")
        resultsPerPage = dictionary.getIntValue(key:"totalResults")
        
//        resultsPerPage = dictionary["resultsPerPage"] as? Int
//        totalResults = dictionary["totalResults"] as? Int
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if resultsPerPage != nil{
            dictionary["resultsPerPage"] = resultsPerPage
        }
        if totalResults != nil{
            dictionary["totalResults"] = totalResults
        }
        return dictionary
    }
    
}

class Item : NSObject {
    
    var etag : String!
    var id : String!
    var kind : String!
    var status : Status!
    
    init(fromDictionary dictionary: NSDictionary){
        etag = dictionary.getStringValue(key:"etag")
        id = dictionary.getStringValue(key:"id")
        kind = dictionary.getStringValue(key:"kind")

//        etag = dictionary["etag"] as? String
//        id = dictionary["id"] as? String
//        kind = dictionary["kind"] as? String
        if let statusData = dictionary["status"] as? [String:Any]{
            status = Status(fromDictionary: statusData as NSDictionary)
        }
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if etag != nil{
            dictionary["etag"] = etag
        }
        if id != nil{
            dictionary["id"] = id
        }
        if kind != nil{
            dictionary["kind"] = kind
        }
        if status != nil{
            dictionary["status"] = status.toDictionary()
        }
        return dictionary
    }
}

class Status : NSObject {
    
    var embeddable : Bool!
    var license : String!
    var madeForKids : Bool!
    var privacyStatus : String!
    var publicStatsViewable : Bool!
    var uploadStatus : String!
    
    
    init(fromDictionary dictionary: NSDictionary){
        embeddable = dictionary.getBooleanValue(key: "embeddable")
        license = dictionary.getStringValue(key: "license")
        madeForKids = dictionary.getBooleanValue(key: "madeForKids")
        privacyStatus = dictionary.getStringValue(key: "privacyStatus")
        publicStatsViewable = dictionary.getBooleanValue(key: "publicStatsViewable")
        uploadStatus = dictionary.getStringValue(key: "uploadStatus")

//        embeddable = dictionary["embeddable"] as? Bool
//        license = dictionary["license"] as? String
//        madeForKids = dictionary["madeForKids"] as? Bool
//        privacyStatus = dictionary["privacyStatus"] as? String
//        publicStatsViewable = dictionary["publicStatsViewable"] as? Bool
//        uploadStatus = dictionary["uploadStatus"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if embeddable != nil{
            dictionary["embeddable"] = embeddable
        }
        if license != nil{
            dictionary["license"] = license
        }
        if madeForKids != nil{
            dictionary["madeForKids"] = madeForKids
        }
        if privacyStatus != nil{
            dictionary["privacyStatus"] = privacyStatus
        }
        if publicStatsViewable != nil{
            dictionary["publicStatsViewable"] = publicStatsViewable
        }
        if uploadStatus != nil{
            dictionary["uploadStatus"] = uploadStatus
        }
        return dictionary
    }
}

class RandomVideoData : NSObject{
    
    var descriptionField : String!
    var engUrl : String!
    var gujUrl : String!
    var hindiUrl : String!
    var id : Int!
    var moduleId : String!
    var moduleName : String!
    
    init(fromDictionary dictionary: NSDictionary){
        descriptionField = dictionary.getStringValue(key: "description")
        engUrl = dictionary.getStringValue(key: "eng_url")
        gujUrl = dictionary.getStringValue(key: "guj_url")
        hindiUrl = dictionary.getStringValue(key: "hindi_url")
        id = dictionary.getIntValue(key: "id")
        moduleId = dictionary.getStringValue(key: "module_id")
        moduleName = dictionary.getStringValue(key: "module_name")

//        descriptionField = dictionary["description"] as? String
//        engUrl = dictionary["eng_url"] as? String
//        gujUrl = dictionary["guj_url"] as? String
//        hindiUrl = dictionary["hindi_url"] as? String
//        id = dictionary["id"] as? Int
//        moduleId = dictionary["module_id"] as? String
//        moduleName = dictionary["module_name"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if engUrl != nil{
            dictionary["eng_url"] = engUrl
        }
        if gujUrl != nil{
            dictionary["guj_url"] = gujUrl
        }
        if hindiUrl != nil{
            dictionary["hindi_url"] = hindiUrl
        }
        if id != nil{
            dictionary["id"] = id
        }
        if moduleId != nil{
            dictionary["module_id"] = moduleId
        }
        if moduleName != nil{
            dictionary["module_name"] = moduleName
        }
        return dictionary
    }
    
}

class RandomVideoModel : NSObject{
    
    var data : [RandomVideoData]!
    var status : Int!

    init(fromDictionary dictionary: NSDictionary){
        status = dictionary.getIntValue(key: "status")
//        status = dictionary["status"] as? Int
        data = [RandomVideoData]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = RandomVideoData(fromDictionary: dic as NSDictionary)
                data.append(value)
            }
        }
    }
   
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if status != nil{
            dictionary["status"] = status
        }
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        return dictionary
    }
    
}


class ReportModel : NSObject {

    var data : [ReportData]!
    var doneActivityCount : Int!
    var message : String!
    var status : Int!
    var undoneActivityCount : Int!
        
    var report_IQ : Int!
    var report_PQ : Int!
    var report_EQ : Int!
    var report_SQ : Int!
    
    var report_IQ_double : Double!
    var report_PQ_double : Double!
    var report_EQ_double : Double!
    var report_SQ_double : Double!
    
    init(fromDictionary dictionary: NSDictionary){
        doneActivityCount = dictionary.getIntValue(key: "done_activity_count")
        message = dictionary.getStringValue(key: "message")
        status = dictionary.getIntValue(key: "status")
        undoneActivityCount = dictionary.getIntValue(key: "undone_activity_count")
        
//        doneActivityCount = dictionary["done_activity_count"] as? Int
//        message = dictionary["message"] as? String
//        status = dictionary["status"] as? Int
//        undoneActivityCount = dictionary["undone_activity_count"] as? Int
        data = [ReportData]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = ReportData(fromDictionary: dic as NSDictionary)
                data.append(value)
            }
        }
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if doneActivityCount != nil{
            dictionary["done_activity_count"] = doneActivityCount
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if undoneActivityCount != nil{
            dictionary["undone_activity_count"] = undoneActivityCount
        }
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        return dictionary
    }
    
    func getReportData(isFloat:Bool = false){
        if isFloat{
            for report in data{
                if report.activityName == "PQ"{
                    report_PQ_double = report.doneActivityTotal
                }else if report.activityName == "IQ"{
                    report_IQ_double = report.doneActivityTotal
                }else if report.activityName == "EQ"{
                    report_EQ_double = report.doneActivityTotal
                }else{ //SQ
                    report_SQ_double = report.doneActivityTotal
                }
            }
        }else{
            for report in data{
                if report.activityName == "PQ"{
                    report_PQ = report.doneActivityPersentage
                }else if report.activityName == "IQ"{
                    report_IQ = report.doneActivityPersentage
                }else if report.activityName == "EQ"{
                    report_EQ = report.doneActivityPersentage
                }else{ //SQ
                    report_SQ = report.doneActivityPersentage
                }
            }
        }
    }
    
}


class ReportData : NSObject {
    
    var activityName : String!
    var doneActivityPersentage : Int!
    var doneActivityTotal : Double!
    var totalCategory : String!
    var totalDoneCategory : Int!

    
    init(fromDictionary dictionary: NSDictionary){
        activityName = dictionary.getStringValue(key: "activity_name")
        doneActivityPersentage = dictionary.getIntValue(key: "done_activity_persentage")
        doneActivityTotal = dictionary.getDoubleValue(key: "done_activity_persentage")
        totalCategory = dictionary.getStringValue(key: "total_category")
        totalDoneCategory = dictionary.getIntValue(key: "total_done_category")
        
//        activityName = dictionary["activity_name"] as? String
//        doneActivityPersentage = dictionary["done_activity_persentage"] as? Int
//        doneActivityTotal = dictionary["done_activity_persentage"] as? Double
//        totalCategory = dictionary["total_category"] as? String
//        totalDoneCategory = dictionary["total_done_category"] as? Int
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if activityName != nil{
            dictionary["activity_name"] = activityName
        }
        if doneActivityPersentage != nil{
            dictionary["done_activity_persentage"] = doneActivityPersentage
        }
        if doneActivityTotal != nil{
            dictionary["done_activity_persentage"] = doneActivityTotal
        }
        if totalCategory != nil{
            dictionary["total_category"] = totalCategory
        }
        if totalDoneCategory != nil{
            dictionary["total_done_category"] = totalDoneCategory
        }
        return dictionary
    }
}

class PaymentData: NSObject {
    
    var data : [PaymentVideoModel]!
    var image : String!
    var message : String!
    var status : Int!
    
    init(fromDictionary dictionary: [String:Any]){
        image = dictionary["image"] as? String
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Int
        data = [PaymentVideoModel]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = PaymentVideoModel(fromDictionary: dic as NSDictionary)
                data.append(value)
            }
        }
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if image != nil{
            dictionary["image"] = image
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        return dictionary
    }
}


class PaymentVideoModel : NSObject {
    
    var createdAt : String!
    var id : Int!
    var imageName : String!
    var language : String!
    var text : String!
    var type : String!
    var updatedAt : String!
    var videoUrl : String!
    
    init(fromDictionary dictionary: NSDictionary){
        
        createdAt = dictionary.getStringValue(key: "created_at") //dictionary["created_at"] as? String
        id = dictionary.getIntValue(key: "id") //dictionary["id"] as? Int
        imageName = dictionary.getStringValue(key: "image_name") // dictionary["image_name"] as? String
        language = dictionary.getStringValue(key: "language") //dictionary["language"] as? String
        text = dictionary.getStringValue(key: "text") //dictionary["text"] as? String
        type = dictionary.getStringValue(key: "type") //dictionary["type"] as? String
        updatedAt = dictionary.getStringValue(key: "updated_at") //dictionary["updated_at"] as? String
        videoUrl = dictionary.getStringValue(key: "video_url") //dictionary["video_url"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if id != nil{
            dictionary["id"] = id
        }
        if imageName != nil{
            dictionary["image_name"] = imageName
        }
        if language != nil{
            dictionary["language"] = language
        }
        if text != nil{
            dictionary["text"] = text
        }
        if type != nil{
            dictionary["type"] = type
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if videoUrl != nil{
            dictionary["video_url"] = videoUrl
        }
        return dictionary
    }
}
