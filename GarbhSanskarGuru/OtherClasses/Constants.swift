import UIKit
import Foundation

/*---------------------------------------------------
Screen Size
---------------------------------------------------*/
let _screenSize     = UIScreen.main.bounds.size
let _screenFrame    = UIScreen.main.bounds

/*---------------------------------------------------
 Constants
 ---------------------------------------------------*/
let _defaultCenter  = NotificationCenter.default
let _userDefault    = UserDefaults.standard
let _appDelegator   = UIApplication.shared.delegate! as! AppDelegate
let _application    = UIApplication.shared
let _deviceID       = UIDevice.current.identifierForVendor!.uuidString

/*---------------------------------------------------
 Facebook
 ---------------------------------------------------*/
let _facebookPermission              = ["public_profile", "email", "user_friends"]
let _facebookMeUrl                   = "me"
let _facebookAlbumUrl                = "me/albums"
let _facebookUserField: [String:Any] = ["fields" : "id,first_name,last_name,gender,birthday,email,education,work,picture.height(700)"]
let _facebookJobSchoolField          = ["fields" : "education,work"]
let _facebookAlbumField              = ["fields":"id,name,count,picture"]
let _facebookPhotoField              = ["fields":"id,picture"]

/*---------------------------------------------------
 Privacy and Terms URL
---------------------------------------------------*/
let _aboutUsUrl        = "https://www.google.com"
let _privacyUrl        = "https://www.google.com"
let _helpUrl            = "https://www.google.com"
let _termsUrl         = "https://www.google.com"

/*---------------------------------------------------
 MARK: Paging Structure
 ---------------------------------------------------*/
struct LoadMore{
    var index: Int = 0
    var isLoading: Bool = false
    var limit: Int = 20
    var isAllLoaded = false
    
    var offset: Int{
        return index * limit
    }
}

/*---------------------------------------------------
 Current loggedIn User
 ---------------------------------------------------*/
let _deviceType = "IOS"
var _user: User!
var _giftUser: GiftUser!
var _nTodaysCompletedActivities = 0

/*---------------------------------------------------
 Date Formatter and number formatter
 ---------------------------------------------------*/
let _serverFormatter: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    df.timeZone = TimeZone(secondsFromGMT: 0)
    df.locale = Locale(identifier: "en_US_POSIX")
    return df
}()

let datePickerFormat: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return df
}()

let timeFormat: String = {
    return "h:mm a"
}()

let timeFormatOnlyAMPM: String = {
    return "a"
}()

let _deviceFormatter: DateFormatter = {
    let df = DateFormatter()
    df.timeZone = TimeZone.current
    df.dateFormat = "MM-dd-yyyy"
    return df
}()

let _numberFormatter:NumberFormatter = {
    let format = NumberFormatter()
    format.locale = Locale(identifier: "en_GB")
    format.numberStyle = .currency
    return format
}()

/*---------------------------------------------------
 Place Holder image
 ---------------------------------------------------*/
let _placeImageUser = UIImage(named: "ic_placeholder")
let _placeImage = UIImage(named: "ic_place")
let _timeStamp = Int(Date().timeIntervalSince1970)

let kActivityButtonImageName = ""
let kActivitySmallImageName = ""

/*---------------------------------------------------
 User Default and Notification keys
 ---------------------------------------------------*/
let garbhNotificationKey        = "garbhNotificationKey"
let garbhAuthTokenKey           = "garbhAuthorizationKey"
let garbhLanguageKey            = "garbhAppLanguageKey"
let garbhBoardingKey            = "garbhBoardingKey"
let gcmMessageIDKey             = "gcm.message_id"
let garbhLaunchBoardingKey      = "garbhLaunchBoardingKey"
let garbhPaymentKey             = "garbhPaymentKey"
let garbhOnboardingLanguageKey     = "garbhBoardingLanuage"

let garbhNotificationUpdateActicityStatus = "garbhNotificationUpdateActicityStatus"
let garbhUserDefaultLastAnnouncement = "garbhUserDefaultLastAnnouncement"

let kLocalNotiIdentifier     =   "TIMER_EXPIRED"

/*---------------------------------------------------
 Custom print
 ---------------------------------------------------*/
func kprint(items: Any...) {
    #if DEBUG
        for item in items {
            print(item)
        }
    #endif
}

/*---------------------------------------------------
 Settings Version Maintenance
 ---------------------------------------------------*/
func getAppVersionAndBuild() -> String{
    if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
        return "Version - \(version)(\(build))"
    }else{
        return ""
    }
}

func getAppversion() -> String{
    if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
        return version
    }else{
        return ""
    }
}

func setAppSettingsBundleInformation(){
    if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
        _userDefault.set(build, forKey: "application_build")
        _userDefault.set(version, forKey: "application_version")
        _userDefault.synchronize()
    }
}

/*---------------------------------------------------
 Device Extention
 ---------------------------------------------------*/
extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
}

//MARK:- Constant
//-------------------------------------------------------------------------------------------
// Common
//-------------------------------------------------------------------------------------------
let _statusBarHeight           : CGFloat = _appDelegator.window!.rootViewController!.topLayoutGuide.length
let _navigationHeight          : CGFloat = _statusBarHeight + 44
let _btmNavigationHeight       : CGFloat = _bottomAreaSpacing + 64
let _btmNavigationHeightSearch : CGFloat = _bottomAreaSpacing + 64 + 45
let _bottomAreaSpacing         : CGFloat = _appDelegator.window!.rootViewController!.bottomLayoutGuide.length
let _vcTransitionTime                    = 0.3
let _imageFadeTransitionTime   : Double  = 0.3

//MARK:- GarbhSanskarInfo Ids
let kGSInfo_WhatIsGarbhsanskar  :Int = 1
let kGSInfo_GarbhsanskarBenefits:Int = 2
let kGSInfo_GarbhsanskarHowTo   :Int = 3
let kGSInfo_RecevingDesiredBaby :Int = 4
let kGSInfo_Garbhyatra          :Int = 5
let kGSInfo_HowToFollow         :Int = 6

//MARK:- BasicInfo Ids
let kBasicInfo_DivineMystery    :Int = 7
let kBasicInfo_PregnancyTips    :Int = 8
let kBasicInfo_BundleOfJoy      :Int = 9

//MARK:- AppInfo Ids
let kBasicInfo_AppBenefits      :Int = 10
let kBasicInfo_AppInformation   :Int = 11
let kBasicInfo_FAQ              :Int = 12

//MARK:- Notification Titles
let STR_Notification_Type   = "type"
let STR_Local_Reminder      = "local_reminder"
let STR_HTML_Reminder_Body  = "htmlReminderBody"

let STR_Fcm_Notification_aanouncement = "aanouncement_noti"

let arrZeroByTenTitle = ["It seems you are too much busy today. I am waiting for you. Please Come and start activities mom.", "You are too lovely mom. You are taking good care of mine. Let's start today's activities too.", "Mom, now it seems difficult to wait. I am eager for Today’s Story, Video and Garbh Samvad.", "Mom, you are taking great care of mine. Let us start today’s app activities. It’s Garbh Sanskar Time.", "Why you have not started activities? Time has come. Please come soon mumma.", "1,2,3,4…10, 10 activities and today would be the best day. Let’s start mumma.", "Mom, I am feeling great when we indulge into app activities. Let us start today’s work."]
let arrXxxByTenTitle = ["Dear mom, Smile that comes on your face is really lovable when we complete activities of an app. Let’s become happier by completing rest of the activities.", "Dear mumma, today you have done a great job. Let us finish remaining activities to get relaxed.", "Dear mom, I am waiting for you, come soon and complete rest of the app activities.", "Dear mumma, I know, you are also eager to complete app activities with me. Love you mom.", "Oh wow, today it's fun. Now I could not wait longer for remaining activities. Come soon mumma.", "Dear mom, you taken care of me so well, thank you. Let us enjoy more after completing rest of the activities.", "Dear mumma, you are adorable. I am too much happy for these activities. Let's do the rest of today's work."]
let arrTenByTenTitle = ["I love to spend time with you while doing activities in the app. Even today, enjoyed a lot with you mom.", "Amazing. Today, it is fun to complete app activities. Thank you mom", "You are lovely, I am learning lots of thing mom.", "well done mumma. You looks happy and relaxed after completing all app activities.", "Great. It was a great fun doing all activities of an app. Thank you mumma", "I am feeling lucky. Even after your busy schedule, you are taking care of mine by completing all activities. Great job mom.", "Wow wow. Today we have made a lot of fun. You know mumma ,I feel relaxed only after completing all activities."]


// Storyboard
var isFromArticle = false
var isFromAboutUs = false

let freeAppDashboard = UIStoryboard(name: "FreeAppDashboard", bundle: nil)
let webViewVC = freeAppDashboard.instantiateViewController(withIdentifier:"WebViewController") as! WebViewController

let kEnglish = "en"
let kGujrati = "gu"
let KHindi  = "hi"

let kLongEnglish = "English"
let kLongGujrati = "Gujarati"
let KLongHindi  = "Hindi"
