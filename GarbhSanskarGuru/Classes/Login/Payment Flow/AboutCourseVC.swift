//
//  AboutCourseVC.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 01/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import AVKit

class AboutusInfoCell : UICollectionViewCell {
    @IBOutlet weak var lblAboutusInfo: UILabel!
    @IBOutlet weak var imgAboutUsInfo: UIImageView!
}


class AboutCourseVC: ParentViewController {
    @IBOutlet weak var cvStaticInfo: UICollectionView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var lblTitleInfo: UILabel!
    @IBOutlet weak var lblDetailInfo: UILabel!
    
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var cvAboutusInfo: UICollectionView!
    @IBOutlet weak var cvWebinarAboutUs: UICollectionView!
    
    var arrAboutUsInfo = [AboutInfo(title: "Doctors", image: #imageLiteral(resourceName: "ic_Doctor")),AboutInfo(title: "Ayurveda Experts", image:  #imageLiteral(resourceName: "ic_Ayurveda")),AboutInfo(title: "Child Psychologists", image:  #imageLiteral(resourceName: "ic_ChildPsy")),AboutInfo(title: "Garbhsanskar Coaches", image:  #imageLiteral(resourceName: "ic_Appgabh")),AboutInfo(title: "Yoga Gurus", image:  #imageLiteral(resourceName: "ic_yogaGuru")),AboutInfo(title: "Dietitians", image:  #imageLiteral(resourceName: "ic_Dietitians"))]
    
    var arrHeaderInfo : [HeaderAboutUS]?
    
    var dicWebFirstInfo :  WebinarInfo?
    var arrWebinarList : [WebinarInfo]?
    var arrFinalWebinarList : [WebinarInfo]?
    var strLanguage = _appDelegator.getLanguage()
    var refresher:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvStaticInfo.register(UINib(nibName: "StaticInfoCell", bundle: .main), forCellWithReuseIdentifier: "StaticInfoCell")
        
        self.cvWebinarAboutUs.register(UINib(nibName: "PagerCVCell", bundle: .main), forCellWithReuseIdentifier: "PagerCVCell")
        
        cvWebinarAboutUs.register(UINib(nibName: "WebinarView", bundle: nil),
                                  forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                  withReuseIdentifier: "WebinarView")
        
        
        cvAboutusInfo.register(UINib(nibName: "TitleView", bundle: nil),
                               forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                               withReuseIdentifier: "TitleView")
        setup()
        registerBlock()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getAboutInfo()
    }
    
    @IBAction func btnCall(_ sender: Any) {
        makeCall(number: "919727006001")
    }
    
    @IBAction func btnSubscribe(_ sender: UIButton)
    {
        let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PackagesVC") as! PackagesVC
        self.navigationController?.pushViewController(packagesVC, animated: true)
    }
    
    func registerBlock(){
        _appDelegator.detectBackFromAboutUsView = { () in
            self.setup()
        }
    }
    
    func setup(){
        strLanguage = _appDelegator.getLanguage()
        
        if strLanguage == kEnglish {
            self.lblHeaderTitle.text = "Daily 12 activites as per pregnency day"
            arrHeaderInfo = [HeaderAboutUS( title: "Dear Mom", image: "dearmom",description:"Message of the unborn to the mother"),HeaderAboutUS( title: "Garbh Samvad", image: "garbhsamvad",description:"Conversation with the unbornand positive suggestions for emotional attachment with baby"),HeaderAboutUS( title: "Today's Story", image: "story",description:"A story withe lesson to be learned"),HeaderAboutUS( title: "Activity of the day", image: "activity",description:"Skill Enhancing Activities(Art & Craft, Drawing, House Hack,Best From the waste,etc..)"),HeaderAboutUS(title: "Prayer", image: "prayers",description:"Prayers"),HeaderAboutUS( title: "Yoga", image: "yoga",description:"Pregnancy Yoga and Meditation"),HeaderAboutUS( title: "From Biography", image: "frombiography",description:"Motivational occasions from the biographies of great people"),HeaderAboutUS(title: "Diet", image: "diet",description:"Diet chart and chart based recipes"),HeaderAboutUS( title: "Shloka-Sakhi", image: "shloksakhi",description:"Verse(shloka) explaining the importance of life"),HeaderAboutUS(title: "Video", image: "vdo",description:"Best Video to Watch during pregnancy (Motivational,Enlightening,Informative,Pregnancy Music,Comedy,etc...)"), HeaderAboutUS(title: "Brain Activity", image: "Puzzle",description:"Brain Activity - Puzzle to develop the best of an child's brain"),HeaderAboutUS(title: "Garbh Satsang", image: "GarbhSastsang",description:"\"Garbh Satsang\" for developing a sense of happiness and satisfaction in the life of the child and for the overall development.(summary from Gita, Ramayan, Vedas and other scriptures)")]
        }else if strLanguage == kGujrati {
             self.lblHeaderTitle.text = "દરરોજ 12 પ્રવૃતિઓ, ગર્ભાવસ્થાના દિવસ મુજબ"
            arrHeaderInfo = [HeaderAboutUS( title: "વ્હાલી માં ..!", image: "dearmom",description:"ગર્ભસ્થ શિશુનો માતાને સંદેશ"),HeaderAboutUS(  title: "ગર્ભસંવાદ", image: "garbhsamvad",description:"ગર્ભસ્થ શિશુ જોડે જન્મ પહેલા જ ભાવાત્મક જોડાણ કરવા માટેના ગર્ભસંવાદ અને હકારાત્મક સૂચનો"),HeaderAboutUS(  title: "આજ ની વાર્તા", image: "story",description:"બોધપાઠ મળે તેવી વાર્તા"),HeaderAboutUS(title: "પ્રવૃત્તિ", image: "activity",description:"કૌશલ્ય વર્ધક એક્ટીવીટી ( આર્ટ & ક્રાફ્ટ, ચિત્રકામ,  હાઉસ હેક, વેસ્ટમાં થી બેસ્ટ, વગેરે ..)"),HeaderAboutUS(  title: "પ્રાર્થના", image: "prayers",description:"પ્રાર્થના"),HeaderAboutUS(  title: "યોગ", image: "yoga",description:"પ્રેગ્નન્સી યોગ , ધ્યાન અને મેડીટેશન"),HeaderAboutUS(  title: "જીવનચરિત્ર માંથી ..", image: "frombiography",description:"મહાન લોકો ના જીવનચરિત્રમાં થી પ્રેરણાદાયક પ્રસંગ"),HeaderAboutUS(  title: "આહાર", image: "diet",description:"ડાયટ ચાર્ટ અને ચાર્ટ પ્રમાણેની રેસીપી"),HeaderAboutUS(  title: "શ્લોક-સાખી", image: "shloksakhi",description:"જીવનનું મહત્વ સમજાવતા શ્લોક-સાખી"),HeaderAboutUS(  title: "વિડીઓ", image: "vdo",description:"પ્રેગન્સી માં જોવા જેવો શ્રેષ્ઠ વિડીઓ ( મોટીવેશનલ, જ્ઞાનવર્ધક, માહિતીસભર, પ્રેગ્નન્સી મ્યુઝીક, કોમેડી વગેરે ...)"),HeaderAboutUS( title: "બ્રેઈન એક્ટીવીટી", image: "Puzzle",description:"બાળકના મગજનો શ્રેષ્ઠ વિકાસ કરવા માટે  બ્રેઈન એક્ટીવીટી – પઝલ"),HeaderAboutUS(  title: "ગર્ભ સત્સંગ", image: "GarbhSastsang",description:"બાળકના જીવનમાં સુખ અને સંતોષ ના ભાવ વિકસાવવા તથા સર્વાંગી વિકાસ માટે ગર્ભ સત્સંગ જ્યાં મળશે રામાયણ , ગીતા, વેદો અને ઉપનિષદો ના બોધપાઠ")]
        }else if strLanguage == KHindi {
             self.lblHeaderTitle.text = "12 प्रवृतियां रोजाना, प्रेग्नेंसी के दिन के अनुसार"
             arrHeaderInfo = [HeaderAboutUS(title: "प्यारी माँ..!", image: "dearmom",description:"गर्भस्थ शिशु का माता को संदेश" ),HeaderAboutUS( title: "गर्भसंवाद", image: "garbhsamvad",description:"गर्भस्थ शिशु के साथ भावनात्मक बॉन्डिंग करने के लिए  गर्भसंवाद और सकारात्मक सूचन" ),HeaderAboutUS(title: "आज की कहानी", image: "story",description:"बोध पाठ देने वाली कहानियां" ),HeaderAboutUS(title: "प्रवृत्ति", image: "activity",description:"कौशल्य वर्धक एक्टिविटी (आर्ट एंड क्राफ्ट, ड्राइंग, हाउस हैक, वेस्ट में से बेस्ट इत्यादि)" ),HeaderAboutUS(title: "प्रार्थना", image: "prayers",description:"प्रार्थना" ),HeaderAboutUS(title: "योग", image: "yoga",description:"प्रेगनेंसी योग, ध्यान और मेडिटेशन" ),HeaderAboutUS(title: "जीवनचरित्र में से ..", image: "frombiography",description:"महान व्यक्तियों के जीवन चरित्र में से प्रेरणादाई प्रसंग" ),HeaderAboutUS(title: "आहार", image: "diet",description:"डाइट चार्ट और उसके मुताबिक रेसिपी" ),HeaderAboutUS(title: "श्लोक-साखी", image: "shloksakhi",description:"जीवन का महत्व व्यक्त करती श्लोक साखी-कड़ी" ),HeaderAboutUS(title: "वीडियो", image: "vdo",description:"प्रेगनेंसी में देखने लायक बेस्ट वीडियो (मोटिवेशनल, ज्ञानवर्धक, प्रेगनेंसी म्यूजिक, कॉमेडी, इनफॉर्मेटिव इत्यादि)" ),HeaderAboutUS( title: "ब्रेइन एक्टिविटी", image: "Puzzle",description:"बच्चे के दिमाग का श्रेष्ठ विकास करने के लिए ब्रेन एक्टिविटी – पजल" ),HeaderAboutUS(title: "गर्भ सत्संग", image: "GarbhSastsang",description:"बच्चे के जीवन में सुख और संतोष के भाव विकसित करने के लिए तथा सर्वांगीण विकास के लिए गर्भ सत्संग जहा मिलेंगे गीता , रामायण , वेद तथा उपनिषदोंके सारांश")]
        }
    
        self.lblTitleInfo.text = "\(arrHeaderInfo?[0].title ?? ""):"
        self.lblDetailInfo.text = arrHeaderInfo?[0].description
 
        //reset cv
        if self.strLanguage == kEnglish {
            self.arrWebinarList?.filter({ $0.eng_image != ""})
           
        }else if self.strLanguage == kGujrati {
             self.arrWebinarList?.filter({ $0.guj_image != ""})
        }else if self.strLanguage == KHindi {
               self.arrWebinarList?.filter({ $0.hindi_image != ""})
        }
        self.removeBlankValuefromWebinar()
         self.cvAboutusInfo.reloadData()
 
        self.cvStaticInfo.reloadData()
    }
    
    func getAboutInfo() {
        KPWebCall.call.getAboutFeedback {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = dict["data"]
                self?.arrWebinarList =  [WebinarInfo](dictionaryArray:res as! [NSDictionary])
                if self?.arrWebinarList?.count ?? 0 > 0 {
                    self?.dicWebFirstInfo = self?.arrWebinarList?.first
                    self?.arrWebinarList?.remove(at: 0)
                }
                self?.removeBlankValuefromWebinar()
            }
        }
    }
    
    
    func removeBlankValuefromWebinar (){
           strLanguage = _appDelegator.getLanguage()
        if self.strLanguage == kEnglish {
            self.arrFinalWebinarList =  self.arrWebinarList?.filter({ $0.eng_image != ""})
        }else if self.strLanguage == kGujrati {
            self.arrFinalWebinarList =  self.arrWebinarList?.filter({ $0.guj_image != ""})
        }else if self.strLanguage == KHindi {
            self.arrFinalWebinarList =  self.arrWebinarList?.filter({ $0.hindi_image != ""})
        }
        
        var myValue = [WebinarInfo]()
        if let myItems = self.arrFinalWebinarList {
            for i in 0..<myItems.count{
                if i <= 1 {
                    myValue.append(myItems[i])
                }else {
                    break
                }
            }
        }
        self.arrFinalWebinarList?.removeAll()
        self.arrFinalWebinarList = myValue
        self.cvWebinarAboutUs.reloadData()
    }
}


extension AboutCourseVC :UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == cvStaticInfo {
            return CGSize(width: 0.0, height: 0.0)
        }else {
            // Get the view for the first header
            let indexPath = IndexPath(row: 0, section: section)
            let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
            
            // Use this view to calculate the optimal size based on the collection view's width
            return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
                                                      withHorizontalFittingPriority: .required, // Width is fixed
                verticalFittingPriority: .fittingSizeLevel) // Height can be as large as needed
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if collectionView == cvWebinarAboutUs {
            let friendHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WebinarView", for: indexPath) as! WebinarView
            
            if strLanguage == kEnglish {
                friendHeader.lblHeaderTitle.text = "Why Should you subscribe the course?"
            }else if strLanguage == kGujrati {
                friendHeader.lblHeaderTitle.text = "તમારે કોર્સ શા માટે સબ્સ્ક્રાઇબ કરવો જોઈએ?"
            }else if strLanguage == KHindi {
                friendHeader.lblHeaderTitle.text = "आपको कोर्स क्यों सब्सक्राइब करना चाहिए?"
            }
            

            friendHeader.lblHeaderTitle.textAlignment = .center
            friendHeader.lblHeaderTitle.font = FontBook.Avenir_Medium.of(20)
            friendHeader.lblHeaderDescription.text = ""

            if let dic = dicWebFirstInfo {
                friendHeader.lblNoData.isHidden =  true
                friendHeader.imgVideo.isHidden = false
                friendHeader.btnPlay.isHidden = false
                var strImage : String?
                
                if self.strLanguage == kEnglish {
                    strImage = dic.eng_image
                }else if self.strLanguage == kGujrati {
                    strImage = dic.guj_image
                }else if self.strLanguage == KHindi {
                    strImage = dic.hindi_image
                }
                
                
                if strImage == "" {
                    friendHeader.lblNoData.isHidden =  false
                    friendHeader.imgVideo.isHidden = true
                    friendHeader.btnPlay.isHidden = true
                }else {
                    friendHeader.btnPlay.addTarget(self, action: #selector(btnPlayVideo), for:.touchUpInside)
                    friendHeader.imgVideo.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: nil)
                }
                
                
                if arrWebinarList?.count ?? 0 > 0 {
                    friendHeader.noWebinarView.isHidden = true
                }else {
                    friendHeader.noWebinarView.isHidden = false
                }
                
            }else{
                friendHeader.lblNoData.isHidden = false
                friendHeader.imgVideo.isHidden = true
                friendHeader.btnPlay.isHidden = true
            }
            
            return friendHeader
            
        }else if collectionView == cvAboutusInfo{
            let aboutHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleView", for: indexPath) as! TitleView
            aboutHeader.lblTitle.text = "Daily course of Garbh Sanskar is designed by".localized
            aboutHeader.lblTitle.textAlignment = .center
            aboutHeader.lblTitle.font = FontBook.Avenir_Medium.of(25)
            return aboutHeader
        }else {
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvWebinarAboutUs {
             return CGSize(width: (collectionView.frame.size.width / 2) - 10  , height: UIScreen.width / 3)
        }
        else if collectionView == cvAboutusInfo{
            let width = (UIScreen.width / 2) - 5
            return CGSize(width: width, height: width - 30)
        }else {
            let myHeight = collectionView.frame.size.width / 2.6
            heightConstant.constant = myHeight + myHeight / 2 + 20
            return CGSize(width: myHeight , height: myHeight + myHeight / 2)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvWebinarAboutUs {
            return arrFinalWebinarList?.count ?? 0
        }else if collectionView == cvAboutusInfo{
            print(arrAboutUsInfo.count)
            return  arrAboutUsInfo.count
        }else {
            return  arrHeaderInfo?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvWebinarAboutUs {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"PagerCVCell",    for: indexPath) as! PagerCVCell
            let dic  = arrFinalWebinarList?[indexPath.row]
            
            var strImage : String?
            if self.strLanguage == kEnglish {
                strImage = dic?.eng_image
            }else if self.strLanguage == kGujrati {
                strImage = dic?.guj_image
            }else if self.strLanguage == KHindi {
                strImage = dic?.hindi_image
            }
            
            cell.imgPage.sd_setImage(with:URL(string:strImage ?? "") , placeholderImage: #imageLiteral(resourceName: "ic_Faq"))
            cell.layoutIfNeeded()
            return cell
        }else if collectionView == cvAboutusInfo{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"AboutusInfoCell",    for: indexPath) as! AboutusInfoCell
            cell.imgAboutUsInfo.image = arrAboutUsInfo[indexPath.row].image
            cell.lblAboutusInfo.text = arrAboutUsInfo[indexPath.row].Title
 
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"StaticInfoCell",    for: indexPath) as! StaticInfoCell
            cell.imgStatic.image = UIImage(named: arrHeaderInfo?[indexPath.row].image ?? "")
            cell.btnStaticInfo.setTitle(arrHeaderInfo?[indexPath.row].title ?? "", for:.normal)
            cell.btnStaticInfo.addTarget(self, action: #selector(btnStaticInfoPress), for: .touchUpInside)
            cell.btnTapped.addTarget(self, action: #selector(btnStaticInfoPress), for: .touchUpInside)
            cell.btnStaticInfo.tag = indexPath.row
            cell.btnTapped.tag = indexPath.row
            return cell
        }
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvWebinarAboutUs {
            var strVideo = ""
            let dic = arrFinalWebinarList?[indexPath.row]
            if self.strLanguage == kEnglish {
                strVideo = dic?.eng_video ?? ""
            }else if self.strLanguage == kGujrati {
                strVideo = dic?.guj_video ?? ""
            }else if self.strLanguage == KHindi {
                strVideo = dic?.hindi_video ?? ""
            }
            
            if strVideo != "" {
                videoPlay(strVideo: strVideo)
            }
        }
    }
   
    @objc func btnStaticInfoPress(sender : UIButton) {
        self.lblTitleInfo.text = "\(arrHeaderInfo?[sender.tag].title ?? ""):"
        self.lblDetailInfo.text = arrHeaderInfo?[sender.tag].description
    }
    
    @IBAction private func locationRightButton(_ sender: AnyObject) {
  
        let visibleItems: NSArray = self.cvStaticInfo.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 2, section: 0)
        // This is where I'm trying to detect the value
        if nextItem.count == nil {
            return
        }
        self.cvStaticInfo.scrollToItem(at: nextItem, at: .left, animated: true)
    }
    
    
    @IBAction private func locationLeftButton(_ sender: AnyObject) {
        let visibleItems: NSArray = self.cvStaticInfo.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 2, section: 0)
        // This is where I'm trying to detect the value
        if nextItem.count == nil {
            return
        }
        self.cvStaticInfo.scrollToItem(at: nextItem, at: .right, animated: true)
    }
    
    
    @objc func btnPlayVideo(sender: UIButton) {
        var strVideo = ""
        if self.strLanguage == kEnglish {
            strVideo = dicWebFirstInfo?.eng_video ?? ""
        }else if self.strLanguage == kGujrati {
            strVideo = dicWebFirstInfo?.guj_video ?? ""
        }else if self.strLanguage == KHindi {
            strVideo = dicWebFirstInfo?.hindi_video ?? ""
        }
        
        if strVideo != "" {
            videoPlay(strVideo: strVideo)
        }
    }
}


