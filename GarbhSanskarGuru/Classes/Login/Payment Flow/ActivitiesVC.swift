//
//  ActivitiesVC.swift
//  GarbhSanskarGuru
//
//  Created by Jahanvi Trivedi on 20/01/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ActivitiesVC: UIViewController {

    // MARK: Properties
    @IBOutlet weak var lblTop1: UILabel!
    @IBOutlet weak var lblTop2: UILabel!
    @IBOutlet weak var lblTop3: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel!
    @IBOutlet weak var lbl11: UILabel!
    @IBOutlet weak var lbl12: UILabel!
    @IBOutlet var btnBack: UIButton!
    
    var isShowBackButton:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnBack.isHidden = !self.isShowBackButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        setActivitiesVC()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: Fucntion
    func setActivitiesVC()
    {
        lblTop1.text = _appDelegator.languageBundle?.localizedString(forKey: "Course Includes- ", value: "", table: nil)
        lblTop2.text = _appDelegator.languageBundle?.localizedString(forKey: "Daily 12 Activities,", value: "", table: nil)
        lblTop3.text = _appDelegator.languageBundle?.localizedString(forKey: "As Per Pregnancy Day", value: "", table: nil)
        lbl1.text = _appDelegator.languageBundle?.localizedString(forKey: "1. Message of the unborn to the mother", value: "", table: nil)
        lbl2.text = _appDelegator.languageBundle?.localizedString(forKey: "2. Diet chart and chart based recipes", value: "", table: nil)
        lbl3.text = _appDelegator.languageBundle?.localizedString(forKey: "3. Motivational occasions from the biographies of great people", value: "", table: nil)
        lbl4.text = _appDelegator.languageBundle?.localizedString(forKey: "4. Pregnancy Yoga and Meditation", value: "", table: nil)
        lbl5.text = _appDelegator.languageBundle?.localizedString(forKey: "5. Conversation with the unborn and positive suggestions for emotional attachment with baby", value: "", table: nil)
        lbl6.text = _appDelegator.languageBundle?.localizedString(forKey: "6. Prayers", value: "", table: nil)
        lbl7.text = _appDelegator.languageBundle?.localizedString(forKey: "7. Verse (shloka) explaining the importance of life", value: "", table: nil)
        lbl8.text = _appDelegator.languageBundle?.localizedString(forKey: "8. Skills Enhancing Activities (Art & Craft, Drawing, House Hack, Best from the waste, etc ..)", value: "", table: nil)
        lbl9.text = _appDelegator.languageBundle?.localizedString(forKey: "9. Best Video to Watch during pregnancy (Motivational, Enlightening, Informative, Pregnancy Music, Comedy, etc ...)", value: "", table: nil)
        lbl10.text = _appDelegator.languageBundle?.localizedString(forKey: "10. A story with a lesson to be learned", value: "", table: nil)
        lbl11.text = _appDelegator.languageBundle?.localizedString(forKey: "11. Brain Activity - Puzzle to develop the best of a child's brain", value: "", table: nil)
        lbl12.text = _appDelegator.languageBundle?.localizedString(forKey: "12. “Garbh Satsang” for developing a sense of happiness and satisfaction in the life of the child and for the overall development. (Summary from Gita, Ramayan, Vedas and other scriptures )", value: "", table: nil)
    }
    
    @IBAction func btnSubscribe(_ sender: UIButton)
    {
        let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PackagesVC") as! PackagesVC
        self.navigationController?.pushViewController(packagesVC, animated: true)
    }
    
    @IBAction func btnbackTapped(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
