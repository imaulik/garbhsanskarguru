//
//  NotificationVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 27/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class NotificationVC: ParentViewController {
    
    var notificationData : NotificationInfo?
    var notificationURL : String?
    @IBOutlet weak var btnViewAnnouncement: UIButton!
    
//    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.scrollView.bounces = false
        
        if let notiInfo = notificationData {
            let type = notiInfo.announcement_type
            var value = ""
            value =  notiInfo.announcement_text
            
            if type == "image" {
                btnViewAnnouncement.isHidden = false
            }
            
            webView.loadHTMLString(value, baseURL: nil)
        }
        else if((UserDefaults.standard.value(forKey: garbhUserDefaultLastAnnouncement)) != nil){
            
            guard let datavalue = UserDefaults.standard.object(forKey: garbhUserDefaultLastAnnouncement) as? [String:Any] else {
                return
            }
            guard let datainfo = datavalue["gcm.notification.data"] as? String else{
                return
            }
            let data = datainfo.data(using: .utf8)!
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                {
                    print(jsonArray)
                    let type = jsonArray["type"] as! String
                    var value = ""
                    if type == "text"{
                        value = jsonArray["show_data"] as! String
                    }else if type == "image"{
                        //                       value = "<img src=\"" + "\(jsonArray["url"] as! String)" + "\">"
                        value = "<img src=" + "\(jsonArray["url"] as! String)" + ">"
                    }else if type == "html"{
                        value = jsonArray["show_data"] as! String
                    }
                    print("load notification data : \(value)")
                    webView.loadHTMLString(value, baseURL: nil)
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
//            let strHTML = UserDefaults.standard.string(forKey: garbhUserDefaultLastAnnouncement) ?? ""
//            webView.loadHTMLString(strHTML, baseURL: nil)
        }
        else {
            webView.isHidden = true
        }
    }
    
    @IBAction func pushGallery(_ sender: Any) {
        
        guard let myurl  = notificationURL , let noti = notificationData?.announcement_image else {
            return
        }
      
        let gallery = freeAppDashboard.instantiateViewController(withIdentifier: "ImageviewVC") as! ImageviewVC
        gallery.urlImage = myurl + noti
        gallery.modalPresentationStyle = .fullScreen
        self.present(gallery, animated:true, completion: nil)
    }
}

extension NotificationVC {

    @IBAction func onDismissBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true) {
        }
    }
}
