//
//  QuizVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 10/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class QuizVC: ParentViewController {
    
    @IBOutlet weak var tblQuiz: UITableView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var  lblSelectAnswer : UILabel!
    let strLanguage = _appDelegator.getLanguage()
    
    var arrQuizList : [QuizQuestionModel]?
    var finalArrQuizList : QuizQuestionModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Quiz"
        self.tblQuiz.register(UINib(nibName: "QuizCell", bundle: nil), forCellReuseIdentifier: "QuizCell")
        getQuiz()
    }
    
    func getQuiz(){
        let param = ["user_id" : _user.id]
        
        self.showCentralSpinner()
        KPWebCall.call.getQuizData(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                
                let result = QuizModel(dictionary: dict)
                guard let res = result.data else {
                    return
                }
                
                var searchText = ""
                if weakself.strLanguage == kEnglish {
                    searchText = kLongEnglish
                }else if weakself.strLanguage == kGujrati {
//                    searchText = kLongGujrati
                    searchText = "Gujrati"
                }else if weakself.strLanguage == KHindi {
                    searchText = KLongHindi
                }
                
                weakself.arrQuizList = res.filter { catalogName in
                    return catalogName.language ==  searchText
                }
                if weakself.arrQuizList?.count ?? 0 > 0 {
                    weakself.finalArrQuizList = weakself.arrQuizList?[0]
                    weakself.lblQuestion.text = "Q: \(weakself.finalArrQuizList?.quize_question ?? "")"
                    weakself.lblSelectAnswer.isHidden = false
                    weakself.tblQuiz.reloadData()
                }
            }
        }
        
        //        callWS_GetQuiz(parms: parms) { result in
        //            guard let res = result?.data else {
        //                return
        //            }
        //
        //            var searchText = ""
        //            if self.strLanguage == "en" {
        //                searchText = kLongEnglish
        //            }else if self.strLanguage == "gu" {
        //                searchText = "Gujrati"
        //            }else if self.strLanguage == "hi" {
        //                searchText = KLongHindi
        //            }
        //
        //            self.arrQuizList = res.filter { catalogName in
        //                return catalogName.language ==  searchText
        //            }
        //            if self.arrQuizList?.count ?? 0 > 0 {
        //                self.finalArrQuizList = self.arrQuizList?[0]
        //                self.lblQuestion.text = "Q: \(self.finalArrQuizList?.quize_question ?? "")"
        //                self.lblSelectAnswer.isHidden = false
        //                self.tblQuiz.reloadData()
        //            }
        //        }
    }
    
}

extension QuizVC {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  finalArrQuizList?.option?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell", for: indexPath) as! QuizCell
        let obj = finalArrQuizList?.option?[indexPath.row]
        cell.lblAnswer.text = obj?.quize_option
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = finalArrQuizList?.option?[indexPath.row]
        let param = ["user_id":_user.id ,"question_id" : "\(finalArrQuizList?.id ?? "")" ,"answer_id" : "\(obj?.id ?? 0)" , "data_id" : "\(finalArrQuizList?.data_id ?? "")"]
        if  self.finalArrQuizList?.is_answer == 0 {
            
            self.showCentralSpinner()
            KPWebCall.call.checkAnswer(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let result = EmptyModelWithData(dictionary: dict)
                    if Int(result.status) == 200{
                        weakself.finalArrQuizList?.is_answer = 1
                    }
                }
            }
            
            //            callWS_Checkaswer(parms:parm) { result in
            //                if Int(result?.status ?? "") == 200{
            //                    self.finalArrQuizList?.is_answer = 1
            //                }
            //
            //            }
        }else {
            self.showErrorView(data: "You already answered", view: self.view)
            //            showAlert("You already answered")
        }
    }
}
