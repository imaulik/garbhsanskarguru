//
//  BasicDetailVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct BasicInfoData {
    var country = ""
    var state = ""
    var city = ""
    
    func isValidData() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: country) {
            result.isValid = false
            result.error = "Please enter country"
            return result
        }
        
        if String.validateStringValue(str: state) {
            result.isValid = false
            result.error = "Please enter state"
            return result
        }
        
        if String.validateStringValue(str: city) {
            result.isValid = false
            result.error = "Please enter city"
            return result
        }
        return result
    }
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["user_id"] = _user.id
        dict["full_name"] = _user.name
        dict["phone_number"] = _user.mobile
        dict["country"] = country
        dict["city"] = city
        dict["state"] = state
        return dict
    }
}

class BasicDetailVC: ParentViewController {
    
    var arrDetails: [BasicDetailCell] = [.spaceCell, .textCell, .textCell, .textCell, .spaceCell]
    var basicInfoData = BasicInfoData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //prepareUI()
    }
}

extension BasicDetailVC {
    func prepareUI(with status: PregnancyStatus) {
        setKeyboardNotifications()
        tableView.reloadData()
    }
    
    func getEntryCell(_ row: Int) -> BasicInfoCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? BasicInfoCell
        return cell
    }
}

extension BasicDetailVC {
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        let validate = basicInfoData.isValidData()
        if validate.isValid {
            self.updateProfile { (done) in
                if done {
                    //self.performSegue(withIdentifier: "desiredSegue", sender: nil)
                    /*let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BabyCategoryVC") as! BabyCategoryVC
                    self.navigationController?.pushViewController(packagesVC, animated: true)*/
                    _appDelegator.navigateDirectlyToHome()
                }
            }
        }else {
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
}

extension BasicDetailVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDetails.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrDetails[indexPath.row].cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BasicInfoCell
        let info = arrDetails[indexPath.row]
        cell = tableView.dequeueReusableCell(withIdentifier: info.rawValue, for: indexPath) as! BasicInfoCell
        cell.parentVC = self
        cell.screenType = .basicInfoVC
        cell.setUpBasicUI(tag: indexPath.row, data: info)
        return cell
    }
}

extension BasicDetailVC {
    
    func updateProfile(block: @escaping (Bool) -> ()) {
        let param = basicInfoData.paramDict()
        self.showCentralSpinner()
        KPWebCall.call.updateUserProfile(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    DispatchQueue.main.async {
                        _user.setRegionData(dict: data)
                        _appDelegator.saveContext()
                    }
                    block(true)
                }else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}
