//
//  GiftUser.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 19/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import Foundation

enum CurrentStatus: String {
    case active = "active"
    case inActive = "Inactive"
    case none = "none"
}

class GiftUser {
    
    let id: Int
    let amount: Int32
    let comment: String
    let email: String
    let forPayment: String
    let giftCode: String
    let gst_price : Int32
    let name: String
    let noOFPerson: Int
    var payStatus: PaymentStatus
    let currentStatus: CurrentStatus
    let uniqueId: String
    let usdPrice: Int32
    
    init(dict: NSDictionary) {
        id = dict.getIntValue(key: "id")
        name = dict.getStringValue(key: "name")
        amount = dict.getInt32Value(key: "amount")
        comment = dict.getStringValue(key: "comment")
        email = dict.getStringValue(key: "email")
        forPayment = dict.getStringValue(key: "for_payment")
        giftCode = dict.getStringValue(key: "gift_code")
        noOFPerson = dict.getIntValue(key: "number_of_person")
        uniqueId = dict.getStringValue(key: "unique_id")
        payStatus = PaymentStatus(rawValue: dict.getStringValue(key: "payment_status")) ?? .pending
        currentStatus = CurrentStatus(rawValue: dict.getStringValue(key: "status")) ?? .none
        usdPrice = dict.getInt32Value(key: "usd_price")
        gst_price = dict.getInt32Value(key: "gst_price")
    }
    
    func setPaymentStatus(dict: NSDictionary) {
        payStatus = PaymentStatus(rawValue: dict.getStringValue(key: "payment_status")) ?? .pending
    }
}
