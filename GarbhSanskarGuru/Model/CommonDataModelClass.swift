//
//  CommonDataModelClass.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 21/06/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import EVReflection

class AnswerList: EVObject {
    var question_id =  ""
    var answer = ""
    var user_id = ""
    var firstname = ""
    var created_at = ""
    var updated_at = ""
    var id = ""
}

class AnswerModel: EVObject {
    var answer_count = 0
    var status = 0
    var question_count = 0
    var data:[AnswerList]?
    var message:Any?
}

class QuestionList: EVObject {
    var ans_count = 0
    var created_at = ""
    var firstname = ""
    var id = ""
    var question = ""
    var updated_at = ""
    var user_id = ""
}

class AppinfoList : EVObject {
    var eng_thb = ""
    var eng_video = ""
    var eng_youtube = ""
    var guj_thb = ""
    var guj_video = ""
    var guj_youtube = ""
    var hindi_thb = ""
    var hindi_video = ""
    var hindi_youtube = ""
    var id = 0
}

class VideoList:EVObject  {
    var cat_is_read = ""
    var eng_image = ""
    var eng_title = ""
    var eng_url = ""
    var guj_image = ""
    var guj_title = ""
    var guj_url = ""
    var hindi_image = ""
    var hindi_title = ""
    var hindi_url = ""
    var id = ""
    var module_is_read = ""
    var notification_id = ""
    var category_name = ""
}

class TipsCategoryList: EVObject {
    var cat_id_read = ""
    var eng_name = ""
    var guj_name = ""
    var hindi_name = ""
    var notification_id = ""
    var id = ""
    var name = ""
}

class PagerViewList: EVObject {
    var category_name = ""
    var image_name = ""
    var language = ""
    var title = ""
    var video_url = ""
    var month = ""
    var url = ""
    var image = ""
    var id = ""
}

class TipsList : EVObject {
    var cat_id_read = ""
    var id = ""
    var language = ""
    var module_is_read = ""
    var notification_id = ""
    var text_image = ""
    var title = ""
    var type = ""
    var pdf_name = ""
}

class PagerViewModel: EVObject {
    var message = ""
    var status = ""
    var image_url = ""
    var data : [PagerViewList]?
}

class QuizQuestionModel : EVObject {
    var data_id = ""
    var id = ""
    var is_answer = 0
    var language = ""
    var option : [QuizAnswerList]?
    var quize_answer = ""
    var quize_question = ""
}

class QuizAnswerList : EVObject {
    var id = 0;
    var question_id = 0;
    var quize_option = "";
}


class QuizModel : EVObject {
    var status = ""
    var message = ""
    var image_url = ""
    var data : [QuizQuestionModel]?
}
 
class AudioList:EVObject  {
    var id = ""
    var image = ""
    var title = ""
    var language = ""
    var type = ""
}

class ArticleList: EVObject {
    var cat_id_read = ""
    var id = ""
    var image = ""
    var language = ""
    var module_is_read = ""
    var notification_id = ""
    var title = ""
    var url = ""
}

class ChatAutoQuestionList : EVObject {
    var id = ""
    var question = ""
}

class QuestionModel: EVObject {
    var status = 0
    var question_count = 0
    var data:[QuestionList]?
    var message:Any?
}

class AppInfoModel: EVObject {
    var message = ""
    var status = ""
    var url = ""
    var data : [AppinfoList]?
}

class VideoViewModel: EVObject {
    var message = ""
    var status = ""
    var image_url = ""
    var data : [VideoList]?
}

class TipsDetailModel: EVObject {
    var message = ""
    var status = ""
    var image_url = ""
    var data : [TipsList]?
}
 
class ArticleModel: EVObject {
    var message = ""
    var status = ""
    var image_url = ""
    var data : [ArticleList]?
}

class EmptyModelWithData : EVObject {
    var message:Any?
    var status  = ""
    var data:AnyObject?
}

class AudioViewModel: EVObject {
    var message = ""
    var status = ""
    var image_url = ""
    var data : [AudioList]?
}

class AboutUsModel : EVObject {
    var results:[AboutUsM]?
    var message  = ""
    var status = 0
}

class AboutUsM: EVObject {
    var about_id = 0
    var about_contenthi = ""
    var about_contenten = ""
    var about_contentgu = ""
}

class TipsViewModel: EVObject {
    var message = ""
    var status = ""
    var image_url = ""
    var data : [TipsCategoryList]?
}
 
class PopupInfo : EVObject{
    var created_date = ""
    var eng_image = ""
    var guj_image = ""
    var hindi_image = ""
    var eng_Uimage: UIImage?
    var guj_Uimage: UIImage?
    var hindi_Uimage: UIImage?
    var id = 0
    var marathi_image  = ""
    var update_date = ""
}

class WebinarInfo: EVObject{
   var eng_image = ""
    var eng_video = ""
    var guj_image = ""
    var guj_video = ""
    var hindi_image = ""
    var hindi_video = ""
    var id = 0
    var modes = ""
}


class StatisticsInfo  {
    var Title = ""
    var Description = ""
    var value = ""
    var image = UIImage()
    
    init(title : String , description : String,image : UIImage){
        self.Title = title
        self.Description = description
        self.image = image
    }
}


class AboutInfo  {
    var Title = ""
    var image = UIImage()
    
    init(title : String,image : UIImage){
        self.Title = title
        self.image = image
    }
}


class HeaderAboutUS {
    var title = ""
    var image = ""
    var description = ""
  
    init(title : String,image : String ,description : String){
        self.title = title
        self.image = image
        self.description = description
    }
}


class NotificationInfo : EVObject {
    var announcement_image = ""
    var announcement_text = ""
    var announcement_title = ""
    var announcement_type = ""
    var created_date = ""
    var id = ""
    var noti_id = 0
}

 
