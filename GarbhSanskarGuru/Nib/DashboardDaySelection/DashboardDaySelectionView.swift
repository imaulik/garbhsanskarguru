//
//  DashboardDaySelectionView.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 07/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class DashboardDaySelectionView: UIView {

    @IBOutlet weak var vwDayCircle: UIView!
    @IBOutlet var lblDay: UILabel!
    @IBOutlet weak var constraintTrailingVwDay: NSLayoutConstraint!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
