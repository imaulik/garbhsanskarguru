//
//  ModelGarbhSamvad.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import Foundation

enum GarbhScreenType: String {
    case todayStory = "getStory"
    case activityOfDay = "activityofday"
    case prayer = "getplayer"
    case yoga = "getyogaactity"
    case biography = "getBiography"
    case shloka = "getshloka"
    case video = "getvideo"
    case brainActivity = "puzzle"
    case garbhSatsang = "garbhsatsang"
    
    var navTitle: String {
        switch self {
        case .todayStory:
            return "Today's Story"
        case .activityOfDay:
            return "Activity of Day"
        case .prayer:
            return "Prayer"
        case .yoga:
            return "Yoga"
        case .biography:
            return "Biography"
        case .shloka:
            return "Shloka"
        case .video:
            return "Video"
        case .brainActivity:
            return "Brain Activity"
        case .garbhSatsang:
            return "Garbh Satsang"
        }
    }
    
    var strDoneActivityTitle: String {
        switch self {
        case .todayStory:
            return "today_story"
        case .activityOfDay:
            return "activity_day"
        case .prayer:
            return "prayer"
        case .yoga:
            return "yoga"
        case .biography:
            return "biography"
        case .shloka:
            return "shloka"
        case .video:
            return "video"
        case .brainActivity:
            return "puzzle"
        case .garbhSatsang:
            return "garbhsatsang"
        }
    }
}

class ModelGarbhSamvad {
    var activitySeen: ActivitySeenStatus = .undone
    var imgStr = ""
    var type: DisplayType = .image
    var arrGarbhSamvad = [GarbhSamvadDetail]()
    
    init(dict: NSDictionary, with screenType: GarbhScreenType = .activityOfDay) {
        imgStr = dict.getStringValue(key: "image_url")
        
        //abhi comment below line because key comes into "data" dictionary
        //activitySeen = ActivitySeenStatus(rawValue: dict.getStringValue(key: "activity_seen")) ?? .undone
        let scrnType = screenType == .yoga ? "yoda_data" : "activity_detail"
        if let jsonDict = dict["data"] as? NSDictionary, let arrData = jsonDict[scrnType] as? [NSDictionary] {
            type = DisplayType(rawValue: jsonDict.getStringValue(key: "type")) ?? .image
            activitySeen = ActivitySeenStatus(rawValue: jsonDict.getStringValue(key: "activity_seen")) ?? .undone
            for data in arrData {
                self.arrGarbhSamvad.append(GarbhSamvadDetail(dict: data))
            }
        }
    }
    
    init() { }
}

class GarbhSamvadDetail {
    
    let id: Int
    let yogId: String
    let image: String
    let activityId: String
    let language: DownloadLanguage
    let displayType: DisplayType
    let type: String
    let day: String
    let videoUrl: String
    
    init(dict: NSDictionary) {
        id = dict.getIntValue(key: "id")
        yogId = dict.getStringValue(key: "yog_id")
        image = dict.getStringValue(key: "image")
        activityId = dict.getStringValue(key: "activity_id")
        displayType = DisplayType(rawValue: dict.getStringValue(key: "type")) ?? .image
        language = DownloadLanguage(rawValue: dict.getStringValue(key: "language")) ?? .english
        type = dict.getStringValue(key: "type")
        day = dict.getStringValue(key: "day")
        videoUrl = dict.getStringValue(key: "video_url")
    }
    
    var getYoutubeId: String? {
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            
            let match = regex.firstMatch(in: image, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, image.lengthOfBytes(using: String.Encoding.utf8)))
            
            let range = match?.range(at: 0)
            let youTubeID = (image as NSString).substring(with: range!)
            return youTubeID
        } catch let err {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: err.localizedDescription)
        }
        return nil
    }
    
    var imgUrl: URL {
        return URL(string: image)!
    }
}

class ModelRahasyaTitle{
    
    let id: String
    let titleEng: String
    let titleHindi: String
    let titleGuj: String
    
    init(dict: NSDictionary) {
        id  = dict.getStringValue(key: "cat_id")
        titleEng  = dict.getStringValue(key: "cat_titleEn")
        titleHindi  = dict.getStringValue(key: "cat_titleHi")
        titleGuj  = dict.getStringValue(key: "cat_titleGu")
    }
}

class ModelRahasyaContent{
    
    let contentId: String
    let sequenceId: String
    let content_Eng: String
    let content_Hindi: String
    let content_Gujarati: String
    
    init(dict: NSDictionary) {
        contentId = dict.getStringValue(key: "content_id")
        sequenceId = dict.getStringValue(key: "sequence")
        content_Eng = dict.getStringValue(key: "content_ImgEn")
        content_Hindi = dict.getStringValue(key: "content_ImgHi")
        content_Gujarati = dict.getStringValue(key: "content_ImgGu")
    }
}

class ModelGarbhSatsang {
    var activitySeen: ActivitySeenStatus = .undone
    var imgStr = ""
    var type: DisplayType = .image
    var arrGarbhSamvad = [GarbhSatsangDetail]()
    
    init(dict: NSDictionary, with screenType: GarbhScreenType = .activityOfDay) {
        imgStr = dict.getStringValue(key: "image_url")
        
        //abhi comment below line because key comes into "data" dictionary
        //activitySeen = ActivitySeenStatus(rawValue: dict.getStringValue(key: "activity_seen")) ?? .undone
        let scrnType = screenType == .yoga ? "yoda_data" : "activity_detail"
        if let jsonDict = dict["data"] as? NSDictionary, let arrData = jsonDict[scrnType] as? [NSDictionary]
        {
            type = DisplayType(rawValue: arrData[0].getStringValue(key: "type")) ?? .image
            activitySeen = ActivitySeenStatus(rawValue: jsonDict.getStringValue(key: "activity_seen")) ?? .undone
            for data in arrData {
                self.arrGarbhSamvad.append(GarbhSatsangDetail(dict: data))
            }
        }
    }
    
    init() { }
}

class GarbhSatsangDetail {
    
    let id: Int
    let yogId: String
    let image: String
    let activityId: String
    let language: DownloadGarbhsatsangLanguage
    let displayType: DisplayType
    let type: String
    let day: String
    let videoUrl: String
    var categoryId : String!
    
    init(dict: NSDictionary) {
        id = dict.getIntValue(key: "id")
        categoryId = dict.getStringValue(key: "category_id")
        yogId = dict.getStringValue(key: "yog_id")
        image = dict.getStringValue(key: "text_image_name")
        activityId = dict.getStringValue(key: "activity_id")
        displayType = DisplayType(rawValue: dict.getStringValue(key: "type")) ?? .image
        language = DownloadGarbhsatsangLanguage(rawValue: dict.getStringValue(key: "language")) ?? .english
        type = dict.getStringValue(key: "type")
        day = dict.getStringValue(key: "day")
        videoUrl = dict.getStringValue(key: "text_image_name")
    }
    
    var getYoutubeId: String? {
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            
            let match = regex.firstMatch(in: image, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, image.lengthOfBytes(using: String.Encoding.utf8)))
            
            let range = match?.range(at: 0)
            let youTubeID = (image as NSString).substring(with: range!)
            return youTubeID
        } catch let err {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: err.localizedDescription)
        }
        return nil
    }
    
    var imgUrl: URL {
        return URL(string: image)!
    }
}
