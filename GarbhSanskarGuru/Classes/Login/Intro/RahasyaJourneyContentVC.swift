//
//  RahasyaJourneyContentVC.swift
//  Garbhsanskar
//
//  Created by Jayesh Tejwani on 04/12/18.
//  Copyright © 2018 Jahanvi Trivedi. All rights reserved.
//

import UIKit

class ModelJourney{
    
    let id: String
    let month_En: String
    let month_Hi: String
    let month_Gu: String
    
    init(dict: NSDictionary) {
        id = dict.getStringValue(key: "month_id")
        month_En = dict.getStringValue(key: "monthDataEn")
        month_Hi = dict.getStringValue(key: "monthDataHi")
        month_Gu = dict.getStringValue(key: "monthDataGu")
    }
}

class RahasyaJourneyContentVC: ParentViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    
    var monthType: JourneyMonths = .month1
    var objJourney: ModelJourney!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setImage()
    }
    
    func prepareImageUI() {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        
        switch currLanguage{
        case .hindi:
            self.imgView.kf.setImage(with: URL(string: objJourney.month_Hi), placeholder: _placeImage)
        case .english:
            self.imgView.kf.setImage(with: URL(string: objJourney.month_En), placeholder: _placeImage)
        case .gujarati:
            self.imgView.kf.setImage(with: URL(string: objJourney.month_Gu), placeholder: _placeImage)
        }
    }
}

extension RahasyaJourneyContentVC{
    
    func setImage(){
        let param: [String:Any] = ["month_id" : monthType.rawValue]
        self.showCentralSpinner()
        KPWebCall.call.getGarbhYatraContent(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary {
                self.objJourney = ModelJourney(dict: dict)
                DispatchQueue.main.async {
                    self.prepareImageUI()
                }
            } else {
                self.showError(data: json, view: self.view)
            }
        }
    }
}
