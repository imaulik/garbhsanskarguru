//
//  SuccessfullPayVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SuccessfullPayVC: ParentViewController {

    @IBOutlet weak var lblGiftCode: UILabel!
    @IBOutlet weak var btnPopView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    func prepareUI() {
        lblGiftCode.text = _giftUser.giftCode
        btnPopView.setTitle(_user == nil ? "Back To Login" : "Back To Dashboard", for: .normal)
    }
    
    func navToDashBoard() {
        if let navControllers = self.navigationController?.viewControllers {
            for vc in navControllers {
                if vc is SlideMenuContainerVC {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func btnCopyCode(_ sender: UIButton) {
        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Gift Code Copied")
        UIPasteboard.general.string = _giftUser.giftCode
    }
    
    @IBAction func btnNavigateToLogin(_ sender: UIButton) {
        if _user == nil {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            self.navToDashBoard()
        }
    }
}
