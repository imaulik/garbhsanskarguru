//
//  CompletedReportController.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 28/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class CompletedReportController: ParentViewController {
    
    @IBOutlet var labelTitle: JPWidthLabel!
    @IBOutlet var labelName: JPWidthLabel!
    @IBOutlet var viewBackground: UIView!
    
    @IBOutlet var progress_IQ: MBCircularProgressBarView!
    @IBOutlet var progress_PQ: MBCircularProgressBarView!
    @IBOutlet var progress_EQ: MBCircularProgressBarView!
    @IBOutlet var progress_SQ: MBCircularProgressBarView!
    
    var reportModel:ReportModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareForUI()
        prepareForUIUpdate()
        prepareProgressUIUpdate()
    }
    
}

extension CompletedReportController {
    
    func prepareForUI(){
        let view_bg = UIColor.createGradient(gradientStyle: .leftToRight, colors: [GarbhColor.gradientLeftBlue,GarbhColor.gradientRightBlue], frame: self.viewBackground.bounds)
        viewBackground.backgroundColor = view_bg
    }
    
    func prepareForUIUpdate(){
        labelTitle.text = labelTitle.text! + " (\(_user.currDay)) "
        labelName.text = labelName.text! + " \(_user.name) "
    }
    
    func prepareProgressUIUpdate(){
        progress_IQ.value = CGFloat(self.reportModel!.report_IQ_double)
        progress_PQ.value = CGFloat(self.reportModel!.report_PQ_double)
        progress_EQ.value = CGFloat(self.reportModel!.report_EQ_double)
        progress_SQ.value = CGFloat(self.reportModel!.report_SQ_double)
    }
    
}
