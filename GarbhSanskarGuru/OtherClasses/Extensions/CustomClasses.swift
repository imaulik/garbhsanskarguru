//  Created by iOS Development Company on 19/04/16.
//  Copyright © 2016 iOS Development Company All rights reserved.
//

import Foundation
import UIKit

class KPTouch: ConstrainedView {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class KPRoundButton: UIButton {
    
    @IBInspectable var isRatioAppliedOnSize: Bool = false
    
    @IBInspectable var isRationAppliedOnText: Bool = false{
        didSet{
            if isRationAppliedOnText, let afont = titleLabel?.font {
                titleLabel?.font = afont.withSize(afont.pointSize * _widthRatio)
            }
        }
    }
    
    @IBInspectable var cornerRadious: CGFloat = 0{
        didSet{
            if cornerRadious == 0{
                layer.cornerRadius = isRatioAppliedOnSize ? (self.frame.height * _widthRatio) / 2 : self.frame.height / 2
            }else{
                layer.cornerRadius = isRatioAppliedOnSize ? cornerRadious * _widthRatio : cornerRadious
            }
        }
    }
    
//    @IBInspectable var borderColor: UIColor = UIColor.clear{
//        didSet{
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//
//    @IBInspectable var borderWidth: CGFloat = 0{
//        didSet{
//            layer.borderWidth = borderWidth
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.masksToBounds = true
    }
}

class KPRoundLabel: UILabel {
    
    @IBInspectable var isRatioAppliedOnSize: Bool = false
    
    @IBInspectable var isRationAppliedOnText: Bool = false{
        didSet{
            if isRationAppliedOnText {
                font = font.withSize(font.pointSize * _widthRatio)
            }
        }
    }
    
//    @IBInspectable var borderColor: UIColor = UIColor.clear{
//        didSet{
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//
//    @IBInspectable var borderWidth: CGFloat = 0{
//        didSet{
//            layer.borderWidth = borderWidth
//        }
//    }
    
    @IBInspectable var cornerRadious: CGFloat = 0{
        didSet{
            if cornerRadious == 0{
                layer.cornerRadius = isRatioAppliedOnSize ? (self.frame.height * _widthRatio) / 2 : self.frame.height / 2
            }else{
                layer.cornerRadius = isRatioAppliedOnSize ? cornerRadious * _widthRatio : cornerRadious
            }
        }
    }
    
    @IBInspectable var letterSpace : CGFloat = 0{
        didSet{
            letterSpace = letterSpace * _widthRatio
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class KPRoundTextField: UITextField {
    
    @IBInspectable var isRatioAppliedOnSize: Bool = false
    
    @IBInspectable var isRationAppliedOnText: Bool = false{
        didSet{
            if isRationAppliedOnText {
                font = font?.withSize((font?.pointSize)! * _widthRatio)
            }
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadious: CGFloat = 0{
        didSet{
            if cornerRadious == 0{
                layer.cornerRadius = isRatioAppliedOnSize ? (self.frame.height * _widthRatio) / 2 : self.frame.height / 2
            }else{
                layer.cornerRadius = isRatioAppliedOnSize ? cornerRadious * _widthRatio : cornerRadious
            }
        }
    }
    
    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.masksToBounds = true
    }
}

class KPRoundView: UIView {
    
    @IBInspectable var isRatioAppliedOnSize: Bool = false
    
    @IBInspectable var cornerRadious: CGFloat = 0{
        didSet{
            if cornerRadious == 0{
                layer.cornerRadius = isRatioAppliedOnSize ? (self.frame.height * _widthRatio) / 2 : self.frame.height / 2
            }else{
                layer.cornerRadius = isRatioAppliedOnSize ? cornerRadious * _widthRatio : cornerRadious
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.masksToBounds = true
    }
}

class KPRoundImageView: UIImageView {
    
    @IBInspectable var isRatioAppliedOnSize: Bool = false
    
    @IBInspectable var cornerRadious: CGFloat = 0{
        didSet{
            if cornerRadious == 0{
                layer.cornerRadius = isRatioAppliedOnSize ? (self.frame.height * _widthRatio) / 2 : self.frame.height / 2
            }else{
                layer.cornerRadius = isRatioAppliedOnSize ? cornerRadious * _widthRatio : cornerRadious
            }
        }
    }
    
//    @IBInspectable var borderColor: UIColor = UIColor.clear{
//        didSet{
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//    
//    @IBInspectable var borderWidth: CGFloat = 0{
//        didSet{
//            layer.borderWidth = borderWidth
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.masksToBounds = true
    }
}

class KPNavigationBar: UIView{
    
    @IBInspectable var cornerRadious: CGFloat = 0
    @IBOutlet var lblTitle:UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        let rect = CGRect(origin: self.bounds.origin, size: CGSize(width: _screenSize.width, height: _btmNavigationHeight))
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadious, height: cornerRadious))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class KPSingleRoundCornerView: UIView{
    
    @IBInspectable var isTopLeft: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let rect = CGRect(origin: self.bounds.origin, size: CGSize(width: _screenSize.width / 2 - 1, height: 62))
        let corner:UIRectCorner = isTopLeft ? [UIRectCorner.topLeft] : [UIRectCorner.topRight]
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corner, cornerRadii: CGSize(width: 7, height: 7))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.lineWidth = 2
        mask.strokeColor = isTopLeft ? GarbhColor.red.cgColor : GarbhColor.gray.cgColor
        mask.fillColor = UIColor.white.cgColor
        self.layer.addSublayer(mask)
        for view in self.subviews{
            self.bringSubviewToFront(view)
        }
    }
}

class RoundShadowView: KPRoundView{
    
    @IBInspectable var xPos: CGFloat = 0
    @IBInspectable var yPos: CGFloat = 0
    @IBInspectable var radious: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clipsToBounds = true
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: xPos, height: yPos)
        layer.shadowRadius = radious
        let shadowRect = CGRect(origin: CGPoint.zero, size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        let roundPath = UIBezierPath(roundedRect: shadowRect, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: self.frame.size.height / 2, height: self.frame.size.height / 2))
        layer.shadowPath = roundPath.cgPath
    }
}

class ShadowView: BaseView{
    @IBInspectable var xPos: CGFloat = 0
    @IBInspectable var yPos: CGFloat = 0
    @IBInspectable var radius: CGFloat = 0
    @IBInspectable var isBorderApplied: Bool = false
    @IBInspectable var isShadowApplied: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowColor = isShadowApplied ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.12) : UIColor.clear.cgColor
        layer.shadowOpacity = isShadowApplied ? 1 : 0
        layer.shadowOffset = isShadowApplied ? CGSize(width: xPos, height: yPos) : CGSize.zero
        layer.shadowRadius = isShadowApplied ? 3 : 0
        layer.cornerRadius = radius
        layer.borderColor = isBorderApplied ? #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.8) .cgColor : UIColor.clear.cgColor
        layer.borderWidth = isBorderApplied ? 0.4 : 0
    }
}

//MARK: - Constained Classes for All device support
class KPGradientView : UIView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.applyGradientEffects([GarbhColor.gradient1Pink, GarbhColor.gradient2Pink], gradientPoint: .leftRight)
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: self.frame.size.height)
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
}

class KPBlackGredientView : UIView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.applyGradient(colours: [UIColor.black.withAlphaComponent(0),UIColor.black.withAlphaComponent(1)])
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: _btmNavigationHeight)
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
}

//class BlurView: VisualEffectView{
//    
//    @IBInspectable var radious: CGFloat = 0 {
//        didSet{
//            blurRadius = radious
//        }
//    }
//    
//    @IBInspectable var color: UIColor = UIColor.clear{
//        didSet{
//            self.colorTint = color
//        }
//    }
//    
//    @IBInspectable var colorAlpha: CGFloat = 0{
//        didSet{
//            self.colorTintAlpha = colorAlpha
//        }
//    }
//    
//    override func awakeFromNib() {
//        self.backgroundColor = UIColor.clear
//    }
//}

class SimpleDarkBlur: UIView {
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        self.sendSubviewToBack(blurEffectView)
    }
}

class SimpleLightBlur: UIView {
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        self.sendSubviewToBack(blurEffectView)
    }
}

/// This Class will decrease font size as well it will make intrinsiz content size 10 pixel bigger as we need padding on both side of label
class KPointsLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        font = font.withSize(font.pointSize * _widthRatio)
        self.layer.cornerRadius = ((self.bounds.size.height + 4) * _widthRatio)/2
        self.layer.masksToBounds = true
    }

    override var intrinsicContentSize: CGSize{
        let asize = super.intrinsicContentSize
        if self.text?.count == 1{
            return CGSize(width: (22 * _widthRatio) , height: asize.height + (4 * _widthRatio))
        }else{
            let width = asize.width + (2 * _widthRatio)
            let height = asize.height + (4 * _widthRatio)
            return CGSize(width: (width < height ? height : width) , height: height)
        }
    }
}

class BaseView: UIView {
    @IBInspectable public var isRound: Bool {
        get { return (layer.cornerRadius == (frame.size.width/2) * _widthRatio) || (layer.cornerRadius == (frame.size.height/2) * _heightRatio) }
        set { layer.cornerRadius = newValue == true ? (frame.size.height/2) * _widthRatio : layer.cornerRadius }
    }
    @IBInspectable public var isViewRound: Bool {
        get { return (layer.cornerRadius == (frame.size.width/2) ) || (layer.cornerRadius == (frame.size.height/2)) }
        set { layer.cornerRadius = newValue == true ? (frame.size.height/2) : layer.cornerRadius }
    }
    @IBInspectable public var borderWidth: CGFloat {
        get { return self.layer.borderWidth }
        set { self.layer.borderWidth = newValue }
    }
    @IBInspectable public var borderColor: UIColor {
        get { return self.layer.borderColor == nil ? UIColor.clear : UIColor(cgColor: self.layer.borderColor!) }
        set { self.layer.borderColor = newValue.cgColor }
    }
    @IBInspectable public var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
    @IBInspectable public var shadowRadius: CGFloat {
        get { return layer.shadowRadius }
        set { layer.shadowRadius = newValue }
    }
    @IBInspectable public var shadowOpacity: Float {
        get { return layer.shadowOpacity }
        set { layer.shadowOpacity = newValue }
    }
    @IBInspectable public var shadowColor: UIColor? {
        get { return layer.shadowColor != nil ? UIColor(cgColor: layer.shadowColor!) : nil }
        set { layer.shadowColor = newValue?.cgColor }
    }
    @IBInspectable public var shadowOffset: CGSize {
        get { return layer.shadowOffset }
        set { layer.shadowOffset = newValue }
    }
    @IBInspectable public var zPosition: CGFloat {
        get { return layer.zPosition }
        set { layer.zPosition = newValue }
    }
}

class BaseButton: UIButton {
    @IBInspectable public var isRound: Bool {
        get { return (layer.cornerRadius == (frame.size.width/2) ) || (layer.cornerRadius == (frame.size.height/2)) }
        set { layer.cornerRadius = newValue == true ? (frame.size.height/2) : layer.cornerRadius }
    }
    @IBInspectable public var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
}

@IBDesignable
class PlaceHolderTextView: UITextView {

    @IBInspectable var placeholder: String = "" {
         didSet{
             updatePlaceHolder()
        }
    }

    @IBInspectable var placeholderColor: UIColor = UIColor.gray {
        didSet {
            updatePlaceHolder()
        }
    }

    private var originalTextColor = UIColor.darkText
    private var originalText: String = ""

    private func updatePlaceHolder() {

        if self.text == "" || self.text == placeholder  {

            self.text = placeholder
            self.textColor = placeholderColor
            if let color = self.textColor {

                self.originalTextColor = color
            }
            self.originalText = ""
        } else {
            self.textColor = self.originalTextColor
            self.originalText = self.text
        }

    }

    override func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        if self.text == placeholder{
            self.text = self.originalText
        }
        self.textColor = self.originalTextColor
        return result
    }
    override func resignFirstResponder() -> Bool {
        let result = super.resignFirstResponder()
        updatePlaceHolder()

        return result
    }
}

//MARK: Extension for change cursor color in whole app
class AttrTextField: CustomTextField {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBInspectable public var placeholderColor: UIColor {
        get {
            return self.placeholderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder!, attributes:[NSAttributedString.Key.foregroundColor: newValue])
        }
    }
}
