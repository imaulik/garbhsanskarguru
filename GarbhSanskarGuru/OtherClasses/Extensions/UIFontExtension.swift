//  Created by iOS Development Company on 13/01/16.
//  Copyright © 2016 The App Developers. All rights reserved.
//

import UIKit

enum GarbhSanskarFont: String {
    case medium         = "Avenir-Medium"
    case regular        = "SofiaProRegular"
    case light          = "SofiaPro-Light"
    
    case Avenir_Book         = "Avenir-Book"
    case Avenir_Black        = "Avenir-Black"
    case Avenir_Heavy        = "Avenir-Heavy"
    case Avenir_Light        = "Avenir-Light"
    
    case Ubuntu_Bold         = "Ubuntu-Bold"
    case WorkSans_Light      = "WorkSans-Light"
}

extension UIFont {
    
    class func GarbhFontWith(_ name: GarbhSanskarFont, size: CGFloat) -> UIFont{
        return UIFont(name: name.rawValue, size: size)!
    }
}


enum FontBook: String {
    //MARK:- Avenir Font
    case Avenir_Book         = "Avenir-Book"
    case Avenir_Black        = "Avenir-Black"
    case Avenir_Medium       = "Avenir-Medium"
    case Avenir_Heavy        = "Avenir-Heavy"
    case Avenir_Light        = "Avenir-Light"
    
    //MARK:- AvenirNext Font
    case AvenirNext_Medium            = "AvenirNext-Medium"
    case AvenirNext_Heavy             = "AvenirNext-Heavy"
    case AvenirNext_UltraLight        = "AvenirNext-UltraLight"
    
    //MARK:- HelveticaNeue Font
    
    case HelveticaNeue_Light                    = "HelveticaNeue-Light"
    case HelveticaNeue_Regular                  = "HelveticaNeue"
    
    //MARK:- Method
    
    func of(_ size: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: size)!
    }
}
