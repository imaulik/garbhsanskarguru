
class PopupEditQuestion: MyPopUpView {
    @IBOutlet weak var lblPopTitle: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtQuestion:UITextView!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    var myQuestion: QuestionList?
    var myAnswer: AnswerList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let question = objData as? QuestionList{
            myQuestion = question
            lblPopTitle.text = "Edit Question"
            lblPlaceHolder.text = "Edit Question"
            txtQuestion.text = myQuestion?.question
        }
        else {
            myAnswer = objData as? AnswerList
            lblPopTitle.text = "Edit Answer"
            lblPlaceHolder.text = "Edit Answer"
            txtQuestion.text = myAnswer?.answer
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func btnOkClick(_ sender: UIButton) {
        if txtQuestion.text?.trimmedString()  == "" {
            if let _ = objData as? QuestionList{
                self.showErrorView(data: "Please enter Question", view: self.view)
//                showAlert("Please enter Question", position: .Center)
            }
            else {
                self.showErrorView(data: "Please enter Answer", view: self.view)
//                showAlert("Please enter Answer", position: .Center)
            }
        }
        else{
            
            if let _ = objData as? QuestionList{
                let param = ["user_id" : _user.id, "question_id":myQuestion?.id ?? "0","question":txtQuestion.text ?? ""]
                self.showCentralSpinner()
                KPWebCall.call.editQuestion(param: param) { [weak self] (json, statusCode) in
                    guard let weakself = self else {return}
                    weakself.hideCentralSpinner()
                    if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                        if (weakself.pressOK != nil) {
                            weakself.pressOK!()
                        }
                    }
                }
            }
            else {
                let param = ["user_id" : _user.id, "answer_id":myAnswer?.id ?? "0", "answer" :txtQuestion.text ?? ""]
                self.showCentralSpinner()
                KPWebCall.call.editAnswer(param: param) { [weak self] (json, statusCode) in
                    guard let weakself = self else {return}
                    weakself.hideCentralSpinner()
                    if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                         if (weakself.pressOK != nil) {
                            weakself.pressOK!()
                        }
                    }
                }
            }
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        
        if (self.pressCancel != nil) {
            self.pressCancel!()
        }
    }
    
}


