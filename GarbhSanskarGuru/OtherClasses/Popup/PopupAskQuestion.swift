import SearchTextField


class PopupAskQuestion: MyPopUpView {
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtQuestion:UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnOkClick(_ sender: UIButton) {
        if txtQuestion.text?.trimmedString()  == "" {
            self.showErrorView(data: "Please enter Question", view: self.view)
            //            showAlert("Please enter Question", position: .Center)
        }
        else if txtQuestion.text?.trimmedString().count ?? 0 < 10 {
            self.showErrorView(data: "Please enter Minimum 10 character", view: self.view)
            //            showAlert("Please enter Minimum 10 character", position: .Center)
        }
        else{
            let param = ["user_id" : _user.id,"question":txtQuestion.text ?? ""]
            
            self.showCentralSpinner()
            KPWebCall.call.askQuestion(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                     if (weakself.pressOK != nil) {
                        weakself.pressOK!()
                    }
                }
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        
        if (self.pressCancel != nil) {
            self.pressCancel!()
        }
    }
    
}


