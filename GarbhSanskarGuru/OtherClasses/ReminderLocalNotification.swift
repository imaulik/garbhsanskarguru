//
//  ReminderLocalNotification.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 13/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import UserNotifications

enum ActivityFinished {
    
    case zero
    case xxx
    case all
}

func createReminderNotificatinsForNextSevenDays() {
    
    removeAllReminderNotificatins()
    
    //abhi: uncomment below code for test instant notification
    /*
    addReminderNotification(forDate: Date(timeIntervalSinceNow: 10), forFinishType: .all)
    return
    */
    
    for index in 0...6 {
        
        let dateMorning: Date = getDateForNotification(daysToAdd: index, isForMorningNoti: true)
        let dateEvening: Date = getDateForNotification(daysToAdd: index, isForMorningNoti: false)
        
        if(index == 0) {
            
            var activity : ActivityFinished
            
            if(_nTodaysCompletedActivities == 0){
                activity = .zero
            }
            else if(_nTodaysCompletedActivities == 10){
                activity = .all
            }
            else {
                activity = .xxx
            }
            if(dateMorning.timeIntervalSinceNow.sign == .plus) {
                
                addReminderNotification(forDate: dateMorning, forFinishType: activity)
            }
            if(dateEvening.timeIntervalSinceNow.sign == .plus) {
                
                addReminderNotification(forDate: dateEvening, forFinishType: activity)
            }
        }
        else {
            
            addReminderNotification(forDate: dateMorning, forFinishType: .zero)
            addReminderNotification(forDate: dateEvening, forFinishType: .zero)
        }
    }
    print("---- ---- Pending Notification ---- ---- ")
    UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
        
        for request in requests {
            print("\(request.identifier) -> \(request.content.body)")
        }
    }
    print("---- ---- ---- ---- ---- ---- ---- ---- ")
}

func updateReminderNotificatinsForToday() {

    /*
     Note:
     No need to delete todays old notification, because we use same old identifier so it will automatically new notification replace with old notification
     */
    
    let dateMorning: Date = getDateForNotification(daysToAdd: 0, isForMorningNoti: true)
    let dateEvening: Date = getDateForNotification(daysToAdd: 0, isForMorningNoti: false)
    
    var activity : ActivityFinished
    
    if(_nTodaysCompletedActivities == 0){
        activity = .zero
    }
    else if(_nTodaysCompletedActivities == 10){
        activity = .all
    }
    else {
        activity = .xxx
    }
    
    if(dateMorning.timeIntervalSinceNow.sign == .plus) {
        
        addReminderNotification(forDate: dateMorning, forFinishType: activity)
    }
    if(dateEvening.timeIntervalSinceNow.sign == .plus) {
        
        addReminderNotification(forDate: dateEvening, forFinishType: activity)
    }
    
    print("---- ---- Updated Notification ---- ---- ")
    UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
        
        for request in requests {
            print("\(request.identifier) -> \(request.content.body)")
        }
    }
    print("---- ---- ---- ---- ---- ---- ---- ---- ")
}

func addReminderNotification(forDate: Date, forFinishType: ActivityFinished) {
    
    let notificationContent = UNMutableNotificationContent()

    let formatter = DateFormatter()
    formatter.locale = NSLocale.current
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let identifer = formatter.string(from: forDate)

    // Configure Notification Content
    notificationContent.title = _appDelegator.languageBundle?.localizedString(forKey: "Baby Calling", value: "", table: nil) ?? "Baby Calling"
    notificationContent.body = _appDelegator.languageBundle?.localizedString(forKey: getNotificationBody(forDate: forDate, forFinishType: forFinishType), value: "", table: nil) ?? ""
    notificationContent.userInfo = [STR_Notification_Type:STR_Local_Reminder, STR_HTML_Reminder_Body:getNotificationBodyInHtmlContentForString(strBody: notificationContent.body)]
    
    let attachement = try! UNNotificationAttachment(identifier: "image", url: Bundle.main.url(forResource: "notification_baby_image", withExtension: "png")!, options: nil)
    notificationContent.attachments = [ attachement ]
    
    notificationContent.sound = UNNotificationSound.default
    notificationContent.badge = 1
    
    // Create Notification Trigger
    let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: forDate)
    let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
    
    // Create Notification Request
    let notificationRequest = UNNotificationRequest(identifier: "Reminder \(identifer)", content: notificationContent, trigger: trigger)
    
    // Add Request to User Notification Center
    UNUserNotificationCenter.current().add(notificationRequest) { (error) in
        
        if let error = error {
            print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
        }
    }
}

func removeAllReminderNotificatins() {

    UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    
    prepareForRegisterReminder()
}

func getNotificationBody(forDate: Date, forFinishType: ActivityFinished) -> String {

    let weekday = Calendar.current.component(.weekday, from: forDate)

    var strBody: String = ""
    
    switch forFinishType {
    case .zero:
        strBody = arrZeroByTenTitle[weekday-1]
        break
    case .xxx:
        strBody = arrXxxByTenTitle[weekday-1]
        break
    case .all:
        strBody = arrTenByTenTitle[weekday-1]
        break
    }

    return strBody
}

func getNotificationBodyInHtmlContentForString(strBody: String) -> String {
    
    let strHtmlBody: String = "<html><body style = \"font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 300; line-height: 1.5; padding: 10px;\"><div>\(strBody)</div></body></html>"
    
    return strHtmlBody
}

func getDateForNotification(daysToAdd: Int, isForMorningNoti: Bool) -> Date {
 
    var dateComponent = DateComponents()
    dateComponent.day = daysToAdd
    var futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())
    
    let formatter = DateFormatter()
    formatter.locale = NSLocale.current
    formatter.dateFormat = "yyyy-MM-dd"
    
    var myString = formatter.string(from: futureDate!)
    myString = "\(myString) 00:00:00"
    
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    futureDate = formatter.date(from: myString)
    
    if(isForMorningNoti){
        
        //morning - 10:15 AM
        dateComponent.day = 0
        dateComponent.hour = 10
        dateComponent.minute = 15
        dateComponent.second = 0
    }
    else {
        
        //evening - 09:15 PM
        dateComponent.day = 0
        dateComponent.hour = 21
        dateComponent.minute = 15
        dateComponent.second = 0
    }
    futureDate = Calendar.current.date(byAdding: dateComponent, to: futureDate!)
    
    return futureDate!
}

// All methods for Reminder screen from Menu.

func prepareForRegisterReminder(){
    
    prepareGetAllDataFromDatabase()
}

func prepareGetAllDataFromDatabase(){
      
    // Function for get all data from database and register from begining.
    
    if ReminderDataModel.shared.loadReminder() != nil{
        var arrayReminder = [ReminderDataModel]()
        arrayReminder = ReminderDataModel.shared.loadReminder()!
        
        for reminder in arrayReminder{
            let arrayWeeks = reminder.weekDays.components(separatedBy: ",")
            var weekInt = [Int]()
            for value in arrayWeeks{
                weekInt.append(Int(value)!)
            }
            registerNotification(objReminder: reminder, weekDay: weekInt)
        }
    }
}

func registerNotification(objReminder:ReminderDataModel, weekDay:[Int]){
    // WeekDay (Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7)
    let content = UNMutableNotificationContent()
    content.title = NSString.localizedUserNotificationString(forKey: _appName, arguments: nil)
    content.body = NSString.localizedUserNotificationString(forKey: objReminder.message, arguments: nil)
    content.categoryIdentifier = "TIMER_EXPIRED"
    content.sound = UNNotificationSound.default
    content.badge = 1
    
    let reminderDate = Date(milliseconds: Int64(objReminder.reminder_date)!)
    let calendar = Calendar.current
    let hours = calendar.component(.hour, from: reminderDate)
    let minute = calendar.component(.minute, from: reminderDate)
    
    for value in weekDay {
        
        var dateInfo = DateComponents()
        dateInfo.hour = hours
        dateInfo.minute = minute
        dateInfo.weekday = value
        dateInfo.timeZone = .current
        
        let uniqIdentifier = UUID().uuidString
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: true)
        let request = UNNotificationRequest(identifier: uniqIdentifier, content: content, trigger: trigger)
        // Update Reminder for identifiers.
        kprint(items: "Reminder Identifier : \(uniqIdentifier)")
        updateReminder(reminder:objReminder, weekDay: value, identifier: uniqIdentifier)
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                kprint(items: ("Uh oh! We had an error: \(error)"))
            }else{
                kprint(items: ("Uh add notification"))
            }
        }
    }
    
}

func updateReminder(reminder:ReminderDataModel, weekDay:Int, identifier:String){
    setIdentifierFromDay(objReminder:reminder, wekDay: weekDay, idnty: identifier)
    let weekDays = reminder.weekDays
    reminder.weekDays = weekDays
    if ReminderDataModel.shared.updateReminder(reminder: reminder){
        kprint(items: "Update Reminder success")
    }else{
        kprint(items: "Update Reminder fail")
    }
}

func setIdentifierFromDay(objReminder:ReminderDataModel ,wekDay:Int, idnty:String){
    if wekDay == 1{
        objReminder.indetifier_day_1 = idnty
    }else if wekDay == 2{
        objReminder.indetifier_day_2 = idnty
    }else if wekDay == 3{
        objReminder.indetifier_day_3 = idnty
    }else if wekDay == 4{
        objReminder.indetifier_day_4 = idnty
    }else if wekDay == 5{
        objReminder.indetifier_day_5 = idnty
    }else if wekDay == 6{
        objReminder.indetifier_day_6 = idnty
    }else{
        objReminder.indetifier_day_7 = idnty
    }
}
