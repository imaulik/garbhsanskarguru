//
//  GarbhVC2.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 09/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import WebKit

class GarbhVC2: ParentViewController {
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var webVideo: WKWebView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var btnVideo: UIView!
    @IBOutlet weak var imgVideo: UIImageView!

    var data: ModelGarbhSamvad!
     var timer: Timer?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        prepereForTimer()
        prepareUI()
        getGarbhSamvadSuggestion()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    func prepereForTimer(){
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(prepereForAnimation), userInfo: nil, repeats: true)
    }
    
    @objc func prepereForAnimation(){
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.imgVideo.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 2.0, animations: {() -> Void in
                self.imgVideo.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        })
        
    }

}

extension GarbhVC2 {
    
    func prepareUI() {
        
        viewVideo.isHidden = true
        btnLeft.isHidden = true
        //abhi added
        btnRight.isHidden = true
        //-----------

        baseView.isHidden = true
        setLayout()
    }
    
    func setLayout(){
        layout.scrollDirection = .horizontal
        collectionView.isPagingEnabled = true
        layout.itemSize = CGSize(width: _screenSize.width, height: _screenSize.height - _navigationHeight.widthRatio - 50.widthRatio)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    
    func filterCollData() {
        let garbhDetail: [GarbhSamvadDetail]
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        switch currLanguage {
        case .hindi:
            garbhDetail = self.data.arrGarbhSamvad.filter{$0.language == .hindi}
        case .english:
            garbhDetail = self.data.arrGarbhSamvad.filter{$0.language == .english}
        case .gujarati:
            garbhDetail = self.data.arrGarbhSamvad.filter{$0.language == .gujarati}
        }
        self.data.arrGarbhSamvad.removeAll()
        self.data.arrGarbhSamvad = garbhDetail
        
        //abhi added
        if(self.data.arrGarbhSamvad.count > 1){
            btnRight.isHidden = false
        }
        //-----------
        
        self.collectionView.reloadData()
    }
}

extension GarbhVC2 {
    
    @IBAction func btnLeftTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath - 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnRightTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath + 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnVideo(_ sender: UIButton){
        self.viewVideo.isHidden = false
        let indexPath = self.collectionView.currentPage
        let strVideoUrl = self.data.arrGarbhSamvad[indexPath].videoUrl
        let youTubeID: String = getYoutubeId(StrLink: strVideoUrl) ?? "I1GwERXrlcU"
//        let url = URL(string: "https://www.youtube.com/embed/\(youTubeID)")
//        self.webVideo.load(URLRequest(url: url!))
        //         New code for play embedded video in browser. commented above code.
        self.webVideo.loadHTMLString("<iframe height=\"\(self.webVideo.frame.size.height)\" src=\"https://www.youtube.com/embed/\(youTubeID)\" style=\"width: 100%; height: 100%; position: absolute; top: 0px; left: 0px;\" width=\"\(self.webVideo.frame.size.width)\"></iframe>", baseURL: URL(string : "https://www.youtube.com"))
    }
    
    @IBAction func btnCloseVideo(_ sender: UIButton){
        self.viewVideo.isHidden = true
    }
}

extension GarbhVC2: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.data == nil ? 0 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.arrGarbhSamvad.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GarbhSamvadCollCell
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GarbhSamvadCollCell
        let imgUrl = URL(string: data.imgStr + data.arrGarbhSamvad[indexPath.row].image)
        cell.imgView.kf.setImage(with: imgUrl, placeholder: _placeImage)
        
        if indexPath.row == 0
        {
            let strVideoUrl = self.data.arrGarbhSamvad[indexPath.row].videoUrl
            if strVideoUrl != ""
            {
                btnVideo.isHidden = false
                imgVideo.isHidden = false
            }
            else
            {
                imgVideo.isHidden = true
                btnVideo.isHidden = true
            }
        }
        
        return cell
    }
}

extension GarbhVC2 {
    
    func getYoutubeId(StrLink: String) -> String?
    {
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            
            let match = regex.firstMatch(in: StrLink, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, StrLink.lengthOfBytes(using: String.Encoding.utf8)))
            
            let range = match?.range(at: 0)
            let youTubeID = (StrLink as NSString).substring(with: range!)
            return youTubeID
        } catch let err {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: err.localizedDescription)
        }
        return nil
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView{
            let currPage = scrollView.currentPage
            /*
            if currPage == self.data.arrGarbhSamvad.count - 1{
                self.btnRight.isHidden = true
            }else if currPage == 0{
                self.btnLeft.isHidden = true
            }else {
                btnRight.isHidden = false
                btnLeft.isHidden = false
            }
            */
            
            let strVideoUrl = self.data.arrGarbhSamvad[currPage].videoUrl
            if strVideoUrl != ""
            {
                btnVideo.isHidden = false
                imgVideo.isHidden = false
            }
            else
            {
                imgVideo.isHidden = true
                btnVideo.isHidden = false
            }
            
            //Abhi added
            if(currPage == 0){
                btnLeft.isHidden = true
                if(self.data.arrGarbhSamvad.count > 1){
                    btnRight.isHidden = false
                }
            }
            else if(currPage == self.data.arrGarbhSamvad.count - 1) {
                btnRight.isHidden = true
                if(self.data.arrGarbhSamvad.count > 1){
                    btnLeft.isHidden = false
                }
            }
            else {
                btnRight.isHidden = false
                btnLeft.isHidden = false
            }
            //-----------
        }
    }
}

extension GarbhVC2 {
    
    func getGarbhSamvadSuggestion() {
        let param: [String: Any] = ["day": _user.currDay, "user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getGarbhSamvadSuggestion(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            weakself.data = ModelGarbhSamvad()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    weakself.data = ModelGarbhSamvad(dict: dict)
                    weakself.baseView.isHidden = weakself.data.arrGarbhSamvad.count == 0
                    weakself.filterCollData()
            } else {
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
}

extension UIImageView {

    func zoomIn(duration: TimeInterval = 0.2) {
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = .identity
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    func zoomOut(duration : TimeInterval = 0.2) {
        self.transform = .identity
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        }) { (animationCompleted: Bool) -> Void in
        }
    }

}
