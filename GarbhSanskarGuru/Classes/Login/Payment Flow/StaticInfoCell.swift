//
//  StaticInfoCell.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 03/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class StaticInfoCell: UICollectionViewCell {
    @IBOutlet weak var imgStatic: UIImageView!
    @IBOutlet weak var btnStaticInfo: UIButton!
    @IBOutlet weak var btnTapped: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgStatic.clipsToBounds = true
        imgStatic.layer.cornerRadius = 15
        imgStatic.layer.maskedCorners =   [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }

}
