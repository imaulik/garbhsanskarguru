//
//  PaymentStatusView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 08/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class PaymentStatusView: ConstrainedView {
    
    @IBOutlet weak var baseView: UIView!
    var actionBlock: (KAction)->Void = {_ in}

    class func instantiateView(with view: UIView) -> PaymentStatusView {
        let objView = UINib(nibName: "PaymentStatusView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PaymentStatusView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if !touch.view!.isEqual(self.baseView) {
                self.removeFromSuperview()
            }
        }
    }
}

extension PaymentStatusView {
    
    @IBAction func btnPaytmTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func btnPayPalTapped(_ sender: UIButton) {
        self.actionBlock(.other)
    }
}

