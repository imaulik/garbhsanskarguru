//
//  EntryVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class EntryVC: ParentViewController {
    
    @IBOutlet weak var imgBannerView: UIImageView!
    @IBOutlet weak var giftView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       prepareUI()
    }
}

extension EntryVC {
    
    func prepareUI() {
        
        //abhi comment below line because put this in didFinidhLaunching
        //_appDelegator.registerPushNotification()
        
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        self.giftView.isHidden = !isPaymentAllowed
        if isPaymentAllowed {
            self.getBannerImage()
        }
    }
    
    func setImage(imgUrl: URL?) {
        imgBannerView.kf.setImage(with: imgUrl)
    }
}

extension EntryVC {

    @IBAction func btnLetsStart(_ sender: UIButton) {
        performSegue(withIdentifier: "loginSegue", sender: nil)
    }
    
    @IBAction func btnGiftApp(_ sender: UIButton) {
        performSegue(withIdentifier: "giftSegue", sender: nil)
    }
}

extension EntryVC {
    
    func getBannerImage() {
        self.showCentralSpinner()
        KPWebCall.call.getBannerImage { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let dictData = dict["data"] as? NSDictionary {
                    guard let imgUrl = dictData["banner_image_url"] as? String else {
                       return weakself.showError(data: dictData, view: weakself.view)
                    }
                    weakself.setImage(imgUrl: URL(string: imgUrl))
                } else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }
        }
    }
    
}
