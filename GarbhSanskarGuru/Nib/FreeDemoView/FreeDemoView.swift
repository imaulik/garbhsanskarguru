//
//  SignOutView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 06/02/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class FreeDemoView: ConstrainedView {

    var actionBlock: (KAction)->Void = {_ in}
    @IBOutlet weak var baseView: UIView!

    class func instantiateView(with view: UIView) -> FreeDemoView {
        let objView = UINib(nibName: "FreeDemoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FreeDemoView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
}

extension FreeDemoView {
    
    @IBAction func btnLaterTapped(_ sender: UIButton) {
          self.actionBlock(.done)
    }
}
