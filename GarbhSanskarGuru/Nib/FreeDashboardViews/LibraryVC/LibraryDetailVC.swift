//
//  LibraryDetailVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 07/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit
import SafariServices

class LibraryDetailVC: ParentViewController {
    
    @IBOutlet weak var cvDetail: UICollectionView!
    
    var arrSelectedTips:TipsCategoryList?
    let strLanguage = _appDelegator.getLanguage()
    var arrDetail:[TipsList]?
    var imageUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvDetail.register(UINib(nibName: "LIbDetailCell", bundle:nil), forCellWithReuseIdentifier: "LIbDetailCell")
        getLibDetail()
        if self.strLanguage == kEnglish {
            self.title = arrSelectedTips?.eng_name
        }else if self.strLanguage == kGujrati {
             self.title = arrSelectedTips?.guj_name
        }else if self.strLanguage == KHindi {
             self.title = arrSelectedTips?.hindi_name
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        hideNoDataFound()
    }
    
    func getLibDetail(){
        let param = ["user_id" : _user.id,"category_id" : arrSelectedTips?.id ?? ""]
 
        self.showCentralSpinner()
        KPWebCall.call.getLibraryData(param: param) {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                
                let result = TipsDetailModel(dictionary: dict)
                guard let list = result.data else {
                    return
                }
                
                var searchText = ""
                if weakself.strLanguage == kEnglish {
                    searchText = kLongEnglish
                }else if weakself.strLanguage == kGujrati {
                    searchText = kLongGujrati
                }else if weakself.strLanguage == KHindi {
                    searchText = KLongHindi
                }
                weakself.imageUrl = result.image_url
                weakself.arrDetail = list.filter { catalogName in
                    return catalogName.language ==  searchText
                }
                if weakself.arrDetail?.count == 0 {
                    weakself.showNoDataFound(text: "Library will be available soon")
                    return
                }else{
                    weakself.hideNoDataFound()
                }
                weakself.cvDetail.reloadData()
            }
        }
        
//        callWS_CategoryDetail(parms: parm) { result in
//
//            guard let list = result?.data else {
//                return
//            }
//
//            var searchText = ""
//            if self.strLanguage == kLongEnglish {
//                searchText = kLongEnglish
//            }else if self.strLanguage == kLongGujrati {
//                searchText = kLongGujrati
//            }else if self.strLanguage == KLongHindi {
//                searchText = KLongHindi
//            }
//            self.imageUrl = result?.image_url ?? ""
//            self.arrDetail = list.filter { catalogName in
//                return catalogName.language ==  searchText
//            }
//            if self.arrDetail?.count == 0 {
//                self.showNoDataFound(text: "Library will be available soon")
//                return
//            }else{
//                self.hideNoDataFound()
//            }
//            self.cvDetail.reloadData()
//        }
    }
}

extension LibraryDetailVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width - 10 , height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDetail?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"LIbDetailCell", for: indexPath) as! LIbDetailCell
        let obj = arrDetail?[indexPath.row]
        cell.lblTitle.text = obj?.pdf_name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let safariVC = SFSafariViewController(url: URL(string:imageUrl + (arrDetail?[indexPath.row].pdf_name ?? ""))!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }
}

extension LibraryDetailVC: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
