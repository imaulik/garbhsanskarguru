//
//  ReferalCodeVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct ReferralData {
    
    var referralCode = ""
    var giftAppCode = ""
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["user_id"] = _user.id
        dict["ref_code"] = referralCode
        dict["gift_code"] = giftAppCode
        return dict
    }
}

class ReferalCodeVC: ParentViewController {

    var referralData = ReferralData()
    var arrReferal: [ReferenceCellType] = [.txtcell, .separatorCell, .txtcell, .separatorCell, .infoCell]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension ReferalCodeVC {
    
    func prepareUI() {
        self.lblTitle?.text = _user.name
        preparePriceOnBoarding()
        setKeyboardNotifications()
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
    }
    
    func getEntryCell(_ row: Int) -> ReferralCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? ReferralCell
        return cell
    }
    
    func prepareSkipPaymentBoarding() {
        let onBoarding = SkipPaymentView.instantiateView(with: self.view)
        onBoarding.bounceView(animateView: onBoarding.baseView)
        onBoarding.actionBlock = { (action) in
            if action == .done {
                onBoarding.removeFromSuperview()
                self.view.endEditing(true)
                self.skipGiftCode { (done) in
                    if done {
                        if _user.paymentStatus == .pending {
                            self.performSegue(withIdentifier: "paymentSegue", sender: nil)
                        }else {
                            self.performSegue(withIdentifier: "directSegue", sender: nil)
                        }
                    }
                }
            }else if action == .cancel {
                onBoarding.removeFromSuperview()
            }
        }
    }
    
    func preparePriceOnBoarding() {
        let onBoardingView = ExploreView.instantiateView(with: self.view, with: "\(_user.amount)")
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            if action == .done {
                if _user.referralStatus == .pending {
                    if _user.pregnancyStatus == .pregnant {
                        onBoardingView.removeFromSuperview()
                        DispatchQueue.main.async {
                            self.prepareOnBoarding()
                        }
                    }else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            onBoardingView.removeFromSuperview()
                        })
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        onBoardingView.removeFromSuperview()
                    })
                }
            }else if action == .other {
                self.openFreeAppURL()
            }
        }
    }
    
    func openFreeAppURL() {
        if let url = URL(string:"https://itunes.apple.com/us/app/majestic-garbh-sanskar/id1448235489?ls=1&mt=8"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.actionBlock = { (action) in
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            self.getPregnancyDay(currentDay: currDay, block: { (done) in
                if done {
                    onBoardingView.removeFromSuperview()
                }
            })
        }
    }
}

extension ReferalCodeVC {
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if referralData.referralCode.isEmpty && referralData.giftAppCode.isEmpty {
            _ = ValidationToast.showStatusMessage(message: "Please enter any one code")
        }else {
            self.view.endEditing(true)
            chechGiftCode { (done) in
                if done {
                    if _user.paymentStatus == .pending {
                        self.performSegue(withIdentifier: "paymentSegue", sender: nil)
                    } else {
                        self.updatePaymentStatus(block: { (complete) in
                            if complete {
                                self.performSegue(withIdentifier: "directSegue", sender: nil)
                            }
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func btnSkipTapped(_ sender: UIButton) {
        self.prepareSkipPaymentBoarding()
    }
}

extension ReferalCodeVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReferal.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrReferal[indexPath.row].cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ReferralCell
        let referalInfo = arrReferal[indexPath.row]
        cell = tableView.dequeueReusableCell(withIdentifier: referalInfo.rawValue, for: indexPath) as! ReferralCell
        cell.parent = self
        cell.setUpCell(field: referalInfo, tag: indexPath.row)
        return cell
    }
}

extension ReferalCodeVC {
    
    func chechGiftCode(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.checkGiftCode(param: referralData.paramDict()) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["message"] as? NSDictionary{
                    DispatchQueue.main.async {
                        _user.setReferral(dict: data)
                        _appDelegator.saveContext()
                        block(true)
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func skipGiftCode(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.skipGiftCode { (json, statusCode) in
            self.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary{
                    DispatchQueue.main.async {
                        _user.setReferral(dict: data)
                        _appDelegator.saveContext()
                        block(true)
                    }
                } else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            } else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func updatePaymentStatus(block: @escaping (Bool) -> ()) {
        
        let randomNumber = arc4random_uniform(55555)
        let param: [String: Any] = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode": "COUPAN_100", "transaction_id": randomNumber, "amount": _user.amount]
        
        self.showCentralSpinner()
        KPWebCall.call.updatePaymentStatus(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    _user.setPaymentStatus(dict: data)
                    _appDelegator.saveContext()
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment \(data.getStringValue(key: "payment_status"))")
                    block(true)
                }else if let msg = dict["message"] as? NSDictionary{
                    weakself.showError(data: msg, view: weakself.view)
                    block(false)
                }else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}
