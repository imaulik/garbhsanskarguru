//
//  AudioVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 08/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit
import AVKit

class AudioVC: ParentViewController {
    
    @IBOutlet weak var tblAudio: UITableView!
    var arrAudiList : [AudioList]?
    var arrSelectedTips:TipsCategoryList?
    var strLanguage = _appDelegator.getLanguage()
    var imageURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Play Now"
        self.tblAudio.register(UINib(nibName: "AudioCell", bundle: nil), forCellReuseIdentifier: "AudioCell")
        self.tblAudio.tableFooterView = UIView()
        getAudio()
    }
    
    func getAudio() {
        let param = ["user_id":_user.id,"category_id": arrSelectedTips?.id ?? ""]
        
        self.showCentralSpinner()
        KPWebCall.call.getAudioList(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = AudioViewModel(dictionary: dict)
                
                var searchText = ""
                if weakself.strLanguage == kEnglish {
                    searchText = kLongEnglish
                }else if weakself.strLanguage == kGujrati {
                    searchText = kLongGujrati
                }else if weakself.strLanguage == KHindi {
                    searchText = KLongHindi
                }
                
                weakself.arrAudiList = res.data?.filter { catalogName in
                    return catalogName.language ==  searchText
                }
                
                weakself.imageURL = res.image_url
                weakself.tblAudio.reloadData()
            }
        }
        
        
//        callWS_Audiolist(parms: parm) { videoList in
//            guard let res = videoList else {
//                return
//            }
//
//            var searchText = ""
//            if self.strLanguage == kLongEnglish {
//                searchText = kLongEnglish
//            }else if self.strLanguage == kLongGujrati {
//                searchText = kLongGujrati
//            }else if self.strLanguage == KLongHindi {
//                searchText = KLongHindi
//            }
//
//            self.arrAudiList = res.data?.filter { catalogName in
//                return catalogName.language ==  searchText
//            }
//
//            self.imageURL = res.image_url
//            self.tblAudio.reloadData()
//        }
    }
 }


extension AudioVC {
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAudiList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioCell", for: indexPath) as! AudioCell
        let obj = arrAudiList?[indexPath.row]
        cell.lblAudio.text = obj?.title
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var ytURL:String?
              let obj = arrAudiList? [indexPath.item]
              ytURL = obj?.image ?? ""
              
        videoPlay(strVideo: ytURL ?? "")
              
    }
}
