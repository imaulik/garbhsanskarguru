//
//  DashboardListCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 14/04/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class DashboardListCell: UITableViewCell {
    @IBOutlet weak var imgDashList: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblBottomDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
