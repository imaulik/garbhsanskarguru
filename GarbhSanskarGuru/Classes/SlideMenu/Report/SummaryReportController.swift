//
//  SummaryReportController.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 28/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class SummaryReportController: ParentViewController {
    
    @IBOutlet var labelName: JPWidthLabel!
    @IBOutlet var viewBackground: UIView!
    
    @IBOutlet var progress_IQ: MBCircularProgressBarView!
    @IBOutlet var progress_PQ: MBCircularProgressBarView!
    @IBOutlet var progress_EQ: MBCircularProgressBarView!
    @IBOutlet var progress_SQ: MBCircularProgressBarView!
    
    @IBOutlet var view_undoneActivities: UIView!
    @IBOutlet var viewDoneActivities: UIView!
    
    @IBOutlet var labelDays: JPWidthLabel!
    @IBOutlet var labelUndone: JPWidthLabel!
    @IBOutlet var labelDone: JPWidthLabel!
    
    var reportModel:ReportModel? = nil
    var fromDayValue = ""
    var toDayValue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareForUI()
        prepareDaysForInitial()
        prepareReportApiCall(fromDay: _user.currDay > 7 ? Int(_user.currDay - 7) : Int(_user!.currDay) , toDay: Int(_user!.currDay))
        prepareDropDownUI(isExpand: false)
    }
    
    @IBAction func action_calander(_ sender: UIButton) {
        prepareChangeActivityDay { (from, to, action) in
            if action == .done{
                self.labelDays.text = from! + " To " + to!
                self.prepareReportApiCall(fromDay: Int(from!)!, toDay: Int(to!)!)
            }
        }
    }
    
    @IBAction func action_dropdown(_ sender: BaseButton) {
        sender.isSelected = !sender.isSelected
        prepareDropDownUI(isExpand: sender.isSelected)
    }
}

extension SummaryReportController {
    
    func prepareDaysForInitial(){
        if _user.currDay > 7{
            fromDayValue = "\(_user.currDay - 7)"
        }else{
            fromDayValue = "\(_user.currDay)"
        }
        toDayValue = "\(_user.currDay)"
        labelDays.text = fromDayValue + " To " + toDayValue
    }
    
    func prepareForUI(){
        viewBackground.applyGradientEffects([GarbhColor.gradientLeftBlue, GarbhColor.gradientRightBlue], gradientPoint: .leftRight)
    }
    
    func prepareForUIUpdate(){
        labelName.text = "Hey \(_user.name) "
        labelUndone.text = " \(String(describing: reportModel!.undoneActivityCount!)) "
        labelDone.text = " \(String(describing: reportModel!.doneActivityCount!)) "
    }
    
    func prepareChangeActivityDay(completionBlock:@escaping (String?,String?,kReminderAction) -> Void){
        let onBoardingView = ChangeReportDay.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (fromday, today, action) in
            onBoardingView.removeFromSuperview()
            completionBlock(fromday, today, action)
        }
    }
    
    func prepareDropDownUI(isExpand:Bool){
        view_undoneActivities.isHidden = !isExpand
        viewDoneActivities.isHidden = !isExpand
    }
    
    func prepareProgressUIUpdate(){
        prepareForUIUpdate()
        progress_IQ.value = CGFloat(self.reportModel!.report_IQ_double)
        progress_PQ.value = CGFloat(self.reportModel!.report_PQ_double)
        progress_EQ.value = CGFloat(self.reportModel!.report_EQ_double)
        progress_SQ.value = CGFloat(self.reportModel!.report_SQ_double)
    }
    
}

extension SummaryReportController {
        
    func prepareReportApiCall(fromDay:Int, toDay:Int) {
                
        let param: [String: Any] = ["user_id": _user.id, "from_day": fromDay, "to_day": toDay]
        self.showCentralSpinner()
        KPWebCall.call.userMainReport(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
                        
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    weakself.reportModel = ReportModel(fromDictionary: dict as! NSDictionary)
                    weakself.reportModel?.getReportData(isFloat: true)
                    weakself.prepareProgressUIUpdate()
                } else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }
            else {
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
}
