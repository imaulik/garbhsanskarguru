//
//  ReminderViewController.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 24/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ReminderTableviewCell : UITableViewCell{
    
    @IBOutlet var labelTime: JPWidthLabel!
    @IBOutlet var labelAMPM: JPWidthLabel!
    @IBOutlet var labelDays: JPWidthLabel!
}

class ReminderViewController: UIViewController {

    @IBOutlet var tableview: UITableView!
    @IBOutlet var buttonReminder: UIButton!
    @IBOutlet weak var viewAccessory: UIView!
    
    var arrayReminder = [ReminderDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        getDataFromDatabase()
    }
}

extension ReminderViewController{
    
    func prepareUI(){
        buttonReminder.layer.cornerRadius = buttonReminder.frame.size.height / 2
        buttonReminder.clipsToBounds = true
        viewAccessory.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: 44)
    }
    
    func prepareforReminderUI(isUpdate:Bool,selecteReminder:ReminderDataModel? = nil ,completionBlock:@escaping (kReminderAction) -> Void){
        let onBoardingView = AddReminderView.instantiateView(with: self.view)
        onBoardingView.txtviewMessage.inputAccessoryView = viewAccessory
        if isUpdate{
            onBoardingView.prepareUI(forUpdate: true, reminder: selecteReminder)
        }else{
            onBoardingView.prepareUI(forUpdate: false)
        }
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            completionBlock(action)
        }
    }
    
}

extension ReminderViewController{
    
    @IBAction func action_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_addReminder(_ sender: UIButton) {
        prepareforReminderUI(isUpdate: false) { (action) in
            self.getDataFromDatabase()
        }
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton){
        self.view.endEditing(true)
    }
}

extension ReminderViewController {
    
    func getDataFromDatabase() {
        if ReminderDataModel.shared.loadReminder() != nil{
            self.arrayReminder = ReminderDataModel.shared.loadReminder()!
            tableview.isHidden = !(self.arrayReminder.count > 0)
            self.tableview.reloadData()
        }else{
            tableview.isHidden = true
        }
    }
    
    func getWeekName(weekNumber:String) -> [String] {
        let aryWeekNumber = weekNumber.components(separatedBy: ",")
        var value = [String]()
        for numer in aryWeekNumber{
            if numer == "2"{ value.append("Mon") }
            else if numer == "3"{ value.append("Tues") }
            else if numer == "4"{ value.append("Wed") }
            else if numer == "5"{ value.append("Thu") }
            else if numer == "6"{ value.append("Fri") }
            else if numer == "7"{ value.append("Sat") }
            else{ value.append("Sun") }
        }
        return value
    }
    
    func updateLabelUIforDays(text:String) -> NSAttributedString{
        let daysName = "Mon Tues Wed Thu Fri Sat Sun"
        let attributedWithTextColor: NSAttributedString = daysName.attributedStringWithColor(getWeekName(weekNumber: text), color: GarbhColor.pink)
        return attributedWithTextColor
    }
    
}

extension ReminderViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayReminder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderTableviewCell") as! ReminderTableviewCell
        let reminder = self.arrayReminder[indexPath.row]
        let reminderDate = Date(milliseconds: Int64(reminder.reminder_date)!)
        
        cell.labelTime.text = reminderDate.stringFromDate(withFormat: timeFormat)
//        cell.labelAMPM.text = reminderDate.stringFromDate(withFormat: timeFormatOnlyAMPM)
        cell.labelDays.attributedText = updateLabelUIforDays(text: reminder.weekDays)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reminder = self.arrayReminder[indexPath.row]
        
        prepareforReminderUI(isUpdate: true, selecteReminder: reminder) { (action) in
            kprint(items: action)
            self.getDataFromDatabase()
        }
    }
    
}
