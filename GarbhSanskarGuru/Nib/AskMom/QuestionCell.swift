//
//  QuestionCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 18/04/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {
    @IBOutlet weak var imgQueUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var totalAnswer: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var editLine: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var editStackView: UIStackView!
    @IBOutlet weak var totalAnsView: UIView!
    @IBOutlet weak var lblTotalAnswer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cardView.layer.cornerRadius = 5
        cardView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
