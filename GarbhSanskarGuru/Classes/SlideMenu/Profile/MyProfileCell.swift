//
//  MyProfileCell.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 16/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

enum EnumMyProfileCellType: String {
    
    case imgCell = "imageCell"
    case txtcell = "textCell"
    case dbltxtCell = "dbltxtCell"
    case radioCell = "radioCell"
    
    var cellHeight: CGFloat {
        switch self {
        case .imgCell:
            return 120.widthRatio
        case .txtcell:
            return 60.widthRatio
        case .dbltxtCell:
            return 60.widthRatio
        case .radioCell:
            return 80.widthRatio
        }
    }
}

class MyProfileCell: ConstrainedTableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var tfInput: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    weak var parentProfile: MyProfileVC!
    
    var proData: ProfileData {
        return parentProfile.data
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(with data: EnumMyProfileCellType, and tag: Int) {
        switch data {
        case .imgCell:
            if parentProfile.selectedImage == nil {
                imgView.kf.setImage(with: parentProfile.data.userImg, placeholder: _placeImageUser)
            } else {
                imgView.image = parentProfile.selectedImage
            }
            
        case .txtcell:
            tfInput.tag = tag
            tfInput.autocorrectionType = .no
            tfInput.autocapitalizationType = .none
            tfInput.keyboardType = .asciiCapable
            tfInput.returnKeyType = .next
            tfInput.inputAccessoryView = nil
            switch tag {
            case 1:
                tfInput.autocapitalizationType = .words
                tfInput.keyboardType = .asciiCapable
                tfInput.placeholder = "Full Name"
                tfInput.text = proData.fullName
                break
            case 2:
                tfInput.keyboardType = .emailAddress
                tfInput.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                tfInput.isUserInteractionEnabled = false
                tfInput.placeholder = "Email"
                tfInput.text = proData.email
                break
            case 3:
                tfInput.inputAccessoryView = parentProfile.viewAccessory
                tfInput.keyboardType = .numberPad
                tfInput.placeholder = "Mobile number"
                tfInput.text = proData.mobilenumber
                break
            case 4:
                tfInput.autocapitalizationType = .words
                tfInput.keyboardType = .asciiCapable
                tfInput.placeholder = "Country"
                tfInput.text = proData.country
                break
            default:
                break
            }
        case .dbltxtCell:
            tfState.tag = tag
            tfCity.tag = tag
            
            switch tag {
            case 5:
                tfState.autocapitalizationType = .words
                tfState.placeholder = "State"
                tfState.text = proData.state
               
                tfCity.autocapitalizationType = .words
                tfCity.placeholder = "City"
                tfCity.text = proData.city
                tfCity.returnKeyType = .done
                break
            default:
                break
            }
        case .radioCell:
            btnMale.isSelected = _user.userGender == .male
            btnFemale.isSelected = _user.userGender == .female
        }
    }
}

extension MyProfileCell {
    
    @IBAction func btnMaleTapped(_ sender: UIButton) {
        btnMale.isSelected = true
        btnFemale.isSelected = false
        _user.gender = "Male"
    }
    
    @IBAction func btnFemaleTapped(_ sender: UIButton) {
        btnMale.isSelected = false
        btnFemale.isSelected = true
        _user.gender = "Female"
    }
}

extension MyProfileCell: UITextFieldDelegate {
    
    @IBAction func tfEditingChange(textfield: UITextField) {
        if textfield == tfInput {
            if textfield.tag == 1 {
                parentProfile.data.fullName = textfield.text!.trimmedString()
            }else if textfield.tag == 2 {
                parentProfile.data.email = textfield.text!.trimmedString()
            }else if textfield.tag == 3 {
                parentProfile.data.mobilenumber = textfield.text!.trimmedString()
            }else if textfield.tag == 4 {
                parentProfile.data.country = textfield.text!.trimmedString()
            }
        }else if textfield == tfState {
            parentProfile.data.state = textfield.text!.trimmedString()
        }else {
            parentProfile.data.city = textfield.text!.trimmedString()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next{
            if textField == tfState {
                tfCity.becomeFirstResponder()
                return true
            }
            if let index = parentProfile.tableView.indexPath(for: self){
                let rowIndex = textField.tag == 1 ? index.row + 2 : index.row + 1
                if let cell = parentProfile.getEntryCell(rowIndex){
                    if textField.tag == 4{
                        cell.tfState.becomeFirstResponder()
                    }else{
                        cell.tfInput.becomeFirstResponder()
                    }
                }
            }else{
                textField.resignFirstResponder()
            }
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
