//
//  TodaySpecialVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class TodaySpecialVC: ParentViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var imgView: UIImageView!
    
    var data: ModelDearMom!

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension TodaySpecialVC {
    
    func prepareUI() {
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 5.0
        
        self.getDietActivityData { (done) in
            if done {
                self.imgView.kf.setImage(with: self.data.getMomImage(), placeholder: _placeImage)
            }
        }
    }
}

extension TodaySpecialVC {
    
    func getDietActivityData(block: @escaping(Bool) -> ()) {
        let param: [String: Any] = ["current_day": _user.currDay, "user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getDietActivityData(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let dictData = dict["data"] as? NSDictionary {
                    weakself.data = ModelDearMom(dict: dictData)
                    block(true)
                } else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }
        }
    }
}

extension TodaySpecialVC {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        scrollview.flashScrollIndicators()
    }
    
}

