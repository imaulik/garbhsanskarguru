//
//  MenuCells.swift
//  AussieFood
//
//  Created by Yudiz Solutions Pvt.Ltd. on 12/09/16.
//  Copyright © 2016 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Menu Profile Cell
class MenuProfileCell: ConstrainedTableViewCell {
    @IBOutlet var imgvProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


//MARK: - Menu Item Cell

class MenuItemCell: ConstrainedTableViewCell{
    
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func prepareToFillData(_ menu:MenuSpecific,index : Int) {
        self.lblTitle.text = menu.items[index].rawValue
        imgView.tintColor = #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1)
        
        switch menu.items[index] {
        case .About:
          imgView.image = UIImage(named: "ic_aboutus")?.withRenderingMode(.alwaysTemplate)
        case .Profile:
            imgView.image = UIImage(named: "ic_edit_profile")?.withRenderingMode(.alwaysTemplate)
        case .GarbhSanskarInfo:
            imgView.image = UIImage(named: "ic_garbh_info")?.withRenderingMode(.alwaysTemplate)
        case .Reports:
            imgView.image = UIImage(named: "ic_reports")?.withRenderingMode(.alwaysTemplate)
        case .AddReminder:
            imgView.image = UIImage(named: "ic_reminder")?.withRenderingMode(.alwaysTemplate)
         case .ChangeDay:
            imgView.image = UIImage(named: "ic_calendar")?.withRenderingMode(.alwaysTemplate)
         case .PauseApp:
            imgView.image = UIImage(named: "ic_calendar")?.withRenderingMode(.alwaysTemplate)
        case .ResumeApp:
            imgView.image = UIImage(named: "ic_reminder")?.withRenderingMode(.alwaysTemplate)
        case .Language:
            imgView.image = UIImage(named: "ic_change_language")?.withRenderingMode(.alwaysTemplate)
        case .StartPaidCourse:
            imgView.image = UIImage(named: "ic_contact_us")?.withRenderingMode(.alwaysTemplate)
        case .GiftApp:
            imgView.image = UIImage(named: "ic_upgrade_pro")?.withRenderingMode(.alwaysTemplate)
        case .BuyFullCourse:
            imgView.image = UIImage(named: "ic_buy_full_course")?.withRenderingMode(.alwaysTemplate)
        case .Contact:
            imgView.image = UIImage(named: "ic_contact_us")?.withRenderingMode(.alwaysTemplate)
        case .FAQ:
            imgView.image = UIImage(named: "ic_ExpertMom")?.withRenderingMode(.alwaysTemplate)
        }
    }
}


class MenuFooterCell: ConstrainedTableViewCell {
    
    @IBOutlet var btnLogout: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
