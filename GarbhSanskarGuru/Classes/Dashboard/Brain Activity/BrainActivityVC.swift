//
//  BrainActivityVC.swift
//  GarbhSanskarGuru
//
//  Created by Jahanvi Trivedi on 18/12/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ModelBrainActivity {
    var activitySeen: ActivitySeenStatus = .undone
    var imgStr = ""
    var type: DisplayBrainActivityType = .text
    var arrGarbhSamvad = [BrainActivityDetail]()
    
    init(dict: NSDictionary, with screenType: GarbhScreenType = .brainActivity) {
        imgStr = dict.getStringValue(key: "image_url")
        
        let scrnType = screenType == .yoga ? "yoda_data" : "activity_detail"
        if let jsonDict = dict["data"] as? NSDictionary, let arrData = jsonDict[scrnType] as? [NSDictionary]
        {
            type = DisplayBrainActivityType(rawValue: jsonDict.getStringValue(key: "type")) ?? .text
            activitySeen = ActivitySeenStatus(rawValue: jsonDict.getStringValue(key: "activity_seen")) ?? .undone
            for data in arrData
            {
                self.arrGarbhSamvad.append(BrainActivityDetail(dict: data))
            }
        }
    }
    
    init() { }
}

class BrainActivityDetail {
    
    let id: Int
    let yogId: String
    let text: String
    let answer: String
    let activityId: String
    let language: DownloadGarbhsatsangLanguage
    let displayType: DisplayBrainActivityType
    let type: String
    let day: String
    var categoryId : String!
    
    init(dict: NSDictionary) {
        id = dict.getIntValue(key: "id")
        yogId = dict.getStringValue(key: "yog_id")
        text = dict.getStringValue(key: "q_data")
        answer = dict.getStringValue(key: "ans_data")
        activityId = dict.getStringValue(key: "activity_id")
        categoryId = dict.getStringValue(key: "category_id")
        displayType = DisplayBrainActivityType(rawValue: dict.getStringValue(key: "type")) ?? .text
        language = DownloadGarbhsatsangLanguage(rawValue: dict.getStringValue(key: "language")) ?? .english
        type = dict.getStringValue(key: "type")
        day = dict.getStringValue(key: "day")
    }
}


class BrainActivityVC: ParentViewController {

    var screenType: GarbhScreenType = .brainActivity
    var data: ModelBrainActivity!
    var isActivityDone: Bool = false
    var selected_DashboardId:Int? = nil
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
//    @IBOutlet weak var webQuestionView: UIWebView!
    @IBOutlet weak var webQuestionView: WKWebView!
//    @IBOutlet weak var webAnswerView: UIWebView!
    @IBOutlet weak var webAnswerView: WKWebView!
    @IBOutlet weak var imgQuestionView: UILabel!
    @IBOutlet weak var imgAnswerView: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        prepareUI()
    }
}

extension BrainActivityVC {
    
    func filterCollData() {
        
        let storyDetail: [BrainActivityDetail]
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        switch currLanguage {
        case .hindi:
            storyDetail = self.data.arrGarbhSamvad.filter{$0.language == .hindi}
        case .english:
            storyDetail = self.data.arrGarbhSamvad.filter{$0.language == .english}
        case .gujarati:
            storyDetail = self.data.arrGarbhSamvad.filter{$0.language == .gujarati}
        }
        self.data.arrGarbhSamvad.removeAll()
        self.data.arrGarbhSamvad = storyDetail
        
        // Commnet
//        switch data.type {
//        case .text:
//            webQuestionView.loadHTMLString("<html><body><p>" + data.arrGarbhSamvad[0].text + "</p></body></html>", baseURL: nil)
//            webAnswerView.loadHTMLString("<html><body><p>" + data.arrGarbhSamvad[0].answer + "</p></body></html>", baseURL: nil)
//        case .image:
//        print("Called")
//        }
        
        if storyDetail[0].type == "Image" {
            let requestURL_question = URL(string:data.imgStr + storyDetail[0].text)
            let requestURL_ans = URL(string:data.imgStr + storyDetail[0].answer)
            webQuestionView.load(URLRequest.init(url: requestURL_question!))
            webQuestionView.contentMode = UIView.ContentMode.scaleAspectFit
            webAnswerView.load(URLRequest.init(url: requestURL_ans!))
            webAnswerView.contentMode = UIView.ContentMode.scaleAspectFit
        }else{
            var changeFont_webQuestionView = data.arrGarbhSamvad[0].text.replacingOccurrences(of: "font-size:16px", with: "font-size:40px")
            changeFont_webQuestionView = changeFont_webQuestionView.replacingOccurrences(of: "font-size:14px", with: "font-size:40px")
            var changeFont_webAnswerView = data.arrGarbhSamvad[0].answer.replacingOccurrences(of: "font-size:16px", with: "font-size:40px")
            changeFont_webAnswerView = changeFont_webAnswerView.replacingOccurrences(of: "font-size:14px", with: "font-size:40px")
//            webQuestionView.loadHTMLString("<html><body><p>" + data.arrGarbhSamvad[0].text + "</p></body></html>", baseURL: nil)
//            webAnswerView.loadHTMLString("<html><body><p>" + data.arrGarbhSamvad[0].answer + "</p></body></html>", baseURL: nil)
            webQuestionView.loadHTMLString("<html><body><p>" + changeFont_webQuestionView + "</p></body></html>", baseURL: nil)
            webAnswerView.loadHTMLString("<html><body><p>" + changeFont_webAnswerView + "</p></body></html>", baseURL: nil)
        }
    }
    
    func prepareUI() {
        
        imgAnswerView.isHidden = true
        webAnswerView.isHidden = true
        self.roundView.backgroundColor = self.isActivityDone ? GarbhColor.green : GarbhColor.pink
        
        self.getBrainActivityData { (done) in
            if done {
                
                print(self.data.activitySeen)
                print(self.data.arrGarbhSamvad.count)
               // self.imgView.kf.setImage(with: self.data.getMomImage(), placeholder: _placeImage)
                //self.roundView.backgroundColor = self.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
            }
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = TaskCompleteView.instantiateView(with: self.view, with: "Brain Activity")
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                let activity_id = self.data.arrGarbhSamvad[0].categoryId == "" ? "0" : self.data.arrGarbhSamvad[0].categoryId
                self.getActivityDone(with: "puzzle", activityId: activity_id!, dailyActivityId: "\(self.data.arrGarbhSamvad[0].id)") { (completed) in
                    if completed {
                        onBoardingView.removeFromSuperview()
                        NotificationCenter.default.post(name: Notification.Name(garbhNotificationUpdateActicityStatus), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    func openDisaplyInfoView() {
        let onBoardingView = DisplayInfoView.instantiateView(with: self.view)
        onBoardingView.setupUI()
        onBoardingView.setupValues(valueIndex: self.selected_DashboardId!)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
        }
    }
}

extension BrainActivityVC {
    
    func getBrainActivityData(block: @escaping(Bool) -> ()) {
        let param: [String: Any] = ["day": _user.currDay, "user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getBrainActivityData(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let dictData = dict["data"] as? NSDictionary {
                    print(dictData)
                    weakself.data = ModelBrainActivity(dict: dict, with: weakself.screenType)
                    weakself.filterCollData()
                   // block(true)
                } else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }
        }
    }
}

extension BrainActivityVC {
    
    @IBAction func btnTaskTapped(_ sender: UIButton) {        
        if(isActivityDone) {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kTaskCompleted)
        }
        else {
            self.prepareOnBoarding()
        }
    }
    
    @IBAction func btnQuestion(_ sender: UIButton) {
        webQuestionView.isHidden = false
        webAnswerView.isHidden = true
        imgAnswerView.isHidden = true
        imgQuestionView.isHidden = false
    }
    
    @IBAction func btnAnswer(_ sender: UIButton) {
        
        let onBoardingView = TaskCompleteView.instantiatePuzzleView(with: self.view, with: "Brain Activity")
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                onBoardingView.removeFromSuperview()
                self.webQuestionView.isHidden = true
                self.webAnswerView.isHidden = false
                self.imgAnswerView.isHidden = false
                self.imgQuestionView.isHidden = true
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
           self.openDisaplyInfoView()
       }
}
