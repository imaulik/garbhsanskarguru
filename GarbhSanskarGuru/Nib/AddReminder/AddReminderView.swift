//
//  AddReminderView.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 24/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class AddReminderView: UIView {

    var actionBlock: (kReminderAction)->Void = {_ in}
    
    var reminderDate = Date()
    var Minutes:Int = 0
    var Hours:Int = 0
    var Year:Int = 0
    
    var objReminder = ReminderDataModel()
    var aryPendingNotification:[UNNotificationRequest] = []
    var isForUpdate:Bool = false
    var registerIdentifiers:[String] = []
    
    @IBOutlet var datepicker: UIDatePicker!{
        didSet{
            datepicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        }
    }
    @IBOutlet var txtviewMessage: PlaceHolderTextView!{
        didSet{
            txtviewMessage.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
        }
    }
    
    @IBOutlet var viewMessage: UIView!{
        didSet{
            viewMessage.layer.borderColor = GarbhColor.pink.cgColor
            viewMessage.layer.borderWidth = 1.5
            viewMessage.layer.cornerRadius = 8
            viewMessage.clipsToBounds = true
        }
    }
    
    @IBOutlet var btnAdd: BaseButton!
    @IBOutlet var spaceDelete: UIView!
    @IBOutlet var btnDelete: BaseButton!
    @IBOutlet var btnDays: [UIButton]!
    @IBOutlet var animatedViews: UIView!
    
}

extension AddReminderView{
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> AddReminderView {
        let objView = UINib(nibName: "AddReminderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AddReminderView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }

    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews.transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.animatedViews.transform = .identity
            }, completion: { _ in
            })
        })
    }
    
}

extension AddReminderView{
    
    func prepareConfirmationPopup(message:String,completionBlock:@escaping (KAction) -> Void){
        let onBoardingView = ConfirmationPopup.instantiateView(with: self)
        onBoardingView.updateUI(keyName: message, isCancelable: true)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            completionBlock(action)
        }
    }
    
    func prepareUI(forUpdate:Bool, reminder:ReminderDataModel? = nil){
        if forUpdate{
            isForUpdate = true
            spaceDelete.isHidden = false
            btnDelete.isHidden = false
            self.objReminder = reminder!
            self.btnAdd.setTitle("Update", for: .normal)
            setWeekDay(value: self.objReminder.weekDays)
            if self.objReminder.message.trimmedString() != ""{
                txtviewMessage.text = self.objReminder.message
            }
            datepicker.date = Date(milliseconds: Int64(self.objReminder.reminder_date)!)
            reminderDate = datepicker.date
        }else{
            isForUpdate = false
            self.btnAdd.setTitle("Add", for: .normal)
            spaceDelete.isHidden = true
            btnDelete.isHidden = true
        }
    }
        
    func getWeekDay(aryButton:[UIButton]) -> String {
        var value = [String]()
        for button in aryButton{
            value.append("\(button.tag)")
        }
        return value.joined(separator: ",")
    }
    
    func setWeekDay(value : String){
        let value = value.components(separatedBy: ",")
        for tag in value{
            btnDays[Int(tag)!-1].isSelected = true
        }
    }
    
    func getRegisterIdentifiers(){
        if objReminder.indetifier_day_1 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_1)
        }
        if objReminder.indetifier_day_2 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_2)
        }
        if objReminder.indetifier_day_3 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_3)
        }
        if objReminder.indetifier_day_4 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_4)
        }
        if objReminder.indetifier_day_5 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_5)
        }
        if objReminder.indetifier_day_6 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_6)
        }
        if objReminder.indetifier_day_7 != ""{
            registerIdentifiers.append(objReminder.indetifier_day_7)
        }
    }
    
    func setIdentifierFromDay(wekDay:Int, idnty:String){
        if wekDay == 1{
            objReminder.indetifier_day_1 = idnty
        }else if wekDay == 2{
            objReminder.indetifier_day_2 = idnty
        }else if wekDay == 3{
            objReminder.indetifier_day_3 = idnty
        }else if wekDay == 4{
            objReminder.indetifier_day_4 = idnty
        }else if wekDay == 5{
            objReminder.indetifier_day_5 = idnty
        }else if wekDay == 6{
            objReminder.indetifier_day_6 = idnty
        }else{
            objReminder.indetifier_day_7 = idnty
        }
    }
    
    func deletePendinNotificationById(){
        let center = UNUserNotificationCenter.current()
        getRegisterIdentifiers()
        for identifier in registerIdentifiers {
            kprint(items: "Identifier : \(identifier)")
            center.removePendingNotificationRequests(withIdentifiers: [identifier])
        }
    }
    
}

extension AddReminderView{
        
    @IBAction func action_daySelect(_ sender: UIControl) {
        self.endEditing(true)
        btnDays[sender.tag-1].isSelected = !btnDays[sender.tag-1].isSelected
    }
    
    @IBAction func action_close(_ sender: UIButton) {
        self.endEditing(true)
        self.actionBlock(.cancel)
    }
    
    @IBAction func action_forceClose(_ sender: UIControl) {
        self.endEditing(true)
//        self.actionBlock(.cancel)
    }
    
    @IBAction func action_add(_ sender: UIButton) {
        self.endEditing(true)
        let weekDays = btnDays.filter { $0.isSelected }
        if weekDays.count > 0{
            if isForUpdate{
                updateRecordDatabase()
            }else{
                if addReminder(weekDays: getWeekDay(aryButton: weekDays)){
                    if self.getLastRecord() != nil{
                        self.objReminder = self.getLastRecord()!
                        self.registerNotification(weekDay: weekDays, hours: Hours, minute: Minutes)
                        self.actionBlock(.done)
                    }else{
                        kprint(items: "Data not found.")
                    }
                }
            }
        }else{
            kprint(items: "Weekdays not selected")
            _ = ValidationToast.showStatusMessage(message: "Please select day.")
        }
//        self.registerNotification(weekDay: weekDays, hours: Hours, minute: Minutes)
    }
    
    @IBAction func action_delete(_ sender: UIButton) {
        self.endEditing(true)
        self.prepareConfirmationPopup(message: "delete") { (action) in
            if action == .done{
                if self.deleteReminder(){
                    self.actionBlock(.delete)
                }
            }
        }
    }
    
}

extension AddReminderView : UIPickerViewDelegate
{
    @objc func dateChanged(_ sender: UIDatePicker) {
        self.endEditing(true)
        reminderDate = sender.date
        let calendar = Calendar.current
        Hours = calendar.component(.hour, from: sender.date)
        Minutes = calendar.component(.minute, from: sender.date)
        Year = calendar.component(.year, from: sender.date)
    }
}

extension AddReminderView {
    
    func registerNotification(weekDay : [UIButton], hours:Int, minute:Int){
        // WeekDay (Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7)
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: _appName, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: txtviewMessage.text, arguments: nil)
        content.categoryIdentifier = "TIMER_EXPIRED"
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        for button in weekDay {
            
            var dateInfo = DateComponents()
            dateInfo.hour = hours
            dateInfo.minute = minute
            dateInfo.weekday = button.tag
            dateInfo.timeZone = .current
            
            let uniqIdentifier = UUID().uuidString
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: true)
            let request = UNNotificationRequest(identifier: uniqIdentifier, content: content, trigger: trigger)
            // Update Reminder for identifiers.
            kprint(items: "Reminder Identifier : \(uniqIdentifier)")
            self.updateReminder(weekDay: button.tag, identifier: uniqIdentifier)
            UNUserNotificationCenter.current().add(request) {(error) in
                if let error = error {
                    kprint(items: ("Uh oh! We had an error: \(error)"))
                }else{
                    kprint(items: ("Uh add notification"))
                }
            }
        }
        
    }
    
}

extension AddReminderView {
    
    func addReminder(weekDays:String) -> Bool {
        var success = false
        let reminder = ReminderDataModel()
        debugPrint(txtviewMessage.text)
        if (txtviewMessage.text == txtviewMessage.placeholder) || (txtviewMessage.text.trimmedString() == ""){
            txtviewMessage.text = ""
        }
        reminder.message = txtviewMessage.text
        reminder.weekDays = weekDays
        reminder.reminder_date = "\(reminderDate.millisecondsSince1970)"
        if ReminderDataModel.shared.insertReminderData(reminder: reminder){
            kprint(items: "Reminder insert success")
            success = true
        }else{
            kprint(items: "Reminder insert fail")
        }
        return success
    }
    
    func updateRecordDatabase(){
        
        // convert selected days to weekday.
        let weekDays = btnDays.filter { $0.isSelected }
        objReminder.weekDays = getWeekDay(aryButton: weekDays)
        if (txtviewMessage.text == txtviewMessage.placeholder) || (txtviewMessage.text.trimmedString() == ""){
            txtviewMessage.text = ""
        }else{
            objReminder.message = txtviewMessage.text
        }
        objReminder.reminder_date = "\(reminderDate.millisecondsSince1970)"
        
        // convert miliseconds to date.
        let calendar = Calendar.current
        Hours = calendar.component(.hour, from: reminderDate)
        Minutes = calendar.component(.minute, from: reminderDate)
        
        self.deletePendinNotificationById()
        self.registerNotification(weekDay: weekDays, hours: Hours, minute: Minutes)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.actionBlock(.update)
        }
    }
    
    func updateReminder(weekDay:Int, identifier:String){
        self.setIdentifierFromDay(wekDay: weekDay, idnty: identifier)
        let weekDays = btnDays.filter { $0.isSelected }
        objReminder.weekDays = getWeekDay(aryButton: weekDays)
        if ReminderDataModel.shared.updateReminder(reminder: objReminder){
            kprint(items: "Update Reminder success")
        }else{
            kprint(items: "Update Reminder fail")
        }
    
    }
    
    func deleteReminder() -> Bool{
        var success = false
        self.deletePendinNotificationById()
        if ReminderDataModel.shared.deleteReminder(withID: objReminder.id){
            kprint(items: "Delete success")
            success = true
        }else{
            kprint(items: "Delete fail")
        }
        return success
    }
    
    func getLastRecord() -> ReminderDataModel? {
        var lastRecord:ReminderDataModel? = nil
        
        if ReminderDataModel.shared.loadReminder(lastrecord: true) != nil{
            let reminderData = ReminderDataModel.shared.loadReminder(lastrecord: true)!
            if reminderData.count > 0{
                lastRecord = reminderData[0]
            }
        }
        
        return lastRecord
    }
    
}
