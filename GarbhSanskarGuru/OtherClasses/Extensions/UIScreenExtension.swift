
import Foundation
import UIKit

extension UIScreen {
    
    static var height : CGFloat {
        return UIScreen.main.bounds.height
    }
    
    static var width : CGFloat {
        return UIScreen.main.bounds.width
    }
    
    var height : CGFloat {
        return UIScreen.main.bounds.height
    }
    
    var width : CGFloat {
        return UIScreen.main.bounds.width
    }
}
