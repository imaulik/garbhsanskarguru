//
//  FaqVC.swift
//  GarbhSanskarGuru
//
//  Created by Jahanvi Trivedi on 19/02/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class FaqCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    func setUpEnglishFaq(at index: IndexPath) {
        if index.row ==  0 {
            lblTitle.text = "1. What is Garbh Sanskar Guru App ?"
            lblDesc.text = """
            This App contains 280-days course on  Garbh Sanskar which can be carried out during pregnancy time. It provides day-to-day activities with material. Activities will be provided according to the current day of pregnancy.
            """
        }else if index.row == 1 {
            lblTitle.text = "2. Can we achieve desired qualities in baby only by following this app?"
            lblDesc.text = """
            Yes, if you follow it in right direction with positive attitude then you can achieve the results as per your expectations. It’s better to do something rather not doing anything.
            """
        }else if index.row == 2 {
            lblTitle.text = "3. How to count the current days of pregnancy for the app?"
            lblDesc.text = """
            To calculate your today's pregnancy day, calculate from the first day of your last menstrual period. For example, if your first day of last menstrual period was March 1st and on 1st of May you start using this App, then consider your current pregnancy day as 62.
            """
        }else if index.row == 3 {
            lblTitle.text = "4. Is there a solution to the questions related to medical/health issue in the app?"
            lblDesc.text = """
            No
            """
        }
        else if index.row == 4 {
            lblTitle.text = "5. If I start using this app from third month of pregnancy, can I use or get the material for previous two months?"
            lblDesc.text = """
            No. Activities given in the app are relevant to the current day of pregnancy by considering current development of the child in womb. But you can explore the material of 3 previous days of your current day.
            """
        }
        else if index.row == 5 {
            lblTitle.text = "6. If I am a working woman, will I get enough time to spent on App?"
            lblDesc.text = """
            It is all about getting 90 years of work done in 9 month, so one should not miss an opportunity for the development of child. With this Mobile App you can stay active anywhere at any time.
            """
        }
        else if index.row == 6 {
            lblTitle.text = "7. I have started using this app today, what about the past activities of the app which I missed?"
            lblDesc.text = """
            Do not feel sorry for the activities which you have missed. It is never too late to mend. Time once lost is never found back again but future is in your hand. There is lot to do in remaining days.
            """
        }
        else if index.row == 7 {
            lblTitle.text = "8. We wanted to have a Baby boy/Baby Girl. Can we have our gender specific desired baby by using this app?"
            lblDesc.text = """
            Garbh Sanskar(Prenatal Education) Guru app is about getting the baby with best qualities. It has nothing to do with the gender of the baby.Majestic Garbh Sanskar does not promote such things.
            """
        }
        else if index.row == 8 {
            lblTitle.text = "9. Can this app be gifted? how?"
            lblDesc.text = """
            Yes, you can gift this app to the one who is expecting a baby in your group of friends or on the wedding occasion or any other group. To know the process, we would be happy to help you. Simply, call on our helpline number.
            """
        }
        else if index.row == 9 {
            lblTitle.text = "10. I'm a housewife. Will there be a fixed time slot to complete the activities of this app?"
            lblDesc.text = """
            You can perform/follow  the activities given in the app at any time throughout the day. But it is recommended to practice Yoga and Meditation early in the morning.
            """
        }
        else if index.row == 10 {
            lblTitle.text = "11. What if I could not understand or interpret Video or information provided in the app due to Language barrier or lack of subject knowledge?"
            lblDesc.text = """
            App data is provided in English, Hindi and Gujarati language. Data is provided in such a manner so that unborn child in the womb can be benefited in terms of learning the things from diversified fields. If you are not understanding the content due to lack of subject knowledge, then also take the things positively without considering it as a confusion. Some videos might be in single language due to unavailability of such content in English language.
            """
        }
    }
    
    func setUpHindiFaq(at index: IndexPath) {
        if index.row ==  0 {
            lblTitle.text = "1. गर्भ संस्कार गुरु एप में क्या है?"
            lblDesc.text = """
            इस ऐप में गर्भावस्था के 280 दिन का गर्भ संस्कार का कोर्स है जिसमें हर रोज करने की गतिविधि जानकारी और मटेरियल के साथ है.
            """
        }else if index.row == 1 {
            lblTitle.text = "2. क्या इस ऐप के अनुसार पालन करने से इच्छित गुणों वाले संतान की प्राप्ति हो सकती है?"
            lblDesc.text = """
            हां, जरूर. इस समय के दौरान सही दिशा में पॉजिटिव  रह कर यदि प्रयास किया जाए तो परिणाम अवश्य मिलता है कुछ ना करना उससे बेहतर है कि जो भी करें वह सही विचार और भाव के साथ करें.
            """
        }else if index.row == 2 {
            lblTitle.text = "3. अपने प्रेगनेंसी का दिन किस तरह गिन कर डालें?"
            lblDesc.text = """
            आपका आज का गर्भावस्था का दिन गिनने के लिए के  आखरी महावरी के प्रथम दिन से गिनती शुरू करें. उदाहरण: आपकी आखिरी महावरी 1 मार्च को थी और 1 मई से आप एप का इस्तेमाल शुरु करते हैं तो आज आपका 62 है.
            """
        }else if index.row == 3 {
            lblTitle.text = "4. ऐप में मेडिकल साइंस के अनुसंधान के प्रश्नों की समाधान है?"
            lblDesc.text = """
            नहीं
            """
        }
        else if index.row == 4 {
            lblTitle.text = "5. मेरा तीसरा महीना चल रहा है तो क्या मैं बीते हुए महीनों का मटीरियल इस्तेमाल कर सकती हूं?"
            lblDesc.text = """
            नहीं. एप में दी हुई गतिविधियां गर्भावस्था के दिन और महीने के अनुरूप शिशु के विकास के मुताबिक दी हुई है परंतु आप आज का जो दिन है उसके अलावा बीते हुए 3 दिन का मटीरियल देख सकते हैं.
            """
        }
        else if index.row == 5 {
            lblTitle.text = "6. मैं वर्किंग वुमन हूं तो क्या इतना समय निकाल पाऊंगी?"
            lblDesc.text = """
            गर्भ संस्कार द्वारा हमें 90 साल का काम 9 महीनों में करना है अपने बालक के विकास का यह सर्वोत्तम समय न गवाएं. इस मोबाइल ऐप द्वारा आप किसी भी जगह किसी भी समय प्रवृत्त रह सकते हैं.
            """
        }
        else if index.row == 6 {
            lblTitle.text = "7. मुझे ऐप की जानकारी आज ही मिली है तो बीते हुए महीनों का क्या?"
            lblDesc.text = """
            "जब जागे तभी सवेरा" जो समय बीत गया उसका अफसोस ना करते हुए जो समय हाथ में है उसका श्रेष्ठ उपयोग करें.
            """
        }
        else if index.row == 7 {
            lblTitle.text = "8. क्या इस ऐप के इस्तेमाल से मनपसंद संतान( पुत्र या पुत्री) पा सकते हैं?"
            lblDesc.text = """
            गर्भ संस्कार उत्तम गुणों वाला संतान प्राप्त करने के लिए है पुत्र या पुत्री नहीं. मजेस्टिक गर्भ संस्कार ऐसे किसी भी विचार को प्रोत्साहित नहीं करता.
            """
        }
        else if index.row == 8 {
            lblTitle.text = "9. क्या इस एप को गिफ्ट कर सकते हैं? कैसे?"
            lblDesc.text = """
            हां, आप अपने मित्र वर्ग, शादी समारोह, या अन्य समूह को गिफ्ट कर सकते हैं प्रोसेस जानने के लिए 9727006001 नंबर पर संपर्क करें.
            """
        }
        else if index.row == 9 {
            lblTitle.text = "10. मैं एक हाउसवाइफ हूं तो क्या यह ऐप के इस्तेमाल का कोई निश्चित समय है?"
            lblDesc.text = """
            एप में दी हुई गतिविधियां पूरे समय के दौरान आपकी सुविधा अनुसार किसी भी वक्त कर सकते हैं.
            """
        }
        else if index.row == 10 {
            lblTitle.text = "11. ऐप में दी हुई कुछ वीडियो/ जानकारी उसकी भाषा या विषय के कारण समझने में दुविधा लगती है तो क्या करें?"
            lblDesc.text = """
            ऐप की वीडियो/ जानकारी सोच समझ कर रखी गई है कि जिससे गर्भस्थ शिशु मैं नया जानने और सीखने की रुचि विकसित हो
            इसलिए अस्वस्थ ना होते हुए पॉजिटिव रहकर नया जानने समझने की कोशिश करें.
            """
        }
    }
    
    func setUpGujaratiFaq(at index: IndexPath) {
        if index.row ==  0 {
            lblTitle.text = "1. ગર્ભસંસ્કાર ગુરુ App માં શું છે?"
            lblDesc.text = """
            આ App ગર્ભાવસ્થાના 280 દિવસનો ગર્ભસંસ્કાર નો કોર્સ છે જેમાં રોજે રોજ કરવાની પ્રવૃત્તિઓની માહિતી અને મટિરિયલ છે.
            """
        }else if index.row == 1 {
            lblTitle.text = "2. શું આ App મુજબ અનુસરવાથી ઇચ્છિત ગુણોવાળા સંતાનની પ્રાપ્તિ થઇ શકે?"
            lblDesc.text = """
            હા ચોક્કસ આ સમય દરમિયાન સાચી દિશામાં પૉઝિટિવ રહીને પ્રયત્નો કરવામાં આવે તો સારું પરિણામ ચોક્કસ મળે છે.  કઈજ ન કરવું એના કરતાં જેટલું પણ થાય એ સારા વિચાર અને ભાવ સાથે કરવું
            """
        }else if index.row == 2 {
            lblTitle.text = "3. Appમાં પ્રેગનેન્સીનો દિવસ કઈ રીતે ગણીને નાખવો?"
            lblDesc.text = """
            તમારો આજનો ગર્ભાવસ્થાનો દિવસ ગણવા માટે છેલ્લે આવેલા માસિકના પ્રથમ દિવસથી ગણતરી કરવી. ઉદાહરણ તરીકે તમારું છેલ્લું માસિક 1st માર્ચ એ આવ્યું હોય અને 1st મે એ તમે App વાપરવાનું શરૂ કરો તો તમારો આજનો દિવસ ૭૬મો દિવસ ગણવો.
            """
        }else if index.row == 3 {
            lblTitle.text = "4. Appમાં તબીબી વિજ્ઞાનને(Medical Science) લગતા પ્રશ્નોના સમાધાન છે?"
            lblDesc.text = """
            ના
            """
        }
        else if index.row == 4 {
            lblTitle.text = "5. મારે ત્રીજો મહિનો ચાલે છે તો હું વીતેલા બે મહિનાનું મટીરિયલ વાપરી શકું?"
            lblDesc.text = """
            ના.  Appમાં આપેલી પ્રવૃત્તિઓ ગર્ભાવસ્થાના દિવસ અને જે તે મહિનામાં થતા બાળકના વિકાસને અનુરૂપ છે. પરંતુ તમે આજે જે દિવસ ચાલતો હોય એના ત્રણ દિવસ પહેલા નું મટીરીયલ જોઈ શકાશે.
            """
        }
        else if index.row == 5 {
            lblTitle.text = "6. હું વર્કિંગ વુમન છું તો શું આટલો સમય ફાળવી શકીશ?"
            lblDesc.text = """
            ગર્ભસંસ્કાર દ્વારા આપણે 90 વર્ષનું કામ નવ મહિનામાં કરવાનું છે એટલે પોતાના બાળકના વિકાસ માટેના ઉત્તમ સમયને ગુમાવશો નહીં. આ મોબાઇલ App દ્વારા તમે ગમે તે જગ્યાએ ગમે તે સમયે પ્રવૃત્ત રહી શકો છો.
            """
        }
        else if index.row == 6 {
            lblTitle.text = "7. મને App ની માહિતી આજે જ ખબર પડી છે તો વીતેલા મહિનાનું શું?"
            lblDesc.text = """
            "જાગ્યા ત્યાંથી સવાર જે વીતી ગયું છે તેનો અફસોસ ન કરતા જે સમય આપણા હાથમાં છે તેનો શ્રેષ્ઠ ઉપયોગ કરવો.
            """
        }
        else if index.row == 7 {
            lblTitle.text = "8. શું આ App વાપરવાથી મનગમતું સંતાન (પુત્ર કે પુત્રી) મેળવી શકાય છે?"
            lblDesc.text = """
            ગર્ભસંસ્કાર એ ઉત્તમ ગુણોવાળા સંતાન મેળવવા માટે છે પુત્ર કે પુત્રી નહીં મજેસ્ટિક ગર્ભસંસ્કાર આવા કોઈ વિચારને પ્રોત્સાહન આપતું નથી.
            """
        }
        else if index.row == 8 {
            lblTitle.text = "9. શું આ App ગિફ્ટ કરી શકાય? કેવી રીતે?"
            lblDesc.text = """
            હા. આ અપ્પ મિત્ર વર્તુળમાં, લગ્ન પ્રસંગે કે કોઈપણ એક સમૂહ ને ગિફ્ટ આપી શકો છો પ્રોસેસ જાણવા માટે 97 27006001 હેલ્પલાઇન નંબર પર ફોન કરશો
            """
        }
        else if index.row == 9 {
            lblTitle.text = "10. હું એક હાઉસવાઈફ છું શું App વાપરવા માટે નો કોઈ નિશ્ચિત સમય રહેશે?"
            lblDesc.text = """
            Appમાં આપેલી પ્રવૃત્તિઓ આખા દિવસમાં ગમે તે સમયે તમારી અનુકૂળતા મુજબ કરી શકશો.
            Note: યોગ અને પ્રાણાયામ શક્ય હોય તો વહેલી સવારે ભૂખ્યા પેટે કરવા.
            """
        }
        else if index.row == 10 {
            lblTitle.text = "11. App માં અમુક વિડીયો/માહિતી ભાષા કે વિષયના કારણે સમજાતી નથી તો શું કરવું?"
            lblDesc.text = """
            App ની માહિતી/વિડીયો સમજી-વિચારીને એ રીતે રાખવામાં આવી છે કે જેથી ગર્ભમાં રહેલા બાળકને નવા વિષયો જાણવાની/શીખવાની રૂચી કેળવાય એટલે આને મૂંઝવણ તરીકે ન લેતાં પૉઝિટિવ રહીને શીખવાનું કે સમજવાનો પ્રયત્ન કરવો.
            """
        }
    }
}

class FaqVC: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension FaqVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FaqCell
        cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FaqCell
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        switch currLanguage {
        case .hindi:
            cell.setUpHindiFaq(at: indexPath)
        case .english:
            cell.setUpEnglishFaq(at: indexPath)
        case .gujarati:
            cell.setUpGujaratiFaq(at: indexPath)
        }
        return cell
    }
}
