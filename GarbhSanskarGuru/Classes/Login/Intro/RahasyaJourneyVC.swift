//
//  RahasyaJourneyVC.swift
//  Garbhsanskar
//
//  Created by Jayesh Tejwani on 04/12/18.
//  Copyright © 2018 Jahanvi Trivedi. All rights reserved.
//

import UIKit

enum JourneyMonths: Int{
    
    case month1 = 1,month2,month3,month4,month5,month6,month7,month8,month9
}

class RahasyaJourneyVC: ParentViewController {

    var monthType: JourneyMonths = .month1
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueMonth"{
            let destVC = segue.destination as! RahasyaJourneyContentVC
            destVC.monthType = (sender as! JourneyMonths)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMonthsTapped(sender: UIButton){
        self.monthType = JourneyMonths(rawValue: sender.tag)!
        switch monthType{
        case .month1, .month2, .month3, .month4, .month5, .month6, .month7, .month8, .month9:
            performSegue(withIdentifier: "segueMonth", sender: monthType)
        }
    }
}
