//
//  AppInformationVC.swift
//  GarbhSanskarGuru
//
//  Created by Jahanvi Trivedi on 18/02/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import WebKit

class AppInformationVC: ParentViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webVideo1: WKWebView!
    @IBOutlet weak var webVideo2: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.showCentralSpinner()
        KPWebCall.call.getAppInformationVideo { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["success"] as? Int
            {
                if status == 200, let dictData = dict["data"] as? NSArray {
                    for i in 0..<dictData.count
                    {
                        let dicData: NSDictionary = dictData.object(at: i) as! NSDictionary
                        var strVideoUrl = String()
                        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
                        switch currLanguage {
                        case .hindi:
                            strVideoUrl = dicData.value(forKey: "hindi_youtube") as! String
                        case .english:
                            strVideoUrl = dicData.value(forKey: "eng_youtube") as! String
                        case .gujarati:
                            strVideoUrl = dicData.value(forKey: "guj_youtube") as! String
                        }
                        let youTubeID: String = self!.getYoutubeId(StrLink: strVideoUrl) ?? "xXbSLYW3MP0"
                        let url = URL(string: "https://www.youtube.com/embed/\(youTubeID)")
                        if i == 0
                        {
                           self?.webVideo1.load(URLRequest(url: url!))
                        }
                        else
                        {
                            self?.webVideo2.load(URLRequest(url: url!))
                        }
                    }   
                } else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }
        }
    }
    
    func getYoutubeId(StrLink: String) -> String?
    {
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            
            let match = regex.firstMatch(in: StrLink, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, StrLink.lengthOfBytes(using: String.Encoding.utf8)))
            
            let range = match?.range(at: 0)
            let youTubeID = (StrLink as NSString).substring(with: range!)
            return youTubeID
        } catch let err {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: err.localizedDescription)
        }
        return nil
    }
    
    @IBAction func btnBack(_ sender: AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
