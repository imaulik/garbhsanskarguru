//
//  VerifyPaymentVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 16/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct VerifyPayment {
    var transID = ""
    var comments = ""
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["transaction_id"] = transID
        dict["comment"] = comments
        return dict
    }
    
    func isValidData() -> (isValid: Bool, error: String){
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: transID){
            result.isValid = false
            result.error = "Please Enter Transaction ID"
            return result
        }
        return result
    }
}

class VerifyPaymentCell: ConstrainedTableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var tfInput: UITextField!
    
    weak var parent: VerifyPaymentVC!
    
    func setUpCellUI(with tag: Int, and data: VerifyPayment) {
        tfInput.tag = tag
        tfInput.inputAccessoryView = nil
        tfInput.keyboardType = .default
        switch tag {
        case 1:
            tfInput.placeholder = "Transaction ID"
            tfInput.text = data.transID
            tfInput.keyboardType = .numberPad
            tfInput.inputAccessoryView = parent.viewAccessory
            break
        case 2:
            tfInput.placeholder = "Comment (optional)"
            tfInput.text = data.comments
            tfInput.returnKeyType = .done
            break
        default:
            break
        }
    }
    
    @IBAction func tfEditingChange(_ textfield: UITextField) {
        if textfield.tag == 1 {
            parent.data.transID = textfield.text!.trimmedString()
        }else if textfield.tag == 2{
            parent.data.comments = textfield.text!.trimmedString()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next{
            if let index = parent.tableView.indexPath(for: self){
                if let cell = parent.getEntryCell(index.row + 1){
                    cell.tfInput.becomeFirstResponder()
                }
            }else{
                textField.resignFirstResponder()
            }
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}


class VerifyPaymentVC: ParentViewController {

    @IBOutlet weak var viewAccessory: UIView!
    
    var data = VerifyPayment()
    var block: ((_ isHidden: Bool) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension VerifyPaymentVC {
    
    func prepareUI() {
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        viewAccessory.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: 40)
    }
    
    func getEntryCell(_ row: Int) -> VerifyPaymentCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? VerifyPaymentCell
        return cell
    }

}

extension VerifyPaymentVC {
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        let validate = data.isValidData()
        if validate.isValid {
            if _user.pregnancyStatus == .none {
                self.verifyPayment { (done) in
                    if done {
                        self.block?(true)
                    }
                }
            } else {
                self.checkPaymentStatus { (done) in
                    if done {
                        self.block?(true)
                    }
                }
            }
        }else {
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        block?(true)
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        self.view.endEditing(true)
    }
 }

extension VerifyPaymentVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 90.heightRatio : 50.heightRatio
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VerifyPaymentCell
        let cellID = indexPath.row == 0 ? "infoCell" : indexPath.row == 3 ? "btncell" : "txtcell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! VerifyPaymentCell
        if indexPath.row != 0 && indexPath.row != 3 {
            cell.parent = self
            cell.setUpCellUI(with: indexPath.row, and: data)
        }
        return cell
    }
    
}

extension VerifyPaymentVC {
    
    func verifyPayment(block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.verifyPayment(param: data.paramDict()) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, let msg = dict["message"] as? String {
                if status == 200 {
                    _ = ValidationToast.showStatusMessage(message: msg)
                    block(true)
                } else {
                    _ = ValidationToast.showStatusMessage(message: msg)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
    
    func checkPaymentStatus(block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id, "amount": _user.amount, "payment_mode": "DC", "transaction_id": data.transID, "comment": data.comments]
        self.showCentralSpinner()
        KPWebCall.call.checkPaymentStatus(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: dict.getStringValue(key: "message"))
                block(true)
            } else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
}
