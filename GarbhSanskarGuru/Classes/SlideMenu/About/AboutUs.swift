//
//  AboutUs.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 20/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class AboutCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    
    func setUpAboutUI(at index: IndexPath) {
        if index.row ==  0 {
            lblTitle.text = "About us"
            lblDesc.text = """
            Team Majestic Garbh Sanskar consists expert doctors in Ayurveda, Gynaecologists,
            Professors, Child Psychologists and Social Reformers. Majestic Garbh Sanskar team
            has been making sincere efforts to spread this science to the world over the last
            10 years. Now In today's age of technology, this Vedic science can be delivered to
            each home using the technology itself.
            """
        }else if index.row == 1 {
            lblTitle.text = "Vision"
            lblDesc.text = """
            Empowering humanity for timeless happiness by nurturing the unborn child. Re-
            establishing vedic Indian values to achieve ultimate happiness through the noble
            work of GARBHSANSKAR (Parental Education)
            """
        }else if index.row == 2 {
            lblTitle.text = "Mission"
            lblDesc.text = """
            At GARBHSANSKAR our mission is to enrich the life of your offsprings’ with
            prenatal education practices as well as disciplined lifestyle provided by our
            highly skilled team members to step up in this dynamic &amp; contemporary world.
            Through the Vedic knowledge of GARBHSANSKAR, we are aiming an India to become the
            Universal leader
            """
        }else {
            lblTitle.text = "Our Activities"
            lblDesc.text = """
            ❑ Webinar
            ❑ Seminar
            ❑ Workshop (For limited participants only)
            ❑ Counselling
            """
            let version: String = Bundle.main.infoDictionary!["CFBundleShortVersionString"]! as! String
            lblVersion.text = "App Version " + String(version)
        }
    }
}

class AboutUs: ParentViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}

extension AboutUs {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AboutCell
        cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AboutCell
        cell.setUpAboutUI(at: indexPath)
        return cell
    }
}
