//
//  VideoVC.swift
//  PregnancyGuru
//
//  Created by Maulik on 08/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit
import AVKit


class VideoVC: ParentViewController {
    var dashboardSelItem : DashboardList?
    var arrSelectedTips:TipsCategoryList?
    var imageURL = ""
    var arrVideosList: [VideoList]?
    var isFromAppInformation = false
    let strLanguage = _appDelegator.getLanguage()
    
    var arrAppinformation: [AppinfoList]?
    
    
    @IBOutlet weak var cvVideoList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cvVideoList.register(UINib(nibName: "VideoCell", bundle: .main), forCellWithReuseIdentifier: "VideoCell")
        if isFromAppInformation {
            self.title = "App Information"
            getInformation()
        }else {
           
            if dashboardSelItem?.category_name == DashBoardCategoryList.GSYoga.rawValue ||  dashboardSelItem?.category_name == DashBoardCategoryList.GSGarbhSavand.rawValue{
                self.title = dashboardSelItem?.title?.localized
                getList()
            }
            else {
                if self.strLanguage == kEnglish {
                    self.title = arrSelectedTips?.eng_name
                }else if self.strLanguage == kGujrati {
                    self.title = arrSelectedTips?.guj_name
                }else if self.strLanguage == KHindi {
                    self.title = arrSelectedTips?.hindi_name
                }
                getVideoList()
            }
        }
        
    }
    
    func getInformation(){
        self.showCentralSpinner()
        KPWebCall.call.getAppInformation {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = AppInfoModel(dictionary: dict)
                weakself.arrAppinformation = res.data
                weakself.imageURL = res.url
                weakself.cvVideoList.reloadData()
            }
        }
//        callWS_GetAppInformation { result in
//            guard let res = result else { return }
//            self.arrAppinformation = res.data
//            self.imageURL = res.url
//            self.cvVideoList.reloadData()
//        }
    }
    
    func getList(){
        let param = ["user_id":_user.id,"category_name":dashboardSelItem?.category_name ?? ""]
                
        self.showCentralSpinner()
        KPWebCall.call.getVideosOfGS(param: param) {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = VideoViewModel(dictionary: dict)
                weakself.arrVideosList = res.data
                weakself.imageURL = res.image_url
                weakself.cvVideoList.reloadData()
            }
        }
        
//        callWS_GetVideosOfGS(parms: parm) { videoList in
//            guard let res = videoList else {
//                return
//            }
//            self.arrVideosList = res.data
//            self.imageURL = res.image_url
//            self.cvVideoList.reloadData()
//        }
    }
    
    func getVideoList(){
        let param = ["user_id":_user.id,"category_id": arrSelectedTips?.id ?? ""]
        
        self.showCentralSpinner()
        KPWebCall.call.getVideoList(param: param) {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = VideoViewModel(dictionary: dict)
                weakself.arrVideosList = res.data
                weakself.imageURL = res.image_url
                weakself.cvVideoList.reloadData()
            }
        }
        
//        callWS_Videlist(parms: parm) { videoList in
//            guard let res = videoList else {
//                return
//            }
//            self.arrVideosList = res.data
//            self.imageURL = res.image_url
//            self.cvVideoList.reloadData()
//        }
    }
    
}


extension VideoVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width - 10 , height: 200)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromAppInformation {
            return arrAppinformation?.count ?? 0
        }
        return arrVideosList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"VideoCell", for: indexPath) as! VideoCell
        if isFromAppInformation {
            let obj = arrAppinformation? [indexPath.item]
            if strLanguage == kEnglish {
//                cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.eng_thb ?? "")) , placeholderImage: nil)
                cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.eng_thb ?? "")))
                 cell.lblPlayerTitle.text = ""
            }
            else if strLanguage == KHindi {
//                cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.hindi_thb ?? "")) , placeholderImage: nil)
                cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.hindi_thb ?? "")))
                cell.lblPlayerTitle.text = ""
            }
            else if strLanguage == kGujrati {
//                cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.guj_thb ?? "")) , placeholderImage: nil)
                cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.guj_thb ?? "")))
                 cell.lblPlayerTitle.text = ""
            }else{
//                cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.eng_thb ?? "")) , placeholderImage: nil)
                cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.eng_thb ?? "")))
                cell.lblPlayerTitle.text = ""
            }
        }else {
            let obj = arrVideosList? [indexPath.item]
            if dashboardSelItem?.category_name == DashBoardCategoryList.GSYoga.rawValue ||  dashboardSelItem?.category_name == DashBoardCategoryList.GSGarbhSavand.rawValue{
                
                if strLanguage == kEnglish {
                    cell.lblPlayerTitle.text = obj?.eng_title
                }else if strLanguage == KHindi {
                    cell.lblPlayerTitle.text = obj?.hindi_title
                }else if strLanguage == kGujrati {
                    cell.lblPlayerTitle.text = obj?.guj_title
                }else {
                    cell.lblPlayerTitle.text = obj?.eng_title
//                    cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.eng_image ?? "")) , placeholderImage: nil)
                    cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.eng_image ?? "")))
                }
                
                if dashboardSelItem?.category_name == DashBoardCategoryList.GSGarbhSavand.rawValue{
                    cell.imgPlayer.image = UIImage(named: "ic_GarbhSavand")
                }else {
                    cell.imgPlayer.image = UIImage(named: "ic_Yoga")
                }
            }else {
                if strLanguage == kEnglish {
                    cell.lblPlayerTitle.text = obj?.eng_title
//                    cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.eng_image ?? "")) , placeholderImage: nil)
                    cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.eng_image ?? "")))
                    
                }else if strLanguage == KHindi {
                    cell.lblPlayerTitle.text = obj?.hindi_title
//                    cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.hindi_image ?? "")) , placeholderImage: nil)
                    cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.hindi_image ?? "")))
                }else if strLanguage == kGujrati {
                    cell.lblPlayerTitle.text = obj?.guj_title
//                    cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.guj_image ?? "")) , placeholderImage: nil)
                    cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.guj_image ?? "")))
                    
                }else {
                    cell.lblPlayerTitle.text = obj?.eng_title
//                    cell.imgPlayer.sd_setImage(with:URL(string: imageURL + (obj?.eng_image ?? "")) , placeholderImage: nil)
                    cell.imgPlayer.kf.setImage(with: URL(string: imageURL + (obj?.eng_image ?? "")))
                }
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var ytURL:String?
        if isFromAppInformation {
            let obj = arrAppinformation? [indexPath.item]
            if strLanguage == kEnglish {
                ytURL = obj?.eng_youtube ?? ""
            }else if strLanguage == KHindi {
                ytURL = obj?.hindi_video ?? ""
            }else if strLanguage == kGujrati {
                ytURL = obj?.guj_youtube ?? ""
            }else {
                ytURL = obj?.eng_youtube ?? ""
            }
        }
        else {
            let obj = arrVideosList? [indexPath.item]
            if strLanguage == kEnglish {
                ytURL = obj?.eng_url ?? ""
            }else if strLanguage == KHindi {
                ytURL = obj?.hindi_url ?? ""
            }else if strLanguage == kGujrati {
                ytURL = obj?.guj_url ?? ""
            }else {
                ytURL = obj?.eng_url ?? ""
            }
        }
        
        videoPlay(strVideo: ytURL ?? "")
    }
}
