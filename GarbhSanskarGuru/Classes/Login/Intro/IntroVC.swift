//
//  IntroVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 24/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import SwiftGifOrigin

struct ModelBasicInfo {
    var id = 0
    var image = ""
}

class AppInfoColCell: UICollectionViewCell {
    @IBOutlet weak var imgApp: UIImageView!
}

class IntroTitleCell: ConstrainedTableViewCell {    
    @IBOutlet weak var lblTitle: UILabel!
}

class IntroAppTitleCell: ConstrainedTableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
}

class IntroAppInfoCell: ConstrainedTableViewCell {
    @IBOutlet var colInfo: UICollectionView!
}

class IntroBasicInfoCell: ConstrainedTableViewCell {
    @IBOutlet var vwSwipe: SwipeView!
}

class PaymentInfoCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var vwFullCourse: UIView!
    @IBOutlet weak var lblFullCourse: UILabel!
    @IBOutlet weak var lblCareForBaby: UILabel!
    @IBOutlet weak var lblStartDailyCourse: UILabel!
    @IBOutlet weak var lblFullInJust: UILabel!
    @IBOutlet weak var lblFullPrice: UILabel!
    @IBOutlet weak var btnFullPay: UIButton!
    
    @IBOutlet weak var vwTrialCourse: UIView!
    @IBOutlet weak var lblTrialCourse: UILabel!
    @IBOutlet weak var lblStillConfused: UILabel!
    @IBOutlet weak var lblTry3DaysDemo: UILabel!
    @IBOutlet weak var lblTrialInJust: UILabel!
    @IBOutlet weak var lblTrialPrice: UILabel!
    @IBOutlet weak var btnTrialPay: UIButton!
    @IBOutlet weak var imgGS: UIImageView!
    @IBOutlet weak var btnClickHere: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.backgroundColor = UIColor.white
        
        /*vwFullCourse.layer.borderWidth = 1.0
         vwTrialCourse.layer.borderWidth = 1.0
         vwFullCourse.layer.borderColor = UIColor.gray.cgColor
         vwTrialCourse.layer.borderColor = UIColor.gray.cgColor
         
         btnFullPay.layer.cornerRadius = 5
         btnTrialPay.layer.cornerRadius = 5*/
    }
}

class JoinUsInfoCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var lblPriceforOutsideIndia: UILabel!
    @IBOutlet weak var lblStartFromAnyDay: UILabel!
    @IBOutlet weak var lblJoinFreeCounselling: UILabel!
}

class IntroVC: ParentViewController, SwipeViewDelegate, SwipeViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppInfoColCell", for: indexPath) as! AppInfoColCell
        cell.imgApp.image = UIImage(named: arrAppInfo![indexPath.item].image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            self.performSegue(withIdentifier: "segueInfoContentStatic", sender: nil)
        }
        else if indexPath.row == 1
        {
            let appInformationVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "AppInformationVC") as! AppInformationVC
            self.navigationController?.pushViewController(appInformationVC, animated: true)
        }
        else if indexPath.row == 2
        {
            let faqVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "FaqVC") as! FaqVC
            self.navigationController?.pushViewController(faqVC, animated: true)
        }
    }
    
    var arrBasicInfo: [ModelBasicInfo]!
    var arrAppInfo: [ModelBasicInfo]!
    var arrData: [ModelRahasyaTitle]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        //self.getRahasyaTitle()
        initModel()
    }
    
    // Add will appear.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
            self.prepareOnBoardingFreeDashboard()
        }else{
            self.checkOtherLoginDevice()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "introDetailSegue"
        {
            let destVC = segue.destination as! InfoContentVC
            destVC.objRahasya = (sender as? ModelRahasyaTitle)
        }
        else if segue.identifier == "garbhSanskarInfoSegue"
        {
            let destVC = segue.destination as! GarbhSanskarInfoVC
            destVC.isForDivineMystery = true
        }
        else if segue.identifier == "segueInfoContentStatic"
        {
            let destVC = segue.destination as! InfoContentStaticVC
            destVC.nTypeForStaticData = kBasicInfo_AppBenefits
        }
    }
}

extension IntroVC{
    
    // Make new method. For check Other device login.
    func checkOtherLoginDevice(){
        self.getToday(block: { (done) in
        })
    }
    
    
    func prepareOnBoardingFreeDashboard() {
        let onBoardingView = FreeDashBoard.instantiateView(with: self.view)
        onBoardingView.updateUI()
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .later{
                // show free dashboard.
                //                _appDelegator.setOnBoarding(isOnBoardingShown: true)
                //                _appDelegator.navigateUserToHome() // don't need to call method again and again.
            }else{
                // show paid dashboard with set today date in pregnent day as first.
                self.prepareOnBoarding()
            }
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.actionBlock = { (action) in
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            self.getPregnancyDay(currentDay: currDay, block: { (done) in
                if done {
                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                    onBoardingView.removeFromSuperview()
                    DispatchQueue.main.async {
                        self.getToday(block: { (done) in
                            if done {
                                _appDelegator.navigateDirectlyToHome()
                            }
                        })
                    }
                }
            })
        }
    }
    
    func getToday(block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        
                        if(!deviceId.isEqual(str: _deviceID)){
                            
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            block(false)
                        }
                        else {
                            
                            let demoDay = dict.getInt16Value(key: "demo_day")
                            let whichUser = dict.getStringValue(key: "which_user ")
                            
                            print("\(demoDay) \(whichUser)")
                            _user.setCurrentDay(dict: dict)
                            _user.setWhichUserType(strType: whichUser)
                            _appDelegator.saveContext()
                            
                            block(true)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
}

extension IntroVC {
    
    func prepareUI() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 30
        
        self.lblTitle?.text = _user.name
    }
    
    func prepareSignOutOnBoarding() {
        let onBoardingView = SignOutView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                self.prepareForLogout()
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
   
    
    func openFaceBook() {
        let username =  "MAJESTICGARBHSANSKAR"
        let appURL = NSURL(string: "fb://page/\(username)")!
        let webURL = NSURL(string: "http://www.facebook.com/\(username)")!
        let application = UIApplication.shared
        if application.canOpenURL(appURL as URL){
            application.open(appURL as URL)
        }else{
            application.open(webURL as URL)
        }
    }
    
    func openInstagram() {
        let username =  "mgarbhsanskar" // Your Instagram Username here
        let appURL = NSURL(string: "instagram://user?username=\(username)")!
        let webURL = NSURL(string: "https://instagram.com/\(username)")!
        let application = UIApplication.shared
        
        if application.canOpenURL(appURL as URL){
            application.open(appURL as URL)
        }else{
            application.open(webURL as URL)
        }
    }
    
    func openWhatsApp() {
        
        //let phoneNumber =  ""
        //let appURL = NSURL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
        let appURL = NSURL(string: "https://chat.whatsapp.com/GcOyOw4s9zvfsjakgagKZNI")!
        if UIApplication.shared.canOpenURL(appURL as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL as URL)
            }
        }
        else {
            // Whatsapp is not installed
        }
    }
}

extension IntroVC {
    
    @IBAction func btnMobileTapped(_ sender: UIButton) {
        makeCall(number: "9727006001")
    }
    
    @IBAction func btnFacebookTapped(_ sender: UIButton) {
        openFaceBook()
    }
    
    @IBAction func btnInstagramTapped(_ sender: UIButton) {
        openInstagram()
    }
    
    @IBAction func btnWhatsappTapped(_ sender: UIButton) {
        openWhatsApp()
    }
    
    @IBAction func btnReferralTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "referralSegue", sender: nil)
    }
    
    @IBAction func btnActivities(_ sender: UIButton)
    {
        let activitiesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SubscribeContainer") as! SubscribeContainer
        activitiesVC.isShowBackButton = true
        self.navigationController?.pushViewController(activitiesVC, animated: true)
    }
    
    @IBAction func btnSignOutTapped(_ sender: UIButton) {
        self.prepareSignOutOnBoarding()
    }
    
    @IBAction func btnShareApp(_ sender: UIButton) {
        self.shareApp(sender: sender)
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        _appDelegator.presentAnnouncementVC()
    }
}

extension IntroVC {
    
    func shareApp(sender: UIButton) {
        let title = _appName
        let textToShare = "Garbh Sanskar Guru App is a unique and first of it’s kind mobile application to nurture the values in unborn child. This App contains total 280 days of various activities for pregnant lady. Expecting moms can perform these activities on day to day basis which is going to help overall development of child.\n"
        let appStoreText = "Link for App Store"
        let playStoreText = "Link for Play Store"
        let myWebsite = URL(string: "https://itunes.apple.com/us/app/majestic-garbh-sanskar/id1448235489?ls=1&mt=8")
        let otherWebsite = URL(string: "http://play.google.com/store/apps/details?id=com.gs.garbhsanskarguru")
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [textToShare,appStoreText,myWebsite!,playStoreText,otherWebsite!], applicationActivities: nil)
        activityViewController.setValue(title, forKey: "Subject")
        activityViewController.popoverPresentationController?.sourceView = (sender)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension IntroVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
        if (_user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed) || (_user.app_stop == "Pause") {
            return 5
        }else{
            return 7
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 1 || indexPath.row == 3)
        {
            return (UIDevice.deviceType == .iPad) ? 225 : 150
        }
        else if(indexPath.row == 5)
        {
            //            if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
            if (_user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed) || (_user.app_stop == "Pause"){
                return UITableView.automaticDimension
            }
            else{
                return (UIDevice.deviceType == .iPad) ? 300 : 200
            }
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 1)
        {
            let cell: IntroBasicInfoCell = tableView.dequeueReusableCell(withIdentifier: "IntroBasicInfoCell", for: indexPath) as! IntroBasicInfoCell
            
            cell.vwSwipe.bounces = false
            cell.vwSwipe.isPagingEnabled = false
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                cell.vwSwipe.reloadData()
            }
            
            cell.selectionStyle = .none
            return cell
        }
        if(indexPath.row == 2)
        {
            let cell: IntroAppTitleCell = tableView.dequeueReusableCell(withIdentifier: "IntroAppTitleCell", for: indexPath) as! IntroAppTitleCell
            
            cell.lblTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "About App", value: "", table: nil)
            
            cell.selectionStyle = .none
            return cell
        }
        if(indexPath.row == 3)
        {
            let cell: IntroAppInfoCell = tableView.dequeueReusableCell(withIdentifier: "IntroAppInfoCell", for: indexPath) as! IntroAppInfoCell
            cell.colInfo.reloadData()
            cell.selectionStyle = .none
            return cell
            //        }else if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
        }else if (_user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed) || (_user.app_stop == "Pause"){
            
            if(indexPath.row == 4){
                let cell: JoinUsInfoCell = tableView.dequeueReusableCell(withIdentifier: "JoinUsInfoCell", for: indexPath) as! JoinUsInfoCell
                
                /*cell.lblPriceforOutsideIndia.text = "*Price for the users outside of India: \(_user.usdPriceDemoApp)$(Trial) and \(_user.usdPrice)$(Full Course)"
                 
                 cell.lblStartFromAnyDay.text = _appDelegator.languageBundle?.localizedString(forKey: "This course could be started from any day of pregnancy.", value: "", table: nil)*/
                cell.lblJoinFreeCounselling.text = _appDelegator.languageBundle?.localizedString(forKey: "Join us for free counselling on \"Garbhsanskar\"", value: "", table: nil)
                
                cell.selectionStyle = .none
                return cell
            }else{
                let cell: IntroTitleCell = tableView.dequeueReusableCell(withIdentifier: "IntroTitleCell", for: indexPath) as! IntroTitleCell
                
                if(indexPath.row == 0)
                {
                    cell.lblTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "Basic Information", value: "", table: nil)
                }
                //                else if(indexPath.row == 4)
                //                {
                //                    cell.lblTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "Daily Practice of Garbhsanskar", value: "", table: nil)
                //                }
                
                cell.selectionStyle = .none
                return cell
            }
        }else {
            if(indexPath.row == 5)
            {
                let cell: PaymentInfoCell = tableView.dequeueReusableCell(withIdentifier: "PaymentInfoCell", for: indexPath) as! PaymentInfoCell
                let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
                if isPaymentAllowed {
                    cell.btnClickHere.isHidden = false
                    let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
                    switch currLanguage {
                    case .hindi:
                        //                        cell.imgGS.image = UIImage.init(named: "free_poster_hindi.jpg")
                        cell.imgGS.image = UIImage.gif(name: "App-Banner-Hindi")
                    case .gujarati:
                        //                        cell.imgGS.image = UIImage.init(named: "free_poster_guj.jpg")
                        cell.imgGS.image = UIImage.gif(name: "App-Banner-Gujarati")
                    case .english:
                        //                        cell.imgGS.image = UIImage.init(named: "free_poster_eng.jpg")
                        cell.imgGS.image = UIImage.gif(name: "App-Banner-English")
                    }
                }
                else
                {
                    cell.btnClickHere.isHidden = true
                    cell.imgGS.image = UIImage.init(named: "english_for IOS")
                }
                
                /*cell.lblFullCourse.text = _appDelegator.languageBundle?.localizedString(forKey: "Full Course", value: "", table: nil)
                 cell.lblCareForBaby.text = _appDelegator.languageBundle?.localizedString(forKey: "Yes, we care for our baby", value: "", table: nil)
                 cell.lblStartDailyCourse.text = _appDelegator.languageBundle?.localizedString(forKey: "Start Daily Course Of Garbhsanskar", value: "", table: nil)
                 cell.lblFullInJust.text = _appDelegator.languageBundle?.localizedString(forKey: "In Just", value: "", table: nil)
                 cell.lblFullPrice.text = "₹ \(_user.amount)"
                 
                 //_user.amount
                 
                 cell.lblTrialCourse.text = _appDelegator.languageBundle?.localizedString(forKey: "Trial Course", value: "", table: nil)
                 cell.lblStillConfused.text = _appDelegator.languageBundle?.localizedString(forKey: "Still CONFUSED about full course?", value: "", table: nil)
                 cell.lblTry3DaysDemo.text = _appDelegator.languageBundle?.localizedString(forKey: "Try 3 Days Demo Course Of Garbhsanskar", value: "", table: nil)
                 cell.lblTrialInJust.text = _appDelegator.languageBundle?.localizedString(forKey: "In Just", value: "", table: nil)
                 cell.lblTrialPrice.text = "₹ \(_user.amountDemoApp)"*/
                
                cell.selectionStyle = .none
                return cell
            }
            else if(indexPath.row == 6)
            {
                let cell: JoinUsInfoCell = tableView.dequeueReusableCell(withIdentifier: "JoinUsInfoCell", for: indexPath) as! JoinUsInfoCell
                
                /*cell.lblPriceforOutsideIndia.text = "*Price for the users outside of India: \(_user.usdPriceDemoApp)$(Trial) and \(_user.usdPrice)$(Full Course)"
                 
                 cell.lblStartFromAnyDay.text = _appDelegator.languageBundle?.localizedString(forKey: "This course could be started from any day of pregnancy.", value: "", table: nil)*/
                cell.lblJoinFreeCounselling.text = _appDelegator.languageBundle?.localizedString(forKey: "Join us for free counselling on \"Garbhsanskar\"", value: "", table: nil)
                
                cell.selectionStyle = .none
                return cell
            }else{
                let cell: IntroTitleCell = tableView.dequeueReusableCell(withIdentifier: "IntroTitleCell", for: indexPath) as! IntroTitleCell
                
                if(indexPath.row == 0)
                {
                    cell.lblTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "Basic Information", value: "", table: nil)
                }
                else if(indexPath.row == 4)
                {
                    cell.lblTitle.text = _appDelegator.languageBundle?.localizedString(forKey: "Daily Practice of Garbhsanskar", value: "", table: nil)
                }
                
                cell.selectionStyle = .none
                return cell
            }
        }
        /*
         let cell: IntroTableCell
         let cellId = indexPath.row == arrData.count ? "cell2" : "cell1"
         cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! IntroTableCell
         if cellId.isEqual(str: "cell1") {
         cell.setTitle(data: arrData[indexPath.row])
         }
         return cell
         */
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        return
        
        guard indexPath.row != arrData.count else {return}
        DispatchQueue.main.async {
            if indexPath.row == self.arrData.count - 1 {
                self.performSegue(withIdentifier: "segueJourney", sender: nil)
            } else {
                self.performSegue(withIdentifier: "introDetailSegue", sender: self.arrData[indexPath.row])
            }
        }
    }
}


extension IntroVC {
    
    func getRahasyaTitle(){
        self.showCentralSpinner()
        KPWebCall.call.getRahasyaTitle { (json, statusCode) in
            self.hideCentralSpinner()
            self.arrData = []
            if statusCode == 200,let dict = json as? NSDictionary{
                if let arrResult = dict["results"] as? [NSDictionary]{
                    for result in arrResult{
                        let obj = ModelRahasyaTitle(dict: result)
                        self.arrData.append(obj)
                    }
                    self.arrData.removeLast()
                }else{
                    self.showError(data: json, view: self.view)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func initModel()
    {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        
        switch currLanguage {
        case .hindi:
            arrBasicInfo = [ModelBasicInfo(id: kBasicInfo_DivineMystery, image: "n_1"), ModelBasicInfo(id: kBasicInfo_PregnancyTips, image: "n_2"), ModelBasicInfo(id: kBasicInfo_BundleOfJoy, image: "n_3")]
            arrAppInfo = [ModelBasicInfo(id: kBasicInfo_AppBenefits, image: "n_4"), ModelBasicInfo(id: kBasicInfo_AppInformation, image: "n_5"), ModelBasicInfo(id: kBasicInfo_FAQ, image: "n_6")]
        case .english:
            arrBasicInfo = [ModelBasicInfo(id: kBasicInfo_DivineMystery, image: "n_1"), ModelBasicInfo(id: kBasicInfo_PregnancyTips, image: "n_2"), ModelBasicInfo(id: kBasicInfo_BundleOfJoy, image: "n_3"),ModelBasicInfo(id: kBasicInfo_AppBenefits, image: "n_4"), ModelBasicInfo(id: kBasicInfo_AppInformation, image: "n_5"), ModelBasicInfo(id: kBasicInfo_FAQ, image: "n_6")]
            arrAppInfo = [ModelBasicInfo(id: kBasicInfo_AppBenefits, image: "n_4"), ModelBasicInfo(id: kBasicInfo_AppInformation, image: "n_5"), ModelBasicInfo(id: kBasicInfo_FAQ, image: "n_6")]
        case .gujarati:
            arrBasicInfo = [ModelBasicInfo(id: kBasicInfo_DivineMystery, image: "n_1"), ModelBasicInfo(id: kBasicInfo_PregnancyTips, image: "n_2"), ModelBasicInfo(id: kBasicInfo_BundleOfJoy, image: "n_3")]
            arrAppInfo = [ModelBasicInfo(id: kBasicInfo_AppBenefits, image: "n_4"), ModelBasicInfo(id: kBasicInfo_AppInformation, image: "n_5"), ModelBasicInfo(id: kBasicInfo_FAQ, image: "n_6")]
        }
        tableView.reloadData()
    }
}

extension IntroVC {
    
    //MARK: SwipeView DataSource & Delegate
    func numberOfItems(in swipeView: SwipeView!) -> Int
    {
        return 3
    }
    
    func swipeView(_ swipeView: SwipeView!, viewForItemAt index: Int, reusing view: UIView!) -> UIView!
    {
        let cell = UIView.init(frame: CGRect.init(x: 0, y: 0, width: swipeView.frame.size.height+10, height: swipeView.frame.size.height))
        
        let imgVw = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: swipeView.frame.size.height, height: swipeView.frame.size.height))
        
        cell.clipsToBounds = true
        imgVw.clipsToBounds = true
        imgVw.image = UIImage(named: arrBasicInfo[index].image)
        imgVw.layer.cornerRadius = (UIDevice.deviceType == .iPad) ? 20 : 12
        cell.addSubview(imgVw)
        
        return cell
    }
    
    func swipeView(_ swipeView: SwipeView!, didSelectItemAt index: Int) {
        
        DispatchQueue.main.async {            
            switch (self.arrBasicInfo[index].id) {
            case kBasicInfo_DivineMystery:
                self.performSegue(withIdentifier: "garbhSanskarInfoSegue", sender: nil)
                break
            case kBasicInfo_PregnancyTips:
                self.performSegue(withIdentifier: "garbhSanskarInfoSegue", sender: nil)
                break
            case kBasicInfo_BundleOfJoy:
                self.performSegue(withIdentifier: "garbhSanskarInfoSegue", sender: nil)
                break
            default:
                break
            }
        }
    }
}
