//
//  DearMomVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

enum ActivitySeenStatus: String {
    case done = "Done"
    case undone = "Undone"
}

class ModelDearMom {
    
    let id: Int
    let activityId: Int
    let day: Int
    let engImage: String
    let gujImage: String
    let hindiImage: String
    let urlStr: String
    let activitySeen: ActivitySeenStatus
    
    init(dict: NSDictionary) {
        id = dict.getIntValue(key: "id")
        activityId = dict.getIntValue(key: "activity_id")
        day = dict.getIntValue(key: "day")
        
        engImage = dict.getStringValue(key: "eng_image")
        hindiImage = dict.getStringValue(key: "hindi_image")
        gujImage = dict.getStringValue(key: "guj_image")
        
        urlStr = dict.getStringValue(key: "url")
        activitySeen = ActivitySeenStatus(rawValue: dict.getStringValue(key: "activity_seen")) ?? .undone
    }
    
    func getMomImage() -> URL? {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        switch currLanguage {
        case .hindi:
            return URL(string: urlStr + hindiImage)
        case .english:
            return URL(string: urlStr + engImage)
        case .gujarati:
            return URL(string: urlStr + gujImage)
        }
    }
}

class DearMomVC: ParentViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var roundView: UIView!
    
    var data: ModelDearMom!
    var isActivityDone: Bool = false
    var selected_DashboardId:Int? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension DearMomVC {
    
    func prepareUI() {
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 5.0
        
        
        self.roundView.backgroundColor = self.isActivityDone ? GarbhColor.green : GarbhColor.pink
        
        self.getDearMomData { (done) in
            if done {
                self.imgView.kf.setImage(with: self.data.getMomImage(), placeholder: _placeImage)
                //self.roundView.backgroundColor = self.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
            }
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = TaskCompleteView.instantiateView(with: self.view, with: "Dear Mom")
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                self.getActivityDone(with: "dear_mom", activityId: String(self.data.activityId), dailyActivityId: "\(self.data.id)") { (completed) in
                    if completed {
                        onBoardingView.removeFromSuperview()
                        NotificationCenter.default.post(name: Notification.Name(garbhNotificationUpdateActicityStatus), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    func openDisaplyInfoView() {
        let onBoardingView = DisplayInfoView.instantiateView(with: self.view)
        onBoardingView.setupUI()
        onBoardingView.setupValues(valueIndex: self.selected_DashboardId!)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
        }
    }
}

extension DearMomVC {
    
    @IBAction func btnTaskTapped(_ sender: UIButton) {
        
        if(isActivityDone){
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kTaskCompleted)
        }
        else {
            self.prepareOnBoarding()
        }
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
           self.openDisaplyInfoView()
       }
}


extension DearMomVC {
    
    func getDearMomData(block: @escaping(Bool) -> ()) {
        let param: [String: Any] = ["current_day": _user.currDay, "user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getMomData(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let dictData = dict["data"] as? NSDictionary {
                    weakself.data = ModelDearMom(dict: dictData)
                    block(true)
                } else {
                  weakself.showError(data: json, view: weakself.view)
                  block(false)
                }
            }
        }
    }
}

extension DearMomVC {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        scrollview.flashScrollIndicators()
    }
    
}
