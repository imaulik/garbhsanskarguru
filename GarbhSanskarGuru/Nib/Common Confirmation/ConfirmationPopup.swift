//
//  ConfirmationPopup.swift
//  GarbhSanskarGuru
//
//  Created by Tushar on 24/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ConfirmationPopup: UIView {
    
    var actionBlock: (KAction)->Void = {_ in}
    
    var isCancel:Bool = false
    
    @IBOutlet var viewMain: UIView!
    @IBOutlet var animatedViews: UIView!
    
    @IBOutlet var buttonYes: UIButton!
    @IBOutlet var buttonNo: UIButton!
    
    @IBOutlet var stack_title: UIStackView!
    @IBOutlet var stack_button: UIStackView!
    
    @IBOutlet var label_title: JPWidthLabel!
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> ConfirmationPopup {
        let objView = UINib(nibName: "ConfirmationPopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ConfirmationPopup
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
    
    func updateUI(keyName:String, isCancelable:Bool = false, isSingleButton:Bool = false, isShowTitle:Bool = false, buttonTitle:String = "YES"){
        label_title.text = _appDelegator.languageBundle?.localizedString(forKey: "confirm_\(keyName)", value: "", table: nil)
        self.isCancel = isCancelable
        if isSingleButton{
            self.stack_button.isHidden = true
            self.buttonYes.setTitle(buttonTitle, for: .normal)
        }else{
            self.stack_button.isHidden = false
        }
        
        if isShowTitle{
            self.stack_title.isHidden = false
        }else{
            self.stack_title.isHidden = true
        }
    }
    
    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews.transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
            
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.animatedViews.transform = .identity
            }, completion: { _ in
            })
        })
    }
    
    
    @IBAction func onBtnYesAction(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func onViewCancel(_ sender: UIControl) {
        if isCancel{
            self.actionBlock(.cancel)
        }
    }
    
    @IBAction func onBtnNoAction(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
}
