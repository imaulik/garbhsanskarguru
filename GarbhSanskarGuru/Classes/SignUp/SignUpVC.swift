//
//  SignUpVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SignUpVC: ParentViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var pageView: [UIView]!
    @IBOutlet weak var btnNext: UIButton!
    var strViewName = "SignUp"
    
    var childSP1: SignUpFirstStep {
        return (self.children[0] as? SignUpFirstStep)!
    }
    
    var childSP2: SignupSecondStep {
        return (self.children[1] as? SignupSecondStep)!
    }
    
    var childSP3: SignupThirdStep {
        return (self.children[2] as? SignupThirdStep)!
    }
    
    var childAgreement: SignUpVC2 {
        return (self.children[3] as? SignUpVC2)!
    }
    
    // commented.
    // Old design code.
    
//    var childSignUp: SignUpVC1
//    {
//        return (self.children[0] as? SignUpVC1)!
//    }
    
    var childCountry: SignUpVC3
    {
        return (self.children[1] as? SignUpVC3)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension SignUpVC {
    
    func prepareUI()
    {
        scrollView.delaysContentTouches = false
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        if isPaymentAllowed
        {
            _ = pageView.map{$0.isHidden = false}
            scrollView.isScrollEnabled = true
        }
        else
        {
            _ = pageView.map{$0.isHidden = true}
            scrollView.isScrollEnabled = true
        }
    }
    
    func navigateToIntro() {
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        if isPaymentAllowed {
            //Abhi comment for free dashboard
            
//            DispatchQueue.main.async {
//                self.performSegue(withIdentifier: "introSegue", sender: nil)
//            }
            
            // Uncomment code
            _appDelegator.navigateDirectlyToHome()
            // comment code
//            let basicDetailVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BasicDetailVC") as! BasicDetailVC
//            self.navigationController?.pushViewController(basicDetailVC, animated: true)
        } else {
            _appDelegator.navigateDirectlyToHome()
            // comment below code and write above line for after change signup flow no need to add city/country/state. Commneted
//            let basicDetailVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BasicDetailVC") as! BasicDetailVC
//            self.navigationController?.pushViewController(basicDetailVC, animated: true)
        }
    }
    
    func setPageView(at page: Int)
    {
        for(idx,pgView) in pageView.enumerated()
        {
            if idx == page
            {
                pgView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1)
            }
            else
            {
                pgView.backgroundColor = #colorLiteral(red: 0.5704585314, green: 0.5704723597, blue: 0.5704649091, alpha: 1)
            }
        }
    }
       
    fileprivate func scrollToDisclaimer(index : Int)
    {
        let scrollHeight = scrollView.frame.height
        let scrollWidth = scrollView.frame.width
        scrollView.scrollRectToVisible(CGRect(x: CGFloat(index) * scrollWidth , y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let currPage = scrollView.currentPage
        setPageView(at: currPage)
        
        let title = currPage == (pageView.count - 1) ? "Hurrey ..!\nClick here to start App" : "Next"
        
        let pregStatus = _appDelegator.objInfo.planingStatus
        // old code
        //childSignUp.objInfo.arrFiled[4].text
        let status: PregnancyStatus = PregnancyStatus(rawValue: pregStatus!) ?? .none
        childAgreement.prepareUI(with: status)
        btnNext.titleLabel?.lineBreakMode  = NSLineBreakMode.byWordWrapping
        btnNext.titleLabel?.textAlignment = .center
        btnNext.setTitle(title, for: .normal)
    }
    
    // Old design flow commented.
        
    //    fileprivate func scrollToDisclaimer()
    //    {
    //        if strViewName == "SignUp"
    //        {
    //            let scrollHeight = scrollView.frame.height
    //            let scrollWidth = scrollView.frame.width
    //            scrollView.scrollRectToVisible(CGRect(x: scrollWidth, y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    //        }
    //        else
    //        {
    //            let scrollHeight = scrollView.frame.height
    //            var scrollWidth = scrollView.frame.width
    //            if strViewName == "details"{
    //                scrollWidth = scrollWidth + UIScreen.width
    //            }
    //            scrollView.scrollRectToVisible(CGRect(x: scrollWidth, y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    //        }
    //    }
}

extension SignUpVC {
    
    // New design flow.
    
    @IBAction func btnNextTapped(_ sender: UIButton){
        let currPage = scrollView.currentPage
        if currPage == 0 || currPage == 1{
            scrollToDisclaimer(index: currPage + 1)
            return
        }
        childSP3.addValues()
        let validate = _appDelegator.objInfo.isValidData(validFor: 2)
        if validate.isValid{
            scrollToDisclaimer(index: currPage + 1)
            // comment payment check condition, Commented. Validation should allow and check without payment and with payment.
//            let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
//            if isPaymentAllowed{
                scrollToDisclaimer(index: currPage + 1)
                if currPage == 2{
                    return
                }
                if childAgreement.btnAgreement.isSelected{
                    print("Register Sucess")
                    self.registerUser()
                }else{
                    _ = ValidationToast.showStatusMessage(message: "Please Accept the Licence Agreement")
                }
//            }else{
//                print("Register Sucess")
//                self.registerUser()
//            }
        }else{
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }

//        let validate = _appDelegator.objInfo.isValidData(validFor: 1)
//        if validate.isValid {
//            scrollToDisclaimer(index: currPage + 1)
//            childSP3.addValues()
//            let validate = _appDelegator.objInfo.isValidData(validFor: 2)
//            if validate.isValid{
//                scrollToDisclaimer(index: currPage + 1)
//                let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
//                if isPaymentAllowed{
//                    scrollToDisclaimer(index: currPage + 1)
//                    if childAgreement.btnAgreement.isSelected{
//                        print("Register Sucess")
//                    }else{
//                        _ = ValidationToast.showStatusMessage(message: "Please Accept the Licence Agreement")
//                    }
//                }else{
//                    print("Register Sucess")
//                }
//            }else{
//                _ = ValidationToast.showStatusMessage(message: validate.error)
//            }
//        }else{
//            _ = ValidationToast.showStatusMessage(message: validate.error)
//        }
    }
    
    
    @IBAction func btnNextTapped1(_ sender: UIButton)
    {
        let currPage = scrollView.currentPage
        var isvalidate = false
        
        if currPage == 0{
            // initial screen
            isvalidate = true
        }else if currPage == 1{
            // pregnancy status screen
            let validate = _appDelegator.objInfo.isValidData(validFor: 1)
            if validate.isValid {
                isvalidate = true
            }else{
                _ = ValidationToast.showStatusMessage(message: validate.error)
            }
        }else if currPage == 2 {
            childSP3.addValues()
            let validate = _appDelegator.objInfo.isValidData(validFor: 2)
            if validate.isValid {
                isvalidate = true
            }else{
                _ = ValidationToast.showStatusMessage(message: validate.error)
            }
        }else{
            let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
            if isPaymentAllowed{
                if childAgreement.btnAgreement.isSelected{
                    isvalidate = true
                    print("Register Sucess")
//                    self.registerUser()
                }else{
                    _ = ValidationToast.showStatusMessage(message: "Please Accept the Licence Agreement")
                }
            }else{
                print("Register Sucess")
//                self.registerUser()
            }
        }
        
        if isvalidate{
            scrollToDisclaimer(index: currPage + 1)
        }
    }
    
    
    // Old Design flow commented.
    
//    @IBAction func btnNextTapped(_ sender: UIButton)
//    {
//
//        let validate = childSignUp.objInfo.isValidData()
//        if validate.isValid {
//            if scrollView.contentOffset.x == 0{
//                scrollToDisclaimer()
//                return
//            }
//            strViewName = "country"
//            let validate = childCountry.basicInfoData.isValidData()
//            if validate.isValid{
//                let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
//                if isPaymentAllowed
//                {
//                    if childAgreement.btnAgreement.isSelected
//                    {
//                        strViewName = "details"
//                        scrollToDisclaimer()
//                        self.registerUser()
//                    }
//                    else
//                    {
//                        strViewName = "details"
//                        if scrollView.contentOffset.x == UIScreen.width{
//                            scrollToDisclaimer()
//                        }else{
//                            _ = ValidationToast.showStatusMessage(message: "Please Accept the Licence Agreement")
//                        }
//                    }
//                }
//                else
//                {
//                    self.registerUser()
//                }
//            }else{
//                _ = ValidationToast.showStatusMessage(message: validate.error)
//            }
//        }
//        else
//        {
//            _ = ValidationToast.showStatusMessage(message: validate.error)
//        }
//    }
}

extension SignUpVC {
    
    // New design flow.
    func registerUser()
    {
        let paramDict = _appDelegator.objInfo.paramDict()
        self.showCentralSpinner()
        KPWebCall.call.registerUser(param: paramDict) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                if let userInfo = dict["data"] as? NSDictionary, let token = userInfo["token"] as? String {
                    _appDelegator.storeAuthorizationToken(strToken: token)
                    KPWebCall.call.setAccesTokenToHeader(token: token)
                    DispatchQueue.main.async {
                        _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
                        _user.initWith(entry: userInfo)
                        _appDelegator.saveContext()
                    
                        let checkfreedemoUser =  userInfo["free_demo_payment"]
                        let checkuser = checkfreedemoUser as? String == "No" ? false  : true
                        UserDefaults.standard.set(checkuser, forKey: "checkdemoUser") //Bool
                        
                        createReminderNotificatinsForNextSevenDays()
                        weakself.navigateToIntro()
                        
                        // Add new api call.
//                        weakself.updateProfile { (done) in
//                            if done {
//                                createReminderNotificatinsForNextSevenDays()
//                                weakself.navigateToIntro()
//                            }
//                        }
                    }
                }else {
                    weakself.showError(data: json, view: weakself.view)
                }
            }else if let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 401  {
                if let emaildict = dict["message"] as? NSDictionary {
                    weakself.showError(data: emaildict, view: weakself.view)
                }
            } else {
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
    
    func updateProfile(block: @escaping (Bool) -> ()) {
        let param = childCountry.basicInfoData.paramDict()
        self.showCentralSpinner()
        KPWebCall.call.updateUserProfile(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    DispatchQueue.main.async {
                        _user.setRegionData(dict: data)
                        _appDelegator.saveContext()
                    }
                    block(true)
                }else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
    
    // Old design flow commented.
    
//    func updateProfile(block: @escaping (Bool) -> ()) {
//        let param = childCountry.basicInfoData.paramDict()
//        self.showCentralSpinner()
//        KPWebCall.call.updateUserProfile(param: param) { (json, statusCode) in
//            self.hideCentralSpinner()
//            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
//                if status == 200, let data = dict["data"] as? NSDictionary {
//                    DispatchQueue.main.async {
//                        _user.setRegionData(dict: data)
//                        _appDelegator.saveContext()
//                    }
//                    block(true)
//                }else {
//                    self.showError(data: json, view: self.view)
//                    block(false)
//                }
//            }else {
//                self.showError(data: json, view: self.view)
//                block(false)
//            }
//        }
//    }
        
//    func registerUser()
//    {
//        let paramDict = childSignUp.objInfo.paramDict()
//        self.showCentralSpinner()
//        KPWebCall.call.registerUser(param: paramDict) { [weak self] (json, statusCode) in
//            guard let weakself = self else {return}
//            weakself.hideCentralSpinner()
//            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
//                if let userInfo = dict["data"] as? NSDictionary, let token = userInfo["token"] as? String {
//                    _appDelegator.storeAuthorizationToken(strToken: token)
//                    KPWebCall.call.setAccesTokenToHeader(token: token)
//                    DispatchQueue.main.async {
//                        _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
//                        _user.initWith(entry: userInfo)
//                        _appDelegator.saveContext()
//
//                        // Add new api call.
//                        weakself.updateProfile { (done) in
//                            if done {
//                                createReminderNotificatinsForNextSevenDays()
//                                weakself.navigateToIntro()
//                            }
//                        }
//                    }
//                }else {
//                    weakself.showError(data: json, view: weakself.view)
//                }
//            }else if let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 401  {
//                if let emaildict = dict["message"] as? NSDictionary {
//                    weakself.showError(data: emaildict, view: weakself.view)
//                }
//            } else {
//                weakself.showError(data: json, view: weakself.view)
//            }
//        }
//    }
}

extension UIScrollView
{
    var currentPage: Int
    {
        return Int((self.contentOffset.x + (0.5 * self.frame.size.width)) / self.frame.width)
    }
}
