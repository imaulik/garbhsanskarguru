//
//  GarbhSamvadVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class GarbhSamvadCollCell: ConstrainedCollectionViewCell,UIScrollViewDelegate{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var scrScroll: UIScrollView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareForScaleImage()
    }

    func prepareForScaleImage(){
        scrScroll.minimumZoomScale = 0.1
        scrScroll.maximumZoomScale = 4.0
        scrScroll.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        gestureRecognizer.numberOfTapsRequired = 2
        self.imgView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateMinZoomScaleForSize(self.imgView.bounds.size)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return imgView
    }
    
    @objc func handleDoubleTap() {
        if scrScroll.zoomScale >= scrScroll.minimumZoomScale && scrScroll.zoomScale != scrScroll.maximumZoomScale {
            scrScroll.setZoomScale(scrScroll.maximumZoomScale, animated: true)
        } else {
            scrScroll.setZoomScale(scrScroll.minimumZoomScale, animated: true)
        }
    }
    
    fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / self.imgView.bounds.width
        let heightScale = size.height / self.imgView.bounds.height
        let minScale = min(widthScale, heightScale)
        
        self.scrScroll.minimumZoomScale = minScale
        self.scrScroll.zoomScale = minScale
        self.scrScroll.layoutIfNeeded()
    }
    
    func setUpImgData(data: ModelRahasyaContent){
        
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        
        switch currLanguage{
        case .hindi:
            imgView.kf.setImage(with: URL(string: data.content_Hindi), placeholder: _placeImage)
        case .english:
             imgView.kf.setImage(with: URL(string: data.content_Eng), placeholder: _placeImage)
        case .gujarati:
             imgView.kf.setImage(with: URL(string: data.content_Gujarati), placeholder: _placeImage)
        }
    }
}

class GarbhSamvadVC: ParentViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollConst: NSLayoutConstraint!
    @IBOutlet weak var btnGarbhsamvad: UIButton!
    @IBOutlet weak var btnAutoSuggestion: UIButton!
    @IBOutlet weak var roundView: UIView!

    var isActivityDone: Bool = false
    var selected_DashboardId:Int? = nil
    
    var garbhVC1: GarbhVC1 {
        return (self.children[0] as? GarbhVC1)!
    }
    
    var garbhVC2: GarbhVC2 {
        return (self.children[1] as? GarbhVC2)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension GarbhSamvadVC {
    
    func prepareUI() {
        
        self.roundView.backgroundColor = self.isActivityDone ? GarbhColor.green : GarbhColor.pink
        
        guard garbhVC1.data != nil else {return}
        //self.roundView.backgroundColor = self.garbhVC1.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
    }
}

extension GarbhSamvadVC {
    
    func prepareOnBoarding() {
        let onBoardingView = TaskCompleteView.instantiateView(with: self.view, with: "Garbh Samvad")
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                let getIndex = self.scrollView.contentOffset.x == _screenSize.width ? 1 : 0
                let activity_id = self.getLanguageForActivity(isForActivityId: true, aryGarbhsamvad: getIndex == 0 ? self.garbhVC1.data.arrGarbhSamvad : self.garbhVC2.data.arrGarbhSamvad)
                let daily_activity_id = self.getLanguageForActivity(isForActivityId: false, aryGarbhsamvad: getIndex == 0 ? self.garbhVC1.data.arrGarbhSamvad : self.garbhVC2.data.arrGarbhSamvad)
                self.getActivityDone(with: "garbhsamvad", activityId: String(activity_id), dailyActivityId: "\(daily_activity_id)") { (completed) in
                    if completed {
                        onBoardingView.removeFromSuperview()
                        NotificationCenter.default.post(name: Notification.Name(garbhNotificationUpdateActicityStatus), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    func openDisaplyInfoView() {
        let onBoardingView = DisplayInfoView.instantiateView(with: self.view)
        onBoardingView.setupUI()
        onBoardingView.setupValues(valueIndex: self.selected_DashboardId!)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
        }
    }
    
    func getLanguageForActivity(isForActivityId:Bool, aryGarbhsamvad:[GarbhSamvadDetail]) -> String{
        
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        var arysamvad = [GarbhSamvadDetail]()
        var activityId = "0"
        
        if currLanguage == .hindi{
            arysamvad = aryGarbhsamvad.filter { $0.language == DownloadLanguage.hindi }
            if isForActivityId{
                activityId = arysamvad[0].activityId
            }else{
                activityId = "\(arysamvad[0].id)"
            }
        }else if currLanguage == .english{
            arysamvad = aryGarbhsamvad.filter { $0.language == DownloadLanguage.english }
            if isForActivityId{
                activityId = arysamvad[0].activityId
            }else{
                activityId = "\(arysamvad[0].id)"
            }
        }else{
            arysamvad = aryGarbhsamvad.filter { $0.language == DownloadLanguage.gujarati }
            if isForActivityId{
                activityId = arysamvad[0].activityId
            }else{
                activityId = "\(arysamvad[0].id)"
            }
        }
        
        return activityId
    }
    
}

extension GarbhSamvadVC {
    
    @IBAction func btnGarbhsamvadTapped(_ sender: UIButton)
    {
        scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height), animated: true)
    }
    
    @IBAction func btnAutoSuggestionTapped(_ sender: UIButton)
    {
        scrollView.scrollRectToVisible(CGRect(x: scrollView.frame.width, y: 0, width: scrollView.frame.width, height: scrollView.frame.height), animated: true)
    }
    
    @IBAction func btnTaskTapped(_ sender: UIButton) {
        
        if(isActivityDone)
        {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kTaskCompleted)
        }
        else
        {
            self.prepareOnBoarding()
        }
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
           self.openDisaplyInfoView()
       }
}


extension GarbhSamvadVC {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let currPage = scrollView.currentPage
        if currPage == 0
        {
            //self.roundView.backgroundColor = self.garbhVC1.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
                btnGarbhsamvad.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .medium)
                btnAutoSuggestion.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .regular)
            scrollConst.constant = 0
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
        else
        {
            //self.roundView.backgroundColor = self.garbhVC2.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
            btnAutoSuggestion.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .medium)
            btnGarbhsamvad.titleLabel?.font = UIFont.systemFont(ofSize: 15.widthRatio, weight: .regular)
            scrollConst.constant = scrollView.frame.width / 2
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
}
