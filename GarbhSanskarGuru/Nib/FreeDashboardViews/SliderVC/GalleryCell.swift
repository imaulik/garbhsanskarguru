//
//  GalleryCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 03/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell,UIScrollViewDelegate {
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var DescriptionView: UIView!
    
    @IBOutlet weak var scrScroll: UIScrollView!
    @IBOutlet weak var imgScale: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        scrScroll.minimumZoomScale = 0.1
        scrScroll.maximumZoomScale = 4.0
        scrScroll.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        gestureRecognizer.numberOfTapsRequired = 2
        self.imgScale.addGestureRecognizer(gestureRecognizer)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateMinZoomScaleForSize(self.imgScale.bounds.size)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return imgScale
    }
    
    @objc func handleDoubleTap() {
        if scrScroll.zoomScale >= scrScroll.minimumZoomScale && scrScroll.zoomScale != scrScroll.maximumZoomScale {
            scrScroll.setZoomScale(scrScroll.maximumZoomScale, animated: true)
        } else {
            scrScroll.setZoomScale(scrScroll.minimumZoomScale, animated: true)
        }
    }
    
    fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / self.imgScale.bounds.width
        let heightScale = size.height / self.imgScale.bounds.height
        let minScale = min(widthScale, heightScale)
        
        self.scrScroll.minimumZoomScale = minScale
        self.scrScroll.zoomScale = minScale
        self.scrScroll.layoutIfNeeded()
    }
    
}
