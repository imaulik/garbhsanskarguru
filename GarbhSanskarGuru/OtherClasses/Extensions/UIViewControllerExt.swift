//
//  UIViewControllerExt.swift
//  MY HealthTracker
//
//  Created by Maulik on 29/03/20.
//  Copyright © 2020  Lets Explore . All rights reserved.
//

import UIKit
import SystemConfiguration
import SDWebImage

private func _swizzling(forClass: AnyClass, originalSelector: Selector, swizzledSelector: Selector) {
    
    if let originalMethod = class_getInstanceMethod(forClass, originalSelector),
        let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector) {
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}


//perfect for debug
func printDebug(object: Any) {
    #if DEBUG
    print(object)
    #endif
}

// 2 - print array 
func printArray<T>(_ array: [T]) {
    #if DEBUG
    for item in array {
        print(item)
    }
    #endif
}
// 2 - In Action
//printArray(strings)
//printArray(integers)

 
  

extension UIViewController{
    
    // if back button text remove globally
    open override func awakeFromNib() {
        super.awakeFromNib()
        let backBarBtnItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarBtnItem
    }
    
    // when keyboard when tap on outside
    func hideKeyBoardWhenTapAround() {
        let tapAround = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard(_:)))
        tapAround.numberOfTapsRequired = 1
        tapAround.cancelsTouchesInView = false
        view.addGestureRecognizer(tapAround)
    }
    
    @objc func hideKeyboard(_ sender: UITapGestureRecognizer?) {
        view.endEditing(true)
    }
    
    
    func showNoDataFoundImage() {
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        imgView.center = self.view.window?.center ?? CGPoint(x: 100, y: 200)
        imgView.tag = 12345
        imgView.image = UIImage(named: "no-result-found")
        self.view.addSubview(imgView)
    }
    
    func hideNoDataFoundImage() {
        for imgView in self.view.window!.subviews {
            if imgView.tag == 12345 {
                imgView.removeFromSuperview()
            }
        }
    }
    
 
    //delay method
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    // remove present style from ios13
    static func preventPageSheetPresentationStyle () {
        UIViewController.preventPageSheetPresentation
    }
    
    static let preventPageSheetPresentation: Void = {
        if #available(iOS 13, *) {
            _swizzling(forClass: UIViewController.self,
                       originalSelector: #selector(present(_: animated: completion:)),
                       swizzledSelector: #selector(_swizzledPresent(_: animated: completion:)))
        }
    }()
    
    @objc @available(iOS 13.0, *)
    private func _swizzledPresent(_ viewControllerToPresent: UIViewController,
                                  animated flag: Bool,
                                  completion: (() -> Void)? = nil) {
        if viewControllerToPresent.modalPresentationStyle == .pageSheet
            || viewControllerToPresent.modalPresentationStyle == .automatic {
            viewControllerToPresent.modalPresentationStyle = .fullScreen
        }
        _swizzledPresent(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    func getWebinars (type : String,Complition : @escaping WSBlock)  {
        let parm = ["type":type]
        KPWebCall.call.getworkshopwebinar(param: parm) { (json, statusCode) in
            Complition(json,statusCode)
        }
    }
    
    func makeCall(number: String){
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func viewbanner(banner_id :String = "10001",Complition : @escaping WSBlock)  {
        let parm = ["user_id" : _user.id ,
                    "banner_id" : banner_id]
        KPWebCall.call.viewbanner(param: parm) { (json, statusCode) in
            Complition(json,statusCode)
        }
    }
    
    
    func eventDetailAPi(Complition : @escaping WSBlock)  {
        KPWebCall.call.getEventDetail { (json, statusCode) in
            Complition(json,statusCode)
        }
    }
}
 

//get class name in string.
public protocol ClassNameProtocol {
    static var className: String { get }
    var className: String { get }
}

public extension ClassNameProtocol {
    static var className: String {
        return String(describing: self)
    }
    
    var className: String {
        return type(of: self).className
    }
}

extension NSObject: ClassNameProtocol {}


