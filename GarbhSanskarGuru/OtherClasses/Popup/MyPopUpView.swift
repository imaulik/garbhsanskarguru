

class MyPopUpView: ParentViewController {
    var pressOK : (()-> ())?
    var pressCancel : (()-> ())?
    var objData : AnyObject?
    var pressOkWithData : ((Any)-> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 0
        self.view.layer.masksToBounds = true
        self.view.frame = {
            var frm = self.view.frame
            frm.size.width = UIScreen.main.bounds.width
            frm.size.height = UIScreen.main.bounds.height
            return frm
        }()
    }
}
