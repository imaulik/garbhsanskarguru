//
//  BuyCouseView.swift
//  GarbhSanskarGuru
//
//  Created by Abhishek Ramani on 18/07/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class BuyCouseView: ConstrainedView {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var btnBuyFullCourse: UIButton!

    var actionBlock: (KAction)->Void = {_ in}

    class func instantiateView(with view: UIView) -> BuyCouseView {
        
        let objView = UINib(nibName: "BuyCouseView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BuyCouseView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        
        objView.btnBuyFullCourse.titleLabel?.numberOfLines = 0
        objView.btnBuyFullCourse.titleLabel?.textAlignment = NSTextAlignment.center
        objView.btnBuyFullCourse.layer.cornerRadius = 10

        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
}

extension BuyCouseView {

    @IBAction func btnBuyFullCoursePopUpTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
    @IBAction func btnCloseBuyCoursePopUpTapped(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
}
