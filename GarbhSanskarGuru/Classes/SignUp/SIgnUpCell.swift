//
//  SIgnUpCell.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class SIgnUpCell: ConstrainedTableViewCell {

    @IBOutlet weak var tfInput: UITextField!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var btnPlanning: UIButton!
    @IBOutlet weak var btnPregnant: UIButton!
    
    //MARK: - Variables
    weak var parentVC: SignUpVC1!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(_ field: UserFiled, tag: Int) {
        switch field.cellType {
        case .txtcell:
            tfInput.tag = tag
            tfInput.placeholder = field.placeHolder
            tfInput.text = field.text
            tfInput.autocorrectionType = .no
            tfInput.autocapitalizationType = .none
            tfInput.keyboardType = .asciiCapable
            tfInput.returnKeyType = .next
            tfInput.isSecureTextEntry = false
            tfInput.inputAccessoryView = nil
            btnShowPass.isHidden = true
            switch tag{
            case 0:
                tfInput.autocapitalizationType = .words
                break
            case 1:
                tfInput.keyboardType = .emailAddress
                break
            case 2:
                tfInput.isSecureTextEntry = true
                btnShowPass.isHidden = false
                break
            case 3:
                tfInput.inputAccessoryView = parentVC.viewAccessory
                tfInput.keyboardType = .numberPad
                break
            default:
                break
            }
        case .infoCell:
            break
        case .none:
            break
        }
    }
}

//MARK: - UITextView Delegate Methods
extension SIgnUpCell: UITextFieldDelegate{
    
    @IBAction func btnStatusTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            parentVC.objInfo.arrFiled[4].text = "planing"
            btnPlanning.isSelected = true
            btnPregnant.isSelected = false
        }else {
            parentVC.objInfo.arrFiled[4].text = "pregnent"
            btnPlanning.isSelected = false
            btnPregnant.isSelected = true
        }
    }
    
    @IBAction func btnShowPass(_ sender: UIButton) {
        guard !tfInput.text!.isEmpty else {return}
        sender.isSelected = !sender.isSelected
        tfInput.isSecureTextEntry = !tfInput.isSecureTextEntry
    }
    
    @IBAction func tftDidChange(_ textfield: UITextField){
        parentVC.objInfo.arrFiled[textfield.tag].text = textfield.text!.trimmedString()
    }
    
    func becomeResponderAgain(textfield: UITextField) {
        if let index = parentVC.tableView.indexPath(for: self) {
            if index.row == 1 {
                self.parentVC.objInfo.arrFiled[index.row].text.removeAll()
                textfield.text?.removeAll()
                textfield.viewWithTag(index.row)?.becomeFirstResponder()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next{
            if let index = parentVC.tableView.indexPath(for: self){
                if let cell = parentVC.getEntryCell(index.row + 1){
                    if index.row == 1{
                        parentVC.view.endEditing(true)
                        let email = parentVC.objInfo.arrFiled[textField.tag].text
                        guard !email.isEmpty else {
                            _ = KPValidationToast.shared.showToastOnStatusBar(message: kEnterEmail)
                             self.becomeResponderAgain(textfield: textField)
                           return false}
                        guard email.isValidEmailAddress()  else {
                           _ =  KPValidationToast.shared.showToastOnStatusBar(message: kInvalidEmail)
                            self.becomeResponderAgain(textfield: textField)
                            return false
                        }
                        parentVC.checkEmailExist(email: email) { (done) in
                            if done {
                                cell.tfInput.becomeFirstResponder()
                            }else {
                                self.becomeResponderAgain(textfield: textField)
                            }
                        }
                    }else {
                        cell.tfInput.becomeFirstResponder()
                    }
                }
            }else{
                textField.resignFirstResponder()
            }
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
