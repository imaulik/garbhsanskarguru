//
//  TodayStoryVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 09/03/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import WebKit

class TodayStoryVC: ParentViewController {
    
    @IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var playerBarView: UIView!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var roundView1: UIView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var btnVideo: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var webVideo: WKWebView!
    
    var data: ModelGarbhSamvad!
    var data1: ModelGarbhSatsang!
    var screenType: GarbhScreenType = .todayStory
    var isActivityDone: Bool = false
    var selected_DashboardId:Int? = nil
    var timer: Timer?
    var videoFilter:[RandomVideoData]? = nil
    var currentLang:AppLanguage = .english
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepereForTimer()
        prepareUI()
        getUserActivityData()
        print("Current title : \(String(describing: self.lblTitle?.text))")
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    func prepereForTimer(){
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(prepereForAnimation), userInfo: nil, repeats: true)
    }
    
    @objc func prepereForAnimation(){
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.imgVideo.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 2.0, animations: {() -> Void in
                self.imgVideo.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        })
        
    }
}

extension TodayStoryVC {
    
    func prepareUI() {
        self.lblTitle?.text = screenType.navTitle
        btnVideo.isHidden = true
        imgVideo.isHidden = true
        viewVideo.isHidden = true
        btnLeft.isHidden = true
        //abhi added
        btnRight.isHidden = true
        //-----------
        
        baseView.isHidden = true
        
        self.roundView.backgroundColor = self.isActivityDone ? GarbhColor.green : GarbhColor.pink
        self.roundView1.backgroundColor = self.isActivityDone ? GarbhColor.green : GarbhColor.pink
        
        setLayout()
    }
    
    func setLayout(){
        layout.scrollDirection = .horizontal
        collectionView.isPagingEnabled = true
        layout.itemSize = CGSize(width: _screenSize.width, height: _screenSize.height - _navigationHeight.widthRatio)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    
    fileprivate func setUpGarbhUI() {
        
        if self.screenType == .garbhSatsang
        {
            switch data1.type {
            case .image:
                self.baseView.isHidden = false
                self.navBarView.isHidden = false
                self.playerView.isHidden = true
                //self.roundView1.backgroundColor = self.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
                
                //abhi added
                if(self.data1.arrGarbhSamvad.count > 1){
                    btnRight.isHidden = false
                }
                //-----------
                
                self.collectionView.reloadData()
            case .video:
                self.playerBarView.isHidden = false
                self.baseView.isHidden = true
                self.navBarView.isHidden = true
                self.collectionView.isHidden = true
                //self.roundView.backgroundColor = self.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
                guard !self.data1.arrGarbhSamvad.isEmpty else {return}
                
                if self.lblTitle!.text == "Prayer" || self.lblTitle!.text == "Garbh Satsang"{
                    self.setUPlayer(with: self.data1.arrGarbhSamvad[0].getYoutubeId)
                }else{
                    self.checkVideoStatus(playerId: self.data1.arrGarbhSamvad[0].getYoutubeId!,block: { (done) in
                        if !done{
                            self.getRandomVideos { (done,videoUrl) in
                                guard let videoLink = videoUrl else { return }
                                self.checkVideoStatusFirst(playerId: videoLink) { (videoId, status) in
                                    if status{
                                        // play
                                        self.setUPlayer(with: videoId!)
                                    }else{
                                        // check again
                                        let videourl = self.currentLang == .hindi ? self.videoFilter![1].hindiUrl : self.currentLang == .english ? self.videoFilter![1].engUrl : self.videoFilter![1].gujUrl
                                        let youtTubeId = self.getYoutubeId(StrLink: videourl!)
                                        self.checkVideoStatusFirst(playerId: youtTubeId!) { (videoId, status) in
                                            if status{
                                                // play video
                                                self.setUPlayer(with: videoId!)
                                            }else{
                                                // no need check again.
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            self.setUPlayer(with: self.data1.arrGarbhSamvad[0].getYoutubeId)
                        }
                    })
                }
            }
        }
        else
        {
            switch data.type {
            case .image:
                self.baseView.isHidden = false
                self.navBarView.isHidden = false
                self.playerView.isHidden = true
                //self.roundView1.backgroundColor = self.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
                
                //abhi added
                if(self.data.arrGarbhSamvad.count > 1){
                    btnRight.isHidden = false
                }
                //-----------
                
                self.collectionView.reloadData()
            case .video:
                self.playerBarView.isHidden = false
                self.baseView.isHidden = true
                self.navBarView.isHidden = true
                self.collectionView.isHidden = true
                //self.roundView.backgroundColor = self.data.activitySeen == .done ? #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1) : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)
                guard !self.data.arrGarbhSamvad.isEmpty else {return}
                
                if self.lblTitle!.text == "Prayer" || self.lblTitle!.text == "Garbh Satsang"{
                    self.setUPlayer(with: self.data.arrGarbhSamvad[0].getYoutubeId)
                }else{
                    self.checkVideoStatus(playerId: self.data.arrGarbhSamvad[0].getYoutubeId! ,block: { (done) in
                        if !done{
                            self.getRandomVideos { (done,videoUrl) in
                                guard let videoLink = videoUrl else { return }
                                self.checkVideoStatusFirst(playerId: videoLink) { (videoId, status) in
                                    if status{
                                        // play
                                        self.setUPlayer(with: videoId!)
                                    }else{
                                        // check again
                                        let videourl = self.currentLang == .hindi ? self.videoFilter![1].hindiUrl : self.currentLang == .english ? self.videoFilter![1].engUrl : self.videoFilter![1].gujUrl
                                        let youtTubeId = self.getYoutubeId(StrLink: videourl!)
                                        self.checkVideoStatusFirst(playerId: youtTubeId!) { (videoId, status) in
                                            if status{
                                                // play video
                                                self.setUPlayer(with: videoId!)
                                            }else{
                                                // no need check again.
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            self.setUPlayer(with: self.data.arrGarbhSamvad[0].getYoutubeId)
                        }
                    })
                }
            }
        }
    }
    
    func filterCollData() {
        if self.screenType == .garbhSatsang
        {
            let storyDetail: [GarbhSatsangDetail]
            let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
            switch currLanguage {
            case .hindi:
                storyDetail = self.data1.arrGarbhSamvad.filter{$0.language == .hindi}
            case .english:
                storyDetail = self.data1.arrGarbhSamvad.filter{$0.language == .english}
            case .gujarati:
                storyDetail = self.data1.arrGarbhSamvad.filter{$0.language == .gujarati}
            }
            self.data1.arrGarbhSamvad.removeAll()
            self.data1.arrGarbhSamvad = storyDetail
            
            setUpGarbhUI()
        }
        else
        {
            let storyDetail: [GarbhSamvadDetail]
            let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
            switch currLanguage {
            case .hindi:
                storyDetail = self.data.arrGarbhSamvad.filter{$0.language == .hindi}
            case .english:
                storyDetail = self.data.arrGarbhSamvad.filter{$0.language == .english}
            case .gujarati:
                storyDetail = self.data.arrGarbhSamvad.filter{$0.language == .gujarati}
            }
            self.data.arrGarbhSamvad.removeAll()
            self.data.arrGarbhSamvad = storyDetail
            
            setUpGarbhUI()
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = TaskCompleteView.instantiateView(with: self.view, with: screenType.navTitle)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                let activity_id = self.screenType == .garbhSatsang ? self.data1.arrGarbhSamvad[0].categoryId : self.data.arrGarbhSamvad[0].activityId
                let daily_activity_id = self.screenType == .garbhSatsang ? self.data1.arrGarbhSamvad[0].id : self.data.arrGarbhSamvad[0].id
                self.getActivityDone(with: self.screenType.strDoneActivityTitle, activityId:activity_id!, dailyActivityId: "\(daily_activity_id)") { (completed) in
                    if completed {
                        onBoardingView.removeFromSuperview()
                        NotificationCenter.default.post(name: Notification.Name(garbhNotificationUpdateActicityStatus), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    func setUPlayer(with playerID: String?) {
        guard let playeID = playerID else {return}
        playerView.setPlaybackQuality(.auto)
        playerView.setShuffle(false)
        playerView.setLoop(false)
        //        playerView.delegate = self
        //        playerView.load(withVideoId: playeID)
        // Add new code.
        let playerVars = ["playsinline": 1, "autohide": 1, "showinfo": 0, "controls":1, "origin" : "http://youtube.com"] as [String : Any] // 0: will play video in fullscreen
        playerView.load(withVideoId: playeID, playerVars: playerVars)
    }
    
    func openDisaplyInfoView() {
        let onBoardingView = DisplayInfoView.instantiateView(with: self.view)
        onBoardingView.setupUI()
        onBoardingView.setupValues(valueIndex: selected_DashboardId!)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
        }
    }
}

extension TodayStoryVC {
    
    @IBAction func btnPlayerCompleTaskTapped(_ sender: UIButton) {
        
        if(isActivityDone){
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kTaskCompleted)
        }
        else {
            self.prepareOnBoarding()
        }
    }
    
    @IBAction func btnCollectionCompleTaskTapped(_ sender: UIButton) {
        
        if(isActivityDone){
            _ = KPValidationToast.shared.showToastOnStatusBar(message: kTaskCompleted)
        }
        else {
            self.prepareOnBoarding()
        }
    }
    
    @IBAction func btnLeftTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath - 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnRightTapped(_ sender: UIButton){
        let indexPath = self.collectionView.currentPage
        self.collectionView.scrollToItem(at: IndexPath(item: indexPath + 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnVideo(_ sender: UIButton){
        DispatchQueue.main.async {
            self.viewVideo.isHidden = false
            let indexPath = self.collectionView.currentPage
            let strVideoUrl = self.data.arrGarbhSamvad[indexPath].videoUrl
            let youTubeID: String = self.getYoutubeId(StrLink: strVideoUrl) ?? "I1GwERXrlcU"
            //        let url = URL(string: "https://www.youtube.com/embed/\(youTubeID)")
            //        self.webVideo.load(URLRequest(url: url!))
            
            //         New code for play embedded video in browser. commented above code.
            self.webVideo.loadHTMLString("<iframe height=\"\(self.webVideo.frame.size.height)\" src=\"https://www.youtube.com/embed/\(youTubeID)\" style=\"width: 100%; height: 100%; position: absolute; top: 0px; left: 0px;\" width=\"\(self.webVideo.frame.size.width)\"></iframe>", baseURL: URL(string : "https://www.youtube.com"))
            
        }
    }
    
    @IBAction func btnCloseVideo(_ sender: UIButton){
        self.viewVideo.isHidden = true
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
        self.openDisaplyInfoView()
    }
    
    
}


extension TodayStoryVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.screenType == .garbhSatsang
        {
            return self.data1 == nil ? 0 : 1
        }
        else
        {
            return self.data == nil ? 0 : 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.screenType == .garbhSatsang
        {
            return self.data1.arrGarbhSamvad.count
        }
        else
        {
            return self.data.arrGarbhSamvad.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GarbhSamvadCollCell
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GarbhSamvadCollCell
        
        if self.screenType == .garbhSatsang
        {
            let imgUrl = URL(string: data1.imgStr + data1.arrGarbhSamvad[indexPath.row].image)
            cell.imgView.kf.setImage(with: imgUrl, placeholder: _placeImage)
        }
        else
        {
            let imgUrl = URL(string: data.imgStr + data.arrGarbhSamvad[indexPath.row].image)
            cell.imgView.kf.setImage(with: imgUrl, placeholder: _placeImage)
            
            if indexPath.row == 0
            {
                let strVideoUrl = self.data.arrGarbhSamvad[indexPath.row].videoUrl
                if strVideoUrl != ""
                {
                    btnVideo.isHidden = false
                    imgVideo.isHidden = false
                }
                else
                {
                    imgVideo.isHidden = true
                    btnVideo.isHidden = true
                }
            }
        }
        return cell
    }
}

extension TodayStoryVC {
    
    func getYoutubeId(StrLink: String) -> String?
    {
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            
            let match = regex.firstMatch(in: StrLink, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, StrLink.lengthOfBytes(using: String.Encoding.utf8)))
            
            let range = match?.range(at: 0)
            let youTubeID = (StrLink as NSString).substring(with: range!)
            return youTubeID
        } catch let err {
            _ = KPValidationToast.shared.showToastOnStatusBar(message: err.localizedDescription)
        }
        return nil
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView{
            let currPage = scrollView.currentPage
            
            /*
             if currPage == self.data.arrGarbhSamvad.count - 1{
             self.btnRight.isHidden = true
             }else if currPage == 0{
             self.btnLeft.isHidden = true
             }else {
             btnRight.isHidden = false
             btnLeft.isHidden = false
             }
             */
            
            if self.screenType == .garbhSatsang
            {
            }
            else
            {
                let strVideoUrl = self.data.arrGarbhSamvad[currPage].videoUrl
                if strVideoUrl != ""
                {
                    btnVideo.isHidden = false
                    imgVideo.isHidden = false
                }
                else
                {
                    imgVideo.isHidden = true
                    btnVideo.isHidden = true
                }
            }            
            
            //Abhi added
            if self.screenType == .garbhSatsang
            {
                if(currPage == 0){
                    btnLeft.isHidden = true
                    if(self.data1.arrGarbhSamvad.count > 1){
                        btnRight.isHidden = false
                    }
                }
                else if(currPage == self.data1.arrGarbhSamvad.count - 1) {
                    btnRight.isHidden = true
                    if(self.data1.arrGarbhSamvad.count > 1){
                        btnLeft.isHidden = false
                    }
                }
                else {
                    btnRight.isHidden = false
                    btnLeft.isHidden = false
                }
            }
            else
            {
                if(currPage == 0){
                    btnLeft.isHidden = true
                    if(self.data.arrGarbhSamvad.count > 1){
                        btnRight.isHidden = false
                    }
                }
                else if(currPage == self.data.arrGarbhSamvad.count - 1) {
                    btnRight.isHidden = true
                    if(self.data.arrGarbhSamvad.count > 1){
                        btnLeft.isHidden = false
                    }
                }
                else {
                    btnRight.isHidden = false
                    btnLeft.isHidden = false
                }
            }
        }
    }
}

extension TodayStoryVC {
    
    func getUserActivityData() {
        let param: [String: Any] = ["day": _user.currDay, "user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getGarbhActivityData(relPath: screenType.rawValue, param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                if self!.screenType == .garbhSatsang
                {
                    weakself.data1 = ModelGarbhSatsang(dict: dict, with: weakself.screenType)
                }
                else
                {
                    weakself.data = ModelGarbhSamvad(dict: dict, with: weakself.screenType)
                }
                weakself.filterCollData()
            } else {
                weakself.navBarView.isHidden = false
                weakself.showError(data: json, view: weakself.view)
            }
        }
    }
}

extension TodayStoryVC{
    
    func checkVideoStatusFirst(playerId:String,block: @escaping(String?,Bool) -> ()){
        self.checkVideoStatus(playerId: playerId,block: { (done) in
            if !done{
                block("I1GwERXrlcU",true)
            }else{
                block(playerId,true)
            }
        })
    }
    
    func checkVideoStatus(playerId:String,block: @escaping(Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.googleApiforYoutubeVideoStatus(youtubeID: playerId) { [weak self] (status)  in
            guard let weakself = self else {return}
            DispatchQueue.main.async {
                weakself.hideCentralSpinner()
                if status == .fail{
                    weakself.sendFailedVideoEmail()
                    block(false)
                }else{
                    if status == .success{
                        block(true)
                    }else{
                        block(false)
                    }
                }
            }
        }
    }
    
    func getRandomVideos(block: @escaping(Bool,String?) -> ()){
        self.showCentralSpinner()
        KPWebCall.call.getRandomVideo { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                
                let randomVideo = RandomVideoModel(fromDictionary: dict)
                var title = ""
                if (self!.lblTitle?.text?.contains(find: "Activity of"))!{
                    title = "Activity of the day"
                }else if (self!.lblTitle?.text?.contains(find: "Today's Story"))!{
                    title = "Today's Story"
                }else{
                    title = "Video"
                }
                if randomVideo.data.count > 0{
                    weakself.videoFilter = randomVideo.data.filter { $0.moduleName.contains(find: title) }
                    if weakself.videoFilter!.count > 0{
                        var videoUrl = ""
                        let randomElement = weakself.videoFilter!.randomElement()!
                        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
                        switch currLanguage {
                        case .hindi:
                            weakself.currentLang = .hindi
                            videoUrl = randomElement.hindiUrl
                        case .english:
                            weakself.currentLang = .english
                            videoUrl = randomElement.engUrl
                        case .gujarati:
                            weakself.currentLang = .gujarati
                            videoUrl = randomElement.gujUrl
                        }
                        let youtTubeId = weakself.getYoutubeId(StrLink: videoUrl) ?? "I1GwERXrlcU"
                        block(true,youtTubeId)
                    }else{
                        block(true,"I1GwERXrlcU")
                    }
                }else{
                    block(true,"I1GwERXrlcU")
                }
            } else {
                weakself.showError(data: json, view: self?.view)
                block(false,nil)
            }
        }
    }
    
    func sendFailedVideoEmail(){
        let param: [String: Any] = ["day": _user.currDay, "module_name":self.lblTitle!.text!, "language":_appDelegator.getLanguage()]
        
        self.showCentralSpinner()
        KPWebCall.call.sendVideoFailedEmail(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                //                if status == 200,let arrResult = dict["data"] as? [NSDictionary]{
                //
                //                }else {
                //                    weakself.showError(data: json, view: weakself.view)
                //                }
            }
        }
    }
    
}
