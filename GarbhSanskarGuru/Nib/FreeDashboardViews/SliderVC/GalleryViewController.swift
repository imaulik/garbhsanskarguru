//
//  GalleryViewController.swift
//  PregnancyGuru
//
//  Created by Maulik on 02/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import QuartzCore

class GalleryViewController: ParentViewController {
   
    @IBOutlet weak var cvImages: UICollectionView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrev: UIButton!
    @IBOutlet weak var btnNextView: UIView!
    @IBOutlet weak var btnPrevView: UIView!
    @IBOutlet weak var imgYoutube: UIImageView!
    @IBOutlet weak var ytView: UIView!
    @IBOutlet weak var btnShare: UIBarButtonItem!
    
 
    var imageURL:String?
    var dashboardSelItem : DashboardList?
    var arrSliderImages:[PagerViewList]?
    var arrTipsList:[TipsList]?
    var arrAppBenifits = [ImageList.AppInfo1.rawValue,ImageList.AppInfo2.rawValue,ImageList.AppInfo3.rawValue,ImageList.AppInfo4.rawValue,ImageList.AppInfo5.rawValue,ImageList.AppInfo6.rawValue]
    var arrSelectedTips:TipsCategoryList?
    var isFromTips = false
    var isFromAppBenefit = false
    let strLanguage = _appDelegator.getLanguage()
    
    var topPresentedViewController: UIViewController {
        var viewController: UIViewController = self
        while let vc = viewController.presentedViewController {
            viewController = vc
        }
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cvImages.register(UINib(nibName: "GalleryCell", bundle:nil), forCellWithReuseIdentifier: "GalleryCell")
        if isFromTips {
            if strLanguage == KHindi {
                self.title = arrSelectedTips?.hindi_name
            }else if strLanguage != kGujrati {
                 self.title =  arrSelectedTips?.guj_name
            }else{
                 self.title =  arrSelectedTips?.eng_name
            }
        }
        else if isFromAppBenefit {
            self.title = "App Benefits"
            self.btnShare.isEnabled = false
            self.btnShare.tintColor = .clear
             self.btnNextView.isHidden = false
             return
        }
        else {
            self.title = dashboardSelItem?.title?.localized
        }

        if dashboardSelItem?.category_name == DashBoardCategoryList.GSbabyimage.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSposters.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.DailyQuote.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSCalender.rawValue{
             self.btnShare.isEnabled = true
             self.btnShare.tintColor = #colorLiteral(red: 0.8470588235, green: 0.2117647059, blue: 0.4196078431, alpha: 1)
        }else {
            self.btnShare.isEnabled = false
            self.btnShare.tintColor = .clear
        }
        setupWebservice()
    }
  
  
    override func viewDidLayoutSubviews() {
 
          expandAnimaion()
    
    }
     
    func expandAnimaion(){
    
          DispatchQueue.main.async{
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           options: [.repeat, .autoreverse, .beginFromCurrentState],
                           animations: {
                            self.imgYoutube.transform = CGAffineTransform(scaleX: 1.3, y:   1.2)
            }, completion: nil)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.imgYoutube.layer.removeAllAnimations()
    }
    
    func setupWebservice(){
        if isFromTips {
            let param = ["user_id":_user.id, "category_id":arrSelectedTips?.id ?? ""]
            
            self.showCentralSpinner()
            KPWebCall.call.categoryTips(param: param) { [weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    
                    let result = TipsDetailModel(dictionary: dict)
                    guard let pagerData = result.data else { return }
                    
                    var searchText = ""
                    if weakself.strLanguage == kEnglish {
                        searchText = kLongEnglish
                    }else if weakself.strLanguage == kGujrati {
                        searchText = kLongGujrati
                    }else if weakself.strLanguage == KHindi {
                         searchText = KLongHindi
                    }

                    weakself.arrTipsList = pagerData.filter { catalogName in
                        return catalogName.language ==  searchText
                    }
                    weakself.hideTipsBothButton()
                    weakself.cvImages.reloadData()
                }
            }
            
            
//            callWS_CategoryTipsDetail(parms: parm) { result in
//                guard let pagerData = result else {
//                    return
//                }
//
//                var searchText = ""
//                if self.strLanguage == kLongEnglish {
//                    searchText = kLongEnglish
//                }else if self.strLanguage == kLongGujrati {
//                    searchText = kLongGujrati
//                }else if self.strLanguage == KLongHindi {
//                     searchText = KLongHindi
//                }
//
//                self.arrTipsList = pagerData.filter { catalogName in
//                    return catalogName.language ==  searchText
//                }
//
//                self.hideTipsBothButton()
//                self.cvImages.reloadData()
//            }
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GS9Months.rawValue {
            let param = ["user_id":_user.id,"month":String(checkMonth)]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(forDashBoard: true, endPoint: "garbhyatra", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
//            callWS_GetPageimage(endPoint:URLS.Month.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.DailyQuote.rawValue {
            //quotes
            let param = ["user_id":_user.id]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "quotes", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.DailyQuote.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSCalender.rawValue {
            let param = ["user_id":_user.id]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "calender", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.Calender.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        } else if dashboardSelItem?.category_name == DashBoardCategoryList.GSposters.rawValue {
            let param = ["user_id":_user.id]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "posters", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.GSPoster.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSbabyimage.rawValue {
            let param = ["user_id":_user.id]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "babyimage", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.GSBabyImage.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSShloka.rawValue {
            let param = ["user_id":_user.id]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "shloka", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.GSShloka.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
        else if dashboardSelItem?.category_name == DashBoardCategoryList.GSHalarda.rawValue {
            let param = ["user_id":_user.id]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "halarda", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.GSHalarda.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
        else {
            let param = ["user_id":_user.id,"category_name":dashboardSelItem?.category_name ?? ""]
            
            self.showCentralSpinner()
            KPWebCall.call.getPageImage(endPoint: "uploadimage", param: param) {[weak self] (json, statusCode) in
                guard let weakself = self else {return}
                weakself.hideCentralSpinner()
                if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                    let pagerData = PagerViewModel(dictionary: dict)
                    weakself.setupData(imgNames:pagerData)
                }
            }
            
//            callWS_GetPageimage(endPoint:URLS.UploadImage.rawValue, parms: parms) { result in
//                guard let pagerData = result else {
//                    return
//                }
//                self.setupData(imgNames:pagerData)
//            }
        }
    }
    
    
    func setupData(imgNames :PagerViewModel){
        self.imageURL = imgNames.image_url
        
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        var searchText = ""
        if currLanguage == .hindi{
            searchText = KLongHindi
        }else if currLanguage == .english{
            searchText = kLongEnglish
        }else{
            searchText = kLongGujrati
        }
        self.arrSliderImages = imgNames.data?.filter { catalogName in
            return catalogName.language ==  searchText
        }
        self.arrSliderImages = imgNames.data?.filter { catalogName in
            return catalogName.language ==  searchText
        }
        
        if dashboardSelItem?.category_name == DashBoardCategoryList.DailyQuote.rawValue{
            
            let lastValue = (arrSliderImages?.count ?? 0) - 1
            let temp = arrSliderImages?[lastValue]
            arrSliderImages = [(temp ?? PagerViewList())]
        }
        self.hideBothButtons()
        self.cvImages.reloadData()
    }
    
    @IBAction func btnPrevPress(_ sender: Any) {
        let visibleItems: NSArray = self.cvImages.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let prevItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        updateButton(prevItem)
         self.cvImages.scrollToItem(at: prevItem, at: .left, animated: true)
    }
    
    @IBAction func btnNextPress(_ sender: Any) {
        let visibleItems: NSArray = self.cvImages.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        updateButton(nextItem)
        self.cvImages.scrollToItem(at: nextItem, at: .left, animated: true)
    }
    
    var indexPath: IndexPath? {
        for cell in self.cvImages.visibleCells {
            let indexPath = self.cvImages.indexPath(for: cell)
            return indexPath
         }

         return nil
    }
    
    
    @IBAction func btnSharePress(_ sender: UIButton) {
        
        var imageName = arrSliderImages?[indexPath?.item ?? 0].image_name ?? ""
        if dashboardSelItem?.category_name == DashBoardCategoryList.GSCalender.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.DailyQuote.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSposters.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSbabyimage.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSShloka.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSHalarda.rawValue {
            imageName = arrSliderImages?[indexPath?.item ?? 0].image ?? ""
        }
        
        if let image = SDImageCache.shared.imageFromDiskCache(forKey:(imageURL ?? "") + imageName) {
            let activityItem: [AnyObject] = [image as AnyObject]
            shareYourItems(shareItems: activityItem, isexcludItems:false , excludItems:[])
        }
    }
    
    @IBAction func btnYTPress(_ sender: Any) {
         var ytURL:String?
        var youTubeID = ""
        
        if isFromTips || isFromAppBenefit {
           
        }else {
            let obj = arrSliderImages?[indexPath?.row ?? 0]
            ytURL = obj?.url
        }
      
        videoPlay(strVideo: ytURL ?? "")
    }
    
    func hideTipsBothButton(){
        if arrTipsList?.count ?? 0 <= 1 {
            self.btnNextView.isHidden = true
            self.btnPrevView.isHidden = true
        }
        else {
            self.btnNextView.isHidden = false
        }
    }
    
    func hideBothButtons(){
         if arrSliderImages?.count ?? 0 <= 1 {
            self.btnNextView.isHidden = true
            self.btnPrevView.isHidden = true
        }
        else {
            self.btnNextView.isHidden = false
        }
    }
}

 // MARK: - AVPlayerViewControllerDelegate
 extension GalleryViewController: AVPlayerViewControllerDelegate {
     
     func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
         if playerViewController.presentingViewController != nil {
             completionHandler(true)
             return
         }
         
         topPresentedViewController.present(playerViewController, animated: true)
         completionHandler(true)
     }
     
 //    func playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart(_ playerViewController: AVPlayerViewController) -> Bool {
 //        return false
 //    }
     
 }

protocol CustomPlayerViewControllerDelegate: class {
    
    func customPlayerViewController(_ customPlayerViewController: GalleryViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void)
    
}

// MARK: - CustomPlayerViewControllerDelegate
extension GalleryViewController: CustomPlayerViewControllerDelegate {
    
    func customPlayerViewController(_ customPlayerViewController: GalleryViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        
        if navigationController!.viewControllers.firstIndex(of: customPlayerViewController) != nil {
            completionHandler(true)
        } else {
            navigationController!.pushViewController(customPlayerViewController, animated: true)
            completionHandler(true)
        }
    }

}


extension GalleryViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width, height:collectionView.frame.size.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromAppBenefit{
            return arrAppBenifits.count
        }
        else if isFromTips {
            return arrTipsList?.count ?? 0
        }
        return arrSliderImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"GalleryCell", for: indexPath) as! GalleryCell
        if isFromAppBenefit {
            let strImageName = arrAppBenifits[indexPath.row].localized
            cell.imgScale.image = UIImage(named: strImageName)
        }
        else if isFromTips {
            let obj = arrTipsList?[indexPath.item]
            if obj?.type == "Text" {
                cell.DescriptionView.isHidden = false
                cell.lblHeader.text = obj?.title
                cell.lblDescription.text = "✭ \(obj?.text_image ?? "")"
            }else {
                 cell.DescriptionView.isHidden = true
            }
            
        }else {
            var imageName = arrSliderImages?[indexPath.item].image_name ?? ""
            if dashboardSelItem?.category_name == DashBoardCategoryList.GSCalender.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.DailyQuote.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSposters.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSbabyimage.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSShloka.rawValue || dashboardSelItem?.category_name == DashBoardCategoryList.GSHalarda.rawValue{
                imageName = arrSliderImages?[indexPath.item].image ?? ""
            }
            
            if let url = URL(string:  (imageURL ?? "") + imageName){
                self.showCentralSpinner()
//                cell.imgScale.kf.setImage(with: url, placeholder: _placeImage) { (result) in
//                    self.hideCentralSpinner()
//                    switch result {
//                    case .success(let value):
//                        print("Image: \(value.image). Got from: \(value.cacheType)")
//                    case .failure(let error):
//                        print("Error: \(error)")
//                    }
//                }
                
                cell.imgScale.sd_setImage(with: url) { (image, err, type, url) in
                    self.hideCentralSpinner()
                    if err != nil {
                        //print("Failed to download image")
                    }
                }
            }
            
            if arrSliderImages?[indexPath.item].video_url != "" || arrSliderImages?[indexPath.item].url != "" {
                ytView.isHidden = false
            }else {
                ytView.isHidden = true
            }
        }
    
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.cvImages.contentOffset
        visibleRect.size = self.cvImages.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.cvImages.indexPathForItem(at: visiblePoint) else { return }
            updateButton(indexPath)
     }
       
    func updateButton(_ index:IndexPath){
        if (arrAppBenifits.count == 1 && isFromAppBenefit) {}
        else if arrTipsList?.count == 1 && isFromTips {}
        else if arrSliderImages?.count == 1{}
        else{
            if index.item == 0 {
                self.btnNextView.isHidden = false
                self.btnPrevView.isHidden = true
            }
            else if  isFromAppBenefit && index.item == arrAppBenifits.count - 1{
                lastButtonHide()
            }
            else if  isFromTips && index.item == (arrTipsList?.count ?? 0) - 1{
                lastButtonHide()
            }
            else if index.item == (arrSliderImages?.count ?? 0) - 1{
                 lastButtonHide()
            }
            else {
                self.btnNextView.isHidden = false
                self.btnPrevView.isHidden = false
            }
        }
    }
    
    func lastButtonHide() {
        self.btnNextView.isHidden = true
        self.btnPrevView.isHidden = false
    }
}

