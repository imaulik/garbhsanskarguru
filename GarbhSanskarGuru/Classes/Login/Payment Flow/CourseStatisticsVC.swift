//
//  CourseStatisticsVC.swift
//  GarbhSanskarGuru
//
//  Created by Maulik on 01/09/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class CourseStatisticsListCell: UITableViewCell {
    @IBOutlet weak var lblNumbers: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblFollowingCourse: UILabel!
    @IBOutlet weak var imgRating: UIImageView!
}


class CourseStatisticsVC: UIViewController {
    @IBOutlet weak var tblStatistics: UITableView!
    var strLanguage = _appDelegator.getLanguage()
    
    var  arrStatstics = [StatisticsInfo]()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerBlock()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setup()
    }
    
    
    
    @IBAction func btnCall(_ sender: Any) {
        makeCall(number: "919727006001")
    }
    
    @IBAction func btnSubscribe(_ sender: UIButton)
    {
        let packagesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PackagesVC") as! PackagesVC
        self.navigationController?.pushViewController(packagesVC, animated: true)
    }
    
    
    func setup(){
        strLanguage = _appDelegator.getLanguage()
        if strLanguage == kEnglish {
            arrStatstics = [StatisticsInfo(title: "Countries mothers \nusing the app", description: "",image: #imageLiteral(resourceName: "global-icon")),StatisticsInfo(title: "Mothers are \nfollowing the course", description: "",image:#imageLiteral(resourceName: "mother-icon")),StatisticsInfo(title: "Babies born by \nafter following the course", description: "",image: #imageLiteral(resourceName: "baby-icon")),StatisticsInfo(title: "Babies born 3+ kilograms \nfollowing the course", description: "",image: UIImage(named: "scale-icon") ?? UIImage() ),StatisticsInfo(title: "Highest rating in the \napp segment", description: "",image:  #imageLiteral(resourceName: "rating-icon"))]
            
        }else if strLanguage == kGujrati {
            arrStatstics = [StatisticsInfo(title: "દેશોની માતાઓ \nએપ વાપરે છે", description: "",image: #imageLiteral(resourceName: "global-icon")),StatisticsInfo(title: "માતાઓ કોર્સ \nફોલો કરે છે", description: "",image:#imageLiteral(resourceName: "mother-icon")),StatisticsInfo(title: "બાળકોના જન્મ થયા", description: "",image: #imageLiteral(resourceName: "baby-icon")),StatisticsInfo(title: "બાળકોના વજન 3+ કિલો", description: "",image: UIImage(named: "scale-icon") ?? UIImage() ),StatisticsInfo(title: "Highest rating in the \napp segment", description: "",image:  #imageLiteral(resourceName: "rating-icon"))]
            
        }else if strLanguage == KHindi {
            arrStatstics = [StatisticsInfo(title: "देशोंकी माताए \nएप यूज करती हैं", description: "",image: #imageLiteral(resourceName: "global-icon")),StatisticsInfo(title: "माताए कोर्स \nफॉलो कर रही हैं", description: "",image:#imageLiteral(resourceName: "mother-icon")),StatisticsInfo(title: "बच्चोके जन्म हुए", description: "",image: #imageLiteral(resourceName: "baby-icon")),StatisticsInfo(title: "बच्चोके वजन 3+ किलो", description: "",image: UIImage(named: "scale-icon") ?? UIImage() ),StatisticsInfo(title: "Highest rating in the \napp segment", description: "",image:  #imageLiteral(resourceName: "rating-icon"))]
            
        }
        getStatisticsData()
        self.tblStatistics.reloadData()
    }
    
    func registerBlock(){
        _appDelegator.detectBackFromStatistic = { () in
            self.setup()
        }
    }
}


extension CourseStatisticsVC {
    func getStatisticsData() {
         KPWebCall.call.getStatisticsData {[weak self] (json, statusCode) in
            guard let weakself = self else {return}
           
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let data  = dict ["data"] as? NSDictionary
                
                self?.arrStatstics[0].value = "\(data? ["county"] as? String ?? "")+"
                self?.arrStatstics[1].value = "\(data? ["mother"] as? Int ?? 0)+"
                self?.arrStatstics[2].value = "\(data? ["babby_born"] as? Int ?? 0)+"
                self?.arrStatstics[3].value = "\(data? ["babby_born_kg"] as? String ?? "")%"
                self?.arrStatstics[4].value = "\(data? ["review"] as? String ?? "")+"
                weakself.tblStatistics.reloadData()
            }
        }
    }
}


extension CourseStatisticsVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStatstics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CourseStatisticsListCell = tableView.dequeueReusableCell(withIdentifier: "CourseStatisticsListCell", for: indexPath) as! CourseStatisticsListCell
        cell.lblDescription.text = arrStatstics[indexPath.row].Title
      
        cell.imgRating.image = arrStatstics[indexPath.row].image
        cell.lblNumbers.text = arrStatstics[indexPath.row].value
        cell.selectionStyle = .none
        return cell
    }
}




