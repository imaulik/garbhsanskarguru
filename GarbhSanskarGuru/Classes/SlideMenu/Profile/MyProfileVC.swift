//
//  MyProfileVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 14/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

struct ProfileData {
    var fullName = _user.name
    var email = _user.email
    var mobilenumber = _user.mobile
    var country = _user.country
    var state = _user.state
    var city = _user.city 
    var userImg = _user.imgUrl
    
    func paramDict() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["user_id"] = _user.id
        dict["full_name"] = fullName
        dict["phone_number"] = mobilenumber
        dict["country"] = country
        dict["state"] = state
        dict["city"] = city
        dict["gender"] = _user.userGender.rawValue
        return dict
    }
    
    func isValidData() -> (isValid: Bool, error: String) {
        var result = (isValid: true, error: "")
        
        if String.validateStringValue(str: fullName){
            result.isValid = false
            result.error = kEnterName
            return result
        }
        
        if String.validateStringValue(str: email) {
            result.isValid = false
            result.error = kEnterEmail
            return result
        }else if !email.isValidEmailAddress() {
            result.isValid = false
            result.error = kInvalidEmail
            return result
        }
        
        if String.validateStringValue(str: mobilenumber){
            result.isValid = false
            result.error = kEnterMobile
            return result
        }else if !mobilenumber.validateContact() {
            result.isValid = false
            result.error = kMobileInvalid
            return result
        }
        return result
    }
}

class MyProfileVC: ParentViewController {

    @IBOutlet weak var viewAccessory: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    
    var selectedImage: UIImage?
    var arrData: [EnumMyProfileCellType] = [.imgCell, .txtcell,.txtcell,.txtcell,.txtcell, .dbltxtCell, .radioCell]
    
    var data = ProfileData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

extension MyProfileVC {
    
    func prepareUI() {
        setKeyboardNotifications()
        changePasswordView.isHidden = true
        viewAccessory.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: 44)
    }
    
    func getEntryCell(_ row: Int) -> MyProfileCell?{
        let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? MyProfileCell
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "forgotSegue" {
            let destVC = segue.destination as! ChangePasswordVC
            destVC.block = { isHidden in
                if isHidden {
                    self.changePasswordView.isHidden = true
                }
            }
        }
    }
    
    func openCameraUI(with source: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = source
        present(picker, animated: true, completion: nil)
    }
    
    func openPicker(sender: UIButton) {
        let alertAction = UIAlertController(title: nil, message: "Choose Profile Image", preferredStyle: .actionSheet)
        alertAction.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.openCameraUI(with: .camera)
        }))
        alertAction.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.openCameraUI(with: .photoLibrary)
        }))
        alertAction.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
       
        if let popoverController = alertAction.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        self.present(alertAction, animated: true, completion: nil)
    }
    
}

extension MyProfileVC {
    
    @IBAction func btnDoneTapped(_ sender: UIButton){
        self.view.endEditing(true)
    }
    
    @IBAction func btnOpenCameraUI(_ sender: UIButton) {
        openPicker(sender: sender)
    }
    
    @IBAction func btnPasswordTapped(_ sender: UIButton) {
        changePasswordView.isHidden = false
    }
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        let validate = data.isValidData()
        if validate.isValid {
            self.updateProfileAPI()
        }else {
            _ = ValidationToast.showStatusMessage(message: validate.error)
        }
    }
}

extension MyProfileVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrData[indexPath.row].cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MyProfileCell
        let objProfile = arrData[indexPath.row]
        cell = tableView.dequeueReusableCell(withIdentifier: objProfile.rawValue, for: indexPath) as! MyProfileCell
        cell.parentProfile = self
        cell.setUpCell(with: objProfile, and: indexPath.row)
        return cell
    }
}

extension MyProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let choosedImg = info[.editedImage] as? UIImage {
            self.selectedImage = choosedImg
            tableView.reloadData()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension MyProfileVC {
    
    func updateProfileAPI() {
        var param = data.paramDict()
        if let imgData = selectedImage?.jpegData(compressionQuality: 0.8) {
            let base64String = imgData.base64EncodedString(options: .lineLength64Characters)
            param["profile_picture"] = "data:image/jpeg;base64," + base64String
        }
        self.showCentralSpinner()
        KPWebCall.call.updateUserProfile(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let userInfo = dict["data"] as? NSDictionary {
                DispatchQueue.main.async {
                    // comment , Restrict to Ovewite data.
//                    _user = User.addUpdateEntity(key: "id", value: userInfo.getStringValue(key: "user_id"))
//                    _user.initWith(entry: userInfo)
                    
                    _user.setFullName(fName: self.data.fullName)
                    _user.setPhoneNumber(number: self.data.mobilenumber)
                    _user.setAddressData(scity: self.data.city, stateName: self.data.state, countryName: self.data.country)
                    _appDelegator.saveContext()
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: "Profile Updated Successfully")
                    self.tableView.reloadData()
                }
            }else {
                self.showError(data: json, view: self.view)
            }
        }
    }
}

