//
//  PaymentVC.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 13/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import PaymentSDK
import Razorpay



enum PaymentMode: String {
    case paytm = "PPI"
    case paypal = "paypal"
    case razorPay = "razorPay"
}

class Payment {
    
    var checkSumHash = ""
    var transactionId = ""
    func paramDict(proceedToPay: Bool) -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["ORDER_ID"] = "order\(paytmtimeStamp)"
        dict["CHANNEL_ID"] = "WAP"
        dict["MID"] = "Majest55259987150356"
        dict["WEBSITE"] = "Majest"
        dict["INDUSTRY_TYPE_ID"] = "Retail109"
        dict["CALLBACK_URL"] = "https://securegw-stage.paytm.in/theia/paytmCallback"
        if proceedToPay {
            dict["CHECKSUMHASH"] = checkSumHash
        }
        return dict
    }
}

var paytmtimeStamp =  Int()

class PaymentVC: ParentViewController {
    
    var isfromCode = false
    var total = 0.0
    @IBOutlet weak var bckButton: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
     
    @IBOutlet weak var lblPriceInfo: UILabel!
    @IBOutlet weak var lblGSTInfo: UILabel!
    @IBOutlet weak var lblTotalPriceInfo: JPWidthLabel!
    
    @IBOutlet weak var verifyView: UIView!
    @IBOutlet weak var signOutView: UIView!

    @IBOutlet weak var vwMakingPayment: UIView!
    @IBOutlet weak var btnFromIndia: UIButton!
    @IBOutlet weak var btnOutsideIndia: UIButton!

    let imgVwBgMakingPaymentVw: UIImageView = UIImageView.init()
    
    var data = Payment()
//    var payPalConfig = PayPalConfiguration()
    var onBoardingPaymentView: PaymentStatusView!
    var isFromEntry = false
    var isHideBackButton = false
    var payMode: PaymentMode!
     var serv = PGServerEnvironment()
     var txnController = PGTransactionViewController()
    
    // RazorPay
    var razorpayObj : RazorpayCheckout? = nil
    var razorpay : RazorpayCheckout? = nil
    
//    var environment:String = PayPalEnvironmentNoNetwork {
//        willSet(newEnvironment) {
//            if (newEnvironment != environment) {
//                PayPalMobile.preconnect(withEnvironment: newEnvironment)
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paytmtimeStamp = Int(Date().timeIntervalSince1970)
        prepareUI()
        getPrice()
        setupRazorPay()
        
        print("================\n unique id " +  _user.uniqueId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imgVwBgMakingPaymentVw.frame = CGRect(x: 0, y: 0, width: vwMakingPayment.frame.size.width, height: vwMakingPayment.frame.size.height)
    }
}

extension PaymentVC {
    
    @IBAction func btnPayTapped(_ sender: UIButton) {
        
        //self.prepareOnBoarding()
        
        //commented below line text from this controller ~ below line is just for reminder set as comment
        //self.onBoardingPaymentView.removeFromSuperview()
        
        if(self.btnFromIndia.isSelected){
            self.payMode = .paytm
            kprint(items: "Initiated Paytm")
            self.generateCheckSum { (done) in
                if done {
                    self.beginPayment()
                }
            }
        }
        else if (self.btnOutsideIndia.isSelected) {
            
            self.payMode = .razorPay
            kprint(items: "Initiated RazorPay")
            self.prepareRazorPayUI()
            self.prepareRazorPayPayment()
        }
//        else if (self.btnOutsideIndia.isSelected) {
//
//            self.payMode = .paypal
//            kprint(items: "Initiated PayPal")
////            self.preparePayPalUI()
////            self.preparePayPalPayment()
//        }
    }
    
    @IBAction func btnSignOutTapped(_ sender: UIButton) {
        self.prepareSignOutOnBoarding()
    }
    
    @IBAction func btnVerifyPaymentTapped(_ sender: UIButton) {
        self.verifyView.isHidden = false
    }
    
    @IBAction func btnFromIndiaTapped(_ sender: UIButton) {
    
        self.btnFromIndia.isSelected = true
        self.btnOutsideIndia.isSelected = false
    }
    
    @IBAction func btnOutsideIndiaTapped(_ sender: UIButton) {
        
        self.btnFromIndia.isSelected = false
        self.btnOutsideIndia.isSelected = true
    }
}

extension PaymentVC {
    
    func setupRazorPay(){
        razorpay = RazorpayCheckout.initWithKey("rzp_live_JMakE3UBeLXckz", andDelegate: self)
//        razorpay = Razorpay.initWithKey("rzp_test_3s2ktV9XdLS4al", andDelegate: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verifySegue" {
            let destVC = segue.destination as! VerifyPaymentVC
            destVC.block = { isHidden in
                if isHidden {
                    self.verifyView.isHidden = true
                }
            }
        }
    }
    
    func prepareUI() {
        
        self.signOutView.isHidden = isFromEntry
        self.verifyView.isHidden = true
        bckButton.isHidden = !isFromEntry
        
        // Custom logic Start.
        // Make new custom logic for disaply back button, written.
        var isNeedBackButton = false
        for navigation in self.navigationController!.viewControllers{
            if navigation is ReferalCodeNewVC{
                isNeedBackButton = true
                break
            }
        }
        if isNeedBackButton{
            bckButton.isHidden = false
        }
        // Custom logic End.
        
        //-----MakingPaymentVw Gradient-------------------
        imgVwBgMakingPaymentVw.frame = CGRect(x: 0, y: 0, width: vwMakingPayment.frame.size.width, height: vwMakingPayment.frame.size.height)
        imgVwBgMakingPaymentVw.clipsToBounds = true
        imgVwBgMakingPaymentVw.image = UIImage.init(named: "gradient-pink.png")
        imgVwBgMakingPaymentVw.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vwMakingPayment.insertSubview(imgVwBgMakingPaymentVw, at: 0)
        
    }
    
//    func preparePayPalUI() {
//        payPalConfig.acceptCreditCards = false
//        payPalConfig.merchantName = "Majestic GarbhSanskarGuru"
//        payPalConfig.merchantPrivacyPolicyURL = URL.init(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
//        payPalConfig.merchantUserAgreementURL = URL.init(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
//        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
//        payPalConfig.payPalShippingAddressOption = .payPal
//        PayPalMobile.preconnect(withEnvironment: PayPalEnvironmentSandbox)
//    }
    
    func prepareRazorPayUI(){
        
    }
    
    func prepareOnBoarding() {
        onBoardingPaymentView = PaymentStatusView.instantiateView(with: self.view)
        onBoardingPaymentView.bounceView(animateView: onBoardingPaymentView.baseView)
        onBoardingPaymentView.actionBlock = { (action) in
            switch action {
            case .cancel:
                break
            case .done:
                self.payMode = .paytm
                kprint(items: "Initiated Paytm")
                self.generateCheckSum { (done) in
                    if done {
                        self.beginPayment()
                    }
                }
            case .share:
                break
            case .other:
                self.payMode = .razorPay
                kprint(items: "Initiated RazorPay")
                self.prepareRazorPayUI()
                self.prepareRazorPayPayment()

//                self.payMode = .paypal
//                kprint(items: "Initiated PayPal")
////                self.preparePayPalUI()
////                self.preparePayPalPayment()
            case .later:
                break
            }
        }
    }
    
    //RazorPay Payment
    
    func prepareRazorPayPayment(){
        var options:[String:Any] = [:]
        
        options["name"] = "Majestic GarbhSanskar"
        options["currency"] = "USD"
       
        if(isFromEntry){
            options["amount"] = _giftUser.usdPrice * 100
            options["description"] = "GiftApp Purchase"
        }else{
            if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue) {
//                item = PayPalItem(name: "Majestic GarbhSanskar", withQuantity: 1, withPrice: NSDecimalNumber(value: _user.usdPriceDemoApp), withCurrency: "USD", withSku: "")
                options["amount"] = _user.usdPriceDemoApp * 100
                options["description"] = "DemoApp Purchase"
            }
            else {
                options["amount"] = _user.usdPrice * 100
                options["description"] = "FullApp Purchase"
//                item = PayPalItem(name: "Majestic GarbhSanskar", withQuantity: 1, withPrice: NSDecimalNumber(value: _user.usdPrice), withCurrency: "USD", withSku: "")
            }
        }
        options["image"] = UIImage.init(named: "logo_changes")! as Any
        options["prefill"] = [
            "contact": _user.mobile,
            "email": _user.email
        ]
        options["theme"] = [
            "color": "#F37254"
        ]
        
//        let options1: [String:Any] = [
//            //            "amount": "100", //This is in currency subunits. 100 = 100 paise= INR 1.
//            "amount": "2500", //This is in currency subunits. 100 = 100 paise= INR 1.
//            //            "currency": "INR",//We support more that 92 international currencies.
//            "currency": "USD",//We support more that 92 international currencies.
//            "description": "purchase description",
//            //            "order_id": "order_\(_timeStamp)",
//            //            "order_id": "order_4xbQrmEoA5WJ0G",
//            "image": "https://url-to-image.png",
//            "name": "Majestic GarbhSanskarGuru",
//            "prefill": [
//                "contact": "9797979797",
//                "email": "foo@bar.com"
//            ],
//            "theme": [
//                "color": "#F37254"
//            ]
//        ]
        print("RazorPay params: \(options)")
        razorpay?.open(options)
    }
    
    // Jahanvi comment
    /*func preparePayPalPayment() {

        //abhi change
        //var item = PayPalItem(name: "Majestic GarbhSanskar", withQuantity: 1, withPrice: NSDecimalNumber(value: _user.usdPrice), withCurrency: "USD", withSku: "")

        var item = PayPalItem.init()
        if(isFromEntry){
            item = PayPalItem(name: "Majestic GarbhSanskar", withQuantity: 1, withPrice: NSDecimalNumber(value: _giftUser.usdPrice), withCurrency: "USD", withSku: "")
        }
        else {
            if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue) {
                item = PayPalItem(name: "Majestic GarbhSanskar", withQuantity: 1, withPrice: NSDecimalNumber(value: _user.usdPriceDemoApp), withCurrency: "USD", withSku: "")
            }
            else {
                item = PayPalItem(name: "Majestic GarbhSanskar", withQuantity: 1, withPrice: NSDecimalNumber(value: _user.usdPrice), withCurrency: "USD", withSku: "")
            }
        }
        
        let items = [item]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0")
        let tax = NSDecimalNumber(string: "0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Majestic GarbhSanskar", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            print("Payment not processalbe: \(payment)")
        }
    }*/
    
    func beginPayment() {
        serv = serv.createProductionEnvironment()
        let type :ServerType = .eServerTypeProduction
        let order = PGOrder(orderID: "", customerID: "", amount: "", eMail: "", mobile: "")
        var param: [String: Any] = data.paramDict(proceedToPay: true)
        if isFromEntry {
            param["TXN_AMOUNT"] = "\(Int(self.total))"
           // param["TXN_AMOUNT"] = "\(_giftUser.amount)"
            param["CUST_ID"] = _giftUser.email
        } else {
            if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue) {
                param["TXN_AMOUNT"] = "\(Int(self.total))"
               // param["TXN_AMOUNT"] = "\(_user.amountDemoApp)"
            }
            else {
                param["TXN_AMOUNT"] = "\(Int(self.total))"
               // param["TXN_AMOUNT"] = "\(_user.amount)"
            }
            param["CUST_ID"] = _user.email
        }
        order.params = param
        self.txnController =  self.txnController.initTransaction(for: order) as! PGTransactionViewController
        self.txnController.title = "Paytm Payments"
        self.txnController.setLoggingEnabled(true)
        if(type != ServerType.eServerTypeNone) {
            self.txnController.serverType = type
        } else {
            return
        }
        self.txnController.merchant = PGMerchantConfiguration.defaultConfiguration()
        self.txnController.delegate = self
        self.navigationController?.pushViewController(self.txnController, animated: true)
    }
    
    func redirectToSuccessView() {
        if isFromEntry {
            performSegue(withIdentifier: "successSegue", sender: nil)
        } else {
            performSegue(withIdentifier: "payNowSegue", sender: nil)
        }
    }
    
    func setPaymentStatus() {
        if isFromEntry {
            self.setPaymentStatus(block: { (done) in
                if done {
                    self.redirectToSuccessView()
                }
            }, with: payMode)
        } else {
            self.updatePaymentStatus(block: { (done) in
                if done {
                    self.redirectToSuccessView()
                }
            }, with: payMode)
        }
    }
    
    func prepareSignOutOnBoarding() {
        let onBoardingView = SignOutView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                self.prepareForLogout()
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
}

// RazorPay Delegate
extension PaymentVC : RazorpayPaymentCompletionProtocol{
    
    public func onPaymentError(_ code: Int32, description str: String){
        _ = KPValidationToast.shared.showToastOnStatusBar(message: str)
    }

    public func onPaymentSuccess(_ payment_id: String){
        debugPrint("Payment Id \(payment_id)")
        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment Success")
        self.data.transactionId = payment_id
//        self.navigationController?.popViewController(animated: true)
        DispatchQueue.main.async {
            self.setPaymentStatus()
        }
    }
    
}

// Jahanvi Comment
/*extension PaymentVC: PayPalPaymentDelegate, PGTransactionDelegate {
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        paymentViewController.dismiss(animated: true, completion: nil)
        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment Cancelled by user")
        //self.onBoardingPaymentView.removeFromSuperview()
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
        if let cnfrmDict = completedPayment.confirmation[AnyHashable("response")] as? NSDictionary {
            if let id = cnfrmDict["id"] as? String, let status = cnfrmDict["state"] as? String {
                if status.isEqual(str: "approved") {
                    self.data.transactionId = id
                    
                    paymentViewController.dismiss(animated: true, completion: nil)
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment Success")
                    //self.onBoardingPaymentView.removeFromSuperview()
                    
                    DispatchQueue.main.async {
                        self.setPaymentStatus()
                    }
                    
                } else {
                    paymentViewController.dismiss(animated: true, completion: nil)
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment \(status)")
                    //self.onBoardingPaymentView.removeFromSuperview()
                }
            }
        }
    }
    
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {
      //  let msg : String = responseString
       // var titlemsg : String = ""
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    
                    if let resMsg = jsonresponse["RESPMSG"] as? String, let status = jsonresponse["STATUS"] as? String {
                        if status == "PENDING" {
                            _ = KPValidationToast.shared.showToastOnStatusBar(message: resMsg)
                            controller.navigationController?.popViewController(animated: true)
                            //self.onBoardingPaymentView.removeFromSuperview()
                        } else if status == "TXN_SUCCESS" {
                            if let txtId = jsonresponse["ORDERID"] as? String {
                                self.data.transactionId = txtId
                            }
                            _ = KPValidationToast.shared.showToastOnStatusBar(message: resMsg)
                            controller.navigationController?.popViewController(animated: true)
                            //self.onBoardingPaymentView.removeFromSuperview()
                            DispatchQueue.main.async {
                                self.setPaymentStatus()
                            }
                        } else {
                            self.showError(data: jsonresponse, view: self.view)
                            controller.navigationController?.popViewController(animated: true)
                            //self.onBoardingPaymentView.removeFromSuperview()
                        }
                    } else {
                        self.showError(data: jsonresponse, view: self.view)
                        controller.navigationController?.popViewController(animated: true)
                        //self.onBoardingPaymentView.removeFromSuperview()
                    }
                }
            } catch {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: "Something went wrong")
                controller.navigationController?.popViewController(animated: true)
                //self.onBoardingPaymentView.removeFromSuperview()
            }
        }
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController) {
        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Transaction Cancelled")
        controller.navigationController?.popViewController(animated: true)
        //self.onBoardingPaymentView.removeFromSuperview()
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController, error: NSError?) {
        controller.navigationController?.popViewController(animated: true)
        //self.onBoardingPaymentView.removeFromSuperview()
    }
}*/

extension PaymentVC: PGTransactionDelegate
{
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {
        //  let msg : String = responseString
        // var titlemsg : String = ""
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    
                    if let resMsg = jsonresponse["RESPMSG"] as? String, let status = jsonresponse["STATUS"] as? String {
                        if status == "PENDING" {
                            _ = KPValidationToast.shared.showToastOnStatusBar(message: resMsg)
                            controller.navigationController?.popViewController(animated: true)
                            //self.onBoardingPaymentView.removeFromSuperview()
                        } else if status == "TXN_SUCCESS" {
                            if let txtId = jsonresponse["ORDERID"] as? String {
                                self.data.transactionId = txtId
                            }
                            _ = KPValidationToast.shared.showToastOnStatusBar(message: resMsg)
                            controller.navigationController?.popViewController(animated: true)
                            //self.onBoardingPaymentView.removeFromSuperview()
                            DispatchQueue.main.async {
                                self.setPaymentStatus()
                            }
                        } else {
                            self.showError(data: jsonresponse, view: self.view)
                            controller.navigationController?.popViewController(animated: true)
                            //self.onBoardingPaymentView.removeFromSuperview()
                        }
                    } else {
                        self.showError(data: jsonresponse, view: self.view)
                        controller.navigationController?.popViewController(animated: true)
                        //self.onBoardingPaymentView.removeFromSuperview()
                    }
                }
            } catch {
                _ = KPValidationToast.shared.showToastOnStatusBar(message: "Something went wrong")
                controller.navigationController?.popViewController(animated: true)
                //self.onBoardingPaymentView.removeFromSuperview()
            }
        }
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController) {
        _ = KPValidationToast.shared.showToastOnStatusBar(message: "Transaction Cancelled")
        controller.navigationController?.popViewController(animated: true)
        //self.onBoardingPaymentView.removeFromSuperview()
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController, error: NSError?) {
        controller.navigationController?.popViewController(animated: true)
        //self.onBoardingPaymentView.removeFromSuperview()
    }
} 

extension PaymentVC {
    
    func getPrice() {
        self.showCentralSpinner()
        KPWebCall.call.getConfigPrice { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int , status == 200{
                
                if let data = dict["data"] as? NSDictionary{
                    print(data)
                    
                    if self.isfromCode == false {
                        _user.amountDemoApp = data.getInt32Value(key: "demo_price")
                        _user.amount = data.getInt32Value(key: "price")
                        _user.gstPrice =  data.getDoubleValue(key:"demo_gst_price")
                        _user.gstPresntage = data.getDoubleValue(key:"demo_gst_per")
                        _user.gstFullPrice = data.getDoubleValue(key:"full_gst_price")
                        
                        _appDelegator.saveContext()
                    }
                   
                    
                    if _user.gstPresntage == 0.0 {
                        _user.gstPresntage =  data.getDoubleValue(key:"demo_gst_per")
                        _appDelegator.saveContext()
                    }
                                           
                    if(self.isFromEntry){
                        let gstPrice = _giftUser.gst_price
                        self.lblPriceInfo.text = "Total Course Price : ₹ \(_giftUser.amount)"
                        self.lblGSTInfo.text = "GST @ \(Int(_user.gstPresntage)) % : ₹ \(gstPrice)"
                        self.lblTotalPriceInfo.text = "Total App Price(Round off)"
                        self.total =  Double(_giftUser.amount + gstPrice)
                        self.lblAmount.text = "₹ \(Int(self.total.rounded()))"
                    }
                    else {
                        
                        if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue) {
                            
                            let gstPrice = _user.gstPrice
                            self.lblPriceInfo.text = "Demo Course Price : ₹ \(_user.amountDemoApp)"
                          
                            self.lblGSTInfo.text = "GST @ \(Int(_user.gstPresntage)) % : ₹ \(Int(gstPrice.rounded()))"
                            self.lblTotalPriceInfo.text = "Demo App Price(Round off)"
                            self.total = Double(_user.amountDemoApp) + gstPrice.rounded()
                            self.lblAmount.text = "₹ \(Int(self.total.rounded()))"
                        }
                        else {
                            
                            let gstPrice = _user.gstFullPrice
                            self.lblPriceInfo.text = "Total Course Price : ₹ \(_user.amount)"
                            self.lblGSTInfo.text = "GST @ \(Int(_user.gstPresntage)) % : ₹ \(Int(gstPrice.rounded()))"
                            self.lblTotalPriceInfo.text = "Total App Price(Round off)"
                            self.total = Double(_user.amount) + gstPrice.rounded()
                            self.lblAmount.text = "₹ \(Int(self.total.rounded()))"
                        }
                    }
                    
                }
            }
        }
    }
    
    func generateCheckSum(block: @escaping (Bool) -> ()) {
        var param: [String: Any] = data.paramDict(proceedToPay: false)
        if isFromEntry {
            param["TXN_AMOUNT"] = "\(Int(self.total))"
            param["CUST_ID"] = _giftUser.email
        } else {
            if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue) {
                param["TXN_AMOUNT"] = "\(Int(self.total))"
            }
            else {
                param["TXN_AMOUNT"] = "\(Int(self.total))"
            }
            param["CUST_ID"] = _user.email
        }
        self.showCentralSpinner()
        KPWebCall.call.generateCheckSum(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary {
                if let chechSum = dict["CHECKSUMHASH"] as? String {
                    self.data.checkSumHash = chechSum
                }
                block(true)
            } else {
                block(false)
              self.showError(data: json, view: self.view)
            }
        }
    }
    
    func updatePaymentStatus(block: @escaping (Bool) -> (), with mode: PaymentMode) {
       
        //abhi change
        //let param: [String: Any] = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode": mode.rawValue, "transaction_id": data.transactionId,"amount": payMode == .paytm ? _user.amount : _user.usdPrice]

        var param: [String: Any]
        
        if(_user.localSelectedPaymentType == PaymentTypeLocalSelected.trial.rawValue){
            // gst
            //Demo Payment
            param = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode": mode.rawValue, "transaction_id": data.transactionId,"amount": payMode == .paytm ? _user.amountDemoApp : _user.usdPriceDemoApp, "which_user": WhichUser.demo_user.rawValue, "check_day": "Done","gst_price":"\(Int(_user.gstPrice.rounded()))"]
        }
        else {
          
            //Full Payment
            param = ["id": _user.id, "unique_id": _user.uniqueId, "payment_mode": mode.rawValue, "transaction_id": data.transactionId,"amount": payMode == .paytm ? _user.amount : _user.usdPrice, "which_user":WhichUser.reg_user.rawValue, "check_day":"Undone","gst_price":"\(Int(_user.gstFullPrice.rounded()))"]
        }
        
        self.showCentralSpinner()
        KPWebCall.call.updatePaymentStatus(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                    _user.setPaymentStatus(dict: data)
                    _user.setLocalSelectedPaymentType(type: .notSelected)
                    _appDelegator.saveContext()
                    _ = KPValidationToast.shared.showToastOnStatusBar(message: "Payment \(data.getStringValue(key: "payment_status"))")
                    block(true)
                }else if let msg = dict["message"] as? NSDictionary{
                    weakself.showError(data: msg, view: weakself.view)
                    block(false)
                }else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func setPaymentStatus(block: @escaping (Bool) -> (), with mode: PaymentMode) {
        let param: [String: Any] = ["id": _giftUser.id,"unique_id": _giftUser.uniqueId, "payment_mode": mode.rawValue, "transaction_id": data.transactionId, "amount": payMode  == .paytm ? _giftUser.amount : _giftUser.usdPrice, "email": _giftUser.email,"gst_price":"\(_giftUser.gst_price)"]
        self.showCentralSpinner()
        KPWebCall.call.setPaymentStatus(param: param) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200, let data = dict["data"] as? NSDictionary {
                     _giftUser.setPaymentStatus(dict: data)
                    _ = ValidationToast.showStatusMessage(message: "Payment \(data.getStringValue(key: "payment_status"))")
                    block(true)
                }else if let msg = dict["message"] as? NSDictionary{
                    self.showError(data: msg, view: self.view)
                    block(false)
                }else {
                    self.showError(data: json, view: self.view)
                    block(false)
                }
            }else {
                self.showError(data: json, view: self.view)
                block(false)
            }
        }
    }
}
