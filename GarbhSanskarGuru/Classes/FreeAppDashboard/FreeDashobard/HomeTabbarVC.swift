//
//  HomeTabbarVC.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 16/06/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import SDWebImage

var homeTabBarVC = HomeTabbarVC()

class HomeTabbarVC: ParentViewController, UITabBarDelegate {
    
    @IBOutlet weak var cvTab: UICollectionView!
    
    let dashBoarVC = UIStoryboard.init(name: "FreeAppDashboard", bundle: nil).instantiateViewController(withIdentifier: "FreeDashboardContainer") as! FreeDashboardContainer
    let askWithExpertVC = UIStoryboard.init(name: "FreeAppDashboard", bundle: nil).instantiateViewController(withIdentifier: "AskExpertMomVC") as! AskExpertMomVC
    let webViewVC = UIStoryboard.init(name: "FreeAppDashboard", bundle: nil).instantiateViewController(withIdentifier:"WebViewController") as! WebViewController
    let activitiesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SubscribeContainer") as! SubscribeContainer
    
    var mycellWidth: CGFloat = 0.0
    var previousIndex = 0
    var selectedIndex: Int = 0
    @IBOutlet weak var containerView: UIView!
    
    var arrImages = [String]()
    var arrTitle = [String]()
    
    var arrTabViewControllers: [UIViewController]!
    var isShowPopup:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = _user.name
        self.prepareTabbar()
        
        homeTabBarVC = self as HomeTabbarVC
        
        let checkFirstTime = UserDefaults.standard.value(forKey: "AppfromBackground")
        if checkFirstTime as? String == "1" {
            UserDefaults.standard.set("0", forKey: "AppfromBackground")
            if _user.paymentStatus == .pending && _appDelegator.getPaymentHideShowStatus() && !_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser() {
                DispatchQueue.main.asyncAfter(
                    deadline: DispatchTime.now() + Double(Int64(15 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                        self.fiftinSecAds()
                })
            }
        }
        
        
        
        loadVC(selectedIndex)
        self.removeVC()
        let label = UILabel(frame: CGRect.zero)
        label.text = arrTitle[0]
        label.sizeToFit()
        mycellWidth = label.frame.size.width
        
        self.cvTab.register(UINib(nibName: "TabCVCell", bundle: .main), forCellWithReuseIdentifier: "TabCVCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // condition manage only for redirection of padi dashboard to freedashboard on garbhsanskar info click from sideMenu
        
        
        if _appDelegator.isClickGarbhSanskarInfo{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "ic_backButton"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backActionFromPaidDashboard))
        }else{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "ic_SlideMenu"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(shutterAction(_:)))
        }
        self.checkOtherLoginDevice()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func prepareTabbar(){
        if ((_user.pregnancyStatus == .planning && _user.paymentStatus == .completed) || (_user.app_stop == "Pause")) || !_appDelegator.getPaymentHideShowStatus() || _appDelegator.isClickGarbhSanskarInfo ||   _appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser(){
            // Paid user
            arrImages = ["ic_Dashboard","ic_ExpertMom","ic_liveFb"]
            arrTitle = ["Dashboard","Ask To Expert Mom","Live Facebook GS"]
            arrTabViewControllers = [dashBoarVC,askWithExpertVC,webViewVC]
        }else{
            // FreeUser, Add new Payment option in tabbar.
            arrImages = ["ic_Dashboard","tab-2","ic_ExpertMom","ic_liveFb"]
            arrTitle = ["Dashboard","Garbhsanskar Course","Ask To Expert Mom","Live Facebook GS"]
            arrTabViewControllers = [dashBoarVC,activitiesVC,askWithExpertVC,webViewVC]
        }
    }
    
    func loadVC(_ selIndex:Int){
        let vc = arrTabViewControllers[selIndex]
        addChild(vc)
        if vc is ActivitiesVC{
            vc.view.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: _screenSize.height)
        }else{
            vc.view.frame = containerView.bounds
        }
        containerView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    func removeVC(){
        let previousVC = arrTabViewControllers[previousIndex]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        loadVC(selectedIndex)
        previousIndex = selectedIndex
    }
    
    @objc func backActionFromPaidDashboard(){
        self.tabBarController?.selectedIndex = 0
    }
    
}



extension UIViewController {
    /// Top most ViewController
    func topMostViewController() -> UIViewController {
        
        if self.presentedViewController == nil {
            return self
        }
        
        if let navigation = self.presentedViewController as? UINavigationController {
            return (navigation.visibleViewController?.topMostViewController())!
        }
        /*
         if let tab = self.presentedViewController as? UITabBarController {
         if let selectedTab = tab.selectedViewController {
         return selectedTab.topMostViewController()
         }
         return tab.topMostViewController()
         }
         */
        return (self.presentedViewController?.topMostViewController())!
    }
}


extension UIApplication {
    
    /// Top most ViewController
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
    
    /// Top visible viewcontroller
    var topMostVisibleViewController : UIViewController? {
        
        if UIApplication.shared.keyWindow?.rootViewController is UINavigationController {
            return (UIApplication.shared.keyWindow?.rootViewController as! UINavigationController).visibleViewController
        }
        return nil
    }
}


extension HomeTabbarVC{
    
    func fiftinSecAds () {
        // call webservice at 15 second after first launch
        KPWebCall.call.getPopupBanner { [weak self] (json, statusCode) in
            guard self != nil else {return}
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int, status == 200 {
                let res = PopupInfo(dictionary: dict["data"] as! NSDictionary)
                
                self?.sdwebImage(res: res ) { succes  in
                    if succes {
                        if let viewControllers = self?.navigationController?.viewControllers {
                            print(viewControllers)
                            for viewController in viewControllers {
                                // some process
                                if  _appDelegator.isUserLoggedIn() {
                                    if (viewController is FreeAppDashboardVC || viewController is HomeTabbarVC)   {
                                        let topController = self?.navigationController?.topViewController
                                        if topController is HomeTabbarVC {
                                            self?.displayViewController(SLpopupViewAnimationType.fade, nibName: "PopupFreeToPaid", blockOK: {
                                                UIViewController().dismissPopUp()
                                                let activitiesVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SubscribeContainer") as! SubscribeContainer
                                                activitiesVC.isShowBackButton = false
                                                self?.navigationController?.pushViewController(activitiesVC, animated: true)
                                            }, blockCancel: {
                                                UIViewController().dismissPopUp()
                                            }, objData:res as AnyObject)
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    func sdwebImage(res:PopupInfo,imageLoad :@escaping ( Bool ) -> ()) {
        let strLanguage = _appDelegator.getLanguage()
        var strUrl = ""
        
        if strLanguage == KHindi{
            strUrl = res.hindi_image
        }else if strLanguage == kGujrati{
            strUrl = res.guj_image
        }else{
            strUrl = res.eng_image
        }
        
        SDWebImageDownloader.shared.downloadImage(with: URL(string: strUrl), options: .highPriority, progress: nil, completed: {  image, data, error, finished  in
            
            if image != nil && finished {
                // Cache image to disk or memory
                if  strLanguage == KHindi{
                    res.hindi_Uimage = image
                }else if  strLanguage == kGujrati{
                    res.guj_Uimage = image
                }else{
                    res.eng_Uimage = image
                }
                imageLoad(true)
            }
        })
    }
    
    
    func checkOtherLoginDevice(){
        self.getToday(isNeedLoader: false, block: { (done) in
        })
    }
    
    func prepareOnBoardingFreeDashboard() {
        let onBoardingView = FreeDashBoard.instantiateView(with: self.view)
        onBoardingView.updateUI()
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .later{
            }else{
                // show paid dashboard with set today date in pregnent day as first.
                self.prepareOnBoarding()
            }
        }
    }
    
    func prepareOnBoarding() {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.setupUI(isCancelable: true)
        onBoardingView.actionBlock = { (action) in
            if action == .cancel{
                onBoardingView.removeFromSuperview()
            }else{
                guard action == .done else {return}
                guard !onBoardingView.txtDay.text!.isEmpty else {
                    _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                    return
                }
                onBoardingView.endEditing(true)
                let currDay = onBoardingView.txtDay.text!.integerValue!
                print("Current Day \(currDay)")
                
                guard currDay > 0 && currDay < 280 else {
                    _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                    return
                }
                self.getPregnancyDay(currentDay: currDay, block: { (done) in
                    if done {
                        _appDelegator.setOnBoarding(isOnBoardingShown: true)
                        onBoardingView.removeFromSuperview()
                        DispatchQueue.main.async {
                            self.isShowPopup = true
                            self.getToday(isNeedLoader: true, block: { (done) in
                                if done {
                                    _appDelegator.navigateDirectlyToHome()
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    func getToday(isNeedLoader:Bool, block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id]
        if isNeedLoader{
            self.showCentralSpinner()
        }
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            if isNeedLoader{
                weakself.hideCentralSpinner()
            }
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        
                        if(!deviceId.isEqual(str: _deviceID)){
                            
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            block(false)
                        }
                        else {
                            
                            let demoDay = dict.getInt16Value(key: "demo_day")
                            let whichUser = dict.getStringValue(key: "which_user ")
                            
                            print("\(demoDay) \(whichUser)")
                            _user.setCurrentDay(dict: dict)
                            _user.setWhichUserType(strType: whichUser)
                            _appDelegator.saveContext()
                            
                            if !weakself.isShowPopup{
                                weakself.isShowPopup = true
                                if !_appDelegator.isClickGarbhSanskarInfo{ // Detect back action from paid to freeDashboard.
                                    if _user.currDay >= 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                                        let getFreePopUp = UserDefaults.standard.bool(forKey: "alreadyOpenPopup")
                                        if !_appDelegator.isClickFromDesierBaby && !getFreePopUp{
                                            _appDelegator.isClickFromDesierBaby = false
                                            weakself.prepareOnBoardingFreeDashboard()
                                        }
                                    }
                                }
                            }
                            block(true)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
}

extension HomeTabbarVC {
    
    @IBAction func btnShareApp(_ sender: UIButton) {
        self.shareApp(sender: sender)
    }
    
    @IBAction func btnNotificationTapped(_  sender: Any) {
        _appDelegator.presentPushNotificationVC()
    }
    
    @IBAction func btnAnnounctment(_ sender: Any) {
        _appDelegator.presentAnnouncementVC()
    }
}

extension HomeTabbarVC {
    
    func shareApp(sender: UIButton) {
        let title = _appName
        let textToShare = "Garbh Sanskar Guru App is a unique and first of it’s kind mobile application to nurture the values in unborn child. This App contains total 280 days of various activities for pregnant lady. Expecting moms can perform these activities on day to day basis which is going to help overall development of child.\n"
        let appStoreText = "Link for App Store"
        let playStoreText = "Link for Play Store"
        let myWebsite = URL(string: "https://itunes.apple.com/us/app/majestic-garbh-sanskar/id1448235489?ls=1&mt=8")
        let otherWebsite = URL(string: "http://play.google.com/store/apps/details?id=com.gs.garbhsanskarguru")
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [textToShare,appStoreText,myWebsite!,playStoreText,otherWebsite!], applicationActivities: nil)
        activityViewController.setValue(title, forKey: "Subject")
        activityViewController.popoverPresentationController?.sourceView = (sender)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension HomeTabbarVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //        if (UIDevice.deviceType == .iPad){
    //            return CGSize(width: self.view.frame.size.width/3, height: 54)
    //        }else {
    //            if indexPath.item == selectedIndex {
    //                if (UIDevice.deviceType == .iPad) {
    //                    return CGSize(width:mycellWidth , height: 54)
    //                }
    //                return CGSize(width:mycellWidth , height: 54)
    //            }
    //            return CGSize(width:(self.view.frame.size.width - mycellWidth)/2 , height: 54)
    //        }
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (UIDevice.deviceType == .iPad){
            return CGSize(width: self.view.frame.size.width/CGFloat(arrTitle.count), height: 54)
        }else {
            if indexPath.item == selectedIndex {
                if (UIDevice.deviceType == .iPad) {
                    return CGSize(width:mycellWidth , height: 54)
                }
                return CGSize(width:mycellWidth , height: 54)
            }
            return CGSize(width:(self.view.frame.size.width - mycellWidth)/CGFloat(arrTitle.count - 1) , height: 54)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"TabCVCell", for: indexPath) as! TabCVCell
        
        cell.imgTab.image = UIImage(named: arrImages[indexPath.item])
        if indexPath.item == selectedIndex{
            cell.lblTabTitle.text = arrTitle[indexPath.item]
            cell.lblTabTitle.textColor = GarbhColor.pink
            cell.imgTab.tintColor =  GarbhColor.pink
        }else {
            cell.lblTabTitle.text = ""//arrTitle[indexPath.item]
            cell.lblTabTitle.textColor = .black
            cell.imgTab.tintColor = .darkGray
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.item
        if selectedIndex == 3 {
            isFromLiveFB = true
        }
        removeVC()
        let label = UILabel(frame: CGRect.zero)
        label.text = arrTitle[indexPath.item]
        label.sizeToFit()
        mycellWidth = label.frame.size.width
        self.cvTab.reloadData()
    }
}
