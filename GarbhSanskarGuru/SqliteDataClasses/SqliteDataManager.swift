//
//  SqliteDataManager.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 31/05/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit
import FMDB

class SqliteDataManager: NSObject {

    static let shared: SqliteDataManager = SqliteDataManager()
    
    let databaseFileName = "GSGuru.sqlite"
     
    var pathToDatabase: String!
     
    var database: FMDatabase!
    
    override init() {
        super.init()
     
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
        
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }else{
                do{
                    let bundlePath = Bundle.main.path(forResource: "GSGuru", ofType: ".sqlite")
                    let urlPath = URL.init(string: pathToDatabase)?.path
                    try FileManager.default.copyItem(atPath: bundlePath!, toPath: urlPath!)
                    
//                    let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
//                    try FileManager.default.copyItem(atPath: documentsDirectory, toPath: pathToDatabase)
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
     
        if database != nil {
            if database.open() {
                return true
            }
        }
     
        return false
    }
    
//    func insertReminderData(reminder:ReminderDataModel) {
//        if openDatabase() {
//            
//            let query = "insert into tbl_reminder ('message','reminder_date','weekdays','identifier_day_1','identifier_day_2','identifier_day_3','identifier_day_4','identifier_day_5','identifier_day_6','identifier_day_7') values ('\(reminder.message!)', '\(reminder.reminder_date!)', \(reminder.weekDays!), '\(reminder.indetifier_day_1!)', '\(reminder.indetifier_day_2!)','\(reminder.indetifier_day_3!)','\(reminder.indetifier_day_4!)','\(reminder.indetifier_day_5!)','\(reminder.indetifier_day_6!)','\(reminder.indetifier_day_7!)');"
//            
//            if !database.executeStatements(query) {
//                print("Failed to insert initial data into the database.")
//                print(database.lastError(), database.lastErrorMessage())
//            }
//            database.close()
//        }
//    }
    
//    func loadReminder() -> [ReminderDataModel]! {
//        var reminderdata: [ReminderDataModel]!
//
//        if openDatabase() {
//            let query = "select * from tbl_reminder"
//            do {
//                print(database)
//                let results = try database.executeQuery(query, values: nil)
//
//                while results.next() {
//                    let reminder = ReminderDataModel()
//                    reminder.id = Int(results.int(forColumn: "id"))
//                    reminder.message = results.string(forColumn: "message")
//                    reminder.reminder_date = results.string(forColumn: "reminder_date")
//                    reminder.weekDays = results.string(forColumn: "weekdays")
//                    reminder.indetifier_day_1 = results.string(forColumn: "identifier_day_1")
//                    reminder.indetifier_day_2 = results.string(forColumn: "identifier_day_2")
//                    reminder.indetifier_day_3 = results.string(forColumn: "identifier_day_3")
//                    reminder.indetifier_day_4 = results.string(forColumn: "identifier_day_4")
//                    reminder.indetifier_day_5 = results.string(forColumn: "identifier_day_5")
//                    reminder.indetifier_day_6 = results.string(forColumn: "identifier_day_6")
//                    reminder.indetifier_day_7 = results.string(forColumn: "identifier_day_7")
//
//                    reminderdata.append(reminder)
//                }
//            }
//            catch {
//                print(error.localizedDescription)
//            }
//            database.close()
//        }
//        return reminderdata
//    }
    
//    func updateMovie(reminder:ReminderDataModel) {
//        if openDatabase() {
//            let query = "update tbl_reminder set message=?, reminder_date=?, weekdays=?, identifier_day_1=?, identifier_day_2=?, identifier_day_3=?, identifier_day_4=?,identifier_day_5=?,identifier_day_6=?,identifier_day_7=? where id=?"
//
//            do {
//                try database.executeUpdate(query, values: [reminder.message!, reminder.reminder_date!,reminder.weekDays!,reminder.indetifier_day_1!,reminder.indetifier_day_2!,reminder.indetifier_day_3!,reminder.indetifier_day_4!,reminder.indetifier_day_5!,reminder.indetifier_day_6!,reminder.indetifier_day_7!, reminder.id!])
//            }
//            catch {
//                print(error.localizedDescription)
//            }
//
//            database.close()
//        }
//    }
//
//    func deleteReminder(withID ID: Int) -> Bool {
//        var deleted = false
//
//        if openDatabase() {
//
//            let query = "delete from tbl_reminder where id =?"
//            do {
//                try database.executeUpdate(query, values: [ID])
//                deleted = true
//            }catch {
//                print(error.localizedDescription)
//            }
//            database.close()
//        }
//        return deleted
//    }
    
}
