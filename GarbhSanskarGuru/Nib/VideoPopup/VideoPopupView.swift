//
//  VideoPopupView.swift
//  GarbhSanskarGuru
//
//  Created by kunjnewmac on 09/07/20.
//  Copyright © 2020 Jayesh Tejwani. All rights reserved.
//

import UIKit

class VideoPopupView: UIView {
    
    @IBOutlet var animatedViews: UIView!
    @IBOutlet weak var playerView: WKYTPlayerView!
    
    
    @IBOutlet var buttonSkip: UIButton!
    @IBOutlet var buttonSubscr: UIButton!
    var actionBlock: (KAction)->Void = {_ in}
    
    @IBOutlet var labelDescription: JPWidthLabel!
    
    class func instantiateView(with view: UIView, animate: Bool = true) -> VideoPopupView {
        let objView = UINib(nibName: "VideoPopupView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! VideoPopupView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
    
    override func layoutSubviews() {
        
    }
    
    func setupUI(btnFirst:String, btnsecond:String, objModel: PaymentVideoModel, isDemoComplete:Bool = false){
        self.buttonSkip.setTitle(btnFirst, for: .normal)
        self.buttonSkip.layer.cornerRadius = buttonSkip.frame.size.height / 2
        self.buttonSkip.clipsToBounds = true
                
        if isDemoComplete {
            self.buttonSubscr.isHidden = false
            
            self.buttonSubscr.setTitle(btnsecond, for: .normal)
            self.buttonSubscr.layer.cornerRadius = buttonSubscr.frame.size.height / 2
            self.buttonSubscr.clipsToBounds = true
        }else{
            self.buttonSubscr.isHidden = true
        }
        self.labelDescription.text = objModel.text
        self.setUPlayer(with: objModel.videoUrl.getYoutubeId())
    }
    
//    func setupValues(objModel: PaymentVideoModel, isDemoComplete:Bool = false){
//        if isDemoComplete {
//            self.buttonSubscr.isHidden = false
//        }else{
//            self.buttonSubscr.isHidden = true
//        }
//        self.labelDescription.text = objModel.text
//        self.setUPlayer(with: objModel.videoUrl.getYoutubeId())
//    }
    
    func animateView(with tag: Int) {
        UIView.animate(withDuration: 1.0, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.animatedViews.transform = CGAffineTransform(translationX: 0, y: 0).concatenating(CGAffineTransform(scaleX: 0.5, y: 0.5))
            
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.animatedViews.transform = .identity
            }, completion: { _ in
            })
        })
    }
        
    @IBAction func onBtnSkipAction(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
    
    @IBAction func onSubscribeAction(_ sender: UIButton) {
        self.actionBlock(.done)
    }
    
}

extension VideoPopupView {
    
    func setUPlayer(with playerID: String?) {
        guard let playeID = playerID else {return}
        playerView.setPlaybackQuality(.auto)
        playerView.setShuffle(false)
        playerView.setLoop(false)
        let playerVars = ["playsinline": 1, "autohide": 1, "showinfo": 0, "controls":1, "origin" : "http://youtube.com"] as [String : Any] // 0: will play video in fullscreen
        playerView.load(withVideoId: playeID, playerVars: playerVars)
    }
    
}
