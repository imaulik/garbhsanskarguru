//
//  SignUpVC2.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 12/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit
import SafariServices

class DetailTextCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func prepareTermsUI() {
        lblTitle.text = """
        ❏ Fee is not refundable in any circumstances.
        
        ❏ Any Information provided should not be taken as a medical advice; consult your doctor in all matters related to your health.
        """
        lblTitle.textColor = #colorLiteral(red: 0.937254902, green: 0.2549019608, blue: 0.4470588235, alpha: 1)
        lblTitle.font = UIFont.GarbhFontWith(GarbhSanskarFont.Avenir_Black, size: 16.widthRatio)
    }
    
    func prepareUI(with status: PregnancyStatus) {
        let currLanguage: AppLanguage = AppLanguage(rawValue: _appDelegator.getAppLanguage() ?? 1)!
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        if isPaymentAllowed{
            switch currLanguage {
            case .hindi:
                switch status {
                case .planning:
                    lblTitle.text = """
                    ❏ इस ऐप को "गर्भ संस्कार" से संबंधित गतिविधियों को करने के लिए विकसित किया गया है जो गर्भवती महिला / या विवाहित जोड़े के लिए डिज़ाइन किया गया है। यह ऐप गर्भावस्था के दिन पर आधारित हैं और उसी अनुसार गतिविधियों को दर्शाता है।
                    
                    ❏ आप एप की पूर्व खरीदी कर सकते हैं हालांकि प्रति दिन की गतिविधियां आपके लिए उपलब्ध नहीं होंगी।
                    
                    ❏ गर्भधारण होने के बाद आपको दैनिक गतिविधि शुरू करने के लिए गर्भधारण दिन एप में डालना पड़ेगा |
                    
                    ❏ ऐप की सामग्री प्राचीन शास्त्रों, संस्कृति, वैज्ञानिक सर्वेक्षण और डोमेन विशेषज्ञों के इनपुट पर आधारित है
                    
                    ❏ ऐप में कुल 280 दिन की गतिविधियां उपलब्ध हैं।
                    
                    ❏ Paid ऐप धारक केवल गर्भावस्था के संबंधित दिन की गतिविधि कर सकता है। किसी भी समय, 3 दिनों से अधिक गतिविधियाँ उपलब्ध नहीं हो सकती हैं। नहीं की गई गतिविधियां चली जाएगी।
                    
                    ❏ एप की गतिविधिया आपकी गर्भावस्था के वर्तमान दिन के अनुसार शुरू होगी। आप अतीत की गतिविधियों तक पहुँचने में सक्षम नहीं होंगे और न ही इसे कर पाएंगे। अनुसूची गर्भावस्था के दिए गए दिन से 280 वें दिन तक चलेगी।
                    
                    ❏ 280 दिनों की गतिविधि खत्म होने के बाद ऐप की दैनिक गतिविधियोंका खंड आपके लिए उपलब्ध नहीं होगा ।
                    
                    ❏ एप में दिए गए मटेरियल का पूर्ण अधिकार मैजेस्टिक गर्भ संस्कार का ही रहेगा । एप में प्रदान की गई सामग्री को साझा करना सख्त वर्जित है।
                    
                    ❏ ऐप के लिए भुगतान किया गया शुल्क किसी भी परिस्थिति में वापस नहीं किया जाएगा |
                    
                    ❏ प्रदान की गई कोई भी जानकारी चिकित्सा सलाह के रूप में नहीं ली जानी चाहिए; अपने स्वास्थ्य से संबंधित सभी मामलों में अपने चिकित्सक से परामर्श करें।
                    
                    ❏ एप की खरीदी इच्छित संतान प्राप्ति की गारंटी नहीं है। आपकी अपेक्षा के अनुसार गुणों का होना अत्यंत आवश्यक है, और यह कई अन्य कारणों पर निर्भर करता है। यह ऐप आपको सिर्फ प्रक्रिया का पालन करने में मदद करेगा।
                    
                    ❏ "इच्छित संतान" वाक्यांश का शिशु के लिंग से कोई लेना-देना नहीं है, " इच्छित संतान" का अर्थ विशिष्ट गुणों वाला बच्चा है।
                    """
                case .pregnant:
                    lblTitle.text = """
                    ❏ ऐप की सामग्री प्राचीन शास्त्रों, संस्कृति, वैज्ञानिक सर्वेक्षण और डोमेन विशेषज्ञों के इनपुट पर आधारित है
                    
                    ❏ ऐप में कुल 280 दिन की गतिविधियां उपलब्ध हैं।
                    
                    ❏ Paid ऐप धारक केवल गर्भावस्था के संबंधित दिन की गतिविधि कर सकता है। किसी भी समय, 3 दिनों से अधिक गतिविधियाँ उपलब्ध नहीं हो सकती हैं। नहीं की गई गतिविधियां चली जाएगी।
                    
                    ❏ एप की गतिविधिया आपकी गर्भावस्था के वर्तमान दिन के अनुसार शुरू होगी। आप अतीत की गतिविधियों तक पहुँचने में सक्षम नहीं होंगे और न ही इसे कर पाएंगे। अनुसूची गर्भावस्था के दिए गए दिन से 280 वें दिन तक चलेगी।
                    
                    ❏ 280 दिनों की गतिविधि खत्म होने के बाद ऐप की दैनिक गतिविधियोंका खंड आपके लिए उपलब्ध नहीं होगा ।
                    ❏ एप में दिए गए मटेरियल का पूर्ण अधिकार मैजेस्टिक गर्भ संस्कार का ही रहेगा । एप में प्रदान की गई सामग्री को साझा करना सख्त वर्जित है।
                    
                    ❏ ऐप के लिए भुगतान किया गया शुल्क किसी भी परिस्थिति में वापस नहीं किया जाएगा |
                    
                    ❏ प्रदान की गई कोई भी जानकारी चिकित्सा सलाह के रूप में नहीं ली जानी चाहिए; अपने स्वास्थ्य से संबंधित सभी मामलों में अपने चिकित्सक से परामर्श करें।
                    
                    ❏ एप की खरीदी इच्छित संतान प्राप्ति की गारंटी नहीं है। आपकी अपेक्षा के अनुसार गुणों का होना अत्यंत आवश्यक है, और यह कई अन्य कारणों पर निर्भर करता है। यह ऐप आपको सिर्फ प्रक्रिया का पालन करने में मदद करेगा।
                    
                    ❏ "इच्छित संतान" वाक्यांश का शिशु के लिंग से कोई लेना-देना नहीं है, " इच्छित संतान" का अर्थ विशिष्ट गुणों वाला बच्चा है।
                    """
                case .none:
                    break
                }
                break
            case .english:
                switch status {
                case .planning:
                    lblTitle.text = """
                    ❏ This app is developed to perform activities related to “Grabh Sanskar” which are designed for pregnant lady/married couple. It shows activities based on current day of pregnancy in progressive manner.
                    
                    ❏ Couple/lady who is planning for a baby, can PRE PURCHASE this APP for future use, however the activities will not be available to you until you enter actual pregnancy day.
                    
                    ❏ Content of the App is based on ancient scriptures, culture, scientific surveys and inputs from domain experts.
                    
                    ❏ Activities for 280 days are available in the app.
                    
                    ❏ Paid APP holder can perform an activity on concerned day of pregnancy only. At any point of time, activities would be available for not more than 3 days. Unperformed activities would lapse automatically.
                    
                    ❏ Activities are planned and it would start according to current day of your pregnancy. You shall not be able to access past activities nor perform it. The schedule shall run from your current day to 280th day of pregnancy period.
                    
                    ❏ Once the 280th day activity is over, daily activities segment of an app would not be available to you.
                    
                    ❏ Majestic GarbhSanskar would be the sole proprietor of all the material. Sharing of the material provided is strictly prohibited.
                    
                    ❏ Fees is not refundable in any circumstances
                    
                    ❏ Any information provided should not be taken as a medical advice; consult your doctor in all matters related to your health.
                    
                    ❏ Purchasing an App does not guarantee to have desired baby. Having the qualities as per your expectation requires utmost care, and that depends on many other factors. This App would only help you to follow the process.
                    
                    ❏ “Icchit Santan” phrase has nothing to do with gender of baby, Meaning of “Icchit Santan(Desired baby )” is a baby with desired qualities
                    """
                case .pregnant:
                    lblTitle.text = """
                    ❏ Content of the App is based on ancient scriptures, culture, scientific surveys and inputs from domain experts.
                    
                    ❏ Activities for 280 days are available in the app.
                    
                    ❏ Paid APP holder can perform an activity on concerned day of pregnancy only. At any point of time, activities would be available for not more than 3 days. Unperformed activities would lapse automatically.
                    
                    ❏ Activities are planned and it would start according to current day of your pregnancy. You shall not be able to access past activities nor perform it. The schedule shall run from your current day to 280th day of pregnancy period.
                    
                    ❏ Once the 280th day activity is over, daily activities segment of an app would not be available to you.
                    
                    ❏ Majestic GarbhSanskar would be the sole proprietor of all the material. Sharing of the material provided is strictly prohibited.
                    
                    ❏ Fees is not refundable in any circumstances
                    
                    ❏ Any information provided should not be taken as a medical advice; consult your doctor in all matters related to your health.
                    
                    ❏ Purchasing an App does not guarantee to have desired baby. Having the qualities as per your expectation requires utmost care, and that depends on many other factors. This App would only help you to follow the process.
                    
                    ❏ “Icchit Santan” phrase has nothing to do with gender of baby, Meaning of “Icchit Santan(Desired baby )” is a baby with desired qualities.
                    """
                case .none:
                    break
                }
                break
            case .gujarati:
                switch status {
                case .planning:
                    lblTitle.text = """
                    ❏ આ એપ્લિકેશન "ગર્ભ સંસ્કર" ને લગતી પ્રવૃત્તિઓ કરવા માટે વિકસાવવામાં આવી છે જે ગર્ભવતી સ્ત્રી / અથવા પરિણીત દંપતિ માટે રચાયેલ છે. તે ગર્ભાવસ્થા દિવસના આધારે પ્રવૃત્તિઓ સૂચવે છે.
                    
                    ❏ બાળકનું આયોજનન કરતા દંપતિ/મહિલાઓ આ એપલીકેશ ની પૂર્વ ખરીદી કરી શકે છે પરંતુ એપ ની દૈનિક પ્રવૃત્તિઓ તમારા માટે ઉપલબ્ધ રહેશે નહીં.
                    
                    ❏ ગર્ભ ધારણ કર્યા બાદ ગર્ભધારણ કર્યાનો દિવસ એપ માં નાખવો પડશે. પછીજ આ એપ્લીકેશન ની દૈનિક પ્રવૃત્તિઓનો પ્રવેશ મળશે.
                    
                    ❏ આ એપ્લિકેશનની સામગ્રી પ્રાચીન ગ્રંથો, સંસ્કૃતિ, વૈજ્ઞાનિક સર્વેક્ષણો અને ડોમેન નિષ્ણાતોના ઇનપુટ્સ પર આધારિત છે.
                    
                    ❏ એપ્લિકેશનમાં કુલ 280 દિવસની પ્રવૃત્તિઓ ઉપલબ્ધ છે.
                    
                    ❏ એપ્લીકેશન ધારક ફક્ત ગર્ભાવસ્થાના સંબંધિત દિવસની જ પ્રવૃત્તિ કરી શકે છે. કોઈપણ સમયે, 3 દિવસથી વધુ દિવસોની પ્રવૃત્તિઓ ઉપલબ્ધ થઈ શકતી નથી. જે તે દિવસ ની પ્રવૃત્તિ એજ દિવસે કરવી પડશે.
                    
                    ❏ પ્રવૃત્તિઓ તમારી ગર્ભાવસ્થાના વર્તમાન દિવસ મુજબ શરૂ થશે. તમે ભૂતકાળની પ્રવૃત્તિઓનો ઉપયોગ કરી શકશો નહીં. એપનું પ્રવૃત્તિઓ નું શેડ્યૂલ તમારા વર્તમાન દિવસથી ગર્ભાવસ્થાના સમયગાળાની 280 મી દિવસ સુધી ચાલશે.
                    
                    ❏ એકવાર 280 દિવસની પ્રવૃત્તિઓ થઈ જાય પછી એપ્લિકેશનની દૈનિક પ્રવૃત્તિ સેગમેન્ટ તમારા માટે ઉપલબ્ધ રહેશે નહીં.
                    
                    ❏ એપ માં આપવામાં આવતી બધી માહિતી મેજેસ્ટીક ગર્ભસંસ્કરની જ રહેશે. અમારા દ્વારા પ્રદાન કરવામાં આવેલ સામગ્રીની વહેંચણી પ્રતિબંધિત છે.
                    
                    ❏ એપ્લિકેશન માટે ચૂકવવામાં આવતી ફી કોઈપણ સંજોગોમાં પરત મળશે નહીં.
                    
                    ❏ એપ્લીકેશનમાં આપવામાં આવતી કોઈ પણ માહિતી ને તબીબી સલાહ તરીકે લેવી નહીં; તમારા સ્વાસ્થ્ય સંબંધિત કોઈપણ બાબતોમાં તમારા ડૉક્ટરનો સંપર્ક કરો.
                    
                    ❏ એપ્લિકેશનને subscribe કરવી એ ઇચ્છિત બાળકની બાંહેધરી નથી. તમારી અપેક્ષા અનુસારના ગુણો  વાળા સંતાન ની ઉત્ત્પત્તિ માટે અત્યંત કાળજીની જરૂર છે, અને તે અન્ય ઘણા પરિબળો પર પણ આધારિત છે. આ એપ્લિકેશન ફક્ત તમને પ્રક્રિયાને અનુસરવામાં સહાય કરશે.
                    
                    ❏ “ઈચ્છિત સંતાન" શબ્દનો બાળકના લિંગ સાથે કાંઈ નિસ્બત નથી, "ઇચ્ચિત સંતન (ઇચ્છિત બાળક)" નો અર્થ ચોક્કસ ગુણો વાળા બાળક તરીકે કરવો.
                    """
                case .pregnant:
                    lblTitle.text = """
                    ❏ આ એપ્લિકેશનની સામગ્રી પ્રાચીન ગ્રંથો, સંસ્કૃતિ, વૈજ્ઞાનિક સર્વેક્ષણો અને ડોમેન નિષ્ણાતોના ઇનપુટ્સ પર આધારિત છે.
                    
                    ❏ એપ્લિકેશનમાં કુલ 280 દિવસની પ્રવૃત્તિઓ ઉપલબ્ધ છે.
                    
                    ❏ એપ્લીકેશન ધારક ફક્ત ગર્ભાવસ્થાના સંબંધિત દિવસની જ પ્રવૃત્તિ કરી શકે છે. કોઈપણ સમયે, 3 દિવસથી વધુ દિવસોની પ્રવૃત્તિઓ ઉપલબ્ધ થઈ શકતી નથી. જે તે દિવસ ની પ્રવૃત્તિ એજ દિવસે કરવી પડશે.
                    ❏ પ્રવૃત્તિઓ તમારી ગર્ભાવસ્થાના વર્તમાન દિવસ મુજબ શરૂ થશે. તમે ભૂતકાળની પ્રવૃત્તિઓનો ઉપયોગ કરી શકશો નહીં. એપનું પ્રવૃત્તિઓ નું શેડ્યૂલ તમારા વર્તમાન દિવસથી ગર્ભાવસ્થાના સમયગાળાની 280 મી દિવસ સુધી ચાલશે.
                    
                    ❏ એકવાર 280 દિવસની પ્રવૃત્તિઓ થઈ જાય પછી એપ્લિકેશનની દૈનિક પ્રવૃત્તિ સેગમેન્ટ તમારા માટે ઉપલબ્ધ રહેશે નહીં.
                    
                    ❏ એપ માં આપવામાં આવતી બધી માહિતી મેજેસ્ટીક ગર્ભસંસ્કરની જ રહેશે. અમારા દ્વારા પ્રદાન કરવામાં આવેલ સામગ્રીની વહેંચણી પ્રતિબંધિત છે.
                    
                    ❏ એપ્લિકેશન માટે ચૂકવવામાં આવતી ફી કોઈપણ સંજોગોમાં પરત મળશે નહીં.
                    
                    ❏ એપ્લીકેશનમાં આપવામાં આવતી કોઈ પણ માહિતી ને તબીબી સલાહ તરીકે લેવી નહીં; તમારા સ્વાસ્થ્ય સંબંધિત કોઈપણ બાબતોમાં તમારા ડૉક્ટરનો સંપર્ક કરો.
                    
                    ❏ એપ્લિકેશનને subscribe કરવી એ ઇચ્છિત બાળકની બાંહેધરી નથી. તમારી અપેક્ષા અનુસારના ગુણો  વાળા સંતાન ની ઉત્ત્પત્તિ માટે અત્યંત કાળજીની જરૂર છે, અને તે અન્ય ઘણા પરિબળો પર પણ આધારિત છે. આ એપ્લિકેશન ફક્ત તમને પ્રક્રિયાને અનુસરવામાં સહાય કરશે.
                    
                    ❏ “ઈચ્છિત સંતાન" શબ્દનો બાળકના લિંગ સાથે કાંઈ નિસ્બત નથી, "ઇચ્ચિત સંતન (ઇચ્છિત બાળક)" નો અર્થ ચોક્કસ ગુણો વાળા બાળક તરીકે કરવો.
                    """
                case .none:
                    break
                }
                break
            }
        }else{
            lblTitle.text = """
            ❏ Content of the App is based on ancient scriptures, culture, scientific surveys and inputs from domain experts.
            
            ❏ Majestic GarbhSanskar would be the sole proprietor of all the material. Sharing of the material provided is strictly prohibited.
                        
            ❏ Any information provided should not be taken as a medical advice; consult your doctor in all matters related to your health.
                        
            ❏ “Icchit Santan” phrase has nothing to do with gender of baby, Meaning of “Icchit Santan(Desired baby )” is a baby with desired qualities.
            """
        }
        
    }
}

class SignUpVC2: ParentViewController {
    
    @IBOutlet weak var btnAgreement: UIButton!
    var pregStatus: PregnancyStatus = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func prepareUI(with status: PregnancyStatus) {
        self.pregStatus = status
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    @IBAction func btnAgreementTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnAgreement_redirect(_ sender: UIButton) {
        let url_terms = "https://www.garbhsanskar.co/LICENSEDAGREEMENT.pdf"
        let controller : SFSafariViewController = SFSafariViewController(url: URL.init(string: url_terms)!)
        controller.hidesBottomBarWhenPushed = true
        self.present(controller, animated: true, completion: nil)
    }
}

extension SignUpVC2 {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        return isPaymentAllowed ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailTextCell
        cell = tableView.dequeueReusableCell(withIdentifier: "detailTextcell", for: indexPath) as! DetailTextCell
        if indexPath.row == 1 {
            cell.prepareTermsUI()
        }else {
            if self.pregStatus != .none {
                cell.prepareUI(with: pregStatus)
            }
        }
        return cell
    }
}
