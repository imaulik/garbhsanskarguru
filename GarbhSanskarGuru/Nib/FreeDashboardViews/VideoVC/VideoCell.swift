//
//  VideoCell.swift
//  PregnancyGuru
//
//  Created by Maulik on 08/05/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {
    @IBOutlet weak var lblPlayerTitle: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
