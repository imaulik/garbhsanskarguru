//
//  SlideMenuContainerVC.swift
//  ELeague
//
//  Created by Yudiz Solutions Pvt.Ltd. on 10/06/16.
//  Copyright © 2016 Yudiz Pvt.Ltd. All rights reserved.
//

import UIKit

enum MenuItem: String {
    case About                          = "About us"
    case Profile                        = "My Profile"
    case GarbhSanskarInfo               = "Garbh Sanskar Info"
    case Reports                        = "Reports"
    case AddReminder                    = "Add Reminder"
    case ChangeDay                      = "Change Day"
    case PauseApp                       = "Pause App"
    case ResumeApp                      = "Resume App"
    case Language                       = "Change Language"
    case StartPaidCourse                = "Start Paid App Dashboard"
    case GiftApp                        = "Give This App As A Gift"
    case BuyFullCourse                  = "Buy Full Course"
    case Contact                        = "Contact us"
    case FAQ                            = "Faq"
    
    static let allValues = [About, Profile, GarbhSanskarInfo, Reports,AddReminder,ChangeDay,PauseApp,ResumeApp, Language, StartPaidCourse,GiftApp, BuyFullCourse, Contact,FAQ]//[About, Profile, GarbhSanskarInfo, Reports, AddReminder, ChangeDay, PauseApp, Language, GiftApp, BuyFullCourse, Contact]
}

//MARK: Point
/**
 *This class use for get center point of menu ,screen
 */
class Point {
    static var centerPoint = CGPoint()
}
//MARK: SlideAction
/**
 * Slide Menu Action Open & close
 */
public enum SlideAction {
    case open
    case close
}

//MARK: SlideAnimationStyle
/**
 *This class use Slide open animation style
 */
public enum SlideAnimationStyle {
    case style1
    case style2
}

public struct SlideMenuOptions {
    public static var animationStyle: SlideAnimationStyle = .style1
    
    public static var screenFrame     = UIScreen.main.bounds
    public static var panVelocity : CGFloat = 800
    public static var panAnimationDuration : TimeInterval = 0.35
    
    public static var pending = UIDevice.current.userInterfaceIdiom == .pad ? _screenSize.width/2.5 : 75 * _widthRatio
    public static var thresholdTrailSpace : CGFloat =  UIScreen.main.bounds.width + pending
    public static var thresholdLedSpace : CGFloat =  UIScreen.main.bounds.width - pending
    public static var panGesturesEnabled: Bool = true
    public static var tapGesturesEnabled: Bool = true
}


struct MenuSpecific {
    var items: [MenuItem] = MenuItem.allValues
    var selectedItem = MenuItem.About
    let normalAlpha: CGFloat = 0.35
    let selectedAlpha: CGFloat = 1
}

//MARK: - SliderContainer VC
class SlideMenuContainerVC: ParentViewController {
    // MARK: - Outlets
    
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    
    @IBOutlet weak var mainContainerTrailSpace: NSLayoutConstraint!
    @IBOutlet weak var mainContainerLedSpace: NSLayoutConstraint!
    
    @IBOutlet weak var menuContainerTrailSpace: NSLayoutConstraint!
    @IBOutlet weak var menuContainerLedSpace: NSLayoutConstraint!
    
    // MARK: Variables
    var transparentView = UIControl()
    var tabbar : UITabBarController!
    var menu = MenuSpecific()
    var menuActionType: SlideAction = .close
    var apiType:kApicallType = .none
    
    // MARK: - iOS Life Cycle
    
    var chilTabBar: SlideMenuTabbarVC {
        return (self.children[0] as? SlideMenuTabbarVC)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        kprint(items: self.children)
        prepareMenuItems()
        preparePaymentStatus()
        prepareConstraintUpdate()
        prepareTableViewUI()
        prepareSlideMenuUI()
        prepareTabBar()
    }
    
    func prepareMenuItems(){
        self.menu.items.removeAll()
       
        if(_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()){
            if _user.pregnancyStatus == .planning {
                if _user.isPaymentSkip  {
                    if isfreeDashboard ||  _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .pending {
                    // free dashboard menu
                        self.menu.items.append(.About)
                        self.menu.items.append(.Profile)
                        self.menu.items.append(.Language)
                        self.menu.items.append(.StartPaidCourse)
                        self.menu.items.append(.Contact)
                    }else {
                        self.menu.items.append(.About)
                        self.menu.items.append(.Profile)
                        self.menu.items.append(.GarbhSanskarInfo)
                        self.menu.items.append(.Reports)
                        self.menu.items.append(.AddReminder)
                        if _user.changeDay == "No"{
                            if _user.currDay < 200 && _user.app_stop != "Pause"{
                                self.menu.items.append(.ChangeDay)
                            }
                        }
                        if _user.app_stop == "No"{
                            if _user.currDay < 200{
                                self.menu.items.append(.PauseApp)
                            }
                        }
                        if _user.app_stop == "Pause"{
                            if _user.currDay < 200{
                                _user.currDay = 0
                                
                                isfreeDashboard = true
                                UserDefaults.standard.setValue(isfreeDashboard, forKey: "freeDashboardSkipFromAdmin")
                                UserDefaults.standard.synchronize()
                                _appDelegator.saveContext()
                                self.menu.items.append(.ResumeApp)
                            }
                        }
                        self.menu.items.append(.Language)
                        self.menu.items.append(.GiftApp)
                        self.menu.items.append(.Contact)
                    }
                }else {
                    if isfreeDashboard ||  _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .pending {
                        // free dashboard menu
                        self.menu.items.append(.About)
                        self.menu.items.append(.Profile)
                        self.menu.items.append(.Language)
                        self.menu.items.append(.StartPaidCourse)
                        self.menu.items.append(.Contact)
                    }else {
                        // paid dashboard menu when demo
                        self.menu.items.append(.About)
                        self.menu.items.append(.Profile)
                        self.menu.items.append(.GarbhSanskarInfo)
                        self.menu.items.append(.Reports)
                        self.menu.items.append(.Language)
                        self.menu.items.append(.GiftApp)
                        self.menu.items.append(.BuyFullCourse)
                        self.menu.items.append(.Contact)
                    }
                }
            }
            else {
                if _user.isPaymentSkip  {
                    self.menu.items.append(.About)
                    self.menu.items.append(.Profile)
                    self.menu.items.append(.GarbhSanskarInfo)
                    self.menu.items.append(.Reports)
                    self.menu.items.append(.AddReminder)
                    if _user.changeDay == "No"{
                        if _user.currDay < 200 && _user.app_stop != "Pause"{
                            self.menu.items.append(.ChangeDay)
                        }
                    }
                    if _user.app_stop == "No"{
                        if _user.currDay < 200{
                            self.menu.items.append(.PauseApp)
                        }
                    }
                    if _user.app_stop == "Pause"{
                        if _user.currDay < 200{
                            _user.currDay = 0
                             isfreeDashboard = true
                             UserDefaults.standard.setValue(isfreeDashboard, forKey: "freeDashboardSkipFromAdmin")
                            UserDefaults.standard.synchronize()
                            _appDelegator.saveContext()
                            self.menu.items.append(.ResumeApp)
                        }
                    }
                    self.menu.items.append(.Language)
                    self.menu.items.append(.GiftApp)
                    self.menu.items.append(.Contact)
                }else {
                    self.menu.items.append(.About)
                    self.menu.items.append(.Profile)
                    self.menu.items.append(.GarbhSanskarInfo)
                    self.menu.items.append(.Reports)
                    self.menu.items.append(.Language)
                    self.menu.items.append(.GiftApp)
                    self.menu.items.append(.BuyFullCourse)
                    self.menu.items.append(.Contact)
                }
            }
        }
        else {
            if !_appDelegator.getPaymentHideShowStatus(){
                self.menu.items.append(.About)
                self.menu.items.append(.Profile)
                self.menu.items.append(.Language)
                self.menu.items.append(.Contact)
            }else if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
                self.menu.items.append(.About)
                self.menu.items.append(.Profile)
                self.menu.items.append(.Language)
                self.menu.items.append(.StartPaidCourse)
                self.menu.items.append(.Contact)
            }
            else if(_user.paymentStatus == .pending && !_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()) {
                self.menu.items.append(.About)
                self.menu.items.append(.Profile)
                self.menu.items.append(.Language)
                self.menu.items.append(.GiftApp)
                self.menu.items.append(.Contact)
                self.menu.items.append(.FAQ)
            }
                
            else if(_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)) {
                self.menu.items.append(.About)
                self.menu.items.append(.Profile)
                self.menu.items.append(.GarbhSanskarInfo)
                self.menu.items.append(.Reports)
                self.menu.items.append(.Language)
                self.menu.items.append(.GiftApp)
                self.menu.items.append(.BuyFullCourse)
                self.menu.items.append(.Contact)
            }
            else{
                self.menu.items.append(.About)
                self.menu.items.append(.Profile)
                self.menu.items.append(.GarbhSanskarInfo)
                self.menu.items.append(.Reports)
                self.menu.items.append(.AddReminder)
                if _user.changeDay == "No"{
                    if _user.currDay < 200 && _user.app_stop != "Pause"{
                        self.menu.items.append(.ChangeDay)
                    }
                }
                if _user.app_stop == "No"{
                    if _user.currDay < 200{
                        self.menu.items.append(.PauseApp)
                    }
                }
                if _user.app_stop == "Pause"{
                    if _user.currDay < 200{
                        self.menu.items.append(.ResumeApp)
                    }
                }
                self.menu.items.append(.Language)
                self.menu.items.append(.GiftApp)
                self.menu.items.append(.Contact)
            }
        }
    }
   
    func preparePaymentStatus() {
        let isPaymentAllowed = _appDelegator.getPaymentHideShowStatus()
        if !isPaymentAllowed {
            //            self.menu.items.remove(at: 5) // comment after made prepareMenuItems method.
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        prepareTabBar()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Navigation Methods
extension SlideMenuContainerVC {
    
    func naviToAboutVC() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "aboutSegue", sender: nil)
        }
    }
    
    func naviToProfileVC() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "profileSegue", sender: nil)
        }
    }
    
    func naviToGarbhSanskarInfoVC() {
        _appDelegator.isClickGarbhSanskarInfo = true
        _appDelegator.navigateUserToHome()
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "garbhSanskarInfoSegue", sender: nil)
        }
    }
    
    func naviToReportVC() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "reportSegue", sender: nil)
        }
    }
    
    func openLanguageView() {
        let onBoardingView = LanguageView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if _appDelegator.detectBackFromDashboard != nil{
                _appDelegator.detectBackFromDashboard!()
            }
            if _appDelegator.detectBackFromWebinar != nil {
                _appDelegator.detectBackFromWebinar!()
            }
            if _appDelegator.detectLanguageChangeFromPreDashboard != nil{
                _appDelegator.detectLanguageChangeFromPreDashboard!()
            }
            if _appDelegator.detectBackFromAboutUsView != nil{
                _appDelegator.detectBackFromAboutUsView!()
            }
            
            if _appDelegator.detectBackFromCourseFeedBack != nil{
                _appDelegator.detectBackFromCourseFeedBack!()
            }
            
            if _appDelegator.detectBackFromStatistic != nil{
                _appDelegator.detectBackFromStatistic!()
            }
        }
    }
    
    func openGiftAppView() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "giftAppSegue", sender: nil)
        }
    }
    
    func openBuyCourseView() {
        
        let onBoardingView = BuyCouseView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            
            switch action {
            case .done:
                self.naviToReferalCodeNewVC()
                break
            default:
                break
            }
            onBoardingView.removeFromSuperview()
        }
    }
    
    func naviToReferalCodeNewVC() {
        
        let vcReferalNew = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ReferalCodeNewVC") as! ReferalCodeNewVC
        //Full Course
        vcReferalNew.paymentType = .full
        vcReferalNew.isFromSlideMenuToBuyFullCourse = true
        self.navigationController?.pushViewController(vcReferalNew, animated: true)
        
        DispatchQueue.main.async {
            self.animatedDrawerEffect()
        }
    }
    
    func naviToSubscriptionVC() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "subscriptionSegue", sender: nil)
        }
    }
    
    func naviToContactVC() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "contactSegue", sender: nil)
        }
    }
    
    func naviToFAQVC() {
        let faqVC = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "FaqVC") as! FaqVC
        self.navigationController?.pushViewController(faqVC, animated: true)
    }
    
    func naviToAddReminder(){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "reminderSegue", sender: nil)
        }
    }
    
    func prepareChangeDay(){
        self.prepareConfirmationPopup(message: "changeday") { (action) in
            if action == .done{
                // change day popup will open.
                self.apiType = .changeDay
                self.prepareOnBoarding(needCancel: true)
            }else{
                // cancel view.
            }
        }
    }
    
    func preparePauseApp(){
        self.prepareConfirmationPopup(message: "pauseapp") { (action) in
            if action == .done{
                // Required action and redirect to FreeDashboard. without payment banner like as Planner with paid user.
                self.pauseAppApiCall { (action) in
                    DispatchQueue.main.async {
                        self.getToday(block: { (done) in
                            if done {
                                _appDelegator.setOnBoarding(isOnBoardingShown: true)
                                _appDelegator.navigateDirectlyToHome()
                            }
                        })
                    }
                }
            }else{
                // cancel remove popup and nothing required to do anything.
            }
        }
    }
    
    func prepareResumeApp(){
        self.apiType = .resumeApp
        self.prepareOnBoarding(needCancel: true)
    }
    
    func prepareConfirmationPopup(message:String,completionBlock:@escaping (KAction) -> Void){
        let onBoardingView = ConfirmationPopup.instantiateView(with: self.view)
        onBoardingView.updateUI(keyName: message, isCancelable: true)
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            completionBlock(action)
        }
    }
    
    func prepareSignOutOnBoarding() {
        let onBoardingView = SignOutView.instantiateView(with: self.view)
        onBoardingView.bounceView(animateView: onBoardingView.baseView)
        onBoardingView.actionBlock = { (action) in
            switch action {
            case .cancel:
                onBoardingView.removeFromSuperview()
            case .done:
                self.prepareForLogout()
            case .share:
                break
            case .other:
                break
            case .later:
                break
            }
        }
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        // set false value, Bcz of once user click on gsGuru info and redirect to FreeDashboard and click on logout and then again login with same credentials so redirecting to freedashbord instead of Paid Dashboard.
        _appDelegator.isClickGarbhSanskarInfo = false
        self.prepareSignOutOnBoarding()
    }
}

//Webservices for Pause App and Change Day
extension SlideMenuContainerVC{
    
    func pauseAppApiCall(block: @escaping (Bool) -> ()){
        
        let param: [String: Any] = ["user_id": _user.id, "status": "Pause"]
        self.showCentralSpinner()
        KPWebCall.call.pauseApplication(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    block(true)
                } else {
                    block(false)
                    weakself.showError(data: json, view: weakself.view)
                }
            }
            else {
                block(false)
                weakself.showError(data: json, view: weakself.view)
            }
        }
        
    }
    
    func resumeAppApiCall(enterdDay: Int, block: @escaping (Bool) -> ()) {
        
        let param: [String: Any] = ["user_id": _user.id, "status": "Completed","day":enterdDay]
        self.showCentralSpinner()
        KPWebCall.call.pauseApplication(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    block(true)
                } else {
                    block(false)
                    weakself.showError(data: json, view: weakself.view)
                }
            }
            else {
                block(false)
                weakself.showError(data: json, view: weakself.view)
            }
        }
        
    }
    
    
    func changeDayApiCall(enterdDay: Int, block: @escaping (Bool) -> ()) {
        
        let param: [String: Any] = ["user_id": _user.id, "day": enterdDay]
        self.showCentralSpinner()
        KPWebCall.call.changePregnancyDay(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    block(true)
                } else {
                    block(false)
                    weakself.showError(data: json, view: weakself.view)
                }
            }
            else {
                block(false)
                weakself.showError(data: json, view: weakself.view)
            }
        }
        
    }
    
}


//MARK:- Private
extension  SlideMenuContainerVC{
    func prepareConstraintUpdate(){
        if SlideMenuOptions.animationStyle == .style1 {
            menuContainerTrailSpace.constant =  SlideMenuOptions.screenFrame.width
            menuContainerLedSpace.constant =  -SlideMenuOptions.thresholdLedSpace
            self.view.bringSubviewToFront(menuContainer)
            
        }else{
            menuContainerTrailSpace.constant = SlideMenuOptions.pending
            menuContainerLedSpace.constant = 0
            self.view.bringSubviewToFront(mainContainer)
            mainContainer.layer.shadowColor = UIColor.black.cgColor
            mainContainer.layer.shadowOpacity = 0.6
            mainContainer.layer.shadowOffset = CGSize(width: 0, height: 2)
            
        }
    }
    
    func prepareTableViewUI() {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            kprint(items: "Version \(version)")
        }
    }
    
    func prepareTabBar(){
        if tabbar == nil{
            for obj in self.children{
                if (obj .isKind(of: UITabBarController.self)){
                    tabbar = (obj as! UITabBarController)
                }
            }
        }
    }
    
    func prepareOnBoardingFreeDashboard() {
        let onBoardingView = FreeDashBoard.instantiateView(with: self.view)
        onBoardingView.updateUI()
        onBoardingView.bounceView(animateView: onBoardingView.animatedViews)
        onBoardingView.actionBlock = { (action) in
            onBoardingView.removeFromSuperview()
            if action == .later{
                // show free dashboard.
                //                _appDelegator.setOnBoarding(isOnBoardingShown: true)
                //                _appDelegator.navigateUserToHome()
            }else{
                // show paid dashboard with set today date in pregnent day as first.
                self.prepareOnBoarding(needCancel: true)
            }
        }
    }
    
    func prepareOnBoarding(needCancel:Bool = false) {
        let onBoardingView = PregnancyStatusView.instantiateView(with: self.view)
        onBoardingView.setupUI(isCancelable: needCancel)
        onBoardingView.actionBlock = { (action) in
            // Add below line. Need to remove view on action == cancel.
            if action == .cancel{
                onBoardingView.removeFromSuperview()
                return
            }
            guard action == .done else {return}
            guard !onBoardingView.txtDay.text!.isEmpty else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            onBoardingView.endEditing(true)
            let currDay = onBoardingView.txtDay.text!.integerValue!
            print("Current Day \(currDay)")
            
            guard currDay > 0 && currDay < 280 else {
                _ = ValidationToast.showStatusMessage(message: "Please select betwen 1 to 280")
                return
            }
            
            isfreeDashboard = false
            UserDefaults.standard.setValue(isfreeDashboard, forKey: "freeDashboardSkipFromAdmin")
            UserDefaults.standard.synchronize()
            
            if self.apiType == .changeDay{
                self.apiType = .none
                self.changeDayApiCall(enterdDay: currDay) { (action) in
                    if action{
                        onBoardingView.removeFromSuperview()
                        DispatchQueue.main.async {
                            self.getToday(block: { (done) in
                                if done {
                                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                                    _appDelegator.navigateDirectlyToHome()
                                }
                            })
                        }
                    }
                }
            }else if self.apiType == .resumeApp{
                self.apiType = .none
                self.resumeAppApiCall(enterdDay: currDay) { (action) in
                    if action{
                        onBoardingView.removeFromSuperview()
                        DispatchQueue.main.async {
                            self.getToday(block: { (done) in
                                if done {
                                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                                    _appDelegator.navigateDirectlyToHome()
                                }
                            })
                        }
                    }
                }
            }
            else{
                self.getPregnancyDay(currentDay: currDay, block: { (done) in
                    if done {
                        onBoardingView.removeFromSuperview()
                        DispatchQueue.main.async {
                            self.getToday(block: { (done) in
                                if done {
                                    _appDelegator.setOnBoarding(isOnBoardingShown: true)
                                    _appDelegator.navigateDirectlyToHome()
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    func getToday(block: @escaping (Bool) -> ()) {
        let param: [String: Any] = ["user_id": _user.id]
        self.showCentralSpinner()
        KPWebCall.call.getToday(param: param) { [weak self] (json, statusCode) in
            guard let weakself = self else {return}
            weakself.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let status = dict["status"] as? Int {
                if status == 200 {
                    DispatchQueue.main.async {
                        
                        let deviceId = dict.getStringValue(key: "device_id")
                        if(!deviceId.isEqual(str: _deviceID)){
                            
                            let alert = UIAlertController(title: "Logout", message: "Kindly verify your credentials again...!", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OKAY", style: .default, handler: { (action) in
                                
                                weakself.prepareForLogout()
                            })
                            alert.addAction(action)
                            _appDelegator.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            block(false)
                        }
                        else {

                            let demoDay = dict.getInt16Value(key: "demo_day")
                            let whichUser = dict.getStringValue(key: "which_user ")
                            print("\(demoDay) \(whichUser)")
                            _user.setCurrentDay(dict: dict)
                            _user.setWhichUserType(strType: whichUser)
                            _user.setChangeDay(dict: dict)
                            _user.setappstop(dict: dict)
                            _appDelegator.saveContext()
                            block(true)
                        }
                    }
                }
                else {
                    weakself.showError(data: json, view: weakself.view)
                    block(false)
                }
            }else {
                weakself.showError(data: json, view: weakself.view)
                block(false)
            }
        }
    }
    
    func getPregnancyDay(currentDay: Int, block: @escaping (Bool) -> ()) {
        self.showCentralSpinner()
        KPWebCall.call.getPregnancyDay(param: ["current_day": currentDay]) { (json, statusCode) in
            self.hideCentralSpinner()
            if statusCode == 200, let dict = json as? NSDictionary, let msg = dict["message"] as? String {
                _ = ValidationToast.showStatusMessage(message: msg)
                block(true)
            }else {
                block(false)
                self.showError(data: json, view: self.view)
            }
        }
    }
}


//MARK:- UITableView DataSource & Delegate

extension  SlideMenuContainerVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.heightRatio
    }
    
    /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
     // Add new if condition.
     // Paid user with status planing and reg_user.
     //        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
     if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed{
     
     switch menu.items[indexPath.row] {
     case .About:
     return 50.0.heightRatio
     case .Profile:
     return 50.0.heightRatio
     case .GarbhSanskarInfo:
     return 0.01
     case .Reports:
     return 0.01
     case .AddReminder:
     return 0.01
     case .ChangeDay:
     return 0.01
     case .PauseApp:
     return 0.01
     case .ResumeApp:
     return 0.01
     case .Language:
     return 50.0.heightRatio
     case .StartPaidCourse:
     return 50.0.heightRatio
     case .GiftApp:
     return 0.01
     case .BuyFullCourse:
     return 0.01
     case .Contact:
     return 50.0.heightRatio
     
     }
     }
     //if(_user.referralStatus == .pending) {
     else if(_user.paymentStatus == .pending && !_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()) {
     
     switch menu.items[indexPath.row] {
     case .About:
     return 50.0.heightRatio
     case .Profile:
     return 50.0.heightRatio
     case .GarbhSanskarInfo:
     return 0.01
     case .Reports:
     return 0.01
     case .AddReminder:
     return 0.01
     case .ChangeDay:
     return 0.01
     case .PauseApp:
     return 0.01
     case .ResumeApp:
     return 0.01
     case .Language:
     return 50.0.heightRatio
     case .StartPaidCourse:
     return 0.01
     case .GiftApp:
     return 0.01
     case .BuyFullCourse:
     return 0.01
     case .Contact:
     return 50.0.heightRatio
     }
     }
     else if(_user.whichUser.isEqual(str: WhichUser.demo_user.rawValue)) {
     
     //abhi: For demo user show all menu items
     //return 50.0.heightRatio
     
     // add switch case for add new condition.
     switch menu.items[indexPath.row] {
     case .About:
     return 50.0.heightRatio
     case .Profile:
     return 50.0.heightRatio
     case .GarbhSanskarInfo:
     return 50.0.heightRatio
     case .Reports:
     return 50.0.heightRatio
     case .AddReminder:
     return 0.01
     case .ChangeDay:
     return 0.01
     case .PauseApp:
     return 0.01
     case .ResumeApp:
     return 0.01
     case .Language:
     return 50.0.heightRatio
     case .StartPaidCourse:
     return 0.01
     case .GiftApp:
     return 50.0.heightRatio
     case .BuyFullCourse:
     return 50.0.heightRatio
     case .Contact:
     return 50.0.heightRatio
     }
     }
     else {
     
     //abhi: comment below line because in previous case there is no option "BuyFullCourse" and now user is paid so no need to show this option
     //return 70.0.heightRatio // paid user
     
     //Paid user -> buy full course remove
     switch menu.items[indexPath.row] {
     case .About:
     return 50.0.heightRatio
     case .Profile:
     return 50.0.heightRatio
     case .GarbhSanskarInfo:
     return 50.0.heightRatio
     case .Reports:
     return 50.0.heightRatio
     case .AddReminder:
     return 50.0.heightRatio
     case .ChangeDay:
     return ((_user.currDay >= 200) || _user.changeDay == "Yes") ? 0.01 : 50.0.heightRatio
     case .PauseApp:
     return (_user.currDay >= 200) ? 0.01 : _user.app_stop == "No" ? 50.0.heightRatio : 0.01
     case .ResumeApp:
     return (_user.currDay >= 200) ? 0.01 : _user.app_stop == "Pause" ? 50.0.heightRatio : 0.01
     case .Language:
     return 50.0.heightRatio
     case .StartPaidCourse:
     return 0.01
     case .GiftApp:
     return 50.0.heightRatio
     case .BuyFullCourse:
     return 0.01
     case .Contact:
     return 50.0.heightRatio
     }
     }
     } */
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 100.0.heightRatio
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 80.0.heightRatio
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! MenuProfileCell
        headerCell.lblName.text = _user.name
        headerCell.imgvProfile.kf.setImage(with: _user.imgUrl)
        return headerCell.contentView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerCell = tableView.dequeueReusableCell(withIdentifier: "footerCell") as! MenuFooterCell
        return footerCell.contentView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MenuItemCell
        cell.prepareToFillData(menu, index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _appDelegator.isClickGarbhSanskarInfo = false // set initial value to be false while click on other menu name.
        switch menu.items[indexPath.row] {
        case .About:
            self.naviToAboutVC()
        case .Profile:
            self.naviToProfileVC()
        case .GarbhSanskarInfo:
            self.naviToGarbhSanskarInfoVC()
        case .Reports:
            self.naviToReportVC()
        case .AddReminder:
            self.naviToAddReminder()
        case .ChangeDay:
            self.prepareChangeDay()
        case .PauseApp:
            self.preparePauseApp()
        case .ResumeApp:
            self.prepareResumeApp()
        case .Language:
            self.openLanguageView()
        case .StartPaidCourse:
            self.prepareOnBoardingFreeDashboard()
        case .GiftApp:
            self.openGiftAppView()
        case .BuyFullCourse:
            self.openBuyCourseView()
        case .Contact:
            self.naviToContactVC()
        case .FAQ:
            self.naviToFAQVC()
        }
        DispatchQueue.main.async {
            if (self.menu.items[indexPath.row] != .Language && self.menu.items[indexPath.row] != .BuyFullCourse) {
                self.animatedDrawerEffect()
            }
        }
    }
}
//MARK: - UIGesture
extension SlideMenuContainerVC {
    func animatedDrawerEffect() {
        if let container = self.findContainerController(){
            if menuActionType == .close
            {
                container.panMenuOpen()
            }else
            {
                container.panMenuClose()
                if _appDelegator.closeSideMenuDetector != nil{
                    _appDelegator.closeSideMenuDetector!(true)
                }
            }
        }
    }
    
    func menuContainerClose(_ animatedView: UIView) {
        if let container = self.findContainerController(){
            menuActionType = .close
            if SlideMenuOptions.animationStyle == .style1{
                container.menuContainerTrailSpace.constant = SlideMenuOptions.screenFrame.width
                container.menuContainerLedSpace.constant   = -SlideMenuOptions.thresholdLedSpace
            }else{
                container.mainContainerTrailSpace.constant = 0
                container.mainContainerLedSpace.constant = 0
            }
            
            UIView.animate(withDuration: SlideMenuOptions.panAnimationDuration, animations: { () -> Void in
                container.tableView.isUserInteractionEnabled = false
                self.transparentView.isEnabled = false
                self.transparentView.alpha = 0
                container.view.layoutIfNeeded()
                
            }, completion: { (finished) -> Void in
                self.transparentView.isHidden = true
                self.transparentView.isEnabled = true
                container.tableView.isUserInteractionEnabled = true
            })
            
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOfGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK: - Swipe Getsure code
    
    @objc func swipePanAction(gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        if !SlideMenuOptions.panGesturesEnabled{
            return
        }
        
        if let navCon: UINavigationController = self.tabbar!.selectedViewController as? UINavigationController{
            if navCon.viewControllers.count != 1 {
                return
            }
        }
        let centerPoint = Point.centerPoint
        
        
        switch gestureRecognizer.state {
        case .began:
            Point.centerPoint = self.mainContainer.center
            break
            
        case .changed:
            let translation: CGPoint = gestureRecognizer.translation(in: self.view)
            moveContainerOnGesture(x: centerPoint.x, translation: translation)
            break
            
        case .ended,.failed,.cancelled:
            let translation: CGPoint = gestureRecognizer.translation(in: self.view)
            let vel: CGPoint = gestureRecognizer.velocity(in: self.view)
            let halfWidth = SlideMenuOptions.screenFrame.width / 2
            self.view.endEditing(true)
            //  recognizer has received touches recognized as the end of the gesture base on menu close/open
            if vel.x > SlideMenuOptions.panVelocity{
                self.panMenuOpen()
            }else if vel.x < -SlideMenuOptions.panVelocity{
                self.panMenuClose()
            }else if  translation.x > halfWidth{
                self.panMenuOpen()
            }else{
                self.panMenuClose()
            }
            
            break
        default:
            break
        }
    }
    //  MenuContainer/MainContainer Constraint update base on moment action
    
    func moveContainerOnGesture(x:CGFloat,translation: CGPoint){
        let ctPoint = (x + translation.x)
        
        let halfWidth = SlideMenuOptions.screenFrame.width / 2
        if ctPoint >= halfWidth {
            if ctPoint - halfWidth > SlideMenuOptions.thresholdLedSpace {
                //Menu Screen rech maximum to open
                if SlideMenuOptions.animationStyle == .style1{
                    if menuContainerLedSpace.constant != 0
                    {
                        self.menuContainerTrailSpace.constant = SlideMenuOptions.pending
                        self.menuContainerLedSpace.constant = 0
                        transparentViewAnimation(x: translation.x)
                        self.view.layoutIfNeeded()
                    }
                }else{
                    if mainContainerLedSpace.constant != SlideMenuOptions.thresholdLedSpace
                    {
                        mainContainerTrailSpace.constant = -SlideMenuOptions.thresholdLedSpace;
                        mainContainerLedSpace.constant = SlideMenuOptions.thresholdLedSpace;
                        menuSlideAnimation(x: translation.x)
                        transparentViewAnimation(x: translation.x)
                        self.view.layoutIfNeeded()
                    }
                }
                
            }else {
                if SlideMenuOptions.animationStyle == .style1{
                    self.menuContainerLedSpace.constant =   (ctPoint - halfWidth) - SlideMenuOptions.screenFrame.width + SlideMenuOptions.pending
                    self.menuContainerTrailSpace.constant =  SlideMenuOptions.screenFrame.width - (ctPoint - halfWidth)
                    transparentViewAnimation(x: translation.x)
                    
                }else{
                    self.mainContainerTrailSpace.constant = -translation.x
                    self.mainContainerLedSpace.constant = translation.x
                    menuSlideAnimation(x: translation.x)
                    transparentViewAnimation(x: translation.x)
                }
                self.view.layoutIfNeeded()
                
            }
            
        }
    }
    
    //  recognizer has received touches recognized as the end of the gesture base on menu close method
    func panMenuClose() {
        menuActionType = .close
        if SlideMenuOptions.animationStyle == .style1{
            menuContainerTrailSpace.constant = SlideMenuOptions.screenFrame.width
            menuContainerLedSpace.constant   = -SlideMenuOptions.thresholdLedSpace
        }else{
            mainContainerTrailSpace.constant = 0
            mainContainerLedSpace.constant = 0
            menuSlideAnimation(x: SlideMenuOptions.pending,isAnimate: false)
        }
        
        UIView.animate(withDuration: SlideMenuOptions.panAnimationDuration, animations: { () -> Void in
            self.transparentView.isEnabled = false
            self.tableView.isUserInteractionEnabled = false
            
            self.transparentView.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.transparentView.isHidden = true
            self.transparentView.isEnabled = true
            self.tableView.isUserInteractionEnabled = true
        })
    }
    //  recognizer has received touches recognized as the end of the gesture base on menu open method
    
    func panMenuOpen() {
        menuActionType = .open
        
        if SlideMenuOptions.animationStyle == .style1{
            menuContainerTrailSpace.constant = SlideMenuOptions.pending
            menuContainerLedSpace.constant = 0
            
        }else{
            mainContainerTrailSpace.constant = -SlideMenuOptions.thresholdLedSpace
            mainContainerLedSpace.constant = SlideMenuOptions.thresholdLedSpace
            menuSlideAnimation(x: SlideMenuOptions.screenFrame.width,isAnimate: false)
        }
        self.transparentView.isHidden = false
        
        UIView.animate(withDuration: SlideMenuOptions.panAnimationDuration, animations: { () -> Void in
            self.transparentView.isEnabled = false
            self.tableView.isUserInteractionEnabled = false
            
            self.transparentView.alpha = 1
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.tableView.isUserInteractionEnabled = true
            
            self.transparentView.isEnabled = true
            self.tableView.reloadData()
        })
    }
    
    //MARK: -  animation method
    //  Menu slide animation with user touch moment code
    
    func menuSlideAnimation(x: CGFloat,isAnimate:Bool = true){
        let progress: CGFloat = (x)/SlideMenuOptions.thresholdLedSpace
        let slideMovement : CGFloat = 100
        var location :CGFloat = (slideMovement * -1) + (slideMovement * progress)
        location = location > 0 ? 0 : location
        self.menuContainerLedSpace.constant = location
        self.menuContainerTrailSpace.constant =  abs(location) + SlideMenuOptions.pending
        if isAnimate{
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //  Transparent view alpha animation with user touch moment code
    func transparentViewAnimation(x: CGFloat){
        let progress: CGFloat = (x)/SlideMenuOptions.thresholdLedSpace
        self.transparentView.isHidden = false
        
        UIView.animate(withDuration: 0.1) {
            self.transparentView.alpha = progress
        }
    }
    
}

//MARK: - Slider Menu UI
extension SlideMenuContainerVC {
    
    func prepareSlideMenuUI() {
        // Pan Gesture Recognizer code
        let corner :UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(SlideMenuContainerVC.swipePanAction(gestureRecognizer:)))
        corner.edges = UIRectEdge.left
        mainContainer.addGestureRecognizer(corner)
        
        //transparentView Code
        addTransparentControlUI()
    }
    func addTransparentControlUI() {
        transparentView =  UIControl()
        transparentView.alpha = 0
        transparentView.isHidden = true
        transparentView.addTarget(self, action: #selector(ParentViewController.shutterAction(_:)), for: UIControl.Event.touchUpInside)
        
        
        if SlideMenuOptions.animationStyle == .style1{
            transparentView.frame =  CGRect(x: 0, y: 0, width: SlideMenuOptions.screenFrame.width, height: SlideMenuOptions.screenFrame.height)
            transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            self.view.addSubview(self.transparentView)
            self.view.bringSubviewToFront(menuContainer)
        }else{
            transparentView.frame =  CGRect(x: SlideMenuOptions.thresholdLedSpace, y: 0, width: SlideMenuOptions.pending, height: SlideMenuOptions.screenFrame.height)
            transparentView.backgroundColor =   UIColor.clear
            
            self.view.addSubview(self.transparentView)
        }
    }
}

