//
//  OpenURLView.swift
//  PregnancyGuru
//
//  Created by Maulik on 25/04/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
 

import UIKit

class OpenURLView: UITableViewHeaderFooterView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func btnFBPress(_ sender: Any) {
        urlTo(scheme: fbURL)
    }
    
    @IBAction func btnInstaPress(_ sender: Any) {
         urlTo(scheme: instaURL)
       }
    
    @IBAction func btnWebsitePress(_ sender: Any) {
          urlTo(scheme: website)
    }
    
    @IBAction func btnWhatsAppPress(_ sender: Any) {
        urlTo(scheme:whatsapp)
    }
   
    @IBAction func btnYoutubePress(_ sender: Any) {
        urlTo(scheme: ytURL)
    }
}
