//
//  SliderTabbarVC.swift
//  ELeague
//
//  Created by Yudiz Solutions Pvt.Ltd. on 10/06/16.
//  Copyright © 2016 Yudiz Pvt.Ltd. All rights reserved.
//

import UIKit

class SlideMenuTabbarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setting View Controllers for Tab
        self.tabBar.isHidden = true
        if let vcs = self.viewControllers{
            for vc in vcs{
                vc.view.layoutIfNeeded()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()){
            if _user.isDemoPaymentSkip {
                if isfreeDashboard || _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .pending {
                        self.selectedIndex = 3
                }else {
                    self.selectedIndex = 2
                }
            }else {
                if isfreeDashboard ||  _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .pending {
                    self.selectedIndex = 3
                }else if _appDelegator.isShowPrePaidDashboard{
                    self.selectedIndex = 2
                }else{
                    self.selectedIndex = 0
                }
            }
        }
        else {
            //if _user.referralStatus == .pending {
            //        if _user.currDay == 0 && _user.pregnancyStatus == .planning && _user.paymentStatus == .completed && _user.whichUser.isEqual(str: WhichUser.reg_user.rawValue){
           
            if !_appDelegator.getPaymentHideShowStatus() || _appDelegator.isClickGarbhSanskarInfo{ // payment status false then only redirec to freedashboard instead of check actual condition.
                self.selectedIndex = 3
            }
            else if (_user.pregnancyStatus == .planning && _user.paymentStatus == .completed){
                // Add new condition.
                // Paid user in planning status but want to start app now, than set free dashboard.
                //            self.selectedIndex = 1
                if (_user.currDay < 200 && (_user.app_stop == "Pause")) || _user.currDay == 0{
                    self.selectedIndex = 3
                }else if _appDelegator.isShowPrePaidDashboard{
                    self.selectedIndex = 2
                }else{
                    self.selectedIndex = 0
                }
            }
                
            else if (_user.paymentStatus == .pending && !_appDelegator.isSkipPaymentOrSkipDemoPaymentForLoginUser()) {
                self.selectedIndex = 3
            }
                
                // add new condition for pause app functionality.
                //        else if (_user.app_stop == "Pause"){
                //            self.selectedIndex = 3
                //        }
            else {
                if (_user.app_stop == "Pause"){
                    self.selectedIndex = 3
                }else{
                    if _appDelegator.isShowPrePaidDashboard{
                        self.selectedIndex = 2
                    }else{
                        if _user.currDay < 200 && (_user.app_stop == "Pause"){
                            self.selectedIndex = 3
                        }else{
                            self.selectedIndex = 0
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

/*
 Tabbar selected
 index
 0 = PaidDashboard
 1 = FreeDashboard
 2 = PrePaidDashboard
 */
