//
//  OpenURLView.swift
//  PregnancyGuru
//
//  Created by Maulik on 25/04/20.
//  Copyright © 2020 GaneshTechnoWorld. All rights reserved.
//

import UIKit

class BannerView: UITableViewHeaderFooterView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var hidebanner: UIView!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var imgDashPoster: UIImageView!
    @IBOutlet weak var bannerInfoView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var hideTimerView: UIView!
}
