
import Foundation
import UIKit

enum GradientStyle {
    case leftToRight, rightToLeft, topToBottom, bottomToTop, topleftToBottomRight, topRightToBottomleft
}

extension UIColor {
    
    class func colorWithGray(gray: Int) -> UIColor {
      return UIColor(red: CGFloat(gray) / 255.0, green: CGFloat(gray) / 255.0, blue: CGFloat(gray) / 255.0, alpha: 1.0)
    }
    
    class func colorWithRGB(r: Int, g: Int, b: Int) -> UIColor {
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
    }

    class func colorWithHexa(hex:Int) -> UIColor{
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        return UIColor(red: components.R, green: components.G, blue: components.B, alpha: 1.0)
    }
    
    @objc class func colourWith(red: Int, green: Int, blue: Int, alpha: CGFloat) -> UIColor {
        return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    @objc class var appGrediantColor1: UIColor {
        return UIColor.colourWith(red: 51, green: 191, blue: 226, alpha: 1.0)
    }
    
    @objc class var appGrediantColor2: UIColor {
        return UIColor.colourWith(red: 98, green: 151, blue: 213, alpha: 1.0)
    }
    
    class func hexStringToUIColor (hexStr: String) -> UIColor {
        var cString:String = hexStr.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
    
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK:- Gradian Color
    class func createGradient(gradientStyle: GradientStyle, colors: [UIColor], frame: CGRect) -> UIColor{
        let gradient = CAGradientLayer()
        gradient.frame = frame
        var cgcolor = Array<CGColor>()
        for colorT in colors {
            cgcolor.append(colorT.cgColor)
        }
        gradient.colors = cgcolor
        switch gradientStyle {
        case .leftToRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .rightToLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .topToBottom:
            gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        case .bottomToTop:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        default:
            break
        }
        UIGraphicsBeginImageContextWithOptions(gradient.frame.size, false, 0)
        gradient.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return UIColor(patternImage: outputImage!)
    }
}

struct GarbhColor {
    static var blue:    UIColor{ return UIColor.hexStringToUIColor(hexStr: "00A0FF")}
    static var red:     UIColor{ return UIColor.hexStringToUIColor(hexStr: "FE4F50")}
    static var gray:    UIColor{ return UIColor.gray}
    static var white:    UIColor{ return UIColor.white}
    static var black:   UIColor{return UIColor.hexStringToUIColor(hexStr: "333333")}
    
    
    static var gradient1Pink:   UIColor{return UIColor.hexStringToUIColor(hexStr: "D8366B")}
    static var gradient2Pink:   UIColor{return UIColor.hexStringToUIColor(hexStr: "DC4593")}
    static var gradient1Blue:   UIColor{return UIColor.hexStringToUIColor(hexStr: "192447")}
    static var gradient2Blue:   UIColor{return UIColor.hexStringToUIColor(hexStr: "0e4d92")}
    
    static var pink:   UIColor{return UIColor.hexStringToUIColor(hexStr: "EF4172")}
    static var green:   UIColor{return UIColor.hexStringToUIColor(hexStr: "008000")}
    static var whiteGray:   UIColor{return UIColor.hexStringToUIColor(hexStr: "C2CCC0")}
    
    static var gradientLeftBlue:   UIColor{return UIColor.hexStringToUIColor(hexStr: "080E31")}
    static var gradientRightBlue:   UIColor{return UIColor.hexStringToUIColor(hexStr: "172166")}
}
