//
//  ForgotPassView.swift
//  GarbhSanskarGuru
//
//  Created by Jayesh Tejwani on 11/01/19.
//  Copyright © 2019 Jayesh Tejwani. All rights reserved.
//

import UIKit

class ForgotPassView: ConstrainedView {

    var actionBlock: (KAction)->Void = {_ in}
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var animateView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setKeyboardNotifications()
    }
    
    class func instantiateView(with view: UIView) -> ForgotPassView {
        let objView = UINib(nibName: "ForgotPassView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ForgotPassView
        objView.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: view.frame.height)
        objView.layoutIfNeeded()
        view.addSubview(objView)
        return objView
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.actionBlock(.cancel)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.actionBlock(.done)
    }
}
extension ForgotPassView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfEmail.resignFirstResponder()
        return true
    }
}

extension ForgotPassView {
    
    func setKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let kbSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            
            var aRect = self.baseView.frame
            aRect.size.height -= kbSize.height
            
            if aRect.contains(tfEmail.frame.origin) {
                let yInset = kbSize.height - tfEmail.frame.origin.y
                let scrollPoint = CGPoint(x: 0, y: yInset)
                scrollView.setContentOffset(scrollPoint, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
}

